# Module kiscord-gateway-interactions

[kiscord-gateway](https://kiscord.aur.rocks/docs/kiscord-gateway/index.html) plugin to run interaction-based app as
gateway bot.

See also [kiscord-interactions](https://kiscord.aur.rocks/docs/kiscord-interactions/index.html)
and [kiscord-gateway](https://kiscord.aur.rocks/docs/kiscord-gateway/index.html) modules.

# Package kiscord.gateway.interactions

Bindings and helper functions for [kiscord-gateway](https://kiscord.aur.rocks/docs/kiscord-gateway/index.html) server.
