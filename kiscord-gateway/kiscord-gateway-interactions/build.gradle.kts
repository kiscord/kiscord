plugins {
    alias(libs.plugins.kotlin.multiplatform)
    alias(libs.plugins.kotlin.serialization)
    alias(libs.plugins.kotlinx.atomicfu)
    alias(libs.plugins.dokka)
    alias(libs.plugins.licenser)
    `maven-publish`
    signing
}

val kiscordBrowser = (ext["kiscord.browser"] as String).toBoolean()

kotlin {
    sourceSets.configureEach {
        languageSettings.optIn("kiscord.api.KiscordUnstableAPI")
    }

    jvm {
        withJava()
    }
    js {
        nodejs()
        if (kiscordBrowser) browser()
    }

    // Linux
    linuxX64()

    // Windows
    mingwX64()

    // Darwin
    iosArm64()
    iosX64()
    macosArm64()
    macosX64()
}

dependencies {
    commonMainApi(project(":kiscord-gateway"))
    commonMainApi(project(":kiscord-interactions"))
}

apply(from = rootProject.file("gradle/java9-module-info.gradle.kts"))
