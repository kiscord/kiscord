/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.gateway.interactions

import io.ktor.util.pipeline.*
import kiscord.*
import kiscord.api.*
import kiscord.api.gateway.*
import kiscord.builder.*
import kiscord.gateway.*
import kiscord.interactions.*
import kotlinx.coroutines.*
import org.kodein.log.*

public class GatewayInteractions(
    public val host: InteractionsHost
) : InteractionsHost by host {

    public class Config internal constructor(
        builder: InteractionsHost.Builder
    ) : InteractionsHost.Builder by builder

    public companion object : Plugin<Config, GatewayInteractions, Kiscord> {
        override val key: TypedAttribute<GatewayInteractions> = TypedAttribute("Kiscord:Gateway:Interactions")
        private val DispatchInteractionPhase = PipelinePhase("DispatchInteraction")

        override fun beforeFeatureAdded(builder: PluggableBuilder<Kiscord>) {
            builder.maybeInstall(Gateway)
        }

        override fun install(scope: Kiscord, block: ConfigurationChain<Config>): GatewayInteractions {
            val logger = scope.loggerFactory.newLogger("kiscord.gateway.interactions", "GatewayInteractions")
            val hostBuilder = DefaultInteractionsHost.Builder(logger)
            block(Config(hostBuilder))
            val host = hostBuilder.build()
            val plugin = GatewayInteractions(host)
            plugin.install(scope)
            return plugin
        }
    }

    private suspend fun GatewayCall.handleResult(
        interaction: Interaction,
        result: InteractionResult
    ): Boolean {
        if (!client.command.createInteractionResponse(interaction.id, interaction.token, result.response)) {
            return false
        }

        result.deferred?.let { deferred ->
            connection.launch { deferred(this@handleResult) }
        }

        return true
    }

    private fun install(client: Kiscord) {
        val gateway = client[Gateway]

        val pipeline = host.pipeline

        gateway.incomingPipeline.insertPhaseAfter(GatewayIncomingPipeline.Process, DispatchInteractionPhase)
        gateway.incomingPipeline.intercept(DispatchInteractionPhase) { payload ->
            if (payload !is GatewayPayload<*>) return@intercept
            if (payload.eventName != EventName.InteractionCreate) return@intercept
            val interaction = payload.data as? Interaction ?: return@intercept
            val interactionCall = GatewayInteractionCall(context, interaction)
            val result = withContext(interactionCall) {
                pipeline.execute(interactionCall, interaction)
            }
            if (result is InteractionResult) {
                context.handleResult(interaction, result)
            }
        }

        client.launch {
            client.waitForReadiness()
            gateway.waitForReadiness()
            val application = client.oAuth2.getCurrentBotApplicationInformation()
            host.commandInitializers.forEach { initializer ->
                initializer.initialize(application.id, client)
            }
        }
    }

    private class GatewayInteractionCall(
        private val call: GatewayCall,
        override val interaction: Interaction,
    ) : InteractionCall<Interaction.Data> {
        override val client: Kiscord get() = call.client
        override val data: Interaction.Data
            get() = interaction.data ?: throw IllegalStateException("Interaction doesn't have a data!")
        override val attributes: TypedAttributes by lazy { TypedAttributes(call.attributes) }
        private val _result = CompletableDeferred<InteractionResult>(call.connection.coroutineContext.job)
        override val result: Deferred<InteractionResult> get() = _result
        override suspend fun respondWith(result: InteractionResult) {
            _result.complete(result)
        }
    }
}

@BuilderDsl
public fun Kiscord.Builder.gatewayInteractions(block: GatewayInteractions.Config.() -> Unit) {
    install(GatewayInteractions, block)
}
