# Module kiscord-gateway

Gateway module for [kiscord-core](https://kiscord.aur.rocks/docs/kiscord-core/index.html).

# Package kiscord.api.gateway

Contains all models and helper functions for Discord Gateway API.

# Package kiscord.gateway

Gateway and WS interaction building blocks.

# Package kiscord.gateway.repository

Interfaces to cache realtime data in backing storage (memory or database).
