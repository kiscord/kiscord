/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.api.gateway

//import kiscord.data.*
//import kotlinx.serialization.json.*
//
//class ShardConfigurationTest : JsonModelTest<ShardConfiguration>() {
//    override val testData get() = Data
//    override val format = Json { prettyPrint = false }
//
//    companion object Data : StringModelTestData<ShardConfiguration>() {
//        override val serializer get() = ShardConfiguration.serializer()
//
//        val Single = produce("[0,1]", ShardConfiguration(0, 1))
//        val FirstOfThree = produce("[0,3]", ShardConfiguration(0, 3))
//        val SecondOfThree = produce("[1,3]", ShardConfiguration(1, 3))
//        val ThirdOfThree = produce("[2,3]", ShardConfiguration(2, 3))
//    }
//}
