/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.api.gateway

import kiscord.serialization.*
import kotlinx.serialization.json.*
import kotlin.test.*

class GatewayPayloadTest {
    private val serializer = GatewayPayload.serializer(JsonObject.serializer())
    private val json = Json(from = DiscordSerialFormat.Json) {
        prettyPrint = true
    }

    @Test
    fun testParsing() {
        val actual = json.decodeFromString(
            serializer, """
            {
                "op": 0,
                "d": {},
                "s": 42,
                "t": "READY"
            }
        """.trimIndent()
        )

        assertEquals(
            GatewayPayload(
                opcode = GatewayOpcode.Dispatch,
                data = JsonObject(emptyMap()),
                sequence = 42L,
                eventName = EventName.Ready
            ), actual
        )
    }

    @Test
    fun testComposing() {
        val actual = json.encodeToString(
            serializer,
            GatewayPayload(
                opcode = GatewayOpcode.Heartbeat,
                data = JsonObject(emptyMap())
            )
        )

        assertEquals(
            """
            {
                "op": 1,
                "d": {}
            }
        """.trimIndent(), actual
        )
    }
}
