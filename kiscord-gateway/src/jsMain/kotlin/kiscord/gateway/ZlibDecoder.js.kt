/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.gateway

import io.ktor.utils.io.core.*
import io.ktor.utils.io.js.*
import kotlinx.coroutines.*
import org.khronos.webgl.*
import kotlin.coroutines.*

@JsModule("zlib")
private external class Inflate {
    fun write(buffer: Uint8Array, encoding: String = definedExternally, cb: () -> Unit = definedExternally): Boolean
    fun destroy(error: Throwable = definedExternally)
    fun reset()
    fun on(event: String, callback: (dynamic) -> Unit)
}

private fun Inflate.onData(callback: (ArrayBufferView) -> Unit) {
    on("data") { callback(it.unsafeCast<ArrayBufferView>()) }
}

@JsModule("zlib")
private external val zlib: ZlibAPI

private external interface ZlibAPI {
    fun createInflate(options: dynamic = definedExternally): Inflate
}

internal actual class ZlibDecoder actual constructor() : Closeable {
    private val inflate: Inflate = zlib.createInflate()
    private val outBuffer = BytePacketBuilder()

    init {
        inflate.onData {
            outBuffer.writeFully(it.buffer, it.byteOffset, it.byteLength)
        }
    }

    actual suspend fun decode(buffer: ByteReadPacket): ByteReadPacket {
        val nodeBuffer = Uint8Array(buffer.readArrayBuffer())

        suspendCancellableCoroutine<Unit> { ct ->
            inflate.write(nodeBuffer) { ct.resume(Unit) }
        }

        return outBuffer.build()
    }

    actual fun reset() {
        inflate.reset()
    }

    actual override fun close() {
        inflate.destroy()
        outBuffer.release()
    }

    actual companion object {
        actual val isSupported: Boolean
            get() {
                @Suppress("UNUSED_VARIABLE") val zlib = zlib
                return js("typeof zlib.createInflate === 'function'").unsafeCast<Boolean>()
            }
    }
}
