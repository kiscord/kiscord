/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.gateway.repository

import kiscord.api.*
import kiscord.builder.*
import java.util.concurrent.*

internal actual abstract class InMemorySnowflakeRepository<Value : Any, in UpdateData : UpdaterFor<Value>>(
    private val map: ConcurrentMap<Snowflake, Value>
) : GatewayRepository.SnowflakeRepository<Value, UpdateData> {
    actual constructor() : this(ConcurrentHashMap())

    protected actual abstract fun extractIdFromValue(value: Value): Snowflake
    protected actual abstract fun extractIdFromUpdateData(updateData: UpdateData): Snowflake
    protected actual open suspend fun transform(id: Snowflake, value: Value?, term: Term): Value? = value

    actual override suspend fun set(value: Value): Value? {
        val id = extractIdFromValue(value)
        map[id] = value
        return transform(id, value, Term.Add)
    }

    actual override suspend fun update(data: UpdateData): Value? {
        val id = extractIdFromUpdateData(data)
        val newValue = map.compute(id) { _, oldValue: Value? ->
            when (oldValue) {
                null -> null
                else -> data.update(oldValue)
            }
        } ?: return null
        return transform(id, newValue, Term.Update)
    }

    actual override suspend fun remove(id: Snowflake): Value? {
        return transform(id, map.remove(id), Term.Remove)
    }

    actual override suspend fun remove(value: Value): Boolean {
        val id = extractIdFromValue(value)
        return map.remove(id, value)
    }

    actual override suspend fun get(id: Snowflake): Value? {
        return transform(id, map[id], Term.Get)
    }

    actual override suspend fun contains(id: Snowflake): Boolean = map.containsKey(id)

    actual suspend fun getOrPut(id: Snowflake, block: () -> Value): Value {
        val value = map.getOrPut(id, block)
        return transform(id, value, Term.Get) ?: value
    }

    actual override suspend fun update(id: Snowflake, block: (id: Snowflake, oldValue: Value?) -> Value?): Value? {
        val newValue = map.compute(id, block)
        return transform(id, newValue, Term.Update)
    }

    actual suspend fun replace(oldValue: Value, newValue: Value): Boolean {
        val oldId = extractIdFromValue(oldValue)
        val newId = extractIdFromValue(newValue)
        if (oldId != newId) return false
        return map.replace(oldId, oldValue, newValue)
    }

    actual enum class Term {
        Add, Update, Remove, Get,
    }
}

internal actual class SnowflakeRelationsTracker actual constructor() {
    private val map: MutableMap<Snowflake, MutableSet<Snowflake>> = ConcurrentHashMap()

    actual fun track(parentId: Snowflake, childId: Snowflake) {
        val set = map.getOrPut(parentId) { ConcurrentSkipListSet() }
        set.add(childId)
    }

    actual fun untrack(parentId: Snowflake, childId: Snowflake) {
        map[parentId]?.remove(childId)
    }

    actual fun get(parentId: Snowflake): Sequence<Snowflake> {
        return map[parentId]?.asSequence().orEmpty()
    }

    actual fun clear(parentId: Snowflake) {
        map.remove(parentId)
    }
}
