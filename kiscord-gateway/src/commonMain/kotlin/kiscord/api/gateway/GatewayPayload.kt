/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.api.gateway

import kiscord.api.*
import kotlinx.serialization.*
import kotlinx.serialization.descriptors.*
import kotlinx.serialization.encoding.*

/**
 * Generic payload container for communicating with Discord gateway.
 *
 * All packets should be encapsulated with this to work properly.
 */
@Serializable
public data class GatewayPayload<out T> @KiscordUnstableAPI constructor(
    /** Opcode for the payload */
    @SerialName("op")
    val opcode: GatewayOpcode,
    /** Payload data */
    @SerialName("d")
    val data: T,
    /**
     * Sequence number, used for resuming sessions and heartbeats
     *
     * Valid only when [opcode] is [GatewayOpcode.Dispatch]
     */
    @SerialName("s")
    val sequence: GatewaySequence? = null,
    /**
     * The event name for this payload
     *
     * Valid only when [opcode] is [GatewayOpcode.Dispatch]
     */
    @SerialName("t")
    @Serializable(with = EventNameSerializer::class)
    val eventName: EventName? = null
)

internal fun <T> GatewayPayload<*>.withData(data: T): GatewayPayload<T> = GatewayPayload(
    opcode = opcode,
    data = data,
    sequence = sequence,
    eventName = eventName
)

@OptIn(ExperimentalSerializationApi::class)
internal object EventNameSerializer : KSerializer<EventName?> {
    override val descriptor: SerialDescriptor =
        PrimitiveSerialDescriptor("kiscord.api.gateway.EventName", PrimitiveKind.STRING).nullable

    override fun serialize(encoder: Encoder, value: EventName?) {
        if (value == null) {
            encoder.encodeNull()
        } else {
            encoder.encodeNotNullMark()
            encoder.encodeString(EventName.enumToValue(value))
        }
    }

    override fun deserialize(decoder: Decoder): EventName? {
        return if (decoder.decodeNotNullMark()) {
            EventName.valueToEnum(decoder.decodeString())
        } else {
            decoder.decodeNull()
        }
    }
}

