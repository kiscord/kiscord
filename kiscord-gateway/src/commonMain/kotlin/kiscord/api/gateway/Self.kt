/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.api.gateway

import kiscord.api.*
import kiscord.builder.*

public interface Self : UserSpec {
    public suspend fun update(patch: UserApi.UserPatch)
    public suspend fun updatePresence(updatePresence: UpdatePresence)
}

public suspend inline fun Self.update(block: UserApi.UserPatch.Builder.() -> Unit) {
    update(UserApi.UserPatch(block))
}

public suspend inline fun Self.updatePresence(block: UpdatePresence.Builder.() -> Unit) {
    updatePresence(UpdatePresence(block))
}
