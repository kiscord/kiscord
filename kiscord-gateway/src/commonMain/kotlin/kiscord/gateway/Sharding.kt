/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.gateway

import kiscord.*
import kiscord.api.gateway.*
import kiscord.builder.*
import kiscord.gateway.Sharding.Custom.Builder.invoke

/**
 * Client-side initial sharding configuration
 */
public sealed interface Sharding {
    /**
     * Total shards count that designates to connect to the gateway,
     * or `null` if gateway default should be used
     */
    public val totalShards: Int? get() = null

    /**
     * Predicate that should allow or forbid to spawn specified shard (called [totalShards] times)
     */
    public fun shouldSpawnShard(shard: ShardConfiguration): Boolean = true

    /**
     * Kiscord should spawn all recommended shards
     */
    public object All : Sharding {
        override fun toString(): String = "Sharding.All"
    }

    /**
     * Kiscord should spawn one (and only one) shard
     */
    public object Single : Sharding {
        override val totalShards: Int get() = 1
        override fun toString(): String = "Sharding.Single"
    }

    /**
     * Kiscord should spawn all [totalShards] shards
     */
    public class Total(public override val totalShards: Int) : Sharding

    /**
     * Base class for custom sharding strategies
     * @see invoke
     */
    public abstract class Custom : Sharding {
        override fun toString(): String = "Sharding.Custom(totalShards=$totalShards)"

        public companion object Builder {
            /**
             * Creates custom sharding strategy, that uses gateway default shard count
             */
            public inline operator fun invoke(crossinline block: (ShardConfiguration) -> Boolean): Sharding =
                object : Sharding.Custom() {
                    override fun shouldSpawnShard(shard: ShardConfiguration) = block(shard)
                }

            /**
             * Creates custom sharding strategy, that uses supplied [totalShards] shards count
             */
            public inline operator fun invoke(
                totalShards: Int,
                crossinline block: (ShardConfiguration) -> Boolean
            ): Sharding = object : Sharding.Custom() {
                override val totalShards: Int get() = totalShards
                override fun shouldSpawnShard(shard: ShardConfiguration) = block(shard)
            }
        }
    }

    public companion object {
        /**
         * Default sharding configuration
         */
        public val Default: Sharding get() = All

        internal val Key: TypedAttribute<Sharding> = TypedAttribute("Kiscord:Gateway:Sharding", Default)
    }
}

public var Kiscord.Builder.sharding: Sharding by Sharding.Key
public val Kiscord.sharding: Sharding by Sharding.Key
