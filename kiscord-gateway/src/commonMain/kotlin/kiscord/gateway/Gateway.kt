/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

@file:OptIn(ExperimentalTime::class)

package kiscord.gateway

import io.ktor.client.plugins.*
import io.ktor.client.plugins.websocket.*
import io.ktor.client.request.*
import io.ktor.http.*
import io.ktor.util.*
import io.ktor.util.pipeline.*
import io.ktor.websocket.*
import kiscord.*
import kiscord.api.*
import kiscord.api.gateway.*
import kiscord.builder.*
import kiscord.ktor.*
import kiscord.serialization.*
import kotlinx.coroutines.*
import kotlinx.coroutines.sync.*
import org.kodein.log.*
import kotlin.coroutines.*
import kotlin.time.*
import kotlin.time.Duration.Companion.milliseconds
import kotlin.time.Duration.Companion.seconds

public class Gateway private constructor(private val client: Kiscord) {
    private val logger = client.loggerFactory.newLogger("kiscord.gateway", "Gateway")

    public val incomingPipeline: GatewayIncomingPipeline = GatewayIncomingPipeline(Kiscord.DEBUG)
    public val outgoingPipeline: GatewayOutgoingPipeline = GatewayOutgoingPipeline(Kiscord.DEBUG)
    public val requestPipeline: GatewayRequestPipeline = GatewayRequestPipeline(Kiscord.DEBUG)

    private lateinit var brokerJob: Job
    public lateinit var shards: Shards
        private set
    private val spawned = CompletableDeferred<Boolean>(client.coroutineContext[Job])

    @OptIn(ExperimentalCoroutinesApi::class)
    public val isSpawned: Boolean get() = spawned.isCompleted && spawned.getCompleted()

    public suspend fun connect(waitForReady: Boolean, join: Boolean) {
        val gatewayEndpoint = getGatewayEndpoint()

        val sharding = client.sharding
        val numShards = sharding.totalShards ?: gatewayEndpoint.defaultShards

        spawnShards(client, numShards, sharding, gatewayEndpoint.gatewayUrl)

        when {
            join -> join()
            waitForReady -> waitForReadiness()
        }
    }

    private suspend fun getGatewayEndpoint(): GatewayRequest.DiscoveryState {
        val discoveryCall = DiscoveryCall(client)
        val result = withContext(discoveryCall) {
            requestPipeline.execute(discoveryCall, GatewayRequest.Init.Discovery)
        }
        check(result is GatewayRequest.DiscoveryState) { "Invalid discovery state" }
        return result
    }

    public suspend fun join() {
        spawned.await()
        shards.forEach { it.join() }
    }

    public suspend fun waitForReadiness(): Boolean {
        spawned.await()
        return shards.all { it.waitForReadiness() }
    }

    private suspend fun spawnShards(scope: CoroutineScope, numShards: Int, sharding: Sharding, url: Url): Job {
        val eventLoop = DumbEventLoop()

        if (::brokerJob.isInitialized) {
            brokerJob.cancel("shard respawn in the progress")
        }
        brokerJob = scope.launch(CoroutineName("shard broker"), CoroutineStart.LAZY) {
            eventLoop.loop()
        }

        val shards = Shards(numShards)

        for (shardId in 0 until numShards) {
            val configuration = ShardConfiguration(shardId, numShards)
            if (!sharding.shouldSpawnShard(configuration)) continue

            val shard = when (shardId) {
                Shard.MainShardId -> Shard.Main(client, configuration)
                else -> Shard.Secondary(client, configuration)
            }

            shards.plusAssign(shard)

            logger.debug { "Created shard $shard" }
        }

        this.shards = shards

        var anyShardSpawned = false

        val connections = shards.map { shard ->
            val waitBefore = anyShardSpawned
            anyShardSpawned = true
            eventLoop.enqueueDeferred(brokerJob) {
                eventLoop.spawnShardAsync(
                    url,
                    shard,
                    waitBefore,
                    firstConnection = true
                )
            }
        }

        if (connections.isEmpty()) {
            spawned.complete(false)
            throw IllegalStateException("No shards spawned")
        }

        spawned.complete(true)
        brokerJob.start()

        connections.forEach {
            it.await().waitForReadiness()
        }

        return brokerJob
    }

    public fun cancel(cause: CancellationException? = null) {
        if (::brokerJob.isInitialized) {
            brokerJob.cancel(cause)
        }
        if (::shards.isInitialized) {
            shards.forEach { it.cancel(cause) }
        }
    }

    public suspend fun close(reason: CloseReason = CloseReason(CloseReason.Codes.NORMAL, "")) {
        val exception = CancellationException("Gateway is about to close: ${reason.code} ${reason.message}".trimEnd())
        if (::brokerJob.isInitialized) {
            brokerJob.cancel(exception)
        }
        if (::shards.isInitialized) {
            shards.forEach { it.close(reason) }
        }
    }

    private class DiscoveryCall(
        override val client: Kiscord,
    ) : GatewayCall {
        override val attributes: TypedAttributes by lazy { TypedAttributes() }
        override val shard get() = throw IllegalStateException("Discovery in progress")
        override val connection get() = throw IllegalStateException("Discovery in progress")
        override val direction: GatewayCall.Direction get() = GatewayCall.Direction.Outgoing
        override val isActive: Boolean get() = true
    }

    private suspend fun DumbEventLoop.spawnShardAsync(
        gatewayUrl: Url,
        shard: Shard,
        waitBefore: Boolean,
        firstConnection: Boolean,
    ): Deferred<GatewayConnection> {
        if (waitBefore) {
            delay(ShardSpawnDelay)
        }

        if (firstConnection) {
            logger.info { "Spawn shard ${shard.displayName}" }
        }

        return try {
            val connection = shard.connect(gatewayUrl, reconnectCallback = { error ->
                if (error is CancellationException) return@connect
                logger.warning(error) { "Shard about to reconnect due to error" }

                enqueue {
                    @Suppress("DeferredResultUnused")
                    spawnShardAsync(
                        gatewayUrl,
                        shard,
                        waitBefore = true,
                        firstConnection = false,
                    )
                }
            })

            if (connection.waitForReadiness()) {
                logger.info { "Shard ${shard.displayName} becomes ready" }
            } else {
                logger.info { "Shard ${shard.displayName} resumed session" }
            }

            CompletableDeferred(connection)
        } catch (e: Exception) {
            logger.error(e) { "Failed to connect shard ${shard.displayName}" }

            enqueueDeferred(brokerJob) {
                spawnShardAsync(gatewayUrl, shard, waitBefore = true, firstConnection = false)
            }
        }
    }

    private fun install() {
        requestPipeline.intercept(GatewayRequestPipeline.Init) { subject ->
            if (subject !is GatewayRequest.Init) return@intercept

            fun identify(): GatewayRequest.IdentifyState {
                return GatewayRequest.IdentifyState(Identify {
                    token = context.client.token.tokenValue
                    properties = Identify.Properties.Builder(
                        os = OSName.Current.name,
                        browser = Kiscord.NAME,
                        device = Kiscord.NAME
                    )
                    shard = context.shard.configuration
                    intents = client.intents
                    presence = client.presence?.toBuilder()
                    largeThreshold = client.largeThreshold
                })
            }

            suspend fun discovery(): GatewayRequest.DiscoveryState {
                while (true) {
                    val gatewayEndpoint = if (client.token.type == Token.Type.Bot && !PlatformUtils.IS_BROWSER) {
                        client.gateway.getGatewayBot()
                    } else {
                        client.gateway.getGateway()
                    }

                    val sessionStartLimit = (gatewayEndpoint as? GetGateway.Bot)?.sessionStartLimit
                    if (sessionStartLimit != null && sessionStartLimit.remaining <= 0) {
                        logger.info { "Session limit reached, waiting ${sessionStartLimit.resetAfter}ms before attempting to connect" }
                        delay(sessionStartLimit.resetAfter)
                        continue
                    }

                    return GatewayRequest.DiscoveryState(
                        gatewayUrl = gatewayEndpoint.url,
                        defaultShards = (gatewayEndpoint as? GetGateway.Bot)?.shards ?: 1
                    )
                }
            }

            val nextContainer: GatewayRequest = when (subject) {
                GatewayRequest.Init.Identify -> identify()
                GatewayRequest.Init.Discovery -> discovery()
            }

            proceedWith(nextContainer)
        }


        requestPipeline.intercept(GatewayRequestPipeline.Init) { request ->
            if (request !is GatewayRequest.ConnectRequest) return@intercept

            val url = URLBuilder(request.url).apply {
                if (encodedPath.isBlank()) {
                    encodedPath = "/"
                }
                parameters["v"] = "${Kiscord.API_VERSION}"
                parameters["encoding"] = "json"
            }.build()

            proceedWith(GatewayRequest.ConnectRequest(url))
        }

        requestPipeline.intercept(GatewayRequestPipeline.Process) { request ->
            if (request !is GatewayRequest.ConnectRequest) return@intercept

            val session = context.client.httpClient.webSocketSession {
                url(request.url)
                anonymous()
            }

            proceedWith(GatewayRequest.ConnectedState(session))
        }

        outgoingPipeline.intercept(GatewayOutgoingPipeline.Send) { subject ->
            if (subject !is Frame) return@intercept
            context.connection.session.outgoing.send(subject)
        }

        incomingPipeline.intercept(GatewayIncomingPipeline.State) { subject ->
            if (subject is Frame.Close) {
                context.connection.close(
                    subject.readReason() ?: CloseReason(
                        CloseReason.Codes.INTERNAL_ERROR,
                        "internal error"
                    )
                )
                return@intercept finish()
            }

            if (subject !is GatewayPayload<*>) return@intercept
            if (!subject.opcode.receive) throw IllegalArgumentException("Opcode must be registered for receiving payloads")

            when (subject.opcode) {
                GatewayOpcode.Dispatch -> {
                    val sequence = subject.sequence ?: throw IllegalArgumentException("Missing sequence number")
                    if (!context.shard.sessionTracker.updateSequence(sequence)) {
                        context.connection.close(CloseReason(CloseReason.Codes.CANNOT_ACCEPT, "sequence desync"))
                        return@intercept finish()
                    }
                }

                GatewayOpcode.Hello -> {
                    val data = subject.data as Hello
                    context.shard.sessionTracker.scheduleHeartbeat(
                        context.connection,
                        data.heartbeatInterval.milliseconds
                    )
                    authorize(this@Gateway)
                }

                GatewayOpcode.Heartbeat -> {
                    if (!context.shard.sessionTracker.heartbeat(context.connection)) {
                        context.connection.close(CloseReason(CloseReason.Codes.CANNOT_ACCEPT, "heartbeat desync"))
                        return@intercept finish()
                    }
                }

                GatewayOpcode.HeartbeatAck -> {
                    if (!context.shard.sessionTracker.heartbeatAck()) {
                        context.connection.close(
                            CloseReason(
                                CloseReason.Codes.CANNOT_ACCEPT,
                                "heartbeat ack desync"
                            )
                        )
                        return@intercept finish()
                    }
                }

                GatewayOpcode.InvalidSession -> {
                    context.shard.sessionTracker.reset()
                    delay(ShardSpawnDelay)
                    authorize(this@Gateway)
                }

                GatewayOpcode.Reconnect -> {
                    context.connection.close(
                        CloseReason(
                            CloseReason.Codes.SERVICE_RESTART,
                            "gateway requested to reconnect"
                        )
                    )
                    return@intercept finish()
                }

                else -> throw IllegalArgumentException("Unhandled payload opcode ${subject.opcode}, kiscord bug ¯\\_(ツ)_/¯")
            }
        }

        incomingPipeline.intercept(GatewayIncomingPipeline.Process) { subject ->
            if (subject !is GatewayPayload<*>) return@intercept
            if (subject.eventName == null) return@intercept
            val newData = dispatch(subject)
            if (newData != null && newData !== subject.data) {
                proceedWith(subject.withData(newData))
            }
        }
    }

    public companion object : Plugin<Unit, Gateway, Kiscord> {
        public override val key: TypedAttribute<Gateway> = TypedAttribute("Kiscord:Gateway:Base")

        public val Intents: TypedAttribute<Set<Identify.Intent>> =
            TypedAttribute("Kiscord:Gateway:Identify:Intents", emptySet())
        public val Presence: TypedAttribute<UpdatePresence?> =
            TypedAttribute("Kiscord:Gateway:Identify:Presence", null)
        public val GuildSubscriptions: TypedAttribute<Boolean> =
            TypedAttribute("Kiscord:Gateway:Identify:GuildSubscriptions", true)
        public val LargeThreshold: TypedAttribute<Int> =
            TypedAttribute("Kiscord:Gateway:Identify:LargeThreshold", 50)

        // We need to wait 5 seconds before attempting to connect next shard
        // See https://discord.com/developers/docs/topics/gateway#identifying
        public val ShardSpawnDelay: Duration = 5.seconds

        override fun afterFeatureAdded(builder: PluggableBuilder<Kiscord>) {
            builder.maybeInstall(GatewayCompression)
        }

        override fun beforeScopeCreated(builder: PluggableBuilder<Kiscord>) {
            builder.install(GatewayJsonCoder) {
                json = DiscordSerialFormat.Json
            }

            (builder as Kiscord.Builder).httpClient {
                install(WebSockets)
            }
        }

        override fun install(scope: Kiscord, block: ConfigurationChain<Unit>): Gateway {
            val plugin = Gateway(scope)
            plugin.install()
            return plugin
        }

        private suspend fun PipelineContext<*, out GatewayCall>.authorize(gateway: Gateway) {
            val connection = context.connection
            val shard = connection.shard
            val tracker = shard.sessionTracker
            val resumeMetadata = tracker.resumeMetadata
            if (resumeMetadata == null) {
                val result = gateway.requestPipeline.execute(context, GatewayRequest.Init.Identify)
                check(result is GatewayRequest.IdentifyState) { "Invalid identify state" }

                connection.send(Identify.serializer(), result.identify)
            } else {
                connection.send(
                    Resume.serializer(), Resume(
                        token = shard.client.token.tokenValue,
                        sessionId = resumeMetadata.sessionId,
                        sequence = tracker.lastSequence
                    )
                )
            }
        }
    }
}
