/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.gateway

import io.ktor.client.plugins.websocket.*
import io.ktor.util.*
import io.ktor.websocket.*
import kiscord.api.gateway.*
import kotlinx.atomicfu.*
import kotlinx.coroutines.*
import kotlinx.serialization.*

@OptIn(ExperimentalCoroutinesApi::class)
public class GatewayConnection internal constructor(
    public val session: DefaultClientWebSocketSession,
    public val shard: Shard
) : CoroutineScope by session, DisposableHandle {
    public val attributes: Attributes = Attributes(concurrent = true)

    private val state = atomic<State>(State.Starting)
    private val stateTrackerJob: Job

    init {
        stateTrackerJob = session.launch(start = CoroutineStart.ATOMIC) {
            val currentState = state.value
            check(currentState === State.Starting)
            val newState = State.Idle(coroutineContext[Job] ?: throw IllegalStateException())
            check(state.compareAndSet(currentState, newState))
            currentState.nextState.complete(newState)
            stateTracker()
        }
    }

    public val closeReason: CloseReason?
        get() = state.value.closeReason ?: session.closeReason.let {
            if (it.isCompleted) it.getCompleted() else null
        }

    private suspend fun stateTracker() {
        while (true) when (val currentState = state.value) {
            is State.Closing -> {
                val newState = State.Closed(currentState)
                if (state.compareAndSet(currentState, newState)) {
                    currentState.nextState.complete(newState)
                    session.close(currentState.closeReason)
                }
            }

            is State.Cancelled -> {
                val newState = State.Closed(currentState)
                if (state.compareAndSet(currentState, newState)) {
                    currentState.nextState.complete(newState)
                    session.cancel(currentState.cause ?: CancellationException("connection closed"))
                }
            }

            is State.Closed -> {
                val newState = State.Disposed(currentState)
                if (state.compareAndSet(currentState, newState)) {
                    currentState.nextState.complete(newState)
                    dispose()
                }
            }

            is State.Disposed -> return
            is State.Starting -> throw IllegalStateException()
            else -> currentState.nextState.join()
        }
    }

    internal fun markAsReady() {
        markAs { State.Ready(it) }
    }

    internal fun markAsResumed() {
        markAs { State.Resumed(it) }
    }

    private fun markAs(block: (Job) -> State) {
        val newState = block(stateTrackerJob)
        while (true) when (val currentState = state.value) {
            is State.Starting -> {
                if (state.compareAndSet(currentState, newState)) {
                    currentState.nextState.complete(newState)
                    return
                }
            }

            is State.Idle -> {
                if (state.compareAndSet(currentState, newState)) {
                    currentState.nextState.complete(newState)
                    return
                }
            }

            else -> throw IllegalStateException("Unable to mark connection as ready")
        }
    }

    internal suspend fun waitForReadiness(): Boolean {
        while (true) when (val currentState = state.value) {
            is State.Starting, is State.Idle -> currentState.nextState.await()
            is State.Ready -> return true
            is State.Resumed -> return false
            else -> when (val reason = currentState.closeReason) {
                null -> throw IllegalStateException("Unable to wait for readiness on closed connection")
                else -> throw IllegalStateException("Unable to wait for readiness on closed connection: $reason")
            }
        }
    }

    public suspend fun close(closeReason: CloseReason = CloseReason(CloseReason.Codes.NORMAL, "")) {
        val newState = State.Closing(stateTrackerJob, closeReason)
        while (true) when (val currentState = state.value) {
            is State.Idle -> {
                if (state.compareAndSet(currentState, newState)) {
                    currentState.nextState.complete(newState)
                    newState.nextState.join()
                    return
                }
            }

            is State.Ready, is State.Resumed, is State.Starting -> {
                if (state.compareAndSet(currentState, newState)) {
                    currentState.nextState.complete(newState)
                    newState.nextState.join()
                    return
                }
            }

            else -> return
        }
    }

    public fun cancel(cause: CancellationException? = null) {
        val newState = State.Cancelled(stateTrackerJob, cause)
        while (true) when (val currentState = state.value) {
            is State.Idle -> {
                if (state.compareAndSet(currentState, newState)) {
                    currentState.nextState.complete(newState)
                    return
                }
            }

            is State.Ready, is State.Resumed, is State.Starting -> {
                if (state.compareAndSet(currentState, newState)) {
                    currentState.nextState.complete(newState)
                    return
                }
            }

            else -> return
        }
    }

    public suspend fun <T : GatewayPayloadData> send(serializer: KSerializer<T>, payload: T) {
        shard.send(this, serializer, GatewayPayload(payload.opcode, payload))
    }

    override fun dispose() {
        check(state.value is State.Disposed)
        for (key in attributes.allKeys) {
            val attr = attributes[key]
            if (attr is DisposableHandle) {
                attr.dispose()
            }
        }
    }

    private sealed class State(parent: Job?) {
        open val closeReason: CloseReason? get() = null
        open val nextState = CompletableDeferred<State>(parent)

        data object Starting : State(null)
        class Idle(parent: Job?) : State(parent)
        class Ready(parent: Job?) : State(parent)
        class Resumed(parent: Job?) : State(parent)
        class Cancelled(parent: Job?, val cause: CancellationException?) : State(parent)
        class Closing(parent: Job?, override val closeReason: CloseReason) : State(parent)

        class Closed(previousState: State) : State(null) {
            override val closeReason: CloseReason? = previousState.closeReason
        }

        class Disposed(previousState: State) : State(null) {
            override val closeReason: CloseReason? = previousState.closeReason
            override val nextState: CompletableDeferred<State> get() = throw IllegalStateException("Disposed is final state")
        }
    }
}

public suspend inline fun <reified T : GatewayPayloadData> GatewayConnection.send(data: T) {
    send(serializer<T>(), data)
}
