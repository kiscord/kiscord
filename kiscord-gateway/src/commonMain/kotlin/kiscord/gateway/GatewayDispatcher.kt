/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.gateway

import io.ktor.util.pipeline.*
import kiscord.*
import kiscord.api.*
import kiscord.api.gateway.*

internal suspend fun PipelineContext<*, GatewayCall>.handle(data: ReadyData): ReadyData {
    check(data.version == Kiscord.API_VERSION) { "Discord API version mismatch" }
    data.shard?.let { check(it == context.shard.configuration) { "Mismatch shard configuration" } }

    context.shard.sessionTracker.updateSession(data.resumeGatewayUrl, data.sessionId)
    context.shard.updateSelf(data.user, context.connection)

    val repository = context.shard.repository

    data.guilds.asSequence()
        .filter { it.unavailable != false }
        .filterIsInstance<Guild>().forEach { guild ->
            repository.guild.set(guild)
        }

    context.connection.markAsReady()
    context.shard._readiness.complete(true)

    return data
}

internal fun PipelineContext<*, GatewayCall>.handle(data: ResumedData): ResumedData {
    context.connection.markAsResumed()
    context.shard._readiness.complete(true)

    context.shard.updateSelfConnection(context.connection)

    return data
}

internal suspend fun PipelineContext<*, GatewayCall>.handle(data: ChannelCreateData): Channel? {
    return context.shard.repository.channel.set(data.value)
}

internal suspend fun PipelineContext<*, GatewayCall>.handle(data: ChannelUpdateData): Channel? {
    return context.shard.repository.channel.update(data.value)
}

internal suspend fun PipelineContext<*, GatewayCall>.handle(data: ChannelDeleteData): Channel? {
    return context.shard.repository.channel.remove(data.value.id)
}

internal suspend fun PipelineContext<*, GatewayCall>.handle(data: ChannelPinsUpdateData): Channel? {
    return context.shard.repository.channel.updateLastPinTimestamp(data)
}

internal suspend fun PipelineContext<*, GatewayCall>.handle(data: ThreadCreateData): Channel? {
    return context.shard.repository.channel.set(data.value)
}

internal suspend fun PipelineContext<*, GatewayCall>.handle(data: ThreadUpdateData): Channel? {
    return context.shard.repository.channel.update(data.value)
}

internal suspend fun PipelineContext<*, GatewayCall>.handle(data: ThreadDeleteData): Channel? {
    return context.shard.repository.channel.remove(data.value.id)
}

internal suspend fun PipelineContext<*, GatewayCall>.handle(data: GuildCreateData): Guild? {
    return context.shard.repository.guild.set(data.value)
}

internal suspend fun PipelineContext<*, GatewayCall>.handle(data: GuildUpdateData): Guild? {
    return context.shard.repository.guild.update(data.value)
}

internal suspend fun PipelineContext<*, GatewayCall>.handle(data: GuildDeleteData): Guild? {
    return context.shard.repository.guild.remove(data.value.id)
}

internal suspend fun PipelineContext<*, GatewayCall>.handle(data: GuildMemberAddData): GuildMemberWithGuild? {
    val member = GuildMember.Builder(data).build()
    return context.shard.repository.guild.memberAdd(data.guildId, member)
}

internal suspend fun PipelineContext<*, GatewayCall>.handle(data: GuildMemberRemoveData): GuildMemberWithGuild? {
    return context.shard.repository.guild.memberRemove(data.guildId, data.user.id)
}

internal suspend fun PipelineContext<*, GatewayCall>.handle(data: GuildRoleCreateData): RoleWithGuild? {
    return context.shard.repository.guild.roleAdd(data.guildId, data.role)
}

internal suspend fun PipelineContext<*, GatewayCall>.handle(data: GuildRoleUpdateData): RoleWithGuild? {
    return context.shard.repository.guild.roleUpdate(data.guildId, data.role)
}

internal suspend fun PipelineContext<*, GatewayCall>.handle(data: GuildRoleDeleteData): RoleWithGuild? {
    return context.shard.repository.guild.roleDelete(data.guildId, data.roleId)
}

internal suspend fun PipelineContext<*, GatewayCall>.handle(data: MessageCreateData): Message? {
    return context.shard.repository.message.set(data.value)
}

internal suspend fun PipelineContext<*, GatewayCall>.handle(data: MessageUpdateData): Message? {
    return context.shard.repository.message.update(data.value)
}

internal suspend fun PipelineContext<*, GatewayCall>.handle(data: MessageDeleteData): Message? {
    return context.shard.repository.message.remove(data.id)
}

internal suspend fun PipelineContext<*, GatewayCall>.handle(data: MessageDeleteBulkData): List<Message> {
    return context.shard.repository.message.removeAll(data.ids)
}

internal suspend fun PipelineContext<*, GatewayCall>.handle(data: MessageReactionAddData): ReactionWithMessage? {
    return context.shard.repository.message.addReaction(data)
}

internal suspend fun PipelineContext<*, GatewayCall>.handle(data: MessageReactionRemoveAllData): Message? {
    return context.shard.repository.message.removeAllReactions(data.messageId)
}

internal suspend fun PipelineContext<*, GatewayCall>.handle(data: GuildBanAddData): UserWithGuild? {
    return context.shard.repository.guild.banAdd(data.guildId, data.user)
}

internal suspend fun PipelineContext<*, GatewayCall>.handle(data: GuildBanRemoveData): UserWithGuild? {
    return context.shard.repository.guild.banRemove(data.guildId, data.user)
}

internal suspend fun PipelineContext<*, GatewayCall>.handle(data: AutoModerationRuleCreateData): AutoModerationRuleWithGuild? {
    return context.shard.repository.guild.autoModerationRuleAdd(data.value.id, data.value)
}

internal suspend fun PipelineContext<*, GatewayCall>.handle(data: AutoModerationRuleUpdateData): AutoModerationRuleWithGuild? {
    return context.shard.repository.guild.autoModerationRuleUpdate(data.value.id, data.value)
}

internal suspend fun PipelineContext<*, GatewayCall>.handle(data: AutoModerationRuleDeleteData): AutoModerationRuleWithGuild? {
    return context.shard.repository.guild.autoModerationRuleDelete(data.value.id, data.value)
}

@Suppress("UnusedReceiverParameter")
internal fun PipelineContext<*, GatewayCall>.handle(data: TypingStartData): TypingStartData = data
