/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.gateway

import kiscord.*
import kotlinx.coroutines.*
import kotlin.coroutines.*

/**
 * Gateway scope, passed through [GatewayIncomingPipeline], [GatewayOutgoingPipeline] and [GatewayRequestPipeline]
 */
public interface GatewayCall : KiscordCall {
    /**
     * Shard instance bound to this call
     *
     * @throws IllegalStateException if shard unavailable
     */
    public val shard: Shard

    /**
     * Gateway connection instance bound to this call
     *
     * @throws IllegalStateException if gateway connection unavailable
     */
    public val connection: GatewayConnection

    override val client: Kiscord get() = shard.client

    /**
     * See [Job.isActive] for details
     */
    public val isActive: Boolean get() = connection.isActive

    @OptIn(ExperimentalStdlibApi::class)
    public companion object Key :
        AbstractCoroutineContextKey<KiscordCall, GatewayCall>(KiscordCall, { it as? GatewayCall })

    public val direction: Direction

    public enum class Direction {
        Incoming,
        Outgoing,
    }
}
