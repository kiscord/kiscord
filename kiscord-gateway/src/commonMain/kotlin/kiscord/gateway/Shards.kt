/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.gateway

public class Shards internal constructor(capacity: Int) : Collection<Shard> {
    init {
        require(capacity > 0) { "ShardMap capacity should be positive" }
    }

    public val main: Shard.Main
        get() = get(Shard.MainShardId) as Shard.Main

    private val shards = arrayOfNulls<Shard>(capacity)

    public operator fun get(shardId: Int): Shard =
        shards[shardId] ?: throw NoSuchElementException("No slave shard with id $shardId found")

    internal operator fun plusAssign(shard: Shard) {
        val shardId = shard.configuration.shardId
        require(shards[shardId] == null)
        shards[shardId] = shard
    }

    override fun iterator(): Iterator<Shard> = iterator {
        yieldAll(shards.asSequence().filterNotNull())
    }

    override val size: Int
        get() = shards.count { it != null }

    override fun contains(element: Shard): Boolean = element in shards

    override fun containsAll(elements: Collection<Shard>): Boolean = elements.all { it in shards }

    override fun isEmpty(): Boolean = shards.all { it == null }
}
