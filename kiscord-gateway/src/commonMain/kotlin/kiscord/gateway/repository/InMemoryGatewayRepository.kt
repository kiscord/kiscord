/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.gateway.repository

import kiscord.api.*
import kiscord.api.gateway.*
import kiscord.builder.*

public class InMemoryGatewayRepository : GatewayRepository {
    private val guildRole = InMemoryGuildRoleRepository()
    private val guildMember = InMemoryGuildMemberRepository()
    private val guildBan = InMemoryGuildBanRepository()
    private val guildAutoModerationRule = InMemoryGuildAutoModerationRuleRepository()

    private val _channel = InMemoryChannelRepository()
    override val channel: GatewayRepository.ChannelRepository get() = _channel
    override val guild: GatewayRepository.GuildRepository =
        InMemoryGuildRepository(_channel, guildRole, guildMember, guildBan, guildAutoModerationRule)
    override val message: GatewayRepository.MessageRepository = InMemoryMessageRepository()

    private class InMemoryGuildRepository(
        private val channel: InMemoryChannelRepository,
        private val guildRole: InMemoryGuildRoleRepository,
        private val guildMember: InMemoryGuildMemberRepository,
        private val guildBan: InMemoryGuildBanRepository,
        private val guildAutoModerationRule: InMemoryGuildAutoModerationRuleRepository,
    ) : InMemorySnowflakeRepository<Guild, Guild.UpdateData>(), GatewayRepository.GuildRepository {
        override fun extractIdFromValue(value: Guild): Snowflake = value.id
        override fun extractIdFromUpdateData(updateData: Guild.UpdateData): Snowflake = updateData.id.value

        override suspend fun transform(id: Snowflake, value: Guild?, term: Term): Guild? {
            when {
                term == Term.Add && value != null -> {
                    guildRole.set(id, value.roles)
                    guildMember.set(id, value.members.orEmpty())
                    channel.set(id, value.channels.orEmpty())
                }
            }
            return super.transform(id, value, term)
        }

        override suspend fun memberAdd(guildId: Snowflake, member: GuildMember): GuildMemberWithGuild? {
            val storedMember = guildMember.set(guildId, member) ?: return null
            return GuildMemberWithGuild(storedMember, get(guildId) ?: return null)
        }

        override suspend fun memberRemove(guildId: Snowflake, userId: Snowflake): GuildMemberWithGuild? {
            val storedMember = guildMember.remove(guildId, userId) ?: return null
            return GuildMemberWithGuild(storedMember, get(guildId) ?: return null)
        }

        override suspend fun roleAdd(guildId: Snowflake, role: Role): RoleWithGuild? {
            val storedRole = guildRole.set(guildId, role) ?: return null
            return RoleWithGuild(storedRole, get(guildId) ?: return null)
        }

        override suspend fun roleUpdate(guildId: Snowflake, role: Role): RoleWithGuild? {
            val storedRole = guildRole.set(guildId, role) ?: return null
            return RoleWithGuild(storedRole, get(guildId) ?: return null)
        }

        override suspend fun roleDelete(guildId: Snowflake, roleId: Snowflake): RoleWithGuild? {
            val storedRole = guildRole.remove(guildId, roleId) ?: return null
            return RoleWithGuild(storedRole, get(guildId) ?: return null)
        }

        override suspend fun banAdd(guildId: Snowflake, user: User): UserWithGuild? {
            val storedUser = guildBan.set(guildId, user) ?: return null
            return UserWithGuild(storedUser, get(guildId) ?: return null)
        }

        override suspend fun banRemove(guildId: Snowflake, user: User): UserWithGuild? {
            val storedUser = guildBan.remove(guildId, user.id) ?: return null
            return UserWithGuild(storedUser, get(guildId) ?: return null)
        }

        override suspend fun autoModerationRuleAdd(
            guildId: Snowflake,
            rule: AutoModerationRule
        ): AutoModerationRuleWithGuild? {
            val storedRule = guildAutoModerationRule.set(guildId, rule) ?: return null
            return AutoModerationRuleWithGuild(storedRule, get(guildId) ?: return null)
        }

        override suspend fun autoModerationRuleUpdate(
            guildId: Snowflake,
            rule: AutoModerationRule
        ): AutoModerationRuleWithGuild? {
            val storedRule = guildAutoModerationRule.set(guildId, rule) ?: return null
            return AutoModerationRuleWithGuild(storedRule, get(guildId) ?: return null)
        }

        override suspend fun autoModerationRuleDelete(
            guildId: Snowflake,
            rule: AutoModerationRule
        ): AutoModerationRuleWithGuild? {
            val storedRule = guildAutoModerationRule.remove(guildId, rule.id) ?: return null
            return AutoModerationRuleWithGuild(storedRule, get(guildId) ?: return null)
        }
    }

    private class InMemoryChannelRepository : InMemorySnowflakeRepository<Channel, Channel.UpdateData>(),
        GatewayRepository.ChannelRepository {
        private val guildChannelTracker = SnowflakeRelationsTracker()

        override fun extractIdFromValue(value: Channel): Snowflake = value.id
        override fun extractIdFromUpdateData(updateData: Channel.UpdateData): Snowflake = updateData.id.value

        override suspend fun transform(id: Snowflake, value: Channel?, term: Term): Channel? {
            when {
                term == Term.Add && value != null -> {
                    val guildId = value.guildId
                    if (guildId != null) {
                        guildChannelTracker.track(guildId, id)
                    }
                }

                term == Term.Remove && value != null -> {
                    val guildId = value.guildId
                    if (guildId != null) {
                        guildChannelTracker.untrack(guildId, id)
                    }
                }
            }
            return super.transform(id, value, term)
        }

        override suspend fun set(guildId: Snowflake, channels: Collection<Channel>) {
            guildChannelTracker.get(guildId).forEach { remove(it) }
            channels.forEach { set(it) }
        }

        override suspend fun updateLastPinTimestamp(data: ChannelPinsUpdateData): Channel? {
            return update(data.channelId) { _, oldValue ->
                oldValue?.copy(lastPinTimestamp = data.lastPinTimestamp)
            }
        }
    }

    private class InMemoryMessageRepository : InMemorySnowflakeRepository<Message, Message.UpdateData>(),
        GatewayRepository.MessageRepository {
        private val channelMessageTracker = SnowflakeRelationsTracker()

        override fun extractIdFromValue(value: Message): Snowflake = value.id
        override fun extractIdFromUpdateData(updateData: Message.UpdateData): Snowflake = updateData.id.value

        override suspend fun transform(id: Snowflake, value: Message?, term: Term): Message? {
            when {
                term == Term.Add && value != null -> {
                    channelMessageTracker.track(value.channelId, id)
                }

                term == Term.Remove && value != null -> {
                    channelMessageTracker.untrack(value.channelId, id)
                }
            }
            return super.transform(id, value, term)
        }

        override suspend fun fetch(channelId: Snowflake): Sequence<Snowflake> {
            return channelMessageTracker.get(channelId)
        }

        override suspend fun removeAll(ids: Collection<Snowflake>): List<Message> {
            return ids.mapNotNull { remove(it) }
        }

        override suspend fun removeAllReactions(messageId: Snowflake): Message? {
            return update(messageId) { _, oldMessage ->
                oldMessage?.copy(reactions = emptySet())
            }
        }

        override suspend fun addReaction(data: MessageReactionAddData): ReactionWithMessage? {
            // TODO: implement this
            return null
        }
    }

    private class InMemoryGuildRoleRepository : InMemorySnowflakeMultiMap<Role, Nothing>() {
        override fun extractIdFromValue(value: Role): Snowflake = value.id
        override fun extractIdFromUpdateData(updateData: Nothing): Nothing = throw NotImplementedError()
    }

    private class InMemoryGuildBanRepository : InMemorySnowflakeMultiMap<User, Nothing>() {
        override fun extractIdFromValue(value: User): Snowflake = value.id
        override fun extractIdFromUpdateData(updateData: Nothing): Nothing = throw NotImplementedError()
    }

    private class InMemoryGuildMemberRepository : InMemorySnowflakeMultiMap<GuildMember, Nothing>() {
        override fun extractIdFromValue(value: GuildMember): Snowflake =
            value.user?.id ?: error("No user id for guild member")

        override fun extractIdFromUpdateData(updateData: Nothing): Nothing = throw NotImplementedError()
    }

    private class InMemoryGuildAutoModerationRuleRepository : InMemorySnowflakeMultiMap<AutoModerationRule, Nothing>() {
        override fun extractIdFromValue(value: AutoModerationRule): Snowflake = value.id
        override fun extractIdFromUpdateData(updateData: Nothing): Nothing = throw NotImplementedError()
    }

    private class InMemoryGuildChannelRepository : InMemorySnowflakeMultiMap<Channel, Channel.UpdateData>() {
        override fun extractIdFromValue(value: Channel): Snowflake = value.id
        override fun extractIdFromUpdateData(updateData: Channel.UpdateData): Snowflake = updateData.id.value
    }
}

internal expect abstract class InMemorySnowflakeRepository<Value : Any, in UpdateData : UpdaterFor<Value>>() :
    GatewayRepository.SnowflakeRepository<Value, UpdateData> {
    protected abstract fun extractIdFromValue(value: Value): Snowflake
    protected abstract fun extractIdFromUpdateData(updateData: UpdateData): Snowflake
    protected open suspend fun transform(id: Snowflake, value: Value?, term: Term): Value?

    override suspend fun set(value: Value): Value?
    override suspend fun update(data: UpdateData): Value?
    override suspend fun remove(id: Snowflake): Value?
    override suspend fun remove(value: Value): Boolean
    override suspend fun get(id: Snowflake): Value?
    override suspend fun contains(id: Snowflake): Boolean
    suspend fun getOrPut(id: Snowflake, block: () -> Value): Value
    override suspend fun update(id: Snowflake, block: (id: Snowflake, oldValue: Value?) -> Value?): Value?
    suspend fun replace(oldValue: Value, newValue: Value): Boolean

    enum class Term {
        Add, Update, Remove, Get,
    }
}

internal abstract class InMemorySnowflakeMultiMap<Value : Any, UpdateData : UpdaterFor<Value>> {
    private val repository = object : InMemorySnowflakeRepository<NestedRepository<Value, UpdateData>, Nothing>() {
        override fun extractIdFromValue(value: NestedRepository<Value, @UnsafeVariance UpdateData>) = value.id
        override fun extractIdFromUpdateData(updateData: Nothing): Nothing = throw NotImplementedError()
    }

    private class NestedRepository<Value : Any, in UpdateData : UpdaterFor<Value>>(
        private val parent: InMemorySnowflakeMultiMap<Value, UpdateData>,
        val id: Snowflake,
    ) : InMemorySnowflakeRepository<Value, UpdateData>() {
        override fun extractIdFromValue(value: Value): Snowflake = parent.extractIdFromValue(value)
        override fun extractIdFromUpdateData(updateData: UpdateData): Snowflake =
            parent.extractIdFromUpdateData(updateData)
    }

    protected abstract fun extractIdFromValue(value: Value): Snowflake
    protected abstract fun extractIdFromUpdateData(updateData: UpdateData): Snowflake

    suspend fun set(key: Snowflake, value: Value): Value? {
        return get(key).set(value)
    }

    suspend fun remove(key: Snowflake, value: Value): Boolean {
        return getOrNull(key)?.remove(value) == true
    }

    suspend fun remove(key: Snowflake, valueId: Snowflake): Value? {
        return getOrNull(key)?.remove(valueId)
    }

    suspend fun removeAll(key: Snowflake): InMemorySnowflakeRepository<Value, UpdateData>? {
        return repository.remove(key)
    }

    suspend fun replace(key: Snowflake, oldValue: Value, newValue: Value): Boolean {
        val collection = getOrNull(key) ?: return false
        return collection.replace(oldValue, newValue)
    }

    suspend fun get(key: Snowflake): InMemorySnowflakeRepository<Value, UpdateData> {
        return repository.getOrPut(key) { NestedRepository(this, key) }
    }

    suspend fun getOrNull(key: Snowflake): InMemorySnowflakeRepository<Value, UpdateData>? {
        return repository.get(key)
    }

    suspend fun set(key: Snowflake, values: Collection<Value>): InMemorySnowflakeRepository<Value, UpdateData> {
        val collection = NestedRepository(this, key)
        values.forEach { collection.set(it) }
        repository.set(collection)
        return collection
    }
}

internal expect class SnowflakeRelationsTracker() {
    fun track(parentId: Snowflake, childId: Snowflake)
    fun untrack(parentId: Snowflake, childId: Snowflake)
    fun get(parentId: Snowflake): Sequence<Snowflake>
    fun clear(parentId: Snowflake)
}
