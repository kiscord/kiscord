/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.gateway.repository

import kiscord.*
import kiscord.api.*
import kiscord.api.gateway.*
import kiscord.builder.*
import kiscord.gateway.*

public interface GatewayRepository {
    public val guild: GuildRepository
    public val channel: ChannelRepository
    public val message: MessageRepository

    public interface SnowflakeRepository<Value : Any, in UpdateData : Any> {
        public suspend fun set(value: Value): Value?
        public suspend fun update(data: UpdateData): Value?
        public suspend fun remove(id: Snowflake): Value?
        public suspend fun remove(value: Value): Boolean
        public suspend fun get(id: Snowflake): Value?
        public suspend fun contains(id: Snowflake): Boolean
        public suspend fun update(id: Snowflake, block: (id: Snowflake, oldValue: Value?) -> Value?): Value?
    }

    public interface GuildRepository : SnowflakeRepository<Guild, Guild.UpdateData> {
        public suspend fun memberAdd(guildId: Snowflake, member: GuildMember): GuildMemberWithGuild?
        public suspend fun memberRemove(guildId: Snowflake, userId: Snowflake): GuildMemberWithGuild?

        public suspend fun roleAdd(guildId: Snowflake, role: Role): RoleWithGuild?
        public suspend fun roleUpdate(guildId: Snowflake, role: Role): RoleWithGuild?
        public suspend fun roleDelete(guildId: Snowflake, roleId: Snowflake): RoleWithGuild?

        public suspend fun banAdd(guildId: Snowflake, user: User): UserWithGuild?
        public suspend fun banRemove(guildId: Snowflake, user: User): UserWithGuild?

        public suspend fun autoModerationRuleAdd(guildId: Snowflake, rule: AutoModerationRule): AutoModerationRuleWithGuild?
        public suspend fun autoModerationRuleUpdate(guildId: Snowflake, rule: AutoModerationRule): AutoModerationRuleWithGuild?
        public suspend fun autoModerationRuleDelete(guildId: Snowflake, rule: AutoModerationRule): AutoModerationRuleWithGuild?
    }

    public interface ChannelRepository : SnowflakeRepository<Channel, Channel.UpdateData> {
        public suspend fun set(guildId: Snowflake, channels: Collection<Channel>)
        public suspend fun updateLastPinTimestamp(data: ChannelPinsUpdateData): Channel?
    }

    public interface MessageRepository : SnowflakeRepository<Message, Message.UpdateData> {
        public suspend fun fetch(channelId: Snowflake): Sequence<Snowflake>
        public suspend fun removeAll(ids: Collection<Snowflake>): List<Message>
        public suspend fun removeAllReactions(messageId: Snowflake): Message?
        public suspend fun addReaction(data: MessageReactionAddData): ReactionWithMessage?
    }
}

public fun interface GatewayRepositoryProvider {
    public fun getRepository(shard: Shard): GatewayRepository

    public object Default : GatewayRepositoryProvider {
        private val repository = InMemoryGatewayRepository()

        override fun getRepository(shard: Shard): GatewayRepository = repository
    }

    public class Global(private val repository: GatewayRepository) : GatewayRepositoryProvider {
        override fun getRepository(shard: Shard): GatewayRepository = repository
    }

    public class PerShard(private val provider: GatewayRepositoryProvider) : GatewayRepositoryProvider {
        private val map = mutableMapOf<ShardConfiguration, GatewayRepository>()

        override fun getRepository(shard: Shard): GatewayRepository {
            return map.getOrPut(shard.configuration) { provider.getRepository(shard) }
        }
    }

    public companion object {
        internal val Key = TypedAttribute<GatewayRepositoryProvider>("Kiscord:Gateway:Repository", Default)
    }
}

@BuilderDsl
public var Kiscord.Builder.repository: GatewayRepositoryProvider by GatewayRepositoryProvider.Key

@BuilderDsl
public val Kiscord.repository: GatewayRepositoryProvider by GatewayRepositoryProvider.Key

@BuilderDsl
public fun Kiscord.Builder.repository(repositoryProvider: GatewayRepositoryProvider) {
    repository = repositoryProvider
}

@BuilderDsl
public fun Kiscord.Builder.repository(cache: GatewayRepository) {
    repository = GatewayRepositoryProvider.Global(cache)
}

