/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.gateway

import io.ktor.http.*
import io.ktor.util.*
import io.ktor.websocket.*
import kiscord.*
import kiscord.api.*
import kiscord.api.gateway.*
import kiscord.builder.*
import kiscord.gateway.repository.*
import kotlinx.atomicfu.*
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.*
import kotlinx.coroutines.sync.*
import kotlinx.serialization.*
import org.kodein.log.*
import kotlin.coroutines.*

public sealed class Shard(
    public val client: Kiscord,
    public val configuration: ShardConfiguration
) : CoroutineScope {
    private val logger = client.loggerFactory.newLogger("kiscord.gateway", "Shard")

    public abstract val kind: Kind
    public val attributes: Attributes = Attributes()
    public val repository: GatewayRepository by lazy { client.repository.getRepository(this) }
    public val displayName: String get() = "#${configuration.shardId}/${configuration.numShards}"
    public val closeReason: Deferred<CloseReason?> get() = _closeReason
    override val coroutineContext: CoroutineContext get() = connection.coroutineContext

    private lateinit var connection: GatewayConnection
    private val gateway = client[Gateway]
    private val connectMutex = Mutex()
    private val _closeReason = CompletableDeferred<CloseReason?>()
    internal val sessionTracker = SessionTracker()
    internal val _readiness = CompletableDeferred<Boolean>()

    @get:OptIn(ExperimentalCoroutinesApi::class)
    public val readiness: Boolean
        get() = when (_readiness.isCompleted) {
            true -> _readiness.getCompleted()
            else -> false
        }

    public suspend fun waitForReadiness(): Boolean {
        return _readiness.await()
    }

    private val _self = atomic<SelfForShard?>(null)

    public val self: Self get() = _self.value ?: throw IllegalStateException("Self user not initialized yet")

    private inner class SelfForShard(
        private val user: User,
        private val connection: GatewayConnection
    ) : Self, UserSpec by user {
        override suspend fun update(patch: UserApi.UserPatch) {
            val newUser = client.user.modifyCurrentUser(patch)
            _self.compareAndSet(this, copy(user = newUser))
        }

        override suspend fun updatePresence(updatePresence: UpdatePresence) {
            client.presence = updatePresence
            connection.updatePresence(updatePresence)
        }

        fun copy(user: User = this.user, connection: GatewayConnection = this.connection): SelfForShard {
            return SelfForShard(user, connection)
        }

        override fun toString() = "Self(user=$user)"
    }

    internal fun updateSelf(user: User, connection: GatewayConnection) {
        _self.lazySet(SelfForShard(user, connection))
    }

    internal fun updateSelfConnection(connection: GatewayConnection) {
        _self.update { self ->
            self?.copy(connection = connection)
        }
    }

    public suspend fun connect(
        gatewayUrl: Url,
        reconnectCallback: (Throwable?) -> Unit
    ): GatewayConnection = connectMutex.withLock {
        if (::connection.isInitialized) {
            connection.close(CloseReason(CloseReason.Codes.SERVICE_RESTART, "reconnecting"))
        }
        val connectCall = ConnectCall(this)
        val resumeMetadata = sessionTracker.resumeMetadata
        val connectUrl = resumeMetadata?.url ?: gatewayUrl
        logger.info { "Shard $displayName connecting to $connectUrl" }
        val connectState = withContext(connectCall) {
            client[Gateway].requestPipeline.execute(connectCall, GatewayRequest.ConnectRequest(connectUrl))
        }
        require(connectState is GatewayRequest.ConnectedState) { "Gateway websocket connection isn't established properly" }

        val session = connectState.session
        val connection = GatewayConnection(session, this)
        this.connection = connection

        connection.launch {
            val pipeline = gateway.incomingPipeline

            for (frame in session.incoming) {
                val call = ShardCall(this@Shard, connection, GatewayCall.Direction.Incoming)
                withContext(call) {
                    pipeline.execute(call, frame)
                }
                if (connection.closeReason != null) {
                    break
                }
            }
        }.invokeOnCompletion {
            connection.cancel()
            if (!closeReason.isCompleted) {
                reconnectCallback(it)
            } else {
                _readiness.complete(false)
            }
        }

        connection
    }

    public fun reconnect() {
        connection.cancel()
    }

    public suspend fun close(reason: CloseReason = CloseReason(CloseReason.Codes.NORMAL, "")) {
        _closeReason.complete(reason)
        if (::connection.isInitialized) {
            connection.close(reason)
        }
    }

    public fun cancel(cause: CancellationException? = null) {
        _closeReason.completeExceptionally(cause ?: CancellationException("shard is about to shutdown"))
        if (::connection.isInitialized) {
            connection.cancel(cause)
        }
    }

    internal suspend fun <T> send(
        connection: GatewayConnection,
        serializer: KSerializer<T>,
        payload: GatewayPayload<T>
    ) {
        val call = ShardCall(this, connection, GatewayCall.Direction.Outgoing)
        withContext(call) {
            gateway.outgoingPipeline.execute(call, GatewayOutgoingPipeline.SerializablePayload(payload, serializer))
        }
    }

    public suspend fun join() {
        _closeReason.join()
    }

    private class ConnectCall(
        override val shard: Shard,
    ) : GatewayCall {
        override val attributes: TypedAttributes by lazy { TypedAttributes() }
        override val connection get() = throw IllegalStateException("Connection in progress")
        override val direction: GatewayCall.Direction get() = GatewayCall.Direction.Outgoing
    }

    private class ShardCall(
        override val shard: Shard,
        override val connection: GatewayConnection,
        override val direction: GatewayCall.Direction
    ) : GatewayCall {
        override val attributes: TypedAttributes = TypedAttributes()
    }

    public class Main internal constructor(
        client: Kiscord, shardConfiguration: ShardConfiguration
    ) : Shard(client, shardConfiguration) {
        init {
            require(shardConfiguration.shardId == MainShardId) { "Invalid shard configuration" }
        }

        override val kind: Kind get() = Kind.Main
    }

    public class Secondary internal constructor(
        client: Kiscord, shardConfiguration: ShardConfiguration
    ) : Shard(client, shardConfiguration) {
        init {
            require(shardConfiguration.shardId != MainShardId) { "Invalid shard configuration" }
        }

        override val kind: Kind get() = Kind.Secondary
    }

    override fun toString(): String = "Shard(kind=$kind, configuration=$configuration)"

    public enum class Kind {
        Main,
        Secondary,
    }

    public companion object {
        public const val MainShardId: Int = 0
    }
}
