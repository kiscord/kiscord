/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.gateway

import kiscord.*
import kiscord.api.gateway.*
import kiscord.builder.*
import kotlinx.coroutines.*
import kotlin.coroutines.*

public val Kiscord.shards: Shards
    get() = this[Gateway].shards

public suspend fun Kiscord.connect(waitForReady: Boolean = false, join: Boolean = false) {
    this[Gateway].connect(waitForReady, join)
}

public suspend fun Kiscord.join() {
    this[Gateway].join()
}

public suspend fun Kiscord.waitForReadiness() {
    this[Gateway].waitForReadiness()
}

public var Kiscord.Builder.intents: Set<Identify.Intent> by Gateway.Intents
public val Kiscord.intents: Set<Identify.Intent> by Gateway.Intents

@BuilderDsl
public fun Kiscord.Builder.intent(intent: Identify.Intent) {
    this.intents = this.intents + intent
}

@BuilderDsl
public fun Kiscord.Builder.intents(vararg intents: Identify.Intent) {
    this.intents = this.intents + intents
}

@BuilderDsl
public fun Kiscord.Builder.intents(intents: Iterable<Identify.Intent>) {
    this.intents = this.intents + intents
}

public var Kiscord.Builder.presence: UpdatePresence? by Gateway.Presence

@Suppress("unused")
public var Kiscord.presence: UpdatePresence? by Gateway.Presence
    internal set

@BuilderDsl
public inline fun Kiscord.Builder.presence(block: UpdatePresence.Builder.() -> Unit) {
    val builder = presence?.let { UpdatePresence.Builder(it) } ?: UpdatePresence.builder()
    block(builder)
    presence = builder.build()
}

public var Kiscord.Builder.largeThreshold: Int by Gateway.LargeThreshold
public val Kiscord.largeThreshold: Int by Gateway.LargeThreshold

@PublishedApi
internal suspend fun gatewayScope(): GatewayCall {
    return coroutineContext[GatewayCall] ?: throw IllegalStateException("Not in gateway scope")
}

/**
 * Waits for [GatewayConnection] to complete
 */
public suspend fun GatewayConnection.join() {
    val job = coroutineContext[Job] ?: error("Connection cannot be joined because it does not have a job: $this")
    job.join()
}

public suspend fun GatewayConnection.updatePresence(updatePresence: UpdatePresence) {
    send(UpdatePresence.serializer(), updatePresence)
}

public suspend inline fun GatewayConnection.updatePresence(block: UpdatePresence.Builder.() -> Unit) {
    updatePresence(UpdatePresence(block))
}

public suspend fun GatewayConnection.updateVoiceState(updateVoiceState: UpdateVoiceState) {
    send(UpdateVoiceState.serializer(), updateVoiceState)
}

public suspend inline fun GatewayConnection.updateVoiceState(block: UpdateVoiceState.Builder.() -> Unit) {
    updateVoiceState(UpdateVoiceState(block))
}

public suspend fun GatewayConnection.requestGuildMembers(requestGuildMembers: RequestGuildMembers) {
    send(RequestGuildMembers.serializer(), requestGuildMembers)
}

public suspend inline fun GatewayConnection.requestGuildMembers(block: RequestGuildMembers.Builder.() -> Unit) {
    requestGuildMembers(RequestGuildMembers(block))
}
