/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.gateway

import io.ktor.websocket.*
import kiscord.*
import kiscord.api.gateway.*
import kiscord.builder.*
import kotlinx.serialization.*
import kotlinx.serialization.json.*
import org.kodein.log.*

internal sealed class GatewayCoder<T : Any> {
    protected abstract fun <R> T.convert(strategy: DeserializationStrategy<R>): R

    protected abstract fun install(kiscord: Kiscord)

    fun beforeDispatch(opcode: GatewayOpcode, data: T, eventName: EventName?): Any = when (opcode) {
        GatewayOpcode.Dispatch -> when (val serializer = eventName?.serializer) {
            null -> data
            else -> data.convert(serializer)
        }
        GatewayOpcode.Heartbeat -> data.convert(Heartbeat.serializer())
        GatewayOpcode.Reconnect -> Reconnect
        GatewayOpcode.InvalidSession -> data.convert(InvalidSession.serializer())
        GatewayOpcode.Hello -> data.convert(Hello.serializer())
        GatewayOpcode.HeartbeatAck -> HeartbeatAck
        else -> data
    }
}

internal class GatewayJsonCoder private constructor(
    private val json: Json,
) : GatewayCoder<JsonElement>() {
    private constructor(config: Config) : this(
        json = config.json ?: throw IllegalStateException("Missing json coder"),
    )

    class Config(
        var json: Json? = null
    )

    companion object : Plugin<Config, GatewayJsonCoder, Kiscord> {
        private val rawSerializer = GatewayPayload.serializer(JsonElement.serializer())
        override val key = TypedAttribute<GatewayJsonCoder>("Kiscord:Gateway:Coder:Json")

        override fun beforeFeatureAdded(builder: PluggableBuilder<Kiscord>) {
            builder.maybeInstall(Gateway)
        }

        override fun install(scope: Kiscord, block: ConfigurationChain<Config>): GatewayJsonCoder {
            val plugin = GatewayJsonCoder(block(Config()))
            plugin.install(scope)
            return plugin
        }
    }

    override fun <R> JsonElement.convert(strategy: DeserializationStrategy<R>) =
        json.decodeFromJsonElement(strategy, this)

    override fun install(kiscord: Kiscord) {
        val logger = kiscord.loggerFactory.newLogger("kiscord.gateway", "GatewayJsonCoder")

        val gateway = kiscord[Gateway]

        gateway.incomingPipeline.intercept(GatewayIncomingPipeline.Parse) { subject ->
            if (subject !is Frame.Text) return@intercept
            proceedWith(subject.readText())
        }

        gateway.incomingPipeline.intercept(GatewayIncomingPipeline.Transform) { subject ->
            if (subject !is String) return@intercept
            val payload = json.decodeFromString(rawSerializer, subject)
            val newData = try {
                beforeDispatch(payload.opcode, payload.data, payload.eventName)
            } catch (e: Exception) {
                logger.error(e) { "Failed to decode payload" }
                finish()
                return@intercept
            }
            proceedWith(payload.withData(newData))
        }

        gateway.outgoingPipeline.intercept(GatewayOutgoingPipeline.Transform) { subject ->
            if (subject !is GatewayOutgoingPipeline.SerializablePayload<*>) return@intercept
            val text = subject.serialize(json)
            proceedWith(Frame.Text(text))
        }
    }
}
