/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.gateway

import io.ktor.client.plugins.websocket.*
import io.ktor.http.*
import io.ktor.util.pipeline.*
import kiscord.api.gateway.*

public sealed interface GatewayRequest {
    public enum class Init : GatewayRequest {
        Discovery,
        Identify,
        ;
    }

    public class ConnectRequest(public val url: Url) : GatewayRequest
    public class ConnectedState(public val session: DefaultClientWebSocketSession) : GatewayRequest
    public class DiscoveryState(public val gatewayUrl: Url, public val defaultShards: Int) : GatewayRequest
    public class IdentifyState(public val identify: Identify) : GatewayRequest
}

public inline fun GatewayRequestPipeline.interceptConnect(
    crossinline block: suspend PipelineContext<GatewayRequest, GatewayCall>.(Url) -> Url
) {
    intercept(GatewayRequestPipeline.State) { subject ->
        if (subject !is GatewayRequest.ConnectRequest) return@intercept
        val newUrl = block(subject.url)
        if (newUrl != subject.url) {
            proceedWith(GatewayRequest.ConnectRequest(newUrl))
        }
    }
}

public inline fun GatewayRequestPipeline.interceptIdentify(
    crossinline block: suspend PipelineContext<GatewayRequest, GatewayCall>.(Identify) -> Identify
) {
    intercept(GatewayRequestPipeline.State) { subject ->
        if (subject !is GatewayRequest.IdentifyState) return@intercept
        val newIdentify = block(subject.identify)
        if (newIdentify != subject.identify) {
            proceedWith(GatewayRequest.IdentifyState(newIdentify))
        }
    }
}
