/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

@file:OptIn(ExperimentalTime::class)

package kiscord.gateway

import kiscord.*
import kiscord.builder.*
import org.kodein.log.*
import kotlin.time.*

public class GatewayBenchmark private constructor(config: Config, kiscord: Kiscord) {
    public val logger: Logger = config.logger ?: kiscord.loggerFactory.newLogger("kiscord.gateway", "GatewayBenchmark")
    public val timeSource: TimeSource = config.timeSource
    public val threshold: Duration = config.threshold

    public data class Config(
        var logger: Logger? = null,
        var timeSource: TimeSource = TimeSource.Monotonic,
        var threshold: Duration = Duration.ZERO
    )

    public companion object : Plugin<Config, GatewayBenchmark, Kiscord> {
        override val key: TypedAttribute<GatewayBenchmark> = TypedAttribute("Kiscord:Gateway:Benchmark")
        private val StartMarkKey = TypedAttribute<TimeMark>("Kiscord:Gateway:Benchmark:StartMark")

        override fun beforeFeatureAdded(builder: PluggableBuilder<Kiscord>) {
            builder.maybeInstall(Gateway)
        }

        override fun install(scope: Kiscord, block: ConfigurationChain<Config>): GatewayBenchmark {
            val config = block(Config())
            val feature = GatewayBenchmark(config, scope)
            val gateway = scope[Gateway]

            gateway.incomingPipeline.intercept(GatewayIncomingPipeline.Before) {
                context.attributes[StartMarkKey] = feature.timeSource.markNow()
            }

            gateway.incomingPipeline.intercept(GatewayIncomingPipeline.After) {
                val startMark = context.attributes.getOrNull(StartMarkKey) ?: return@intercept
                val duration = startMark.elapsedNow()
                if (duration < feature.threshold) return@intercept
                feature.logger.info { "Processing incoming payload took $duration: $subject" }
            }

            return feature
        }
    }
}
