/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.gateway

import io.ktor.util.pipeline.*
import io.ktor.websocket.*
import kiscord.*
import kiscord.api.gateway.*
import kiscord.builder.*
import org.kodein.log.*

public class Logging private constructor(config: Config) {
    private val logger: GatewayLogger = config.logger()
    private val frameEnabled: Boolean = config.frameEnabled
    private val payloadEnabled: Boolean = config.payloadEnabled

    public class Config internal constructor(
        public var logger: () -> GatewayLogger,
        public var frameEnabled: Boolean = false,
        public var payloadEnabled: Boolean = true,
    ) {
        public fun logger(block: () -> GatewayLogger) {
            this.logger = block
        }

        public fun consoleLogger(loggerFactory: LoggerFactory, level: Logger.Level = defaultLoggerLevel): Unit =
            logger { ConsoleLogger(loggerFactory, level) }

        public fun consoleLogger(logger: Logger, level: Logger.Level = defaultLoggerLevel): Unit =
            logger { ConsoleLogger(logger, level) }
    }

    public interface GatewayLogger {
        public fun logFrame(shard: Shard, frame: Frame, direction: Direction)
        public fun logPayload(shard: Shard, payload: GatewayPayload<*>, direction: Direction)

        public enum class Direction {
            Incoming, Outgoing
        }
    }

    public class ConsoleLogger(
        public val logger: Logger,
        public val level: Logger.Level = defaultLoggerLevel
    ) : GatewayLogger {
        public constructor(loggerFactory: LoggerFactory, level: Logger.Level = defaultLoggerLevel) : this(
            logger = loggerFactory.newLogger("kiscord.gateway", "Logging"),
            level = level,
        )

        override fun logFrame(shard: Shard, frame: Frame, direction: GatewayLogger.Direction) {
            when (frame.frameType) {
                FrameType.TEXT -> logger.log(level) {
                    frame as Frame.Text
                    "${direction.name} frame at shard ${shard.displayName}: ${frame.readText()}"
                }
                FrameType.BINARY -> logger.log(level) {
                    frame as Frame.Binary
                    "${direction.name} frame at shard ${shard.displayName}: ${frame.readBytes().contentToString()}"
                }
                else -> {
                    // Ignore other frames
                }
            }
        }

        override fun logPayload(
            shard: Shard,
            payload: GatewayPayload<*>,
            direction: GatewayLogger.Direction
        ) {
            logger.log(level) {
                "${direction.name} payload at shard ${shard.displayName}: $payload"
            }
        }
    }

    public companion object : Plugin<Config, Logging, Kiscord> {
        override val key: TypedAttribute<Logging> = TypedAttribute("Kiscord:Gateway:Logging")
        private val defaultLoggerLevel = Logger.Level.DEBUG
        private val logFramePhase = PipelinePhase("LogFrame")
        private val logPayloadPhase = PipelinePhase("LogPayload")

        override fun beforeFeatureAdded(builder: PluggableBuilder<Kiscord>) {
            builder.maybeInstall(Gateway)
        }

        override fun install(scope: Kiscord, block: ConfigurationChain<Config>): Logging {
            val feature = Logging(block(Config(logger = {
                ConsoleLogger(scope.loggerFactory)
            })))
            feature.install(scope)
            return feature
        }
    }

    private fun install(scope: Kiscord) {
        val gateway = scope[Gateway]

        if (frameEnabled) {
            gateway.incomingPipeline.insertPhaseBefore(GatewayIncomingPipeline.Parse, logFramePhase)
            gateway.incomingPipeline.intercept(logFramePhase) { subject ->
                if (subject !is Frame) return@intercept
                logger.logFrame(context.shard, subject, GatewayLogger.Direction.Incoming)
            }

            gateway.outgoingPipeline.insertPhaseBefore(GatewayOutgoingPipeline.Send, logFramePhase)
            gateway.outgoingPipeline.intercept(logFramePhase) { subject ->
                if (subject !is Frame) return@intercept
                logger.logFrame(context.shard, subject, GatewayLogger.Direction.Outgoing)
            }
        }

        if (payloadEnabled) {
            gateway.incomingPipeline.insertPhaseAfter(GatewayIncomingPipeline.Transform, logPayloadPhase)
            gateway.incomingPipeline.intercept(logPayloadPhase) { subject ->
                if (subject !is GatewayPayload<*>) return@intercept
                logger.logPayload(context.shard, subject, GatewayLogger.Direction.Incoming)
            }

            gateway.outgoingPipeline.insertPhaseBefore(GatewayOutgoingPipeline.Transform, logPayloadPhase)
            gateway.outgoingPipeline.intercept(logPayloadPhase) { subject ->
                if (subject !is GatewayOutgoingPipeline.SerializablePayload<*>) return@intercept
                logger.logPayload(context.shard, subject.payload, GatewayLogger.Direction.Outgoing)
            }
        }
    }
}

@BuilderDsl
public fun PluggableBuilder<Kiscord>.logging(config: Logging.Config.() -> Unit = {}) {
    install(Logging, config)
}
