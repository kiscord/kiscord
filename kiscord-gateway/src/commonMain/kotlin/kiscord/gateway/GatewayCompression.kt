/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.gateway

import io.ktor.http.*
import io.ktor.util.*
import io.ktor.util.pipeline.*
import io.ktor.utils.io.charsets.*
import io.ktor.utils.io.core.*
import io.ktor.websocket.*
import kiscord.*
import kiscord.builder.*
import kotlinx.coroutines.*
import kotlinx.coroutines.sync.*
import org.kodein.log.*

/**
 * Provides `zlib-stream` transport compression support for the [Gateway] feature.
 *
 * [Discord API Reference](https://discord.com/developers/docs/topics/gateway#encoding-and-compression)
 */
public class GatewayCompression(
    public val compressor: Compressor
) {
    public companion object : Plugin<Config, GatewayCompression, Kiscord> {
        override val key: TypedAttribute<GatewayCompression> = TypedAttribute("Kiscord:Gateway:Compression")

        public val DecompressPhase: PipelinePhase = PipelinePhase("Decompress")


        override fun beforeFeatureAdded(builder: PluggableBuilder<Kiscord>) {
            builder.maybeInstall(Gateway)
        }

        override fun install(scope: Kiscord, block: ConfigurationChain<Config>): GatewayCompression {
            val config = block(Config())
            val logger = scope.loggerFactory.newLogger("kiscord.gateway", "GatewayCompression")
            val compressor = config.compressor

            if (!compressor.isSupported) {
                logger.warning { "Compressor ${compressor.name} isn't available, disabling compression" }

                return GatewayCompression(RawCompressor)
            }

            when (compressor) {
                is TransportCompressor -> installTransportCompression(scope, logger, compressor)
                is PayloadCompressor -> installPayloadCompression(scope, logger, compressor)
                RawCompressor -> {
                    // Noop
                }
            }

            return GatewayCompression(compressor)
        }

        private fun installTransportCompression(scope: Kiscord, logger: Logger, compressor: TransportCompressor) {
            val gateway = scope[Gateway]

            gateway.incomingPipeline.insertPhaseBefore(GatewayIncomingPipeline.Parse, DecompressPhase)

            gateway.incomingPipeline.intercept(DecompressPhase) { subject ->
                if (subject !is Frame.Binary) return@intercept

                val connectionContext = compressor.getOrCreateContext(context.connection)
                val isReadyToDecode = connectionContext.append(buildPacket { writeFully(subject.data) })

                if (!isReadyToDecode) {
                    return@intercept finish()
                }

                val decompressed = connectionContext.decode()

                proceedWith(Frame.Text(true, decompressed))
            }

            gateway.requestPipeline.intercept(GatewayRequestPipeline.Init) { container ->
                if (container !is GatewayRequest.ConnectRequest) return@intercept

                val url = URLBuilder(container.url).apply {
                    check("compress" !in parameters) { "More than one transport-compression feature are available" }
                    parameters["compress"] = compressor.name
                }.build()

                proceedWith(GatewayRequest.ConnectRequest(url))
            }

            logger.debug { "Transport-level compression initialized with compressor ${compressor.name}" }
        }

        private fun installPayloadCompression(scope: Kiscord, logger: Logger, compressor: PayloadCompressor) {
            val gateway = scope[Gateway]

            gateway.incomingPipeline.insertPhaseBefore(GatewayIncomingPipeline.Parse, DecompressPhase)

            gateway.incomingPipeline.intercept(DecompressPhase) { subject ->
                if (subject !is Frame.Binary) return@intercept

                val decompressed = compressor.getOrCreateContext(context.connection).decode(buildPacket {
                    writeFully(subject.data)
                })

                proceedWith(Charsets.UTF_8.newDecoder().decode(decompressed))
            }

            gateway.requestPipeline.intercept(GatewayRequestPipeline.State) { subject ->
                if (subject !is GatewayRequest.IdentifyState || subject.identify.compress) return@intercept
                proceedWith(GatewayRequest.IdentifyState(subject.identify.copy(compress = true)))
            }

            logger.debug { "Payload-level compression initialized with compressor ${compressor.name}" }
        }

        /**
         * Returns compressor for transport-level compression with using zlib.
         *
         * [Discord API Reference](https://discord.com/developers/docs/topics/gateway#transport-compression)
         */
        public val ZlibStream: Compressor get() = ZlibStreamCompressor

        /**
         * Returns compressor for payload-level compression with using zlib.
         *
         * [Discord API Reference](https://discord.com/developers/docs/topics/gateway#payload-compression)
         */
        public val Zlib: Compressor get() = ZlibCompressor

        /**
         * Returns no-op compressor, that effectively disables any compression.
         */
        public val Raw: Compressor get() = RawCompressor
    }

    @BuilderDsl
    public class Config(
        @BuilderDsl
        public var compressor: Compressor = ZlibStreamCompressor
    )

    public sealed interface Compressor {
        public val isSupported: Boolean
        public val name: String
    }

    private sealed class PayloadCompressor : Compressor {
        abstract fun getOrCreateContext(connection: GatewayConnection): PayloadCompressorContext
    }

    private sealed class TransportCompressor : Compressor {
        abstract fun getOrCreateContext(connection: GatewayConnection): TransportCompressorContext
    }

    private object ZlibStreamCompressor : TransportCompressor() {
        override val isSupported: Boolean get() = ZlibDecoder.isSupported
        override val name: String get() = "zlib-stream"

        private val ContextKey = AttributeKey<Context>("${key.name}:Context:ZlibStream")
        private const val BYTE_00: Byte = 0
        private const val BYTE_FF: Byte = -1

        override fun getOrCreateContext(connection: GatewayConnection): TransportCompressorContext {
            return connection.attributes.computeIfAbsent(ContextKey) { Context() }
        }

        private class Context(
            private val decoder: ZlibDecoder = ZlibDecoder(),
            private val buffer: BytePacketBuilder = BytePacketBuilder()
        ) : TransportCompressorContext, DisposableHandle {
            override fun append(data: ByteReadPacket): Boolean {
                val bytes = data.readBytes()
                buffer.writeFully(bytes)
                return bytes.size >= 4 &&
                    bytes[bytes.size - 4] == BYTE_00 && bytes[bytes.size - 3] == BYTE_00 &&
                    bytes[bytes.size - 2] == BYTE_FF && bytes[bytes.size - 1] == BYTE_FF
            }

            override suspend fun decode(): ByteReadPacket {
                return decoder.decode(buffer.build())
            }

            override fun dispose() {
                buffer.release()
                decoder.close()
            }
        }
    }

    private object ZlibCompressor : PayloadCompressor() {
        override val isSupported: Boolean get() = ZlibDecoder.isSupported
        override val name: String get() = "zlib"

        private val ContextKey = AttributeKey<Context>("${key.name}:Context:Zlib")

        override fun getOrCreateContext(connection: GatewayConnection): PayloadCompressorContext {
            return connection.attributes.computeIfAbsent(ContextKey) { Context() }
        }

        private class Context(
            private val decoder: ZlibDecoder = ZlibDecoder(),
        ) : PayloadCompressorContext, DisposableHandle {
            private val lock = Mutex()
            override suspend fun decode(data: ByteReadPacket): ByteReadPacket = lock.withLock {
                decoder.decode(data).also {
                    decoder.reset()
                }
            }

            override fun dispose() {
                decoder.close()
            }
        }
    }

    private object RawCompressor : Compressor {
        override val isSupported: Boolean get() = true
        override val name: String get() = "raw"
    }

    private interface TransportCompressorContext {
        fun append(data: ByteReadPacket): Boolean
        suspend fun decode(): ByteReadPacket
    }

    private interface PayloadCompressorContext {
        suspend fun decode(data: ByteReadPacket): ByteReadPacket
    }
}

@BuilderDsl
public fun Kiscord.Builder.gatewayCompression(block: GatewayCompression.Config.() -> Unit = {}) {
    install(GatewayCompression, block)
}

/**
 * Configures [gateway connection][GatewayConnection] to use [no compression][GatewayCompression.Raw]
 *
 * @see GatewayCompression.Raw
 */
@BuilderDsl
public fun GatewayCompression.Config.raw() {
    compressor = GatewayCompression.Raw
}

/**
 * Configures [gateway connection][GatewayConnection] to use [transport-level zlib compression][GatewayCompression.ZlibStream]
 *
 * @see GatewayCompression.ZlibStream
 */
@BuilderDsl
public fun GatewayCompression.Config.zlibStream() {
    compressor = GatewayCompression.ZlibStream
}

/**
 * Configures [gateway connection][GatewayConnection] to use [payload-level zlib compression][GatewayCompression.Zlib]
 *
 * @see GatewayCompression.Zlib
 */
@BuilderDsl
public fun GatewayCompression.Config.zlib() {
    compressor = GatewayCompression.Zlib
}

internal expect class ZlibDecoder constructor() : Closeable {
    suspend fun decode(buffer: ByteReadPacket): ByteReadPacket
    fun reset()
    override fun close()

    companion object {
        val isSupported: Boolean
    }
}
