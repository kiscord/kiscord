/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.gateway

import io.ktor.util.pipeline.*
import kiscord.api.gateway.*
import kotlinx.serialization.*

public class GatewayOutgoingPipeline(
    override val developmentMode: Boolean = false
) : Pipeline<Any, GatewayCall>(Before, Transform, Send, After) {
    public companion object Phases {
        public val Before: PipelinePhase = PipelinePhase("Before")
        public val Transform: PipelinePhase = PipelinePhase("Transform")
        public val Send: PipelinePhase = PipelinePhase("Send")
        public val After: PipelinePhase = PipelinePhase("After")
    }

    public data class SerializablePayload<T>(
        public val payload: GatewayPayload<T>,
        public val serializer: KSerializer<T>
    ) {
        public fun serialize(format: StringFormat): String =
            format.encodeToString(GatewayPayload.serializer(serializer), payload)

        public fun serialize(format: BinaryFormat): ByteArray =
            format.encodeToByteArray(GatewayPayload.serializer(serializer), payload)
    }
}
