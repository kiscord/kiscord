/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.gateway

import kotlinx.atomicfu.*
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.*

internal class DumbEventLoop {
    private val queue = Channel<suspend () -> Unit>(Channel.UNLIMITED)
    private val brokerActive = atomic(false)

    fun <R> enqueueAsync(parent: Job? = null, block: suspend () -> R): Deferred<R> {
        val deferred = CompletableDeferred<R>(parent)
        enqueue {
            deferred.complete(block())
        }
        return deferred
    }

    fun <R> enqueueDeferred(parent: Job? = null, block: suspend () -> Deferred<R>): Deferred<R> = enqueueAsync(parent) {
        block().await()
    }

    fun enqueue(block: suspend () -> Unit) {
        require(queue.trySend(block).isSuccess) { "Channel cannot block during enqueueing" }
    }

    suspend fun loop() {
        if (!brokerActive.compareAndSet(expect = false, update = true)) {
            throw IllegalStateException("EventLoop already active!")
        }
        for (message in queue) {
            message()
        }
    }
}
