/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.gateway

import io.ktor.http.*
import kiscord.api.gateway.*
import kotlinx.atomicfu.*
import kotlinx.coroutines.*
import kotlin.time.*

internal class SessionTracker {
    private val _resumeMetadata = atomic<ResumeMetadata?>(null)
    private val sequence = atomic(0L)
    private val heartbeatSent = atomic(false)
    private lateinit var heartbeatJob: Job

    val lastSequence: GatewaySequence get() = sequence.value

    val resumeMetadata: ResumeMetadata? get() = _resumeMetadata.value

    fun reset() {
        if (::heartbeatJob.isInitialized) heartbeatJob.cancel()
        _resumeMetadata.lazySet(null)
        sequence.lazySet(0L)
    }

    @OptIn(ExperimentalTime::class)
    fun scheduleHeartbeat(connection: GatewayConnection, heartbeatInterval: Duration) {
        if (::heartbeatJob.isInitialized) heartbeatJob.cancel()
        heartbeatSent.lazySet(false)
        heartbeatJob = connection.launch {
            while (isActive) {
                delay(heartbeatInterval)
                if (!heartbeat(connection)) {
                    connection.cancel()
                    break
                }
            }
        }
    }

    fun updateSequence(sequence: GatewaySequence): Boolean = this.sequence.compareAndSet(sequence - 1L, sequence)

    suspend fun heartbeat(connection: GatewayConnection): Boolean {
        val valid = heartbeatSent.compareAndSet(expect = false, update = true)
        if (valid) {
            connection.send(Heartbeat.serializer(), Heartbeat(sequence.value))
        }
        return valid
    }

    fun heartbeatAck() = heartbeatSent.compareAndSet(expect = true, update = false)

    data class ResumeMetadata(
        val url: Url,
        val sessionId: String,
    )

    fun updateSession(url: Url, sessionId: String) {
        if (!_resumeMetadata.compareAndSet(null, ResumeMetadata(url, sessionId))) {
            throw IllegalStateException("Unable to overwrite resume metadata")
        }
    }
}
