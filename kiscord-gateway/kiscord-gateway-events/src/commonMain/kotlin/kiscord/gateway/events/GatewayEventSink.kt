/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.gateway.events

import kotlinx.coroutines.*

/**
 * General-purpose coroutine-based event sink.
 * Can be subscribed for full event flow or select events based on types
 * @see subscribe
 */
@GatewayEventDsl
public interface GatewayEventSink {
    public fun <Event : GatewayPayloadEvent<Data>, Data : Any> subscribe(
        definition: GatewayEventDefinition<Event, Data>,
        concurrent: Boolean = false,
        handler: GatewayEventHandler<Event, Data>
    ): DisposableHandle

    public fun <Event : GatewayPayloadEvent<Data>, Data : Any> unsubscribe(
        definition: GatewayEventDefinition<Event, Data>,
        handler: GatewayEventHandler<Event, Data>
    )
}

