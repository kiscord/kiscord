/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.gateway.events

import kiscord.*
import kiscord.gateway.*
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.*
import kotlinx.coroutines.flow.*

/**
 * [GatewayEventBus] implementation which uses [SharedFlow] as backend.
 *
 * Doesn't support cancellation with [unsubscribe] function,
 * please use returned [DisposableHandle] from [subscribe] for this purpose.
 */
public class SharedFlowEventBus private constructor(
    private val coroutineScope: CoroutineScope, private val flow: MutableSharedFlow<Fire<*, *>>
) : GatewayEventBus {
    public constructor(
        coroutineScope: CoroutineScope,
        replay: Int = 0,
        extraBufferCapacity: Int = 16,
        onBufferOverflow: BufferOverflow = BufferOverflow.SUSPEND
    ) : this(coroutineScope, MutableSharedFlow(replay, extraBufferCapacity, onBufferOverflow))

    private class Fire<Event : GatewayPayloadEvent<Data>, Data : Any>(
        val definition: GatewayEventDefinition<Event, Data>, val event: Event, call: GatewayCall
    ) {
        val call = GatewayEventCallImpl(call, event)

        suspend fun handle(handler: GatewayEventHandler<Event, Data>) {
            withContext(call) {
                with(handler) {
                    try {
                        call.handle(event.data)
                    } catch (e: Throwable) {
                        GatewayEventExceptionHandler(call.client.loggerFactory)
                            .handleException(currentCoroutineContext(), e)
                    }
                }
            }
        }
    }

    private class JobDisposableHandle(private val job: Job) : DisposableHandle {
        override fun dispose() {
            job.cancel("Subscription cancelled")
        }
    }

    override suspend fun <Event : GatewayPayloadEvent<Data>, Data : Any> raise(
        definition: GatewayEventDefinition<Event, Data>, event: Event, scope: GatewayCall
    ) {
        val fire = Fire(definition, event, scope)
        if (!flow.tryEmit(fire)) {
            flow.emit(fire)
        }
    }

    override fun <Event : GatewayPayloadEvent<Data>, Data : Any> subscribe(
        definition: GatewayEventDefinition<Event, Data>, concurrent: Boolean, handler: GatewayEventHandler<Event, Data>
    ): DisposableHandle {
        val collector: FlowCollector<Fire<Event, Data>> = when {
            concurrent -> FlowCollector { fire ->
                fire.call.connection.launch {
                    fire.handle(handler)
                }
            }
            else -> FlowCollector { fire ->
                fire.handle(handler)
            }
        }

        val job = coroutineScope.launch {
            @Suppress("UNCHECKED_CAST")
            val refinedFlow = if (definition == GatewayEventDefinition.All) {
                flow
            } else {
                flow.filter { fire -> fire.definition == definition }
            } as Flow<Fire<Event, Data>>

            refinedFlow.collect(collector)
        }

        return JobDisposableHandle(job)
    }

    override fun <Event : GatewayPayloadEvent<Data>, Data : Any> unsubscribe(
        definition: GatewayEventDefinition<Event, Data>, handler: GatewayEventHandler<Event, Data>
    ): Nothing =
        throw NotImplementedError("Subscription cancellation supported only with initially returned DisposableHandle")
}
