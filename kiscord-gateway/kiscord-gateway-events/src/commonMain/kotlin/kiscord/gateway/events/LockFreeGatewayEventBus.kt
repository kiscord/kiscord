/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.gateway.events

import io.ktor.util.collections.*
import io.ktor.utils.io.*
import kiscord.*
import kiscord.gateway.*
import kotlinx.coroutines.*
import kotlinx.coroutines.internal.*

@OptIn(InternalAPI::class, InternalCoroutinesApi::class)
public class LockFreeGatewayEventBus : GatewayEventBus {
    private val handlers = CopyOnWriteHashMap<GatewayEventDefinition<*, *>, LockFreeLinkedListHead>()

    override suspend fun <Event : GatewayPayloadEvent<Data>, Data : Any> raise(
        definition: GatewayEventDefinition<Event, Data>, event: Event, scope: GatewayCall
    ) {
        val eventScope = GatewayEventCallImpl(scope, event)
        val exceptionHandler by lazy {
            eventScope[CoroutineExceptionHandler] ?: GatewayEventExceptionHandler(scope.client.loggerFactory)
        }

        withContext(eventScope) {
            handlers[definition]?.forEach<EventHandlerRegistration<Event, Data>> { registration ->
                try {
                    registration.handle(eventScope, event)
                } catch (e: Throwable) {
                    exceptionHandler.handleException(currentCoroutineContext(), e)
                }
            }

            if (definition != GatewayEventDefinition.All)
                handlers[GatewayEventDefinition.All]?.forEach<EventHandlerRegistration<GatewayPayloadEvent<Any>, Any>> { registration ->
                    try {
                        registration.handle(eventScope, event)
                    } catch (e: Throwable) {
                        exceptionHandler.handleException(currentCoroutineContext(), e)
                    }
                }
        }
    }

    override fun <Event : GatewayPayloadEvent<Data>, Data : Any> subscribe(
        definition: GatewayEventDefinition<Event, Data>,
        concurrent: Boolean,
        handler: GatewayEventHandler<Event, Data>
    ): DisposableHandle {
        val registration = when {
            concurrent -> EventHandlerRegistration.Concurrent(handler)
            else -> EventHandlerRegistration.Sequential(handler)
        }
        handlers.computeIfAbsent(definition) { LockFreeLinkedListHead() }.addLast(registration)
        return registration
    }

    override fun <Event : GatewayPayloadEvent<Data>, Data : Any> unsubscribe(
        definition: GatewayEventDefinition<Event, Data>,
        handler: GatewayEventHandler<Event, Data>
    ) {
        handlers[definition]?.forEach<EventHandlerRegistration<*, *>> { registration ->
            if (handler == registration.handler) registration.remove()
        }
    }

    private sealed class EventHandlerRegistration<in Event : GatewayPayloadEvent<Data>, Data : Any>(
        val handler: GatewayEventHandler<Event, Data>
    ) : LockFreeLinkedListNode(), DisposableHandle {
        override fun dispose() {
            remove()
        }

        class Sequential<in Event : GatewayPayloadEvent<Data>, Data : Any>(
            handler: GatewayEventHandler<Event, Data>
        ) : EventHandlerRegistration<Event, Data>(handler) {
            override suspend fun handle(call: GatewayEventCall<Event, Data>, event: Event) {
                with(handler) {
                    call.handle(event.data)
                }
            }
        }

        class Concurrent<in Event : GatewayPayloadEvent<Data>, Data : Any>(
            handler: GatewayEventHandler<Event, Data>
        ) : EventHandlerRegistration<Event, Data>(handler) {
            override suspend fun handle(call: GatewayEventCall<Event, Data>, event: Event) {
                call.connection.launch {
                    with(handler) {
                        call.handle(event.data)
                    }
                }
            }
        }

        abstract suspend fun handle(call: GatewayEventCall<Event, Data>, event: Event)
    }
}
