/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.gateway.events

import io.ktor.util.pipeline.*
import kiscord.*
import kiscord.api.gateway.*
import kiscord.builder.*
import kiscord.gateway.*
import kotlinx.coroutines.*

public class GatewayEvents private constructor(eventSink: GatewayEventSink) : GatewayEventSink by eventSink {
    public class Config : GatewayEventSink {
        private lateinit var _eventBus: GatewayEventBus

        @GatewayEventDsl
        public var eventBus: GatewayEventBus
            get() {
                if (!::_eventBus.isInitialized) {
                    _eventBus = LockFreeGatewayEventBus()
                }
                return _eventBus
            }
            set(value) {
                if (::_eventBus.isInitialized) {
                    throw IllegalStateException("EventBus already initialized")
                }
                _eventBus = value
            }

        override fun <Event : GatewayPayloadEvent<Data>, Data : Any> subscribe(
            definition: GatewayEventDefinition<Event, Data>,
            concurrent: Boolean,
            handler: GatewayEventHandler<Event, Data>
        ): DisposableHandle = eventBus.subscribe(definition, concurrent, handler)

        override fun <Event : GatewayPayloadEvent<Data>, Data : Any> unsubscribe(
            definition: GatewayEventDefinition<Event, Data>,
            handler: GatewayEventHandler<Event, Data>
        ): Unit = eventBus.unsubscribe(definition, handler)
    }

    public companion object : Plugin<Config, GatewayEvents, Kiscord> {
        override val key: TypedAttribute<GatewayEvents> = TypedAttribute("Kiscord:Events")

        private val FireEvent: PipelinePhase = PipelinePhase("FireEvent")

        override fun beforeFeatureAdded(builder: PluggableBuilder<Kiscord>) {
            builder.maybeInstall(Gateway)
        }

        override fun install(scope: Kiscord, block: ConfigurationChain<Config>): GatewayEvents {
            val eventBus = block(Config()).eventBus

            val gateway = scope[Gateway]

            gateway.incomingPipeline.insertPhaseAfter(GatewayIncomingPipeline.Process, FireEvent)
            gateway.incomingPipeline.intercept(FireEvent) { subject ->
                require(subject is GatewayPayload<*>) { "Payload expected, got $subject" }
                require(subject.opcode.receive)

                subject.eventDefinition?.safeRaise(eventBus, context, subject.data)
            }

            gateway.outgoingPipeline.insertPhaseBefore(GatewayOutgoingPipeline.Transform, FireEvent)
            gateway.outgoingPipeline.intercept(FireEvent) { subject ->
                require(subject is GatewayOutgoingPipeline.SerializablePayload<*>) { "Payload expected, got $subject" }
                val payload = subject.payload
                require(payload.opcode.send)

                payload.eventDefinition?.safeRaise(eventBus, context, payload.data)
            }

            return GatewayEvents(eventBus)
        }
    }
}

@BuilderDsl
public fun PluggableBuilder<Kiscord>.gatewayEvents(block: @GatewayEventDsl GatewayEvents.Config.() -> Unit = {}) {
    install(GatewayEvents, block = block)
}

public val Kiscord.gatewayEvents: GatewayEvents get() = this[GatewayEvents]

public inline fun Kiscord.gatewayEvents(block: GatewayEvents.() -> Unit) {
    gatewayEvents.apply(block)
}

internal val GatewayPayload<*>.eventDefinition: GatewayEventDefinition<*, *>?
    get() = when (opcode) {
        GatewayOpcode.Dispatch -> eventName?.definition
        GatewayOpcode.Heartbeat -> HeartbeatEvent
        GatewayOpcode.HeartbeatAck -> HeartbeatAckEvent
        GatewayOpcode.Hello -> HelloEvent
        GatewayOpcode.Identify -> IdentifyEvent
        GatewayOpcode.InvalidSession -> InvalidSessionEvent
        GatewayOpcode.Reconnect -> ReconnectEvent
        GatewayOpcode.RequestGuildMembers -> RequestGuildMembersEvent
        GatewayOpcode.Resume -> ResumeEvent
        GatewayOpcode.UpdatePresence -> UpdatePresenceEvent
        GatewayOpcode.UpdateVoiceState -> UpdateVoiceStateEvent
    }

private suspend inline fun <Event : GatewayPayloadEvent<Data>, Data : Any> GatewayEventDefinition<Event, Data>.safeRaise(
    eventBus: GatewayEventBus,
    scope: GatewayCall,
    data: Any?
) {
    eventBus.raise(this, safeBuild(data) ?: return, scope)
}
