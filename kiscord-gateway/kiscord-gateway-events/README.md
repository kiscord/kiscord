# Module kiscord-gateway-events

[kiscord-gateway](https://kiscord.aur.rocks/docs/kiscord-gateway/index.html) plugin to subscribe for gateway events.

# Package kiscord.gateway.events

API and models for subscribing events from [kiscord-gateway](https://kiscord.aur.rocks/docs/kiscord-gateway/index.html) module.
