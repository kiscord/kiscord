plugins {
    alias(libs.plugins.kotlin.multiplatform)
    alias(libs.plugins.kotlin.serialization)
    alias(libs.plugins.kotlinx.atomicfu)
    alias(libs.plugins.dokka)
    alias(libs.plugins.licenser)
    `maven-publish`
    signing
}

apply(from = rootProject.file("gradle/multiplatform-bom.gradle.kts"))
apply(from = rootProject.file("gradle/codegen.gradle.kts"))

val kiscordBrowser = (ext["kiscord.browser"] as String).toBoolean()

val codegenOutput: Provider<Directory> by ext
val generateKotlin by tasks.getting(JavaExec::class) {
    args("--category", "gateway-events")
}

kotlin {
    sourceSets.configureEach {
        languageSettings.optIn("kiscord.api.KiscordUnstableAPI")
    }

    jvm {
        withJava()
    }
    js {
        nodejs()
        if (kiscordBrowser) browser()
    }

    // Linux
    linuxX64()

    // Windows
    mingwX64()

    // Darwin
    iosArm64()
    iosX64()
    macosArm64()
    macosX64()

    sourceSets.commonMain.configure { kotlin.srcDir(files(codegenOutput.get()).builtBy(generateKotlin)) }
}

dependencies {
    commonMainApi(project(":kiscord-gateway"))
}

apply(from = rootProject.file("gradle/java9-module-info.gradle.kts"))
