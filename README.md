# Kiscord

[![License](https://img.shields.io/gitlab/license/kiscord%2Fkiscord?label=License&color=blue)][LICENSE]
[![Kotlin](https://img.shields.io/badge/Kotlin-1.9.22-blue.svg?logo=kotlin)](http://kotlinlang.org)

![Latest stable](https://img.shields.io/gitlab/v/release/kiscord%2Fkiscord?sort=semver&label=Latest%20stable&color=green)
![Latest development](https://img.shields.io/gitlab/v/release/kiscord%2Fkiscord?label=Latest%20development&color=red)

Kiscord is Kotlin-pure multiplatform library for Discord services, including REST API, Gateway and Interactions.

```kotlin
Kiscord {
    token(Token.bot("YOUR_BOT_TOKEN"))

    events {
        onMessageCreate { message ->
            println("Got message: ${message.content}")

            message.reply {
                content = "Kotlin Power!"
            }
        }
    }
}.connect(join = true)
```

## Modules

* [core](https://kiscord.aur.rocks/docs/kiscord-core/index.html) &mdash; Core library of Kiscord, provides APIs for REST services of Discord.
* [gateway](https://kiscord.aur.rocks/docs/kiscord-gateway/index.html) &mdash; Gateway module for [core](kiscord-core/README.md).
  * [gateway-events](https://kiscord.aur.rocks/docs/kiscord-gateway/kiscord-gateway-events/index.html) &mdash; Event bus for [gateway](kiscord-gateway/README.md).
  * [gateway-interactions](https://kiscord.aur.rocks/docs/kiscord-gateway/kiscord-gateway-interactions/index.html) &mdash; Gateway-based interaction app bindings.
* [interactions](https://kiscord.aur.rocks/docs/kiscord-interactions/index.html) &mdash; Building blocks for interaction-based app.
  * [interactions-server](https://kiscord.aur.rocks/docs/kiscord-interactions/kiscord-interactions-server/index.html) &mdash; [Ktor] server plugin to run interaction-based app as Ktor server with webhooks.
* [scripting](https://kiscord.aur.rocks/docs/kiscord-scripting/index.html) &mdash; APIs to run kiscord.kts Kotlin scripts.
* [scripting-runner](https://kiscord.aur.rocks/docs/kiscord-scripting-runner/index.html) &mdash; JVM runner for kiscord.kts Kotlin scripts.

[LICENSE]: https://gitlab.com/kiscord/kiscord/-/blob/HEAD/COPYING.LESSER
[Ktor]: https://ktor.io
