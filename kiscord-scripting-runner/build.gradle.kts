import com.github.jengelman.gradle.plugins.shadow.tasks.*

plugins {
    alias(libs.plugins.kotlin.jvm)
    alias(libs.plugins.shadow)
    alias(libs.plugins.dokka)
    alias(libs.plugins.licenser)
    `maven-publish`
    signing
}

apply(from = rootProject.file("gradle/multiplatform-bom.gradle.kts"))

val bootLoader by configurations.creating {}

dependencies {
    api(project(":kiscord-scripting"))
    api(libs.kotlin.stdlib)
    api(libs.clikt)

    api(libs.kotlin.stdlib.jdk8)
    api(libs.kotlin.reflect)
    implementation(libs.kotlin.scripting.jvm.host)

    runtimeOnly(libs.ktor.client.cio)
    runtimeOnly(libs.logback.classic)

    bootLoader(libs.spring.boot.loader)
}

tasks.named<Jar>("jar") {
    manifest {
        attributes(
            "Main-Class" to "kiscord.scripting.runner.KiscordScriptingRunner",
        )
    }
}

val bootJar by tasks.register<Jar>("bootJar") {
    archiveClassifier = "boot"
    entryCompression = ZipEntryCompression.STORED
    duplicatesStrategy = DuplicatesStrategy.FAIL
    into("BOOT-INF/lib") {
        from(configurations.named("runtimeClasspath"))
        from(tasks.named("jar"))
    }
    from({ bootLoader.map { zipTree(it) } })
    manifest {
        attributes(
            "Main-Class" to "org.springframework.boot.loader.JarLauncher",
            "Start-Class" to "kiscord.scripting.runner.KiscordScriptingRunner",
        )
    }
}

val shadowJar by tasks.named<ShadowJar>("shadowJar") {
    archiveClassifier = "shadow"
    duplicatesStrategy = DuplicatesStrategy.FAIL
    mergeServiceFiles()
    configurations = listOf(project.configurations["runtimeClasspath"])
    from(tasks.named("jar"))
    manifest {
        attributes(
            "Main-Class" to "kiscord.scripting.runner.KiscordScriptingRunner",
        )
    }
}

publishing {
    publications.register<MavenPublication>("jvm") {
        from(components["kotlin"])
        artifact(bootJar)
        artifact(shadowJar)
    }
}
