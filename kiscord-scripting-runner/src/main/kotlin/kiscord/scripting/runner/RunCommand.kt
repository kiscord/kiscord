/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.scripting.runner

import com.github.ajalt.clikt.core.*
import com.github.ajalt.clikt.parameters.arguments.*
import com.github.ajalt.clikt.parameters.options.*
import com.github.ajalt.clikt.parameters.types.*
import kiscord.*
import kiscord.api.*
import kiscord.scripting.*
import java.nio.file.*
import kotlin.script.experimental.api.*
import kotlin.script.experimental.host.*
import kotlin.script.experimental.jvmhost.*
import kotlin.system.*

internal class RunCommand : CliktCommand() {
    private val token by option("-t", "--token", help = "Discord token")
    private val tokenType by option(help = "Token type").enum<Token.Type>().default(Token.Type.Bot)
    private val wait by option("-w", "--wait", help = "Wait for pipeline to execute").flag(default = true)
    private val script by option(
        "-s",
        "--script",
        help = "Kiscord script file to evaluate, omit or use '-' to read standard input"
    )
        .default("-")

    private val arguments by argument(help = "Arguments for script").multiple()

    override fun run() {
        val scriptSource: SourceCode = when (script) {
            "-" -> {
                val stdin = System.`in`.reader().use { it.readText() }
                StringScriptSource(stdin, "STDIN")
            }

            else -> {
                val path = Paths.get(script)
                if (!Files.isReadable(path)) {
                    System.err.println("Script file $path missing or unreadable")
                    exitProcess(1)
                }
                FileScriptSource(path.toFile())
            }
        }

        val scope = DefaultKiscordScriptScope(arguments.toTypedArray())
        token?.let { token ->
            scope.token(
                when (val t = tokenType) {
                    Token.Type.Bot -> Token.bot(token)
                    Token.Type.Bearer -> Token.bearer(token)
                    Token.Type.Raw -> Token.raw(token)
                    else -> throw IllegalArgumentException("Not supported token type: $t")
                }
            )
        }

        val scriptingHost = BasicJvmScriptingHost()

        when (val result = scriptingHost.evalWithTemplate<KiscordScript>(scriptSource, evaluation = {
            constructorArgs(scope)
        })) {
            is ResultWithDiagnostics.Success -> {
                if (!scope.isConnected) {
                    scope.connect(join = wait)
                }
            }

            else -> {
                System.err.println("Failed to execute kiscord script")
                result.reports.forEach {
                    System.err.println(it.render(withStackTrace = true))
                }
                exitProcess(1)
            }
        }
    }
}
