// noinspection JSUnnecessarySemicolon
;(function(config) {
    const webpack = require('webpack');
    config.plugins.push(new webpack.EnvironmentPlugin({
        'DEFAULT_LOGGER_LEVEL': 'NONE',
        'NODE_DEBUG': process.env.NODE_ENV === 'development',
    }));
})(config);

