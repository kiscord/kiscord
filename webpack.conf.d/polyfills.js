// noinspection JSUnnecessarySemicolon
;(function(config) {
    config.resolve.fallback = {
        "os": require.resolve("os-browserify/browser"),
        "zlib": false,
    };
})(config);

