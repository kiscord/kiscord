openapi: 3.0.3
info:
  title: Discord API
  description: Kiscord OpenAPI specification for Discord
  termsOfService: https://discord.com/terms
  contact:
    name: Sergey Shatunov
    email: me@aur.rocks
  license:
    name: MIT License
    url: https://gitlab.com/kiscord/kiscord/-/blob/develop/docs/openapi/LICENSE.md
  version: 9.0-SNAPSHOT
externalDocs:
  description: Discord API Reference
  url: https://discord.com/developers/docs
servers:
  - url: https://discord.com/api/v{api_version}
    description: The production Discord API server
    variables:
      api_version:
        description: API version to use
        default: "10"
security:
  - bearer: [ ]
  - token: [ ]
  - oauth2: [ ]
tags:
  - name: AuditLog
    description: Audit log operations
    externalDocs:
      description: Discord API Reference
      url: https://discord.com/developers/docs/resources/audit-log
    x-kiscord-codegen-package: kiscord.api
  - name: AutoModeration
    description: Auto moderation operations
    externalDocs:
      description: Discord API Reference
      url: https://discord.com/developers/docs/resources/auto-moderation
    x-kiscord-codegen-package: kiscord.api
  - name: Channel
    description: Channel operations
    externalDocs:
      description: Discord API Reference
      url: https://discord.com/developers/docs/resources/channel
    x-kiscord-codegen-package: kiscord.api
  - name: Command
    description: Commands operations
    externalDocs:
      description: Discord API Reference
      url: https://discord.com/developers/docs/interactions/application-commands
    x-kiscord-codegen-package: kiscord.api
  - name: Emoji
    description: Emoji operations
    externalDocs:
      description: Discord API Reference
      url: https://discord.com/developers/docs/resources/emoji
    x-kiscord-codegen-package: kiscord.api
  - name: Gateway
    description: Gateway operations
    externalDocs:
      description: Discord API Reference
      url: https://discord.com/developers/docs/topics/gateway#get-gateway
    x-kiscord-codegen-package: kiscord.api
  - name: Guild
    description: Guild operations
    externalDocs:
      description: Discord API Reference
      url: https://discord.com/developers/docs/resources/guild
    x-kiscord-codegen-package: kiscord.api
  - name: GuildScheduledEvent
    description: Guild scheduled event operations
    externalDocs:
      description: Discord API Reference
      url: https://discord.com/developers/docs/resources/guild-scheduled-event
    x-kiscord-codegen-package: kiscord.api
  - name: GuildTemplate
    description: Guild template operations
    externalDocs:
      description: Discord API Reference
      url: https://discord.com/developers/docs/resources/guild-template
    x-kiscord-codegen-package: kiscord.api
  - name: Invite
    description: Invite operations
    externalDocs:
      description: Discord API Reference
      url: https://discord.com/developers/docs/resources/invite
    x-kiscord-codegen-package: kiscord.api
  - name: OAuth2
    description: OAuth2 operations
    externalDocs:
      description: Discord API Reference
      url: https://discord.com/developers/docs/topics/oauth2
    x-kiscord-codegen-package: kiscord.api
  - name: Stage
    description: Stage instances operations
    externalDocs:
      description: Discord API Reference
      url: https://discord.com/developers/docs/resources/stage-instance
    x-kiscord-codegen-package: kiscord.api
  - name: Sticker
    description: Sticker operations
    externalDocs:
      description: Discord API Reference
      url: https://discord.com/developers/docs/resources/sticker
    x-kiscord-codegen-package: kiscord.api
  - name: User
    description: Users operations
    externalDocs:
      description: Discord API Reference
      url: https://discord.com/developers/docs/resources/user
    x-kiscord-codegen-package: kiscord.api
  - name: Voice
    description: Voice operations
    externalDocs:
      description: Discord API Reference
      url: https://discord.com/developers/docs/resources/voice
    x-kiscord-codegen-package: kiscord.api
  - name: Webhook
    description: Webhooks operations
    externalDocs:
      description: Discord API Reference
      url: https://discord.com/developers/docs/resources/webhook
    x-kiscord-codegen-package: kiscord.api
paths:
  /applications/{application.id}/commands:
    $ref: ./components/resources/applications-(application-id)-commands.yaml
  /applications/{application.id}/role-connections/metadata:
    $ref: ./components/resources/applications-(application-id)-role-connections-metadata.yaml
  /applications/{application.id}/commands/{command.id}:
    $ref: ./components/resources/applications-(application-id)-commands-(command-id).yaml
  /applications/{application.id}/guilds/{guild.id}/commands:
    $ref: ./components/resources/applications-(application-id)-guilds-(guild-id)-commands.yaml
  /applications/{application.id}/guilds/{guild.id}/commands/permissions:
    $ref: ./components/resources/applications-(application-id)-guilds-(guild-id)-commands-permissions.yaml
  /applications/{application.id}/guilds/{guild.id}/commands/{command.id}:
    $ref: ./components/resources/applications-(application-id)-guilds-(guild-id)-commands-(command-id).yaml
  /applications/{application.id}/guilds/{guild.id}/commands/{command.id}/permissions:
    $ref: ./components/resources/applications-(application-id)-guilds-(guild-id)-commands-(command-id)-permissions.yaml
  /channels/{channel.id}:
    $ref: ./components/resources/channels-(channel-id).yaml
  /channels/{channel.id}/followers:
    $ref: ./components/resources/channels-(channel-id)-followers.yaml
  /channels/{channel.id}/invites:
    $ref: ./components/resources/channels-(channel-id)-invites.yaml
  /channels/{channel.id}/messages:
    $ref: ./components/resources/channels-(channel-id)-messages.yaml
  /channels/{channel.id}/messages/bulk-delete:
    $ref: ./components/resources/channels-(channel-id)-messages-bulk-delete.yaml
  /channels/{channel.id}/messages/{message.id}:
    $ref: ./components/resources/channels-(channel-id)-messages-(message-id).yaml
  /channels/{channel.id}/messages/{message.id}/crosspost:
    $ref: ./components/resources/channels-(channel-id)-messages-(message-id)-crosspost.yaml
  /channels/{channel.id}/messages/{message.id}/reactions:
    $ref: ./components/resources/channels-(channel-id)-messages-(message-id)-reactions.yaml
  /channels/{channel.id}/messages/{message.id}/reactions/{emoji}:
    $ref: ./components/resources/channels-(channel-id)-messages-(message-id)-reactions-(emoji).yaml
  /channels/{channel.id}/messages/{message.id}/reactions/{emoji}/@me:
    $ref: ./components/resources/channels-(channel-id)-messages-(message-id)-reactions-(emoji)-@me.yaml
  /channels/{channel.id}/messages/{message.id}/reactions/{emoji}/{user.id}:
    $ref: ./components/resources/channels-(channel-id)-messages-(message-id)-reactions-(emoji)-(user-id).yaml
  /channels/{channel.id}/messages/{message.id}/threads:
    $ref: ./components/resources/channels-(channel-id)-messages-(message-id)-threads.yaml
  /channels/{channel.id}/permissions/{overwrite.id}:
    $ref: ./components/resources/channels-(channel-id)-permissions-(overwrite-id).yaml
  /channels/{channel.id}/pins:
    $ref: ./components/resources/channels-(channel-id)-pins.yaml
  /channels/{channel.id}/pins/{message.id}:
    $ref: ./components/resources/channels-(channel-id)-pins-(message-id).yaml
  /channels/{channel.id}/recipients/{user.id}:
    $ref: ./components/resources/channels-(channel-id)-recipients-(user-id).yaml
  /channels/{channel.id}/thread-members:
    $ref: ./components/resources/channels-(channel-id)-thread-members.yaml
  /channels/{channel.id}/thread-members/@me:
    $ref: ./components/resources/channels-(channel-id)-thread-members-@me.yaml
  /channels/{channel.id}/thread-members/{user.id}:
    $ref: ./components/resources/channels-(channel-id)-thread-members-(user-id).yaml
  /channels/{channel.id}/threads:
    $ref: ./components/resources/channels-(channel-id)-threads.yaml
  /channels/{channel.id}/threads/archived/private:
    $ref: ./components/resources/channels-(channel-id)-threads-archived-private.yaml
  /channels/{channel.id}/threads/archived/public:
    $ref: ./components/resources/channels-(channel-id)-threads-archived-public.yaml
  /channels/{channel.id}/typing:
    $ref: ./components/resources/channels-(channel-id)-typing.yaml
  /channels/{channel.id}/users/@me/threads/archived/private:
    $ref: ./components/resources/channels-(channel-id)-users-@me-threads-archived-private.yaml
  /channels/{channel.id}/webhooks:
    $ref: ./components/resources/channels-(channel-id)-webhooks.yaml
  /gateway:
    $ref: ./components/resources/gateway.yaml
  /gateway/bot:
    $ref: ./components/resources/gateway-bot.yaml
  /guilds:
    $ref: ./components/resources/guilds.yaml
  /guilds/{guild.id}:
    $ref: ./components/resources/guilds-(guild-id).yaml
  /guilds/{guild.id}/audit-logs:
    $ref: ./components/resources/guilds-(guild-id)-audit-logs.yaml
  /guilds/{guild.id}/auto-moderation/rules:
    $ref: ./components/resources/guilds-(guild-id)-auto-moderation-rules.yaml
  /guilds/{guild.id}/auto-moderation/rules/{auto_moderation_rule.id}:
    $ref: ./components/resources/guilds-(guild-id)-auto-moderation-rules-(auto-moderation-rule-id).yaml
  /guilds/{guild.id}/bans:
    $ref: ./components/resources/guilds-(guild-id)-bans.yaml
  /guilds/{guild.id}/bans/{user.id}:
    $ref: ./components/resources/guilds-(guild-id)-bans-(user-id).yaml
  /guilds/{guild.id}/channels:
    $ref: ./components/resources/guilds-(guild-id)-channels.yaml
  /guilds/{guild.id}/emojis:
    $ref: ./components/resources/guilds-(guild-id)-emojis.yaml
  /guilds/{guild.id}/emojis/{emoji.id}:
    $ref: ./components/resources/guilds-(guild-id)-emojis-(emoji-id).yaml
  /guilds/{guild.id}/integrations:
    $ref: ./components/resources/guilds-(guild-id)-integrations.yaml
  /guilds/{guild.id}/integrations/{integration.id}:
    $ref: ./components/resources/guilds-(guild-id)-integrations-(integration-id).yaml
  /guilds/{guild.id}/invites:
    $ref: ./components/resources/guilds-(guild-id)-invites.yaml
  /guilds/{guild.id}/members:
    $ref: ./components/resources/guilds-(guild-id)-members.yaml
  /guilds/{guild.id}/members/@me:
    $ref: ./components/resources/guilds-(guild-id)-members-@me.yaml
  /guilds/{guild.id}/members/search:
    $ref: ./components/resources/guilds-(guild-id)-members-search.yaml
  /guilds/{guild.id}/mfa:
    $ref: ./components/resources/guilds-(guild-id)-mfa.yaml
  /guilds/{guild.id}/preview:
    $ref: ./components/resources/guilds-(guild-id)-preview.yaml
  /guilds/{guild.id}/prune:
    $ref: ./components/resources/guilds-(guild-id)-prune.yaml
  /guilds/{guild.id}/regions:
    $ref: ./components/resources/guilds-(guild-id)-regions.yaml
  /guilds/{guild.id}/roles:
    $ref: ./components/resources/guilds-(guild-id)-roles.yaml
  /guilds/{guild.id}/roles/{role.id}:
    $ref: ./components/resources/guilds-(guild-id)-roles-(role-id).yaml
  /guilds/{guild.id}/scheduled-events:
    $ref: ./components/resources/guilds-(guild-id)-scheduled-events.yaml
  /guilds/{guild.id}/scheduled-events/{guild_scheduled_event.id}:
    $ref: ./components/resources/guilds-(guild-id)-scheduled-events-(guild-scheduled-event-id).yaml
  /guilds/{guild.id}/scheduled-events/{guild_scheduled_event.id}/users:
    $ref: ./components/resources/guilds-(guild-id)-scheduled-events-(guild-scheduled-event-id)-users.yaml
  /guilds/{guild.id}/stickers:
    $ref: ./components/resources/guilds-(guild-id)-stickers.yaml
  /guilds/{guild.id}/stickers/{sticker.id}:
    $ref: ./components/resources/guilds-(guild-id)-stickers-(sticker-id).yaml
  /guilds/{guild.id}/templates:
    $ref: ./components/resources/guilds-(guild-id)-templates.yaml
  /guilds/{guild.id}/templates/{template.code}:
    $ref: ./components/resources/guilds-(guild-id)-templates-(template-code).yaml
  /guilds/{guild.id}/threads/active:
    $ref: ./components/resources/guilds-(guild-id)-threads-active.yaml
  /guilds/{guild.id}/vanity-url:
    $ref: ./components/resources/guilds-(guild-id)-vanity-url.yaml
  /guilds/{guild.id}/voice-states/{user.id}:
    $ref: ./components/resources/guilds-(guild-id)-voice-states-(user-id).yaml
  /guilds/{guild.id}/voice-states/@me:
    $ref: ./components/resources/guilds-(guild-id)-voice-states-@me.yaml
  /guilds/{guild.id}/members/{user.id}:
    $ref: ./components/resources/guilds-(guild-id)-members-(user-id).yaml
  /guilds/{guild.id}/members/{user.id}/roles/{role.id}:
    $ref: ./components/resources/guilds-(guild-id)-members-(user-id)-roles-(role-id).yaml
  /guilds/{guild.id}/webhooks:
    $ref: ./components/resources/guilds-(guild-id)-webhooks.yaml
  /guilds/{guild.id}/welcome-screen:
    $ref: ./components/resources/guilds-(guild-id)-welcome-screen.yaml
  /guilds/{guild.id}/widget:
    $ref: ./components/resources/guilds-(guild-id)-widget.yaml
  /guilds/{guild.id}/widget.json:
    $ref: ./components/resources/guilds-(guild-id)-widget-json.yaml
  /guilds/{guild.id}/widget.png:
    $ref: ./components/resources/guilds-(guild-id)-widget-png.yaml
  /guilds/templates/{template.code}:
    $ref: ./components/resources/guilds-templates-(template-code).yaml
  /interactions/{interaction.id}/{interaction.token}/callback:
    $ref: ./components/resources/interactions-(interaction-id)-(interaction-token)-callback.yaml
  /invites/{invite.code}:
    $ref: ./components/resources/invites-(invite-code).yaml
  /oauth2/@me:
    $ref: ./components/resources/oauth2-@me.yaml
  /oauth2/applications/@me:
    $ref: ./components/resources/oauth2-applications-@me.yaml
  /stage-instances:
    $ref: ./components/resources/stage-instances.yaml
  /stage-instances/{channel.id}:
    $ref: ./components/resources/stage-instances-(channel-id).yaml
  /sticker-packs:
    $ref: ./components/resources/sticker-packs.yaml
  /stickers/{sticker.id}:
    $ref: ./components/resources/stickers-(sticker-id).yaml
  /users/@me:
    $ref: ./components/resources/users-@me.yaml
  /users/@me/applications/{application.id}/role-connection:
    $ref: ./components/resources/users-@me-application-(application-id)-role-connection.yaml
  /users/@me/channels:
    $ref: ./components/resources/users-@me-channels.yaml
  /users/@me/connections:
    $ref: ./components/resources/users-@me-connections.yaml
  /users/@me/guilds:
    $ref: ./components/resources/users-@me-guilds.yaml
  /users/@me/guilds/{guild.id}:
    $ref: ./components/resources/users-@me-guilds-(guild-id).yaml
  /users/@me/guilds/{guild.id}/member:
    $ref: ./components/resources/users-@me-guilds-(guild-id)-member.yaml
  /users/{user.id}:
    $ref: ./components/resources/users-(user-id).yaml
  /voice/regions:
    $ref: ./components/resources/voice-regions.yaml
  /webhooks/{application.id}/{interaction.token}:
    $ref: ./components/resources/webhooks-(application-id)-(interaction-token).yaml
  /webhooks/{application.id}/{interaction.token}/messages/{message.id}:
    $ref: ./components/resources/webhooks-(application-id)-(interaction-token)-messages-(message-id).yaml
  /webhooks/{application.id}/{interaction.token}/messages/@original:
    $ref: ./components/resources/webhooks-(application-id)-(interaction-token)-messages-@original.yaml
  /webhooks/{webhook.id}:
    $ref: ./components/resources/webhooks-(webhook-id).yaml
  /webhooks/{webhook.id}/{webhook.token}:
    $ref: ./components/resources/webhooks-(webhook-id)-(webhook-token).yaml
  /webhooks/{webhook.id}/{webhook.token}/messages/{message.id}:
    $ref: ./components/resources/webhooks-(webhook-id)-(webhook-token)-messages-(message-id).yaml
components:
  schemas:
    gateway-payload:
      $ref: ./components/schemas/gateway/gateway-payload.yaml
    gateway-opcode:
      $ref: ./components/schemas/gateway/gateway-opcode.yaml
    gateway-event-name:
      $ref: ./components/schemas/gateway/gateway-event-name.yaml
  securitySchemes:
    bearer:
      type: http
      scheme: bearer
    token:
      type: apiKey
      name: Authorization
      in: header
    oauth2:
      type: oauth2
      x-client-id: "885893156929933342"
      x-client-secret: "PzyadDFvH282RyNJqmrsr_WGoXdnCIRK"
      flows:
        implicit:
          authorizationUrl: https://discord.com/api/oauth2/authorize
          tokenUrl: https://discord.com/api/oauth2/token
          scopes:
            activities.read: allows your app to fetch data from a user's "Now Playing/Recently Played" list - requires Discord approval
            activities.write: allows your app to update a user's activity - requires Discord approval
            applications.builds.read: allows your app to read build data for a user's applications
            applications.builds.upload: allows your app to upload/update builds for a user's applications - requires Discord approval
            applications.commands: allows your app to use Slash Commands in a guild
            applications.entitlements: allows your app to read entitlements for a user's applications
            applications.store.update: allows your app to read and update store data (SKUs, store listings, achievements, etc.) for a user's applications
            bot: for oauth2 bots, this puts the bot in the user's selected guild by default
            connections: allows /users/@me/connections to return linked third-party accounts
            email: enables /users/@me to return an email
            gdm.join: allows your app to join users to a group dm
            guilds: allows /users/@me/guilds to return basic information about all of a user's guilds
            guilds.join: allows /guilds/{guild.id}/members/{user.id} to be used for joining users to a guild
            guilds.members.read: allows /users/@me/guilds/{guild.id}/member to return a user's member information in a guild
            identify: allows /users/@me without email
            messages.read: for local rpc server api access, this allows you to read messages from all client channels (otherwise restricted to channels/guilds your app creates)
            relationships.read: allows your app to know a user's friends and implicit relationships - requires Discord approval
            role_connections.write: allows your app to update a user's connection and metadata for the app
            rpc: for local rpc server access, this allows you to control a user's local Discord client - requires Discord approval
            rpc.activities.write: for local rpc server access, this allows you to update a user's activity - requires Discord approval
            rpc.notifications.read: for local rpc server access, this allows you to receive notifications pushed out to the user - requires Discord approval
            rpc.voice.read: for local rpc server access, this allows you to read a user's voice settings and listen for voice events - requires Discord approval
            rpc.voice.write: for local rpc server access, this allows you to update a user's voice settings - requires Discord approval
            webhook.incoming: this generates a webhook that is returned in the oauth token response for authorization code grants
        clientCredentials:
          authorizationUrl: https://discord.com/api/oauth2/authorize
          tokenUrl: https://discord.com/api/oauth2/token
          scopes:
            activities.read: allows your app to fetch data from a user's "Now Playing/Recently Played" list - requires Discord approval
            activities.write: allows your app to update a user's activity - requires Discord approval
            applications.builds.read: allows your app to read build data for a user's applications
            applications.builds.upload: allows your app to upload/update builds for a user's applications - requires Discord approval
            applications.commands: allows your app to use Slash Commands in a guild
            applications.commands.update: allows your app to update its Slash Commands via this bearer token
            applications.entitlements: allows your app to read entitlements for a user's applications
            applications.store.update: allows your app to read and update store data (SKUs, store listings, achievements, etc.) for a user's applications
            bot: for oauth2 bots, this puts the bot in the user's selected guild by default
            connections: allows /users/@me/connections to return linked third-party accounts
            email: enables /users/@me to return an email
            gdm.join: allows your app to join users to a group dm
            guilds: allows /users/@me/guilds to return basic information about all of a user's guilds
            guilds.join: allows /guilds/{guild.id}/members/{user.id} to be used for joining users to a guild
            guilds.members.read: allows /users/@me/guilds/{guild.id}/member to return a user's member information in a guild
            identify: allows /users/@me without email
            messages.read: for local rpc server api access, this allows you to read messages from all client channels (otherwise restricted to channels/guilds your app creates)
            relationships.read: allows your app to know a user's friends and implicit relationships - requires Discord approval
            role_connections.write: allows your app to update a user's connection and metadata for the app
            rpc: for local rpc server access, this allows you to control a user's local Discord client - requires Discord approval
            rpc.activities.write: for local rpc server access, this allows you to update a user's activity - requires Discord approval
            rpc.notifications.read: for local rpc server access, this allows you to receive notifications pushed out to the user - requires Discord approval
            rpc.voice.read: for local rpc server access, this allows you to read a user's voice settings and listen for voice events - requires Discord approval
            rpc.voice.write: for local rpc server access, this allows you to update a user's voice settings - requires Discord approval
            webhook.incoming: this generates a webhook that is returned in the oauth token response for authorization code grants
        authorizationCode:
          authorizationUrl: https://discord.com/api/oauth2/authorize
          tokenUrl: https://discord.com/api/oauth2/token
          scopes:
            activities.read: allows your app to fetch data from a user's "Now Playing/Recently Played" list - requires Discord approval
            activities.write: allows your app to update a user's activity - requires Discord approval
            applications.builds.read: allows your app to read build data for a user's applications
            applications.builds.upload: allows your app to upload/update builds for a user's applications - requires Discord approval
            applications.commands: allows your app to use Slash Commands in a guild
            applications.entitlements: allows your app to read entitlements for a user's applications
            applications.store.update: allows your app to read and update store data (SKUs, store listings, achievements, etc.) for a user's applications
            bot: for oauth2 bots, this puts the bot in the user's selected guild by default
            connections: allows /users/@me/connections to return linked third-party accounts
            email: enables /users/@me to return an email
            gdm.join: allows your app to join users to a group dm
            guilds: allows /users/@me/guilds to return basic information about all of a user's guilds
            guilds.join: allows /guilds/{guild.id}/members/{user.id} to be used for joining users to a guild
            guilds.members.read: allows /users/@me/guilds/{guild.id}/member to return a user's member information in a guild
            identify: allows /users/@me without email
            messages.read: for local rpc server api access, this allows you to read messages from all client channels (otherwise restricted to channels/guilds your app creates)
            relationships.read: allows your app to know a user's friends and implicit relationships - requires Discord approval
            role_connections.write: allows your app to update a user's connection and metadata for the app
            rpc: for local rpc server access, this allows you to control a user's local Discord client - requires Discord approval
            rpc.activities.write: for local rpc server access, this allows you to update a user's activity - requires Discord approval
            rpc.notifications.read: for local rpc server access, this allows you to receive notifications pushed out to the user - requires Discord approval
            rpc.voice.read: for local rpc server access, this allows you to read a user's voice settings and listen for voice events - requires Discord approval
            rpc.voice.write: for local rpc server access, this allows you to update a user's voice settings - requires Discord approval
            webhook.incoming: this generates a webhook that is returned in the oauth token response for authorization code grants
