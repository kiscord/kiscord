get:
  tags:
    - Sticker
  summary: List Guild Stickers
  description: Returns an array of sticker objects for the given guild. Includes user
    fields if the bot has the MANAGE_EMOJIS_AND_STICKERS permission.
  externalDocs:
    description: Discord API Reference
    url: https://discord.com/developers/docs/resources/sticker#list-guild-stickers
  operationId: list-guild-stickers
  responses:
    "200":
      $ref: ../responses/stickers.yaml
post:
  tags:
    - Sticker
  summary: Create Guild Sticker
  description: Create a new sticker for the guild. Requires the MANAGE_EMOJIS_AND_STICKERS
    permission. Returns the new sticker object on success.
  externalDocs:
    description: Discord API Reference
    url: https://discord.com/developers/docs/resources/sticker#create-guild-sticker
  operationId: create-guild-sticker
  parameters:
    - $ref: ../parameters/audit-log-reason.yaml
  requestBody:
    content:
      multipart/form-data:
        schema:
          title: StickerApi.CreateGuildSticker
          required:
            - description
            - file
            - name
            - tags
          type: object
          properties:
            name:
              maxLength: 30
              minLength: 2
              type: string
              description: name of the sticker
            description:
              maxLength: 100
              type: string
              description: description of the sticker
            tags:
              maxLength: 200
              minLength: 2
              type: string
              description: the Discord name of a unicode emoji representing the sticker's
                expression
            file:
              description: "the sticker file to upload, must be a PNG, APNG, or Lottie\
                \ JSON file, max 500 KB"
              oneOf:
                - $ref: ../schemas/file-data.yaml
          x-kiscord-codegen-generate-builder: true
          x-kiscord-codegen-package: kiscord.api
    required: true
  responses:
    "200":
      $ref: ../responses/sticker.yaml
parameters:
  - $ref: ../parameters/guild-id.yaml
