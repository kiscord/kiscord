OpenAPI Discord definition
==========================

`openapi.yaml` is main key for kiscord to communicate with rest and gateway of Discord.
`kiscord-codegen` use this to generate DTO classes and rest wrappers.

See [codegen](../../kiscord-codegen) module for the list of vendor extensions used by kiscord.
