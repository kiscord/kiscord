@file:OptIn(ExperimentalKotlinGradlePluginApi::class)

import dev.yumi.gradle.licenser.*
import dev.yumi.gradle.licenser.task.*
import kotlinx.atomicfu.plugin.gradle.*
import org.gradle.api.internal.tasks.*
import org.gradle.configurationcache.extensions.*
import org.gradle.nativeplatform.platform.internal.*
import org.jetbrains.dokka.base.*
import org.jetbrains.dokka.gradle.*
import org.jetbrains.dokka.versioning.*
import org.jetbrains.kotlin.gradle.*
import org.jetbrains.kotlin.gradle.dsl.*
import org.jetbrains.kotlin.gradle.plugin.*
import org.jetbrains.kotlin.gradle.plugin.mpp.*
import org.jetbrains.kotlin.gradle.targets.js.dsl.*
import org.jetbrains.kotlin.gradle.targets.js.nodejs.*
import org.jetbrains.kotlin.gradle.targets.js.webpack.*
import org.jetbrains.kotlin.gradle.targets.js.yarn.*
import org.jetbrains.kotlin.gradle.targets.jvm.*
import org.jetbrains.kotlin.gradle.tasks.*
import org.jetbrains.kotlin.konan.target.*
import org.jetbrains.kotlin.konan.target.Architecture
import java.net.*
import java.time.*

buildscript {
    dependencies {
        classpath(libs.bundles.kotlin)
        classpath(libs.bundles.dokka.plugins)
    }
}

plugins {
    base
    `maven-publish`
    `version-catalog`
    signing
    alias(libs.plugins.dokka)
    alias(libs.plugins.kotlin.jvm) apply false
    alias(libs.plugins.kotlin.multiplatform) apply false
    alias(libs.plugins.kotlin.serialization) apply false
    alias(libs.plugins.kotlinx.atomicfu) apply false
    alias(libs.plugins.kotlinx.kover)
    alias(libs.plugins.licenser) apply false
    alias(libs.plugins.shadow) apply false
}

val publishedProjects by ext<DomainObjectCollection<Array<String>>> {
    objects.domainObjectSet(Array<String>::class)
}

val isCI = System.getenv("CI") == "true"

allprojects {
    repositories {
        mavenCentral()
        maven("https://oss.sonatype.org/content/repositories/snapshots") {
            name = "sonatype-snapshots"

            mavenContent {
                snapshotsOnly()
            }
        }
        maven("https://s01.oss.sonatype.org/content/repositories/snapshots") {
            name = "sonatype-snapshots-s01"

            mavenContent {
                snapshotsOnly()
            }
        }
        maven("https://maven.pkg.jetbrains.space/public/p/kotlinx-html/maven") {
            name = "kotlinx.html"

            content {
                includeGroup("org.jetbrains.kotlinx")
            }
        }
        maven("https://maven.pkg.jetbrains.space/public/p/ktor/eap/") {
            name = "ktor-eap"

            content {
                includeGroup("io.ktor")
            }
        }
        maven("https://maven.pkg.jetbrains.space/public/p/kotlinx-coroutines/maven") {
            name = "kotlinx.coroutines"

            content {
                includeGroup("org.jetbrains.kotlinx")
            }
        }
    }

    pluginManager.withPlugin("org.gradle.publishing") {
        val publishing: PublishingExtension by extensions

        val kiscordName: String = project.ext["kiscord.name"].toString()
        val kiscordUrl: String = project.ext["kiscord.url"].toString()

        if (isCI) {
            val apiUrl = System.getenv("CI_API_V4_URL")
            val projectId = System.getenv("CI_PROJECT_ID")
            val jobToken = System.getenv("CI_JOB_TOKEN")

            if (apiUrl == null || projectId == null || jobToken == null) {
                logger.warn("In CI environment, but missing one of: api url, project id, job token. Publishing to gitlab repo disabled.")
            } else publishing.repositories.maven("$apiUrl/projects/$projectId/packages/maven") {
                name = "GitLab"
                credentials(HttpHeaderCredentials::class) {
                    name = "Job-Token"
                    value = jobToken
                }
                authentication {
                    create<HttpHeaderAuthentication>("header")
                }
            }
        }

        publishing.publications.withType<MavenPublication>().configureEach {
            pom {
                name.convention(kiscordName)
                url.convention(kiscordUrl)
                description.convention(project.description)

                licenses {
                    license {
                        name = "GNU Lesser General Public License"
                        url = "https://www.gnu.org/licenses/lgpl-3.0.html"
                        distribution = "repo"
                    }
                }

                scm {
                    connection = "scm:git:https://gitlab.com/kiscord/kiscord.git"
                    developerConnection = "scm:git:git@gitlab.com:kiscord/kiscord.git"
                    url = "https://kiscord.aur.rocks/"
                }

                developers {
                    developer {
                        id = "Prototik"
                        name = "Sergey Shatunov"
                        email = "me@aur.rocks"
                    }
                }
            }
        }

        publishing.publications.withType<MavenPublication>().all {
            val entry = arrayOf(this@allprojects.name, groupId, artifactId, version)
            if (publishedProjects.none { it.contentEquals(entry) }) {
                publishedProjects.add(entry)
            }
        }
    }

    pluginManager.withPlugin("kotlinx-atomicfu") {
        extensions.configure<AtomicFUPluginExtension>("atomicfu") {
            dependenciesVersion = libs.versions.kotlinx.atomicfu.get()
            jvmVariant = "BOTH"
        }
    }

    pluginManager.withPlugin("dev.yumi.gradle.licenser") {
        extensions.configure<YumiLicenserGradleExtension>("license") {
            rule(rootProject.file("gradle/license-header.txt"))
            projectCreationYear = 2019
        }
    }

    tasks.withType<AbstractCopyTask>().configureEach {
            duplicatesStrategy = DuplicatesStrategy.FAIL
    }
}

val kiscordStartYear = ext["kiscord.start.year"] as String

allprojects {
    pluginManager.withPlugin("org.jetbrains.dokka") {
        dependencies {
            dokkaPlugin(libs.bundles.dokka.plugins)
        }

        tasks.withType<AbstractDokkaLeafTask>().configureEach {
            dokkaSourceSets.configureEach {
                jdkVersion = 21
                suppressInheritedMembers = true
                includes.from("README.md")

                externalDocumentationLink("https://api.ktor.io/")
                externalDocumentationLink("https://kotlin.github.io/kotlinx.coroutines/")
                externalDocumentationLink("https://kotlin.github.io/kotlinx.serialization/")
                externalDocumentationLink(
                    "https://www.slf4j.org/apidocs/",
                    "https://www.slf4j.org/apidocs/element-list"
                )

                sourceLink {
                    localDirectory = rootProject.projectDir
                    val ciCommitSha: String? by lazy { System.getenv("CI_COMMIT_SHA") }
                    val gitlabSourceRef = when {
                        isCI && ciCommitSha != null -> ciCommitSha
                        else -> "develop"
                    }
                    remoteUrl = URI("https://gitlab.com/kiscord/kiscord/-/blob/${gitlabSourceRef}").toURL()
                    remoteLineSuffix = "#L"
                }
            }

            pluginConfiguration<VersioningPlugin, VersioningConfiguration> {
                version = project.version.toString()
            }

            pluginConfiguration<DokkaBase, DokkaBaseConfiguration> {
                customAssets = rootProject.fileTree("docs/dokka/assets").toList()
                customStyleSheets = rootProject.fileTree("docs/dokka/styles") {
                    include("*.css")
                }.toList()
                footerMessage = "© ${kiscordStartYear}-${Year.now().value} Kiscord's authors and contributors"
            }

            inputs.dir(rootProject.file("docs/dokka"))
        }
    }
}

val dokkaHtmlMultiModule by tasks.getting(DokkaMultiModuleTask::class) {
    includes.from("README.md")

    pluginConfiguration<VersioningPlugin, VersioningConfiguration> {
        version = project.version.toString()
    }

    pluginConfiguration<DokkaBase, DokkaBaseConfiguration> {
        customAssets = rootProject.fileTree("docs/dokka/assets").toList()
        customStyleSheets = rootProject.fileTree("docs/dokka/styles") {
            include("*.css")
        }.toList()
        footerMessage = "© ${kiscordStartYear}-${Year.now().value} Kiscord's authors and contributors"
    }
}

//knit {
//    siteRoot = "${project.ext["kiscord.url"]}/docs"
//    moduleDocs = "build/dokka/htmlPartial"
//    dokkaMultiModuleRoot = "build/dokka/htmlMultiModule/"
//
//    files = project.fileTree(project.rootDir) {
//        include("**/*.md")
//        include("**/*.kt")
//        include("**/*.kts")
//        exclude("**/build/*")
//        exclude("**/.gradle/*")
//        exclude("kiscord-core/discord-api-docs/**")
//    }
//}

//tasks.knitPrepare {
//    dependsOn(dokkaHtmlMultiModule)
//}

fun KotlinOnlyTarget<out KotlinJvmCompilation>.configureKiscordJvmTarget(project: Project) {
    project.tasks.named("${targetName}Test", Test::class) {
        configureKiscordJvmTestTask()
    }

    compilations.getByName(KotlinCompilation.TEST_COMPILATION_NAME) {
        dependencies {
            api(libs.bundles.kiscord.test.jvm)
        }
    }

    compilations.configureEach {
        compileTaskProvider.configure {
            compilerOptions.configureKiscordJvmOptions(project)
        }
    }
}

@OptIn(ExperimentalKotlinGradlePluginApi::class)
fun KotlinWithJavaTarget<*, out KotlinJvmCompilerOptions>.configureKiscordJvmTarget(project: Project) {
    val test = project.tasks.named("test", Test::class) {
        configureKiscordJvmTestTask()
    }

    project.tasks.register("jvmTest") {
        description = "Alias to test task"
        group = "verification"
        dependsOn(test)
    }

    compilations.getByName(KotlinCompilation.TEST_COMPILATION_NAME) {
        dependencies {
            api(libs.bundles.kiscord.test.jvm)
        }
    }

    compilerOptions.configureKiscordJvmOptions(project)
}

fun Test.configureKiscordJvmTestTask() {
    useJUnitPlatform()
    systemProperty("io.ktor.development", "true")
}

fun <T> T.configureKiscordJsTarget(project: Project) where T : KotlinJsTargetDsl, T : KotlinJsSubTargetContainerDsl {
    useEsModules()

    whenBrowserConfigured {
        val webpackConfigDirectory =
            project.file("webpack.conf.d").takeIf { it.isDirectory } ?: rootProject.file("webpack.conf.d")

        commonWebpackConfig {
            configDirectory = webpackConfigDirectory
            export = false
        }

        runTask {
            inputs.dir(webpackConfigDirectory)
        }

        webpackTask {
            inputs.dir(webpackConfigDirectory)
        }

        testTask {
            useMocha()
        }
    }

    compilations.configureEach {
        compileTaskProvider.configure {
            compilerOptions.configureKiscordJsOptions(project)
        }
    }
}

@OptIn(ExperimentalKotlinGradlePluginApi::class)
fun HasConfigurableKotlinCompilerOptions<out KotlinCommonCompilerOptions>.configureKiscordCommonTarget(project: Project) {
    compilerOptions {
        configureKiscordCommonOptions(project)
    }
}

subprojects {
    plugins.withType(JavaBasePlugin::class) {
        val kiscordJavaTarget =
            JavaVersion.toVersion(JvmTarget.fromTarget(project.ext["kiscord.java.target"] as String).target)
        extensions.configure<JavaPluginExtension>("java") {
            targetCompatibility = kiscordJavaTarget
            sourceCompatibility = kiscordJavaTarget
        }
    }

    plugins.withType(KotlinBasePlugin::class) {
        val kiscordApiVersion = KotlinVersion.fromVersion(ext["kiscord.kotlin.api.version"] as String)
        val kiscordLanguageVersion = KotlinVersion.fromVersion(ext["kiscord.kotlin.language.version"] as String)

        val e = kotlinExtension
        e.configureKiscordExtension()
        e.sourceSets.configureEach {
            languageSettings {
                apiVersion = kiscordApiVersion.version
                languageVersion = kiscordLanguageVersion.version
                progressiveMode = true

                optIn("kotlin.RequiresOptIn")
            }
        }
        when (e) {
            is KotlinMultiplatformExtension -> {
                e.sourceSets.named("commonTest") {
                    dependencies {
                        api(libs.bundles.kiscord.test.common)
                    }
                }
                e.targets.configureEach {
                    when {
                        this is KotlinJvmTarget -> {
                            configureKiscordJvmTarget(this@subprojects)
                        }
                        this is KotlinJsTargetDsl && this is KotlinJsSubTargetContainerDsl ->
                            configureKiscordJsTarget(this@subprojects)
                        this is KotlinNativeTarget -> configureKiscordCommonTarget(this@subprojects)
                        this is KotlinMetadataTarget -> configureKiscordCommonTarget(this@subprojects)
                        else -> println("Unhandled target: $this")
                    }
                }
            }
            is KotlinJvmProjectExtension -> e.target.configureKiscordJvmTarget(this@subprojects)
        }
    }
}

fun KotlinTopLevelExtension.configureKiscordExtension() {
    explicitApi = ExplicitApiMode.Strict
}

fun KotlinCommonCompilerOptions.configureKiscordCommonOptions(project: Project) {
    val kiscordApiVersion = KotlinVersion.fromVersion(project.ext["kiscord.kotlin.api.version"] as String)
    val kiscordLanguageVersion = KotlinVersion.fromVersion(project.ext["kiscord.kotlin.language.version"] as String)

    apiVersion = kiscordApiVersion
    languageVersion = kiscordLanguageVersion
    progressiveMode = true
    freeCompilerArgs.add("-Xexpect-actual-classes")
}

fun KotlinJvmCompilerOptions.configureKiscordJvmOptions(project: Project) {
    configureKiscordCommonOptions(project)

    jvmTarget = JvmTarget.fromTarget(project.ext["kiscord.java.target"] as String)
}

fun KotlinJsCompilerOptions.configureKiscordJsOptions(project: Project) {
    configureKiscordCommonOptions(project)
    moduleKind = JsModuleKind.MODULE_ES
    sourceMap = true
    useEsClasses = true
}

catalog {
    versionCatalog {
        from(files("gradle/libs.versions.toml"))
        val kiscordVersion = project.version.toString()
        version("kiscord", kiscordVersion)
        publishedProjects.all {
            val (name, groupId, artifactId, version) = this
            val alias = library(name, groupId, artifactId)
            if (groupId == project.group) {
                alias.versionRef("kiscord")
            } else {
                alias.version(version)
            }
        }
    }
}

publishing {
    publications {
        register<MavenPublication>("versionCatalog") {
            from(components["versionCatalog"])
        }
    }
}

subprojects {
    tasks.withType<KotlinNativeLink>().configureEach {
        val family = binary.compilation.konanTarget.family
        when {
            family == Family.MINGW -> {
                val bitness = when (val arch = binary.compilation.konanTarget.architecture) {
                    Architecture.X86 -> 32
                    Architecture.X64 -> 64
                    else -> error("Unsupported architecture for mingw target: $arch")
                }
                if (DefaultNativePlatform.getCurrentOperatingSystem().isWindows) {
                    binary.linkerOpts("-LC:/msys64/clang${bitness}/lib", "-LC:/msys64/mingw${bitness}/lib")
                } else {
                    binary.linkerOpts("-L/clang${bitness}/lib", "-L/mingw${bitness}/lib")
                }
                if (isCI) {
                    onlyIf { DefaultNativePlatform.getCurrentOperatingSystem().isWindows }
                }
            }
            family == Family.LINUX -> {
                if (isCI) {
                    onlyIf { DefaultNativePlatform.getCurrentOperatingSystem().isLinux }
                }
            }
            family.isAppleFamily -> {
                if (isCI) {
                    onlyIf { DefaultNativePlatform.getCurrentOperatingSystem().isMacOsX }
                }
            }
            else -> {
                //NOOP
            }
        }
    }
}

subprojects {
    version = rootProject.version
}
