import org.apache.tools.ant.util.ReflectUtil

val ext = extensions.getByName("ext") as org.gradle.api.plugins.ExtraPropertiesExtension

if (ext.has("kiscord.jpms") && ext.get("kiscord.jpms") == "true") {
    val sourceSets = extensions.getByName("sourceSets") as org.gradle.api.tasks.SourceSetContainer
    val javaToolchains = extensions.getByName("javaToolchains") as org.gradle.jvm.toolchain.JavaToolchainService

    val isMpp = project.plugins.hasPlugin("org.jetbrains.kotlin.multiplatform")

    val java9 by sourceSets.registering

    configurations.named("java9CompileClasspath") {
        extendsFrom(configurations["compileClasspath"])
    }

    val moduleName = if (ext.has("kiscord.module.name")) ext["kiscord.module.name"] as String else project.name.replace('-', '.')
    val java9SourceSet = java9.get().java

    tasks.named<JavaCompile>("compileJava9Java") {
        dependsOn(sourceSets["main"].output)

        targetCompatibility = "9"
        sourceCompatibility = "9"

        javaCompiler = javaToolchains.compilerFor {
            languageVersion = JavaLanguageVersion.of(9)
        }

        destinationDirectory = java9SourceSet.destinationDirectory.asFile.get().resolve("META-INF/versions/9")
        options.sourcepath = files(java9SourceSet.srcDirs)
        val compileClasspath = configurations["java9CompileClasspath"]
        val moduleFiles = objects.fileCollection().from(sourceSets["main"].output)
        val modulePath = compileClasspath.filter { it !in moduleFiles.files }
        classpath = objects.fileCollection().from()
        options.compilerArgumentProviders.add(
            CommandLineArgumentProvider {
                listOf(
                    "--module-path", modulePath.asPath,
                    "--patch-module", "$moduleName=${moduleFiles.asPath}"
                )
            }
        )
    }

    tasks.named<JavaCompile>("compileJava") {
        targetCompatibility = "8"
        sourceCompatibility = "8"
    }

    tasks.named<JavaCompile>("compileTestJava") {
        targetCompatibility = "8"
        sourceCompatibility = "8"
    }

    tasks.named("compileJava9KotlinJvm") {
        val kotlinOptions = ReflectUtil.invoke<Any>(this, "getKotlinOptions")
        ReflectUtil.invoke<Any?>(kotlinOptions, "setJvmTarget", String::class.java, "9")
    }

    tasks.named<Jar>(if (isMpp) "jvmJar" else "jar") {
        manifest {
            attributes("Multi-Release" to true)
        }

        from(java9SourceSet)
    }
}
