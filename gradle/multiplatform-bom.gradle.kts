val platforms: Configuration by configurations.creating {
    isCanBeResolved = false
    isCanBeConsumed = false
}

configurations.all {
    if (isCanBeResolved && !name.endsWith("DependenciesMetadata") && !name.startsWith("dokka")) {
        extendsFrom(platforms)
    }
}

dependencies {
    platforms(platform(project(":kiscord-platform")))
}
