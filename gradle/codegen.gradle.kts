val ext = extensions.getByName("ext") as org.gradle.api.plugins.ExtraPropertiesExtension

var codegenOutput: Provider<Directory> by ext
codegenOutput = layout.buildDirectory.dir("generated/sources/codegen")

val codegen: Configuration by configurations.creating

dependencies {
    codegen(project(":kiscord-codegen"))
}

val generateKotlin by tasks.registering(JavaExec::class) {
    group = "build"
    classpath = codegen
    mainClass = "kiscord.codegen.CodegenCommandKt"

    val openapiYaml = rootProject.file("docs/openapi/openapi.yaml")

    args(
        "--input", openapiYaml,
    )

    argumentProviders += CommandLineArgumentProvider {
        listOf(
            "--codegen-output", codegenOutput.get().toString(),
        )
    }


    inputs.file(openapiYaml)
    inputs.dir(openapiYaml.resolve("../components"))

    doFirst {
        if (!codegenOutput.get().asFile.deleteRecursively()) {
            throw GradleException("Unable to clean codegen output directory ($codegenOutput)")
        }
    }
}
