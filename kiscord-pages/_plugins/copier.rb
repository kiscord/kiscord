require 'json'

module Copier
  class Generator < Jekyll::Generator
    priority :lowest
    safe true

    def generate(site)
      @site = site

      config.each do |entry|
        from = entry["from"]
        to = entry["to"]

        @site.static_files << Jekyll::StaticFile.new(site, File.dirname(from), File.dirname(to), File.basename(from))
      end
    end

    def config
      @config ||= @site.config["copier"] || {}
    end
  end
end
