plugins {
    `java-platform`
    `maven-publish`
    signing
}

val publishedProjects: DomainObjectCollection<Array<String>> by rootProject.ext

dependencies {
    constraints {
        publishedProjects.all {
            val (_, groupId, artifactId, version) = this
            api("$groupId:$artifactId:$version")
        }
    }
}

publishing {
    publications {
        register<MavenPublication>("platform") {
            from(components["javaPlatform"])
        }
    }
}
