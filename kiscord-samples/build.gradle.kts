import org.jetbrains.kotlin.gradle.plugin.mpp.*
import org.jetbrains.kotlin.gradle.targets.js.nodejs.*
import org.jetbrains.kotlin.gradle.targets.js.webpack.*
import org.jetbrains.kotlin.gradle.targets.jvm.*

plugins {
    alias(libs.plugins.kotlin.multiplatform)
    alias(libs.plugins.licenser)
    alias(libs.plugins.properties)
}

apply(from = rootProject.file("gradle/multiplatform-bom.gradle.kts"))

val kiscordBrowser = (ext["kiscord.browser"] as String).toBoolean()
val sampleName: String? by ext

kotlin {
    explicitApi = null

    jvm()
    js {
        binaries.executable()
        nodejs()
        if (kiscordBrowser) browser {
            fun KotlinWebpack.configureSample() {
                val token = findToken("kiscord.token.js.browser", "kiscord.token.js", "kiscord.token")
                onlyIf { token != null && sampleName != null }
                args.addAll(listOf("--env", "KISCORD_TOKEN=${token}", "--env", "KISCORD_SAMPLE=$sampleName"))
            }

            runTask { configureSample() }
            webpackTask { configureSample() }
        }
    }

    // Linux
    linuxX64()

    // Windows
    mingwX64()

    // Darwin
    iosArm64()
    iosX64()
    macosArm64()
    macosX64()

    applyDefaultHierarchyTemplate()

    targets.withType<KotlinNativeTarget>().configureEach {
        binaries.executable {
            entryPoint("kiscord.samples.main")
            runTaskProvider?.configure {
                val token = findToken("kiscord.token.native", "kiscord.token")
                onlyIf { token != null && sampleName != null }
                environment("KISCORD_TOKEN" to token)
                sampleName?.let { args(it) }
            }
        }
    }
}

val jvmScripting: Configuration by configurations.creating {}

dependencies {
    commonMainApi(project(":kiscord-core"))
    commonMainApi(project(":kiscord-gateway"))
    commonMainApi(project(":kiscord-gateway:kiscord-gateway-events"))
    commonMainApi(project(":kiscord-gateway:kiscord-gateway-interactions"))

    commonMainImplementation(libs.ktor.client.logging)

    "jvmMainRuntimeOnly"(libs.kotlinx.coroutines.debug)
    "jvmMainRuntimeOnly"(libs.ktor.client.java)
    "jvmMainRuntimeOnly"(libs.slf4j.simple)

    if (kiscordBrowser) {
        "jsMainRuntimeOnly"(devNpm("html-webpack-plugin", "^5.6.0"))
        "jsMainRuntimeOnly"(devNpm("os-browserify", "^0.3.0"))
    }

    "linuxMainImplementation"(libs.ktor.client.cio)
    "appleMainImplementation"(libs.ktor.client.darwin)
    "mingwMainImplementation"(libs.ktor.client.winhttp)

    jvmScripting(project(":kiscord-scripting-runner"))
}

fun findToken(vararg tokenKeys: String): String? {
    tokenKeys.filter { hasProperty(it) }.map { property(it)?.toString() }.firstOrNull()?.let { return it }
    val tokenKey = tokenKeys.find { ext.has(it) } ?: return null
    return ext[tokenKey] as String
}

tasks.withType<NodeJsExec>().configureEach {
    val token = findToken("kiscord.token.js.node", "kiscord.token.js", "kiscord.token")
    onlyIf { token != null && sampleName != null }
    environment("KISCORD_TOKEN" to token)
    sampleName?.let { environment("KISCORD_SAMPLE" to it) }
}

tasks.withType<KotlinWebpack>().configureEach {
    val token = findToken("kiscord.token.js.browser", "kiscord.token.js", "kiscord.token")
    onlyIf { token != null && sampleName != null }
    args += listOf("--env", "KISCORD_TOKEN=${token}")
    sampleName?.let { args += listOf("--env", "KISCORD_SAMPLE=${it}") }
}

tasks.register("runJvm", JavaExec::class) {
    group = "run"
    description = "Runs sample as a JVM application"

    val jvmMain = provider {
        val jvmTarget = kotlin.targets["jvm"] as KotlinJvmTarget
        jvmTarget.compilations["main"] as KotlinJvmCompilation
    }

    classpath(jvmMain.map { it.compileDependencyFiles })
    classpath(jvmMain.map { it.runtimeDependencyFiles })
    classpath(jvmMain.map { it.output.allOutputs })

    mainClass = "kiscord.samples.Main"

    val token = findToken("kiscord.token.jvm", "kiscord.token")
    onlyIf { token != null && sampleName != null }
    environment("KISCORD_TOKEN" to token)
    sampleName?.let { args(it) }
}

tasks.register<JavaExec>("runJvmScripting") {
    group = "run"
    description = "Runs sample as a JVM scripting application"

    classpath(jvmScripting)
    mainClass = "kiscord.scripting.runner.KiscordScriptingRunner"

    val token = findToken("kiscord.token.jvm.scripting", "kiscord.token.jvm", "kiscord.token")
    onlyIf { token != null && sampleName != null }
    args("-t", token ?: "", "-s", file("src/commonMain/sampleRunner.kiscord.kts"))
    sampleName?.let { args(it) }
}
