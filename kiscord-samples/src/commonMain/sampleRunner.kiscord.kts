@file:Import("kotlin/kiscord/samples/Sample.kt", "kotlin/kiscord/samples/*/*.kt")

import kiscord.*
import kiscord.samples.*
import kotlin.system.*
import org.kodein.log.*
import org.kodein.log.frontend.*

Kiscord.DefaultLoggerFactory = LoggerFactory(slf4jFrontend)

if (args.isEmpty()) {
    Sample.logger.error { "You must pass sample name via an argument" }
    exitProcess(1)
}

val sample = Sample.values().singleOrNull { it.argName == args[0] } ?: exitProcess(2)
with(sample) { init() }
