/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.samples.echoBot

import kiscord.*
import kiscord.api.*
import kiscord.api.gateway.*
import kiscord.gateway.*
import kiscord.gateway.events.*
import kotlinx.datetime.*

fun Kiscord.Builder.echoBot() {
    intents(Identify.Intent.ALL - Identify.Intent.GuildPresences)

    presence {
        status(Status.DND)
        since(Clock.System.now())
    }

    gatewayCompression {
        compressor = GatewayCompression.Zlib
    }

    gatewayEvents {
        onMessageCreate(concurrent = true) { message ->
            println("Message created: ${message.author.username}: ${message.content}")
        }

        onMessageUpdate { message ->
            println("Message updated: ${message.author.username}: ${message.content}")
        }

        onMessageDelete { message ->
            println("Message deleted: ${message.author.username}: ${message.content}")
        }

        onGuildRoleCreate { (role, guild) ->
            println("Role created in guild ${guild.name}: $role")
        }

        onGuildRoleUpdate { (role, guild) ->
            println("Role updated in guild ${guild.name}: $role")
        }

        onGuildRoleDelete { (role, guild) ->
            println("Role deleted in guild ${guild.name}: $role")
        }

        onGuildMemberAdd { (member, guild) ->
            println("Guild member added: ${member.user?.username} in guild ${guild.name}")
        }

        onGuildMemberRemove { (member, guild) ->
            println("Guild member removed: ${member.user?.username} in guild ${guild.name}")
        }

        onGuildCreate { guild ->
            println("Guild created: ${guild.name}")
        }
    }
}
