/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.samples

import io.ktor.client.plugins.logging.*
import kiscord.*
import kiscord.api.*
import kiscord.api.gateway.*
import kiscord.gateway.*
import kiscord.gateway.interactions.*
import kiscord.interactions.*
import kiscord.samples.echoBot.*
import org.kodein.log.*
import io.ktor.client.plugins.logging.LogLevel as KtorLogLevel
import io.ktor.client.plugins.logging.Logger as KtorLogger
import io.ktor.client.plugins.logging.Logging as KtorLogging

enum class Sample(val displayName: String, val argName: String, init: Kiscord.Builder.() -> Unit) {
    EchoBot("Echo Bot", "echoBot", Kiscord.Builder::echoBot),
    ;

    val init: Kiscord.Builder.() -> Unit = {
        val logger = loggerFactory.newLogger("kiscord.samples", "Sample")
        logger.info { "Configuring sample $displayName" }

        httpClient {
            KtorLogging {
                this.logger = KtorLogger.DEFAULT
                this.level = KtorLogLevel.INFO
            }
        }

        presence {
            activity {
                playing("${this@Sample.displayName} / ${Kiscord.VERSION}")
            }
        }

        gatewayInteractions {
            command {
                name("info")
                description("Print information about Kiscord build")
                dmPermission(true)
            } handle {
                message {
                    embed {
                        color(0x7289da)

                        title("Kiscord Info Splash")

                        field("Kiscord Version", Kiscord.VERSION)
                        field("Kotlin Platform", Kiscord.KOTLIN_PLATFORM, inline = true)
                        field("Kotlin Compile Version", Kiscord.KOTLIN_VERSION, inline = true)
                        field("Kotlin Runtime Version", KotlinVersion.CURRENT.toString(), inline = true)

                        author {
                            name("Prototik")
                            url(Kiscord.URL)
                        }
                    }
                }
            }
        }

        init()
    }

    companion object {
        val logger = Kiscord.DefaultLoggerFactory.newLogger("kiscord.samples", "Sample")
    }
}
