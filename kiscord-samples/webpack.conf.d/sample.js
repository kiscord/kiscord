// noinspection JSUnnecessarySemicolon
;

module.exports = (env) => {
    const webpack = require("webpack");
    const HtmlWebpackPlugin = require('html-webpack-plugin');

    config.plugins.push(new webpack.EnvironmentPlugin({
        'DEFAULT_LOGGER_LEVEL': env.DEFAULT_LOGGER_LEVEL || 'NONE',
        'KISCORD_SAMPLE': env.KISCORD_SAMPLE,
        'KISCORD_TOKEN': env.KISCORD_TOKEN,
        'NODE_DEBUG': process.env.NODE_ENV === 'development',
    }), new HtmlWebpackPlugin({
        title: "Kiscord Samples"
    }));

    return config;
}
