# Module kiscord-scripting

APIs to run kiscord.kts Kotlin scripts.

# Package kiscord.scripting

Building blocks to compile and run kiscord.kts scripts.
