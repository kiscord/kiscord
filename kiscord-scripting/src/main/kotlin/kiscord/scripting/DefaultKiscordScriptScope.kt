/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.scripting

import kiscord.*
import kiscord.gateway.*
import kotlinx.coroutines.*

public class DefaultKiscordScriptScope(
    builder: Kiscord.Builder,
    override val args: Array<out String>
) : KiscordScriptScope, Kiscord.Builder by builder {
    public constructor(args: Array<out String>) : this(Kiscord.builder(), args)

    override var isConnected: Boolean = false
        private set

    override fun connect(waitForReady: Boolean, join: Boolean) {
        if (isConnected) return
        isConnected = true
        val client = build()
        CoroutineScope(Dispatchers.Default).launch {
            client.connect(waitForReady = waitForReady, join = join)
        }
        if (join) runBlocking { client.join() }
        else if (waitForReady) runBlocking { client.waitForReadiness() }
    }
}
