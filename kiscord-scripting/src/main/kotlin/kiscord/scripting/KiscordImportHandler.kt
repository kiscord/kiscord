/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.scripting

import java.io.*
import java.nio.file.*
import kotlin.script.experimental.api.*
import kotlin.script.experimental.host.*
import kotlin.streams.*

public object KiscordImportHandler : RefineScriptCompilationConfigurationHandler {
    override fun invoke(context: ScriptConfigurationRefinementContext): ResultWithDiagnostics<ScriptCompilationConfiguration> {
        val annotations =
            context.collectedData?.get(ScriptCollectedData.collectedAnnotations)?.takeIf { it.isNotEmpty() }
                ?: return context.compilationConfiguration.asSuccess()

        val scriptBaseDir: Path = (context.script as? FileBasedScriptSource)?.file?.parentFile?.toPath() ?: File(".").toPath()
        val importedSources = annotations.flatMap { (annotation) ->
            (annotation as? Import)?.paths?.flatMap { sourceName ->
                val matcher = scriptBaseDir.fileSystem.getPathMatcher("glob:$sourceName")
                val scopedMatcher = PathMatcher { path ->
                    matcher.matches(scriptBaseDir.relativize(path))
                }
                Files.walk(scriptBaseDir).filter(scopedMatcher::matches).asSequence().map { scriptFile ->
                    FileScriptSource(scriptFile.toFile())
                }
            } ?: emptyList()
        }

        return ScriptCompilationConfiguration(context.compilationConfiguration) {
            if (importedSources.isNotEmpty()) importScripts.append(importedSources)
        }.asSuccess()
    }

}
