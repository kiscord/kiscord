/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.scripting

import kiscord.*
import kiscord.builder.*

/**
 * Scope for Kiscord scripts. Basically [Kiscord.Builder] with additional properties
 */
@BuilderDsl
public interface KiscordScriptScope : Kiscord.Builder {
    /**
     * Arguments passed for script
     */
    public val args: Array<out String>

    /**
     * Returns `true` if scope already started
     */
    public val isConnected: Boolean

    /**
     * Connects current Kiscord client
     * @param waitForReady block execution until all shards is ready
     * @param join block execution until client closed
     */
    public fun connect(waitForReady: Boolean = false, join: Boolean = false)
}

