/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.scripting

import kotlinx.coroutines.*
import kotlin.script.experimental.api.*
import kotlin.script.experimental.dependencies.*
import kotlin.script.experimental.dependencies.maven.*
import kotlin.script.experimental.jvm.*

public class KiscordDependenciesHandler(
    resolvers: List<ExternalDependenciesResolver>
) : RefineScriptCompilationConfigurationHandler {
    public constructor(vararg resolvers: ExternalDependenciesResolver) : this(resolvers.asList())
    public constructor() : this(MavenDependenciesResolver())

    private val resolver = CompoundDependenciesResolver(resolvers)

    override fun invoke(context: ScriptConfigurationRefinementContext): ResultWithDiagnostics<ScriptCompilationConfiguration> {
        val diagnostics = arrayListOf<ScriptDiagnostic>()

        val annotations =
            context.collectedData?.get(ScriptCollectedData.collectedAnnotations)?.takeIf { it.isNotEmpty() }
                ?: return context.compilationConfiguration.asSuccess()

        val resolvedClassPath = runBlocking {
            resolver.resolveFromScriptSourceAnnotations(annotations)
        }
        diagnostics += resolvedClassPath.reports

        if (resolvedClassPath is ResultWithDiagnostics.Failure) {
            return resolvedClassPath
        }

        return ScriptCompilationConfiguration(context.compilationConfiguration) {
            updateClasspath(resolvedClassPath.valueOrThrow())
        }.asSuccess(diagnostics)
    }
}
