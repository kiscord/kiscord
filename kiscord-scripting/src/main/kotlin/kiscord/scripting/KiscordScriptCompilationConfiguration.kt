/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.scripting

import kotlin.script.experimental.api.*
import kotlin.script.experimental.dependencies.*
import kotlin.script.experimental.host.*
import kotlin.script.experimental.jvm.*
import kotlin.script.experimental.jvm.util.*

public object KiscordScriptCompilationConfiguration : ScriptCompilationConfiguration({
    defaultImports(
        "kiscord.*",
        "kiscord.api.*",
        "kiscord.api.gateway.*",
        "kiscord.builder.*",
        "kiscord.gateway.*",
        "kiscord.gateway.events.*",
        "kiscord.gateway.interactions.*",
        "kiscord.interactions.*",
    )

    defaultImports(DependsOn::class, Import::class, Repository::class)

    val scriptClasspath = scriptCompilationClasspathFromContext(
        classLoader = KiscordScript::class.java.classLoader,
        wholeClasspath = true,
        unpackJarCollections = true
    )

    jvm {
        updateClasspath(scriptClasspath)
    }

    hostConfiguration(defaultJvmScriptingHostConfiguration.with {
        configurationDependencies(JvmDependency(scriptClasspath))
    })

    refineConfiguration {
        onAnnotations(DependsOn::class, Repository::class, handler = KiscordDependenciesHandler())
        onAnnotations(Import::class, handler = KiscordImportHandler)
    }

    ide {
        acceptedLocations(ScriptAcceptedLocation.Everywhere)
    }

    compilerOptions("-Xopt-in=kotlin.RequiresOptIn")
})
