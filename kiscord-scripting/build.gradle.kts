plugins {
    alias(libs.plugins.kotlin.jvm)
    alias(libs.plugins.dokka)
    alias(libs.plugins.licenser)
    `maven-publish`
    signing
}

apply(from = rootProject.file("gradle/multiplatform-bom.gradle.kts"))

dependencies {
    api(project(":kiscord-core"))
    api(project(":kiscord-gateway"))
    api(project(":kiscord-gateway:kiscord-gateway-events"))
    api(project(":kiscord-gateway:kiscord-gateway-interactions"))

    api(libs.kotlin.scripting.jvm)
    api(libs.kotlin.scripting.dependencies)
    api(libs.kotlin.scripting.dependencies.maven)
}

publishing {
    publications.register<MavenPublication>("jvm") {
        from(components["kotlin"])
    }
}
