# Module kiscord-core

Core library of Kiscord, provides APIs for REST services of Discord.

Kiscord uses [Ktor](https://ktor.io) client as HTTP/WS client
and [kotlinx.serialization](https://kotlin.github.io/kotlinx.serialization/) as JSON (de)serializer of Discord's models.

The main entity is [Kiscord][kiscord.Kiscord] class - it encapsulates all REST endpoints and provides plugable mechanism to extend
functionality (e.g. [kiscord-gateway](https://kiscord.aur.rocks/docs/kiscord-gateway/index.html) extends [Kiscord][kiscord.Kiscord] by communicating with Discord's
gateway)


| **Name**                           | **Description**                                                                                                                                                        |
|------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [Kiscord][kiscord.Kiscord]         | Provides access to REST endpoints and plugin mechanism                                                                                                                 |
| [KiscordCall][kiscord.KiscordCall] | [CoroutineContext.Element](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.coroutines/-coroutine-context/-element/) that holds [Kiscord][kiscord.Kiscord] instance |

# Package kiscord

Entrypoint for core library.

# Package kiscord.api

Contains all models and helper functions for Discord REST API

# Package kiscord.builder

Builder interfaces and Plugin mechanism

# Package kiscord.ktor

Plugins for Ktor client for use within Kiscord

<!--- MODULE kiscord-core -->

<!--- INDEX kiscord -->

[kiscord.Kiscord]: https://kiscord.aur.rocks//docs/kiscord-core/kiscord/-kiscord/index.html
[kiscord.KiscordCall]: https://kiscord.aur.rocks//docs/kiscord-core/kiscord/-kiscord-call/index.html

<!--- END -->
