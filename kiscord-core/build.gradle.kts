plugins {
    alias(libs.plugins.kotlin.multiplatform)
    alias(libs.plugins.kotlin.serialization)
    alias(libs.plugins.kotlinx.atomicfu)
    alias(libs.plugins.dokka)
    alias(libs.plugins.licenser)
    `maven-publish`
    signing
}

apply(from = rootProject.file("gradle/multiplatform-bom.gradle.kts"))
apply(from = rootProject.file("gradle/codegen.gradle.kts"))

val codegenOutput: Provider<Directory> by ext
val generateKotlin by tasks.getting(JavaExec::class) {
    val openapiJson = layout.buildDirectory.file("reports/openapi/openapi.json")
    val apiSpiderReport = layout.buildDirectory.file("reports/apiSpider")
    val apiSpiderReportMarkdown = layout.buildDirectory.file("reports/apiSpiderMarkdown.md")
    val apiDocs = layout.projectDirectory.file("discord-api-docs")

    argumentProviders += CommandLineArgumentProvider {
        listOf(
            "--json-output", openapiJson.get().toString(),
            "--api-report", apiSpiderReport.get().toString(),
            "--api-report-markdown", apiSpiderReportMarkdown.get().toString(),
            "--api-docs", apiDocs.toString(),
        )
    }
    args(
        "--build-config-name", "kiscord.BuildConfig",
        "--build-config-field", "NAME:kotlin.String:${project.ext["kiscord.name"]}",
        "--build-config-field", "VERSION:kotlin.String:${project.version}",
        "--build-config-field", "URL:kotlin.String:${project.ext["kiscord.url"]}",
        "--build-config-field", "DISCORD_API_VERSION:kotlin.Int:${project.ext["discord.api.version"]}",
        "--build-config-field", "DISCORD_API_URL:kotlin.String:${project.ext["discord.api.url"]}",
        "--build-config-field", "DISCORD_CDN_URL:kotlin.String:${project.ext["discord.cdn.url"]}",
        "--build-config-field", "KOTLIN_VERSION:kotlin.String:${libs.versions.kotlin.get()}",
        "--build-config-field", "DEBUG:kotlin.Boolean:${(project.ext["kiscord.debug"] as String).toBoolean()}",
        "--reformat-components",
    )

    inputs.dir(apiDocs)
    outputs.file(openapiJson)
    outputs.dir(apiSpiderReport)
    outputs.file(apiSpiderReportMarkdown)
}

val kiscordBrowser = (ext["kiscord.browser"] as String).toBoolean()

kotlin {
    sourceSets.configureEach {
        languageSettings {
            optIn("kiscord.api.KiscordUnstableAPI")
        }
    }

    jvm {
        withJava()
    }
    js {
        nodejs()
        if (kiscordBrowser) browser()
    }

    // Linux
    linuxX64()

    // Windows
    mingwX64()

    // Darwin
    iosArm64()
    iosX64()
    macosArm64()
    macosX64()

    sourceSets.commonMain.configure { kotlin.srcDir(files(codegenOutput.get()).builtBy(generateKotlin)) }
}

dependencies {
    commonMainApi(libs.canard)
    commonMainApi(libs.kotlinx.coroutines.core)
    commonMainApi(libs.kotlinx.datetime)
    commonMainApi(libs.kotlinx.serialization.core)
    commonMainApi(libs.kotlinx.serialization.json)
    commonMainApi(libs.ktor.client.content.negotiation)
    commonMainApi(libs.ktor.client.core)
    commonMainApi(libs.ktor.serialization.kotlinx)
    commonMainApi(libs.ktor.serialization.kotlinx.json)

    commonTestApi(libs.kotlinx.coroutines.test)
    commonTestApi(libs.ktor.client.mock)

    "jvmTestRuntimeOnly"(libs.slf4j.simple)
}

apply(from = rootProject.file("gradle/java9-module-info.gradle.kts"))
