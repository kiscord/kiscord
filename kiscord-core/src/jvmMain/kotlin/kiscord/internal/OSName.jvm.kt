/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.internal

import kiscord.api.*

internal actual fun currentOSName(): OSName {
    val os = System.getProperty("os.name", "unknown").lowercase()
    return when {
        "linux" in os -> {
            val runtime = System.getProperty("java.runtime.name", "unknown").lowercase()
            when {
                "android" in runtime -> OSName.Android
                else -> OSName.Linux
            }
        }
        "win" in os -> OSName.Windows
        "mac" in os || "darwin" in os -> OSName.Darwin
        "nix" in os || "bsd" in os -> OSName.Unix
        else -> OSName.Unknown
    }
}
