/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

@file:Suppress("DuplicatedCode")

package kiscord.ktor

import io.ktor.client.*
import io.ktor.client.plugins.*
import io.ktor.client.request.*
import io.ktor.client.request.forms.*
import io.ktor.http.*
import io.ktor.util.*
import io.ktor.utils.io.charsets.*
import kiscord.builder.*
import kotlinx.serialization.*
import kotlinx.serialization.json.*

@SerializableMultiPartDsl
public class SerializableMultiPartCoder private constructor(config: Config) {
    @SerializableMultiPartDsl
    public sealed interface Codec<out F : SerialFormat> {
        public val format: F
        public val contentTypes: Set<ContentType>
        public val defaultContentType: ContentType

        public interface Builder<F : SerialFormat> : Codec<F> {
            public override var format: F
            public override val contentTypes: MutableSet<ContentType>
            public override var defaultContentType: ContentType
        }
    }

    internal sealed interface CodecImpl<out F : SerialFormat> : Codec<F> {
        fun <T : Any> serializePartValue(value: SerializablePart.Value.Serializable<T>): SerializablePart.Value
    }

    @SerializableMultiPartDsl
    public interface TextCodec : Codec<StringFormat> {
        public val defaultCharset: Charset

        public companion object : BuilderSpec<TextCodec, Builder> {
            override fun builder(): Builder = Builder()
        }

        @SerializableMultiPartDsl
        public class Builder : TextCodec, BuilderFor<TextCodec>, Codec.Builder<StringFormat> {
            override lateinit var format: StringFormat
            override val contentTypes: MutableSet<ContentType> = mutableSetOf()
            override lateinit var defaultContentType: ContentType
            override var defaultCharset: Charset = Charsets.UTF_8

            override fun build(): TextCodec {
                val contentTypes = contentTypes.toSet()
                check(contentTypes.isNotEmpty()) { "at-least one content type must be specified" }
                val defaultContentType =
                    if (::defaultContentType.isInitialized) defaultContentType else contentTypes.first()
                check(defaultContentType in contentTypes) { "default content type must exists in content types list" }
                return TextCodecImpl(format, contentTypes, defaultContentType, defaultCharset)
            }

            internal data class TextCodecImpl(
                override val format: StringFormat,
                override val contentTypes: Set<ContentType>,
                override val defaultContentType: ContentType,
                override val defaultCharset: Charset
            ) : TextCodec, CodecImpl<StringFormat> {
                constructor(codec: TextCodec) : this(
                    format = codec.format,
                    contentTypes = codec.contentTypes.toSet(),
                    defaultContentType = codec.defaultContentType,
                    defaultCharset = codec.defaultCharset
                )

                override fun <T : Any> serializePartValue(value: SerializablePart.Value.Serializable<T>): SerializablePart.Value {
                    return SerializablePart.Value.AsString(format.encodeToString(value.serializer, value.value))
                }
            }
        }
    }

    @SerializableMultiPartDsl
    public class Config {
        private val codecs = mutableSetOf<CodecImpl<*>>()

        public fun textCodec(codec: TextCodec) {
            @Suppress("USELESS_CAST")
            codecs += when (codec) {
                is CodecImpl<*> -> codec
                else -> TextCodec.Builder.TextCodecImpl(codec) as CodecImpl<*>
            }
        }

        internal fun fillMapping(mapping: MutableMap<ContentType, CodecImpl<*>>) {
            codecs.forEach { codec ->
                val contentTypes = codec.contentTypes
                check(codec.defaultContentType in contentTypes) { "default content type must exists in content types list" }
                contentTypes.forEach { contentType ->
                    check(mapping.put(contentType, codec) == null) { "codec clash for $contentType" }
                }
            }
        }
    }

    private val codecs = mutableMapOf<ContentType, CodecImpl<*>>()

    init {
        config.fillMapping(codecs)
    }

    internal fun lookupCodec(contentType: ContentType): CodecImpl<*>? {
        // first try without parameters, then do linear search
        return codecs[contentType.withoutParameters()] ?: codecs.entries.find {
            contentType.match(it.key)
        }?.value
    }

    @SerializableMultiPartDsl
    public companion object Feature : HttpClientPlugin<Config, SerializableMultiPartCoder> {
        override val key: AttributeKey<SerializableMultiPartCoder> = AttributeKey("Kiscord:Ktor:StaticCoder")

        override fun prepare(block: Config.() -> Unit): SerializableMultiPartCoder {
            return SerializableMultiPartCoder(Config().apply(block))
        }

        override fun install(plugin: SerializableMultiPartCoder, scope: HttpClient) {
            scope.requestPipeline.intercept(HttpRequestPipeline.Transform) { body ->
                if (body is SerializableMultiPart) {
                    val parts = body.parts.map { part ->
                        part.toPartData(plugin)
                    }

                    if (context.contentType()?.let { ContentType.MultiPart.FormData.match(it) } == true) {
                        context.headers.remove(HttpHeaders.ContentType)
                    }

                    proceedWith(MultiPartFormDataContent(parts))
                    return@intercept
                }
            }
        }
    }
}

@SerializableMultiPartDsl
public inline fun SerializableMultiPartCoder.Config.textCodec(block: SerializableMultiPartCoder.TextCodec.Builder.() -> Unit) {
    textCodec(SerializableMultiPartCoder.TextCodec(block))
}

@SerializableMultiPartDsl
public fun SerializableMultiPartCoder.Codec.Builder<*>.contentType(contentType: ContentType) {
    contentTypes += contentType
}

@SerializableMultiPartDsl
public fun SerializableMultiPartCoder.TextCodec.Builder.json(json: Json) {
    format = json
    contentType(ContentType.Application.Json)
    defaultContentType = ContentType.Application.Json
    defaultCharset = Charsets.UTF_8
}

@SerializableMultiPartDsl
public inline fun HttpRequestBuilder.multipartBody(block: SerializableMultiPart.Builder.() -> Unit) {
    contentType(ContentType.MultiPart.FormData)
    setBody(SerializableMultiPart.builder().apply(block).build())
}
