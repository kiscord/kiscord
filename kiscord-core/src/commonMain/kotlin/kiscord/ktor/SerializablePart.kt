/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.ktor

import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.utils.io.*
import kiscord.builder.*
import kotlinx.serialization.*

@SerializableMultiPartDsl
public class SerializablePart() : BuilderFor<SerializablePart> {
    public constructor(block: SerializablePart.() -> Unit) : this() {
        block(this)
    }

    public val headers: HeadersBuilder = HeadersBuilder()

    public var contentDisposition: ContentDisposition?
        get() = headers[HttpHeaders.ContentDisposition]?.let {
            ContentDisposition.parse(it)
        }
        set(value) {
            when (value) {
                null -> headers.remove(HttpHeaders.ContentDisposition)
                else -> headers[HttpHeaders.ContentDisposition] = value.toString()
            }
        }

    public var contentType: ContentType?
        get() = headers[HttpHeaders.ContentType]?.let {
            ContentType.parse(it)
        }
        set(value) {
            when (value) {
                null -> headers.remove(HttpHeaders.ContentType)
                else -> headers[HttpHeaders.ContentType] = value.toString()
            }
        }

    public var dispose: () -> Unit = {}

    internal lateinit var value: Value

    internal sealed class Value {
        abstract suspend fun toPartData(part: SerializablePart, coder: SerializableMultiPartCoder): PartData

        class AsString(val value: String) : Value() {
            override suspend fun toPartData(part: SerializablePart, coder: SerializableMultiPartCoder): PartData {
                return PartData.FormItem(value, part.dispose, part.headers.build())
            }
        }

        class AsByteArray(val value: ByteArray) : Value() {
            override suspend fun toPartData(part: SerializablePart, coder: SerializableMultiPartCoder): PartData {
                return AsByteReadChannel { ByteReadChannel(value) }.toPartData(part, coder)
            }
        }

        class AsByteReadChannel(val value: () -> ByteReadChannel) : Value() {
            override suspend fun toPartData(part: SerializablePart, coder: SerializableMultiPartCoder): PartData {
                return PartData.BinaryChannelItem(value, part.headers.build())
            }
        }

        class Serializable<T : Any>(val value: T, val serializer: SerializationStrategy<T>) : Value() {
            override suspend fun toPartData(part: SerializablePart, coder: SerializableMultiPartCoder): PartData {
                val contentType: ContentType = part.headers[HttpHeaders.ContentType]?.let { ContentType.parse(it) }
                    ?: throw SerializationException("No content type provided for part")
                val codec = coder.lookupCodec(contentType)
                    ?: throw SerializationException("No codec for content type $contentType")

                return codec.serializePartValue(this).toPartData(part, coder)
            }
        }

        class Deferred(val value: suspend () -> Value) : Value() {
            override suspend fun toPartData(part: SerializablePart, coder: SerializableMultiPartCoder): PartData {
                return value().toPartData(part, coder)
            }
        }
    }

    internal suspend fun toPartData(coder: SerializableMultiPartCoder): PartData {
        check(::value.isInitialized) { "cannot process part data without value" }
        return value.toPartData(this, coder)
    }

    public companion object : BuilderSpec<SerializablePart, SerializablePart> {
        override fun builder(): SerializablePart = SerializablePart()
    }

    override fun build(): SerializablePart = this
}

@SerializableMultiPartDsl
public fun SerializablePart.dispose(block: () -> Unit) {
    this.dispose = block
}

@SerializableMultiPartDsl
public fun SerializablePart.valueOfString(value: String) {
    this.value = SerializablePart.Value.AsString(value)
}

@SerializableMultiPartDsl
public fun SerializablePart.valueOfByteArray(value: ByteArray) {
    this.value = SerializablePart.Value.AsByteArray(value)
}

@SerializableMultiPartDsl
public fun SerializablePart.valueOfByteReadChannel(value: () -> ByteReadChannel) {
    this.value = SerializablePart.Value.AsByteReadChannel(value)
}

@SerializableMultiPartDsl
public fun <T : Any> SerializablePart.partBody(value: T, serializer: SerializationStrategy<T>) {
    this.value = SerializablePart.Value.Serializable(value, serializer)
}

@SerializableMultiPartDsl
public fun SerializablePart.deferredValueOfString(value: suspend () -> String) {
    this.value = SerializablePart.Value.Deferred {
        SerializablePart.Value.AsString(value())
    }
}

@SerializableMultiPartDsl
public fun SerializablePart.deferredValueOfByteArray(value: suspend () -> ByteArray) {
    this.value = SerializablePart.Value.Deferred {
        SerializablePart.Value.AsByteArray(value())
    }
}

@SerializableMultiPartDsl
public fun <T : Any> SerializablePart.deferredValue(value: suspend () -> Pair<T, SerializationStrategy<T>>) {
    this.value = SerializablePart.Value.Deferred {
        val (realValue, serializer) = value()
        SerializablePart.Value.Serializable(realValue, serializer)
    }
}
