/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.ktor

import io.ktor.client.*
import io.ktor.client.engine.*
import kiscord.builder.*

public sealed class HttpClientFactory {
    public open fun withConfig(config: ConfigurationChain<HttpClientConfig<*>>): HttpClientFactory {
        return Configured(this, config)
    }

    public open fun withConfig(config: HttpClientConfig<*>.() -> Unit): HttpClientFactory {
        return Configured(this, ConfigurationChain(config))
    }

    internal open fun withNewRoot(newRoot: HttpClientFactory): HttpClientFactory {
        return newRoot
    }

    internal abstract fun create(config: ConfigurationChain<HttpClientConfig<*>>): HttpClient

    public class Engine(
        private val engine: HttpClientEngine,
        private val engineConfig: HttpClientConfig<*>.() -> Unit
    ) : HttpClientFactory() {
        override fun create(config: ConfigurationChain<HttpClientConfig<*>>): HttpClient = HttpClient(engine) {
            engineConfig()
            config(this)
        }
    }

    public class EngineFactory<out T : HttpClientEngineConfig>(
        private val engineFactory: HttpClientEngineFactory<T>,
        private val engineConfig: HttpClientConfig<T>.() -> Unit
    ) : HttpClientFactory() {
        override fun create(config: ConfigurationChain<HttpClientConfig<*>>): HttpClient = HttpClient(engineFactory) {
            engineConfig()
            config(this)
        }
    }

    public object Default : HttpClientFactory() {
        override fun create(config: ConfigurationChain<HttpClientConfig<*>>): HttpClient = HttpClient {
            config(this)
        }
    }

    public class Configured(
        private val base: HttpClientFactory,
        private val config: ConfigurationChain<HttpClientConfig<*>>
    ) : HttpClientFactory() {
        override fun create(config: ConfigurationChain<HttpClientConfig<*>>): HttpClient {
            return base.create(config + this.config)
        }

        override fun withConfig(config: HttpClientConfig<*>.() -> Unit): HttpClientFactory {
            return Configured(base, this.config + config)
        }

        override fun withConfig(config: ConfigurationChain<HttpClientConfig<*>>): HttpClientFactory {
            return Configured(base, this.config + config)
        }

        override fun withNewRoot(newRoot: HttpClientFactory): HttpClientFactory {
            return Configured(base.withNewRoot(newRoot), config)
        }
    }
}
