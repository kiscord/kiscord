/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.ktor

import io.ktor.client.request.*
import io.ktor.client.statement.*
import kiscord.*
import kotlinx.coroutines.*
import kotlin.contracts.*
import kotlin.coroutines.*
import kotlin.coroutines.coroutineContext as currentCoroutineContext

public fun interface KiscordRequestMixin : CoroutineContext.Element {
    override val key: CoroutineContext.Key<*> get() = Key

    public fun mixinHttpRequest(builder: HttpRequestBuilder)

    public companion object Key : CoroutineContext.Key<KiscordRequestMixin>
}

@Suppress("unused")
public inline fun Kiscord.mixin(crossinline mixin: HttpRequestBuilder.() -> Unit): KiscordRequestMixin =
    KiscordRequestMixin { builder -> mixin(builder) }

@OptIn(ExperimentalContracts::class)
public suspend inline fun <T> Kiscord.withMixin(
    crossinline mixin: HttpRequestBuilder.() -> Unit,
    noinline block: suspend Kiscord.() -> T
): T {
    contract {
        callsInPlace(block, InvocationKind.EXACTLY_ONCE)
    }

    return withContext(mixin(mixin)) {
        block()
    }
}

internal suspend inline fun Kiscord.request(block: HttpRequestBuilder.() -> Unit): HttpStatement {
    val builder = HttpRequestBuilder()
    builder.apply(block)
    currentCoroutineContext[KiscordRequestMixin]?.mixinHttpRequest(builder)
    return HttpStatement(builder, httpClient)
}
