/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

@file:JvmMultifileClass
@file:JvmName("KiscordUserAgentKt")

package kiscord.ktor

import io.ktor.client.*
import io.ktor.client.plugins.*
import io.ktor.client.request.*
import io.ktor.http.*
import io.ktor.util.*
import kiscord.*
import kiscord.ktor.KiscordBranding.*
import kotlin.jvm.*

/**
 * Kiscord module for Ktor that provides User-Agent according to the reference.
 *
 * [Discord API Reference](https://discord.com/developers/docs/reference#user-agent)
 * @see Config
 */
public class KiscordBranding private constructor(config: Config) {
    private val userAgent: String = config.userAgent
    private val forceUserAgent: Boolean = config.forceUserAgent

    internal fun install(scope: HttpClient) {
        if (forceUserAgent || !PlatformUtils.IS_BROWSER) scope.requestPipeline.intercept(HttpRequestPipeline.State) {
            context.headers[HttpHeaders.UserAgent] = userAgent
        }
    }

    /**
     * Configuration for [KiscordBranding] feature.
     *
     * @param userAgent User-Agent to send with requests.
     * @param forceUserAgent Force send User-Agent even if it may cause any errors in current environment (i.e. in browser)
     */
    public data class Config(
        var userAgent: String = DefaultUserAgent,
        var forceUserAgent: Boolean = false
    )

    /**
     * Companion object used to install [KiscordBranding] feature into [HttpClient]
     * @see HttpClientConfig.install
     */
    public companion object : HttpClientPlugin<Config, KiscordBranding> {
        private val DefaultUserAgent = buildString {
            append("DiscordBot (")
            append(BuildConfig.URL)
            append(", ")
            append(BuildConfig.VERSION)
            append(") (")
            append(BuildConfig.NAME)
            append(" Kotlin/")
            append(kiscord.internal.KotlinPlatform)
            append(" ")
            val currentKotlinVersion = KotlinVersion.CURRENT.toString()
            append(currentKotlinVersion)
            if (currentKotlinVersion != BuildConfig.KOTLIN_VERSION) {
                append('/')
                append(BuildConfig.KOTLIN_VERSION)
            }
            append(')')
        }

        /**
         * @suppress
         */
        override val key: AttributeKey<KiscordBranding> = AttributeKey("Kiscord:Ktor:Branding")

        /**
         * @suppress
         */
        override fun install(plugin: KiscordBranding, scope: HttpClient) {
            plugin.install(scope)
        }

        /**
         * @suppress
         */
        override fun prepare(block: Config.() -> Unit): KiscordBranding = KiscordBranding(Config().apply(block))
    }
}
