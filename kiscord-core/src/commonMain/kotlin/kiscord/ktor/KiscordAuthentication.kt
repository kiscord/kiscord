/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.ktor

import io.ktor.client.*
import io.ktor.client.plugins.*
import io.ktor.client.request.*
import io.ktor.http.*
import io.ktor.util.*
import kiscord.api.*
import kiscord.ktor.KiscordAuthentication.*

/**
 * Kiscord feature to Ktor to allow authentication for requests.
 * @see Config
 * @see anonymous
 * @see token
 */
public class KiscordAuthentication private constructor(config: Config) {
    private val defaultToken: Token = config.defaultToken
    private val apiUrl: Url? = config.apiUrl

    /**
     * Configuration used by [KiscordAuthentication] feature
     * @param defaultToken Default authentication token. Used if no other provided in request builder.
     */
    public data class Config(
        var apiUrl: Url? = null,
        var defaultToken: Token = Token.Default
    )


    /**
     * Companion object used to install [KiscordAuthentication] feature into [HttpClient]
     * @see HttpClientConfig.install
     */
    public companion object : HttpClientPlugin<Config, KiscordAuthentication> {
        /**
         * @suppress
         */
        override val key: AttributeKey<KiscordAuthentication> = AttributeKey("Kiscord:Ktor:Authentication")

        /**
         * @suppress
         */
        override fun install(plugin: KiscordAuthentication, scope: HttpClient) {
            val apiUrl = plugin.apiUrl

            scope.requestPipeline.intercept(HttpRequestPipeline.State) {
                if (context.attributes.getOrNull(ANONYMOUS_REQUEST) == true) return@intercept
                if (HttpHeaders.Authorization in context.headers) return@intercept
                if (apiUrl != null && !context.url.startsWith(apiUrl)) return@intercept
                val token = context.attributes.getOrNull(TOKEN) ?: plugin.defaultToken
                token.getAuthorizationHeader(scope)?.let { header ->
                    context.headers[HttpHeaders.Authorization] = header
                }
            }
        }

        /**
         * @suppress
         */
        override fun prepare(block: Config.() -> Unit): KiscordAuthentication =
            KiscordAuthentication(Config().apply(block))

        internal val ANONYMOUS_REQUEST = AttributeKey<Boolean>("Kiscord:Ktor:Authentication:Anonymous")
        internal val TOKEN = AttributeKey<Token>("Kiscord:Ktor:Authentication:Token")
    }
}

public fun HttpRequestBuilder.anonymous(value: Boolean = true): Unit {
    attributes.put(KiscordAuthentication.ANONYMOUS_REQUEST, value)
}

public fun HttpRequestBuilder.token(token: Token) {
    attributes.put(KiscordAuthentication.TOKEN, token)
}

private fun URLBuilder.startsWith(url: Url): Boolean = when {
    protocol != url.protocol || authority != url.authority -> false
    encodedPath.startsWith(url.encodedPath) -> true
    else -> false
}
