/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.builder

import kotlin.js.*
import kotlin.jvm.*
import kotlin.reflect.*

public abstract class TypedAttribute<out T>(private val classifier: KClassifier) {
    public abstract val name: String

    internal abstract fun safeCast(value: Any): T?

    public open fun defaultValue(): T {
        throw NoSuchElementException("No value for attribute $name")
    }

    public open fun defaultValue(scope: TypedAttributesScope): T {
        return defaultValue()
    }

    final override fun hashCode(): Int = name.hashCode() * 31 + classifier.hashCode()
    final override fun equals(other: Any?): Boolean {
        return other is TypedAttribute<*> && name == other.name && classifier == other.classifier
    }

    final override fun toString(): String = "TypedAttribute($name)"

    public companion object {
        @JvmName("create")
        @JsName("create")
        public inline operator fun <reified T> invoke(name: String): TypedAttribute<T> {
            return object : TypedAttribute<T>(T::class) {
                override val name: String get() = name
                override fun safeCast(value: Any): T? = value as? T
            }
        }

        @JvmName("create")
        @JsName("createWithDefaultValue")
        public inline operator fun <reified T> invoke(
            name: String,
            defaultValue: T
        ): TypedAttribute<T> {
            return object : TypedAttribute<T>(T::class) {
                override val name: String get() = name
                override fun safeCast(value: Any): T? = value as? T
                override fun defaultValue(): T = defaultValue
            }
        }

        @JvmName("create")
        @JsName("createWithDefaultProvider")
        public inline operator fun <reified T> invoke(
            name: String,
            crossinline defaultValue: (TypedAttributesScope) -> T
        ): TypedAttribute<T> {
            return object : TypedAttribute<T>(T::class) {
                override val name: String get() = name
                override fun safeCast(value: Any): T? = value as? T
                override fun defaultValue(scope: TypedAttributesScope): T = defaultValue(scope)
            }
        }
    }
}

public interface TypedAttributesScope {
    public val attributes: TypedAttributes
}

public interface TypedAttributes {
    public val allAttributes: Set<TypedAttribute<*>>

    public operator fun <T> get(attribute: TypedAttribute<T>): T
    public operator fun <T> get(scope: TypedAttributesScope, attribute: TypedAttribute<T>): T

    public fun <T> getOrNull(attribute: TypedAttribute<T>): T?

    public operator fun <T> set(attribute: TypedAttribute<T>, value: T?)

    public operator fun plusAssign(other: TypedAttributes) {
        for (attribute in other.allAttributes) {
            this[attribute] = other[attribute]
        }
    }

    public fun clear()

    public companion object {
        @JvmName("create")
        @JsName("create")
        public operator fun invoke(parent: TypedAttributes? = null): TypedAttributes {
            return MapTypedAttributes(HashMap(), parent)
        }

        public inline fun readOnly(
            parent: TypedAttributes? = null,
            block: TypedAttributes.() -> Unit
        ): TypedAttributes {
            val attributes = invoke(parent)
            attributes.block()
            return attributes.readOnly()
        }
    }
}

private class MapTypedAttributes(
    private val map: MutableMap<TypedAttribute<*>, Any>,
    private val parent: TypedAttributes? = null
) : TypedAttributes {
    override val allAttributes: Set<TypedAttribute<*>> get() = map.keys

    override fun <T> get(attribute: TypedAttribute<T>): T {
        val selfValue = map[attribute]?.let { attribute.safeCast(it) }
        if (selfValue != null) return selfValue
        val parentValue = parent?.getOrNull(attribute)
        if (parentValue != null) return parentValue
        return attribute.defaultValue()
    }

    override fun <T> get(scope: TypedAttributesScope, attribute: TypedAttribute<T>): T {
        val selfValue = map[attribute]?.let { attribute.safeCast(it) }
        if (selfValue != null) return selfValue
        val parentValue = parent?.getOrNull(attribute)
        if (parentValue != null) return parentValue
        return attribute.defaultValue(scope)
    }

    override fun <T> getOrNull(attribute: TypedAttribute<T>): T? {
        return map[attribute]?.let { attribute.safeCast(it) }
    }

    override fun <T> set(attribute: TypedAttribute<T>, value: T?) {
        if (value == null) {
            map.remove(attribute)
        } else {
            map[attribute] = value
        }
    }

    override fun clear() {
        map.clear()
    }
}

private class ReadOnlyTypedAttributes(typedAttributes: TypedAttributes) : TypedAttributes by typedAttributes {
    override fun <T> set(attribute: TypedAttribute<T>, value: T?): Nothing =
        throw NotImplementedError("Read only attributes cannot be modified")

    override fun plusAssign(other: TypedAttributes): Nothing =
        throw NotImplementedError("Read only attributes cannot be modified")

    override fun clear(): Nothing =
        throw NotImplementedError("Read only attributes cannot be modified")
}

public operator fun <T> TypedAttribute<T>.getValue(
    scope: TypedAttributesScope,
    property: KProperty<*>
): T {
    return scope.attributes[scope, this]
}

public operator fun <T> TypedAttribute<T>.getValue(
    attributes: TypedAttributes,
    property: KProperty<*>
): T {
    return attributes[this]
}

public operator fun <T> TypedAttribute<T>.setValue(
    scope: TypedAttributesScope,
    property: KProperty<*>,
    value: T?
) {
    scope.attributes[this] = value
}

public operator fun <T> TypedAttribute<T>.setValue(
    attributes: TypedAttributes,
    property: KProperty<*>,
    value: T?
) {
    attributes[this] = value
}

public fun TypedAttributes.readOnly(): TypedAttributes = when (this) {
    is ReadOnlyTypedAttributes -> this
    else -> ReadOnlyTypedAttributes(this)
}
