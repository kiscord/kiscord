/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.builder

internal abstract class PluggableBuilderImpl<out TScope : PluginScope> : PluggableBuilder<TScope> {
    final override val attributes: TypedAttributes = TypedAttributes()

    private val features = mutableMapOf<TypedAttribute<*>, FeatureState<*, *, TScope>>()
    private val order = mutableListOf<TypedAttribute<*>>()

    @Suppress("UNCHECKED_CAST")
    final override fun <TConfig : Any> install(
        plugin: Plugin<TConfig, *, TScope>,
        block: TConfig.() -> Unit
    ) {
        val key = plugin.key
        var state = features[key]

        if (state == null) {
            plugin.beforeFeatureAdded(this)
            state = features[key]
            if (state == null) {
                order += key
                features[key] = FeatureState(plugin, ConfigurationChain(block))
                plugin.afterFeatureAdded(this)
            } else {
                features[key] = (state as FeatureState<TConfig, *, TScope>).addConfigurator(block)
            }
        } else {
            features[key] = (state as FeatureState<TConfig, *, TScope>).addConfigurator(block)
        }
    }

    final override fun <TConfig : Any> install(
        plugin: Plugin<TConfig, *, TScope>
    ) {
        val key = plugin.key
        var state = features[key]

        if (state == null) {
            plugin.beforeFeatureAdded(this)
            state = features[key]
            if (state == null) {
                order += key
                features[key] = FeatureState(plugin, ConfigurationChain())
                plugin.afterFeatureAdded(this)
            }
        }
    }

    final override fun isFeatureInstalled(plugin: Plugin<*, *, TScope>): Boolean {
        return features.containsKey(plugin.key)
    }

    protected open fun beforeCreating() {}
    protected open fun afterCreating(scope: @UnsafeVariance TScope) {}

    final override fun build(): TScope {
        beforeCreating()
        features.values.toList().forEach { state ->
            state.plugin.beforeScopeCreated(this)
        }

        val attributes = TypedAttributes()
        attributes += this.attributes

        val featureAttributes = TypedAttributes()
        attributes[PluggableBuilder.FEATURES] = featureAttributes

        val scope = create(attributes)

        order.forEach { key ->
            val state = features[key] ?: return@forEach
            state.install(featureAttributes, scope)
        }

        afterCreating(scope)

        return scope
    }

    private data class FeatureState<TConfig : Any, TFeature : Any, TScope : PluginScope>(
        val plugin: Plugin<TConfig, TFeature, TScope>,
        val configuration: ConfigurationChain<TConfig>
    ) {
        fun addConfigurator(block: TConfig.() -> Unit) = copy(configuration = configuration + block)

        fun install(attributes: TypedAttributes, scope: TScope) {
            attributes[plugin.key] = plugin.install(scope, configuration)
        }
    }

    protected abstract fun create(attributes: TypedAttributes): TScope
}
