/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.builder

public operator fun <TFeature : Any, TScope : PluginScope> TScope.get(plugin: Plugin<*, TFeature, TScope>): TFeature {
    return attributes[PluggableBuilder.FEATURES][plugin.key]
}

public fun <TFeature : Any, TScope : PluginScope> TScope.featureOrNull(plugin: Plugin<*, TFeature, TScope>): TFeature? {
    return attributes.getOrNull(PluggableBuilder.FEATURES)?.getOrNull(plugin.key)
}

public fun <TScope : PluginScope> PluggableBuilder<TScope>.maybeInstall(plugin: Plugin<*, *, TScope>) {
    if (!isFeatureInstalled(plugin)) {
        install(plugin)
    }
}

public inline fun <TScope : PluginScope> PluggableBuilder<TScope>.install(
    name: String,
    crossinline block: TScope.() -> Unit
) {
    install(object : CustomPlugin<TScope>(name) {
        override fun install(scope: TScope, block: ConfigurationChain<Unit>): CustomPlugin<TScope> {
            scope.block()
            return this
        }
    })
}

@PublishedApi
internal abstract class CustomPlugin<TScope : PluginScope>(name: String) :
    Plugin<Unit, CustomPlugin<TScope>, TScope> {
    final override val key = TypedAttribute<CustomPlugin<TScope>>("Kiscord:CustomPlugin:$name")
}
