/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.builder

import kotlin.jvm.*

@JvmInline
public value class ConfigurationChain<in T>(
    private val blocks: Collection<T.() -> Unit>
) : Collection<T.() -> Unit> {
    public constructor(vararg blocks: T.() -> Unit) : this(blocks.toList())
    public constructor(block: T.() -> Unit) : this(listOf(block))
    public constructor() : this(emptyList())

    public operator fun invoke(value: T): @UnsafeVariance T {
        for (chain in blocks) {
            chain.invoke(value)
        }
        return value
    }

    public operator fun <R : T> plus(block: R.() -> Unit): ConfigurationChain<R> =
        ConfigurationChain(blocks + block)

    public operator fun <R : T> plus(blocks: Collection<R.() -> Unit>): ConfigurationChain<R> =
        ConfigurationChain(this.blocks + blocks)

    override val size: Int get() = blocks.size
    override fun isEmpty(): Boolean = blocks.isEmpty()
    override fun iterator(): Iterator<T.() -> Unit> = blocks.iterator()
    override fun contains(element: (@UnsafeVariance T).() -> Unit): Boolean = blocks.contains(element)
    override fun containsAll(elements: Collection<(@UnsafeVariance T).() -> Unit>): Boolean =
        blocks.containsAll(elements)
}
