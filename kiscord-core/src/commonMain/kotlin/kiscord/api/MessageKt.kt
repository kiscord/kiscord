/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.api

import kiscord.*

/**
 * Create [reference][MessageReference] from the given [message][Message].
 *
 * @param failIfNotExists fail to create reply if referenced message doesn't exist.
 */
public fun Message.asReference(failIfNotExists: Boolean = true): MessageReference = MessageReference(
    messageId = id,
    channelId = channelId,
    guildId = guildId,
    failIfNotExists = failIfNotExists
)

/**
 * Respond to the message in the originating channel with [message builder][block]
 *
 * Must be called in [KiscordCall] context
 */
@DiscordApiDsl
public suspend inline fun Message.respond(block: CreateMessage.Builder.() -> Unit): Message =
    kiscord().channel.createMessage(channelId, block)

/**
 * Respond to the message in the originating channel with [message builder][block] and typing indicator
 *
 * Must be called in [KiscordCall] context
 */
@DiscordApiDsl
public suspend fun Message.respondWithTypingIndicator(block: suspend CreateMessage.Builder.() -> Unit): Message =
    kiscord().channel.createMessageWithTypingIndicator(channelId, block)

/**
 * Reply to the message in the originating channel with [message builder][block]
 *
 * Must be called in [KiscordCall] context
 *
 * @param failIfNotExists Fail to reply if message doesn't exist
 */
@DiscordApiDsl
public suspend inline fun Message.reply(
    failIfNotExists: Boolean = true,
    block: CreateMessage.Builder.() -> Unit
): Message = respond {
    messageReference(asReference(failIfNotExists).toBuilder())
    block()
}

/**
 * Reply to the message in the originating channel with [message builder][block] and typing indicator
 *
 * Must be called in [KiscordCall] context
 *
 * @param failIfNotExists Fail to reply if message doesn't exist
 */
@DiscordApiDsl
public suspend fun Message.replyWithTypingIndicator(
    failIfNotExists: Boolean = true,
    block: suspend CreateMessage.Builder.() -> Unit
): Message = respondWithTypingIndicator {
    messageReference(asReference(failIfNotExists).toBuilder())
    block()
}

/**
 * Edit the message
 *
 * Must be called in [KiscordCall] context
 */
@DiscordApiDsl
public suspend fun Message.edit(
    block: EditMessage.Builder.() -> Unit
): Message = kiscord().channel.editMessage(channelId, id, block)
