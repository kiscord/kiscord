/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

@file:OptIn(ExperimentalSerializationApi::class, InternalSerializationApi::class)

package kiscord.api

import kiscord.builder.*
import kotlinx.serialization.*
import kotlinx.serialization.builtins.*
import kotlinx.serialization.descriptors.*
import kotlinx.serialization.encoding.*
import kotlin.jvm.*

@Serializable(with = ApplicationCommandOptionCoerceValue.Serializer::class)
public sealed class ApplicationCommandOptionCoerceValue {
    public abstract val value: Number

    public class AsLong(public val long: Long) : ApplicationCommandOptionCoerceValue() {
        override val value: Long get() = long
    }

    public class AsDouble(public val double: Double) : ApplicationCommandOptionCoerceValue() {
        override val value: Double get() = double
    }

    internal object Serializer : KSerializer<ApplicationCommandOptionCoerceValue> {
        override fun deserialize(decoder: Decoder): ApplicationCommandOptionCoerceValue =
            throw NotImplementedError("Deserializing not supported")

        override val descriptor: SerialDescriptor =
            buildSerialDescriptor("kiscord.api.ApplicationCommandOptionCoerceValue", PolymorphicKind.SEALED) {
                element("long", Long.serializer().descriptor, isOptional = true)
                element("double", Double.serializer().descriptor, isOptional = true)
            }

        override fun serialize(encoder: Encoder, value: ApplicationCommandOptionCoerceValue) = when (value) {
            is AsLong -> encoder.encodeLong(value.long)
            is AsDouble -> encoder.encodeDouble(value.double)
        }
    }
}

@BuilderDsl
public fun ApplicationCommandOption.Builder.minValue(minValue: Long): Unit =
    minValue(ApplicationCommandOptionCoerceValue.AsLong(minValue))

@BuilderDsl
public fun ApplicationCommandOption.Builder.maxValue(maxValue: Long): Unit =
    maxValue(ApplicationCommandOptionCoerceValue.AsLong(maxValue))

@BuilderDsl
public fun ApplicationCommandOption.Builder.coerceValue(minValue: Long, maxValue: Long) {
    minValue(minValue)
    maxValue(maxValue)
}

@BuilderDsl
@JvmName("coerceLongValue")
public fun ApplicationCommandOption.Builder.coerceValue(range: ClosedRange<Long>) {
    minValue(range.start)
    maxValue(range.endInclusive)
}

@BuilderDsl
public fun ApplicationCommandOption.Builder.minValue(minValue: Double): Unit =
    minValue(ApplicationCommandOptionCoerceValue.AsDouble(minValue))

@BuilderDsl
public fun ApplicationCommandOption.Builder.maxValue(maxValue: Double): Unit =
    maxValue(ApplicationCommandOptionCoerceValue.AsDouble(maxValue))

@BuilderDsl
public fun ApplicationCommandOption.Builder.coerceValue(minValue: Double, maxValue: Double) {
    minValue(minValue)
    maxValue(maxValue)
}

@BuilderDsl
@JvmName("coerceDoubleValue")
public fun ApplicationCommandOption.Builder.coerceValue(range: ClosedRange<Double>) {
    minValue(range.start)
    maxValue(range.endInclusive)
}
