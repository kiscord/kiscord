/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.api

public class SnowflakeRange(override val start: Snowflake, override val endInclusive: Snowflake) :
    ClosedRange<Snowflake> {
    override fun equals(other: Any?): Boolean {
        return other is SnowflakeRange && (isEmpty() && other.isEmpty() ||
            start == other.start && endInclusive == other.endInclusive)
    }

    override fun hashCode(): Int {
        if (isEmpty()) return -1
        val first = start.id.toLong()
        val last = endInclusive.id.toLong()
        return (31 * (first xor (first ushr 32)) + (last xor (last ushr 32))).toInt()
    }

    override fun toString(): String = "$start..$endInclusive"
}

public operator fun Snowflake.rangeTo(other: Snowflake): SnowflakeRange = SnowflakeRange(this, other)
