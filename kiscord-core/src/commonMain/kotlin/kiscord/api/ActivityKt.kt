/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

@file:JvmMultifileClass
@file:JvmName("ActivityKt")

package kiscord.api

import io.ktor.http.*
import kiscord.api.*
import kiscord.builder.*
import kotlin.jvm.*

// TODO: emoji
public val ActivitySpec.displayName: String get() = type.format.replace("{name}", name)

@BuilderDsl
public fun Activity.Builder.playing(name: String) {
    this.type = Activity.Type.Game
    this.name = name
    this.url = Box.absent
}

@BuilderDsl
public fun Activity.Builder.streaming(name: String, url: Url? = null) {
    this.type = Activity.Type.Streaming
    this.name = name
    this.url = Box.maybe(url)
}

@BuilderDsl
public fun Activity.Builder.streaming(name: String, url: String? = null) {
    streaming(name, url?.let { Url(it) })
}

@BuilderDsl
public fun Activity.Builder.listening(name: String) {
    this.type = Activity.Type.Listening
    this.name = name
    this.url = Box.absent
}

@BuilderDsl
public fun Activity.Builder.watching(name: String) {
    this.type = Activity.Type.Watching
    this.name = name
    this.url = Box.absent
}

@BuilderDsl
public fun Activity.Builder.competing(name: String) {
    this.type = Activity.Type.Competing
    this.name = name
    this.url = Box.absent
}

public operator fun ActivitySpec.contains(flag: Activity.Flag): Boolean {
    val flags = flags ?: return false
    return flag in flags
}

@BuilderDsl
public fun Activity.Builder.button(label: String, url: Url): Unit = button(Activity.Button(label = label, url = url))

@BuilderDsl
public fun Activity.Builder.button(label: String, url: String): Unit = button(label, Url(url))

public val ActivitySpec.largeImage: Image.ApplicationAsset?
    get() {
        val applicationId = applicationId ?: return null
        val assetId = assets?.largeImage ?: return null
        return Image.ApplicationAsset(applicationId, assetId)
    }
public val ActivitySpec.smallImage: Image.ApplicationAsset?
    get() {
        val applicationId = applicationId ?: return null
        val assetId = assets?.smallImage ?: return null
        return Image.ApplicationAsset(applicationId, assetId)
    }
