/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.api

import kiscord.serialization.*
import kotlinx.serialization.*
import kotlinx.serialization.builtins.*
import kotlinx.serialization.descriptors.*
import kotlinx.serialization.encoding.*
import kotlinx.serialization.json.*

@OptIn(ExperimentalSerializationApi::class)
internal object InteractionSerializer : KSerializer<Interaction> {
    override val descriptor: SerialDescriptor = buildClassSerialDescriptor("kiscord.api.Interaction") {
        element("id", Snowflake.serializer().descriptor, isOptional = false)
        element("application_id", Snowflake.serializer().descriptor, isOptional = false)
        element("type", Interaction.Type.serializer().descriptor, isOptional = false)
        element("data", Interaction.Data.serializer().descriptor, isOptional = true)
        element("guild_id", Snowflake.serializer().descriptor, isOptional = true)
        element("channel_id", Snowflake.serializer().descriptor, isOptional = true)
        element("member", GuildMember.serializer().descriptor, isOptional = true)
        element("user", User.serializer().descriptor, isOptional = true)
        element("token", String.serializer().descriptor, isOptional = false)
        element("version", Int.serializer().descriptor, isOptional = false)
        element("message", Message.serializer().descriptor, isOptional = true)
        element("locale", Locale.serializer().descriptor, isOptional = true)
        element("guild_locale", Locale.serializer().descriptor, isOptional = true)
    }

    override fun deserialize(decoder: Decoder): Interaction = decoder.decodeStructure(descriptor) {
        var id: Snowflake? = null
        var applicationId: Snowflake? = null
        var type: Interaction.Type? = null
        var data: Interaction.Data? = null
        var dataJson: JsonObject? = null
        var guildId: Snowflake? = null
        var channelId: Snowflake? = null
        var member: GuildMember? = null
        var user: User? = null
        var token: String? = null
        var version: Int? = null
        var message: Message? = null
        var locale: Locale? = null
        var guildLocale: Locale? = null

        loop@ while (true) {
            when (val index = decodeElementIndex(descriptor)) {
                CompositeDecoder.DECODE_DONE -> break@loop
                0 -> id = decodeSerializableElement(descriptor, index, Snowflake.serializer())
                1 -> applicationId = decodeSerializableElement(descriptor, index, Snowflake.serializer())
                2 -> type = decodeSerializableElement(descriptor, index, Interaction.Type.serializer())
                3 -> {
                    if (type == null) {
                        check(this is JsonDecoder)
                        dataJson = decodeSerializableElement(descriptor, index, JsonObject.serializer())
                        continue
                    }
                    data = when (type) {
                        Interaction.Type.ApplicationCommand, Interaction.Type.ApplicationCommandAutocomplete -> decodeSerializableElement(
                            descriptor, index, Interaction.Data.Command.serializer()
                        )

                        Interaction.Type.MessageComponent -> decodeSerializableElement(
                            descriptor, index, Interaction.Data.Component.serializer()
                        )

                        Interaction.Type.ModalSubmit -> decodeSerializableElement(
                            descriptor, index, Interaction.Data.ModalSubmit.serializer()
                        )

                        else -> throw SerializationException("Unsupported data for type $type")
                    }
                }

                4 -> guildId = decodeSerializableElement(descriptor, index, Snowflake.serializer())
                5 -> channelId = decodeSerializableElement(descriptor, index, Snowflake.serializer())
                6 -> member = decodeSerializableElement(descriptor, index, GuildMember.serializer())
                7 -> user = decodeSerializableElement(descriptor, index, User.serializer())
                8 -> token = decodeStringElement(descriptor, index)
                9 -> version = decodeIntElement(descriptor, index)
                10 -> message = decodeSerializableElement(descriptor, index, Message.serializer())
                11 -> locale = decodeSerializableElement(descriptor, index, Locale.serializer())
                12 -> guildLocale = decodeSerializableElement(descriptor, index, Locale.serializer())
                else -> throw UnknownFieldException(index)
            }
        }

        if (dataJson != null) {
            check(data == null && this is JsonDecoder)
            data = when (type) {
                Interaction.Type.ApplicationCommand, Interaction.Type.ApplicationCommandAutocomplete ->
                    json.decodeFromJsonElement(Interaction.Data.Command.serializer(), dataJson)

                Interaction.Type.MessageComponent ->
                    json.decodeFromJsonElement(Interaction.Data.Component.serializer(), dataJson)

                Interaction.Type.ModalSubmit ->
                    json.decodeFromJsonElement(Interaction.Data.ModalSubmit.serializer(), dataJson)

                else -> throw SerializationException("Unsupported data for type $type")
            }
        }

        val missingFields = mutableListOf<String>()
        if (id == null) missingFields += "id"
        if (applicationId == null) missingFields += "application_id"
        if (type == null) missingFields += "type"
        if (token == null) missingFields += "token"
        if (version == null) missingFields += "version"
        if (data == null && (type == Interaction.Type.ApplicationCommand || type == Interaction.Type.MessageComponent))
            missingFields += "data"

        if (missingFields.isNotEmpty()) {
            throw MissingFieldException(missingFields, descriptor.serialName)
        }

        Interaction(
            id = id!!,
            applicationId = applicationId!!,
            type = type!!,
            data = data,
            guildId = guildId,
            channelId = channelId,
            member = member,
            user = user,
            token = token!!,
            version = version!!,
            message = message,
            locale = locale,
            guildLocale = guildLocale,
        )
    }

    override fun serialize(encoder: Encoder, value: Interaction): Unit = encoder.encodeStructure(descriptor) {
        encodeSerializableElement(descriptor, 0, Snowflake.serializer(), value.id)
        encodeSerializableElement(descriptor, 1, Snowflake.serializer(), value.applicationId)
        encodeSerializableElement(descriptor, 2, Interaction.Type.serializer(), value.type)
        when (val data = value.data) {
            is Interaction.Data.Command -> {
                check(value.type == Interaction.Type.ApplicationCommand) { "Invalid data type ${data::class} for interaction of type ${value.type}" }
                encodeSerializableElement(descriptor, 3, Interaction.Data.Command.serializer(), data)
            }

            is Interaction.Data.Component -> {
                check(value.type == Interaction.Type.MessageComponent) { "Invalid data type ${data::class} for interaction of type ${value.type}" }
                encodeSerializableElement(descriptor, 3, Interaction.Data.Component.serializer(), data)
            }

            is Interaction.Data.ModalSubmit -> {
                check(value.type == Interaction.Type.ModalSubmit) { "Invalid data type ${data::class} for interaction of type ${value.type}" }
                encodeSerializableElement(descriptor, 3, Interaction.Data.ModalSubmit.serializer(), data)
            }

            null -> {
                check(value.type == Interaction.Type.Ping) { "Expected no data for interaction of type ${value.type}" }
            }
        }
        if (value.guildId != null || shouldEncodeElementDefault(descriptor, 4))
            encodeNullableSerializableElement(descriptor, 4, Snowflake.serializer(), value.guildId)
        if (value.channelId != null || shouldEncodeElementDefault(descriptor, 5))
            encodeNullableSerializableElement(descriptor, 5, Snowflake.serializer(), value.channelId)
        if (value.member != null || shouldEncodeElementDefault(descriptor, 6))
            encodeNullableSerializableElement(descriptor, 6, GuildMember.serializer(), value.member)
        if (value.user != null || shouldEncodeElementDefault(descriptor, 7))
            encodeNullableSerializableElement(descriptor, 7, User.serializer(), value.user)
        encodeStringElement(descriptor, 8, value.token)
        encodeIntElement(descriptor, 9, value.version)
        if (value.message != null || shouldEncodeElementDefault(descriptor, 10))
            encodeNullableSerializableElement(descriptor, 10, Message.serializer(), value.message)
        if (value.locale != null || shouldEncodeElementDefault(descriptor, 11))
            encodeNullableSerializableElement(descriptor, 11, Locale.serializer(), value.locale)
        if (value.guildLocale != null || shouldEncodeElementDefault(descriptor, 12))
            encodeNullableSerializableElement(descriptor, 12, Locale.serializer(), value.guildLocale)
    }
}
