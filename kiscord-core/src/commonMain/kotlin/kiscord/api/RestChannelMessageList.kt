/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.api

import kiscord.*
import kiscord.builder.*
import kotlinx.coroutines.flow.*
import kotlinx.datetime.*

public class RestChannelMessageList(
    public val client: Kiscord,
    public val channelId: Snowflake,
    private val filter: MessageFilter = MessageFilter.All
) : MessageList {
    override suspend fun get(id: Snowflake): Message {
        if (!filter.isAcceptableId(id)) throw NoSuchElementException("Message with id $id not found")
        val message = client.channel.getChannelMessage(channelId, id).takeIf { filter.isAcceptable(it) }
        return message ?: throw NoSuchElementException("Message with id $id not found")
    }

    private inline fun copyWithFilter(filter: MessageFilter.Builder.() -> Unit) =
        RestChannelMessageList(client, channelId, MessageFilter.composite {
            add(this@RestChannelMessageList.filter)
            filter()
        })

    override fun subList(fromId: Snowflake?, toId: Snowflake?, toInclusive: Boolean): MessageList = copyWithFilter {
        if (fromId != null) fromId(fromId)
        if (toId != null) toId(toId, toInclusive)
    }

    override fun subList(fromDate: Instant?, toDate: Instant?, toInclusive: Boolean): MessageList = copyWithFilter {
        if (fromDate != null) fromDate(fromDate)
        if (toDate != null) toDate(toDate, toInclusive)
    }

    override fun filterBy(filter: MessageFilter): MessageList = copyWithFilter {
        add(filter)
    }

    override fun asFlow(): Flow<Message> = flow {
        val iterator = iterator()
        while (iterator.hasNext()) {
            emit(iterator.next())
        }
    }

    override fun iterator(): MessageList.MessageIterator = Iterator()

    private companion object {
        private const val FETCH_SIZE: Int = 100 // max allowed by getChannelMessages
    }

    private object MessageComparator : Comparator<Message> {
        override fun compare(a: Message, b: Message): Int {
            return b.id compareTo a.id
        }
    }

    private inner class Iterator : MessageList.MessageIterator {
        private var ended: Boolean = false
        private lateinit var firstKnownMessageId: Snowflake
        private val buffer = ArrayDeque<Message>(FETCH_SIZE)

        override suspend fun next(): Message {
            return fetch(pop = true) ?: throw NoSuchElementException()
        }

        override suspend fun hasNext(): Boolean {
            return fetch(pop = false) != null
        }

        private suspend fun fetch(pop: Boolean): Message? {
            if (ended) return null
            if (buffer.isEmpty()) {
                val messages = if (::firstKnownMessageId.isInitialized) {
                    client.channel.getChannelMessages(
                        channelId,
                        before = firstKnownMessageId,
                        limit = FETCH_SIZE
                    )
                } else {
                    client.channel.getChannelMessages(channelId, limit = FETCH_SIZE)
                }.filter { filter.isAcceptable(it) }
                if (messages.isEmpty()) {
                    ended = true
                    return null
                }
                val sorted = messages.sortedWith(MessageComparator)
                firstKnownMessageId = sorted.last().id
                buffer.addAll(sorted)
            }
            return if (pop) {
                buffer.removeFirst()
            } else {
                buffer.first()
            }
        }
    }
}
