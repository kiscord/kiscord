/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.api

import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.request.*
import io.ktor.http.*
import io.ktor.util.*
import kiscord.*
import kiscord.ktor.*
import kotlinx.atomicfu.*
import kotlinx.coroutines.*
import kotlinx.serialization.*
import kotlin.time.Duration.Companion.minutes
import kotlin.time.Duration.Companion.seconds

public sealed class Token private constructor(public val type: Type = Type.Bot) {
    public enum class Type {
        /**
         * Bot token, obtainable at [developer portal](https://discord.com/developers/applications/)
         */
        Bot,

        /**
         * Bearer token, obtainable through oauth flows
         */
        Bearer,

        /**
         * Raw token, appended to `Authorization` header as-is.
         */
        Raw,

        /**
         * Client credentials grant, mainly used for bot-less webhook-based interaction apps.
         *
         * [Discord API Reference](https://discord.com/developers/docs/topics/oauth2#client-credentials-grant)
         */
        ClientCredentials,
    }

    @KiscordUnstableAPI
    public open val tokenValue: String
        get() = throw IllegalArgumentException("This token type doesn't support retrieving token value")

    internal abstract suspend fun getAuthorizationHeader(client: HttpClient): String?

    private class Stored(override val tokenValue: String, type: Type) : Token(type) {
        override suspend fun getAuthorizationHeader(client: HttpClient): String? {
            if (tokenValue.isBlank()) return null
            return when (type) {
                Type.Bot -> "Bot $tokenValue"
                Type.Bearer -> "Bearer $tokenValue"
                Type.Raw -> tokenValue
                else -> throw IllegalArgumentException("Unsupported token type: $type")
            }
        }
    }

    private class ClientCredentials(
        private val clientId: Snowflake,
        private val clientSecret: String,
        private val scopes: List<String>
    ) : Token(Type.ClientCredentials) {
        private val initializedToken = atomic<Token?>(null)

        override suspend fun getAuthorizationHeader(client: HttpClient): String? {
            val initToken = initializedToken.value
            if (initToken != null)
                return initToken.getAuthorizationHeader(client)

            val response = client.post("${BuildConfig.DISCORD_API_URL}/oauth2/token") {
                parameter("grant_type", "client_credentials")
                parameter("scope", scopes.joinToString(separator = " "))
                header(HttpHeaders.Authorization, "Basic " + "$clientId:$clientSecret".encodeBase64())
                anonymous()
            }.body<TokenResponse>()

            val newToken = when (val t = response.tokenType) {
                "Bearer" -> Stored(response.accessToken, Type.Bearer)
                else -> throw IllegalStateException("Unsupported token type from client credentials grant: $t")
            }
            return if (initializedToken.compareAndSet(null, newToken)) {
                client.launch {
                    delay(response.expiresIn.seconds - 1.minutes)
                    initializedToken.lazySet(null)
                }
                newToken.getAuthorizationHeader(client)
            } else getAuthorizationHeader(client)
        }

        @Serializable
        private data class TokenResponse(
            @SerialName("access_token")
            val accessToken: String,
            @SerialName("token_type")
            val tokenType: String,
            @SerialName("expires_in")
            val expiresIn: Int,
        )
    }

    private data object Empty : Token(Type.Raw) {
        override suspend fun getAuthorizationHeader(client: HttpClient): String? = null
    }

    public companion object {
        public fun bot(token: String): Token = Stored(token, Type.Bot)
        public fun bearer(token: String): Token = Stored(token, Type.Bearer)
        public fun raw(token: String): Token = Stored(token, Type.Raw)
        public fun clientCredentials(clientId: Snowflake, clientSecret: String, scopes: List<String>): Token =
            ClientCredentials(clientId, clientSecret, scopes)

        public fun clientCredentials(clientId: Snowflake, clientSecret: String, vararg scopes: String): Token =
            ClientCredentials(clientId, clientSecret, scopes.toList())

        public val Default: Token get() = Empty
    }
}

