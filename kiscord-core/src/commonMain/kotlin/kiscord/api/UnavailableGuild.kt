/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.api

import kotlinx.serialization.*
import kotlinx.serialization.builtins.*
import kotlinx.serialization.descriptors.*
import kotlinx.serialization.encoding.*
import kotlinx.serialization.json.*

@Serializable
public data class UnavailableGuild(
    @Required
    override val id: Snowflake,
    @Required
    override val unavailable: Boolean
) : GuildSpec {
    override val name: Nothing get() = error()
    override val icon: Nothing get() = error()
    override val iconHash: Nothing get() = error()
    override val splash: Nothing get() = error()
    override val discoverySplash: Nothing get() = error()
    override val owner: Nothing get() = error()
    override val ownerId: Nothing get() = error()
    override val permissions: Nothing get() = error()
    override val afkChannelId: Nothing get() = error()
    override val afkTimeout: Nothing get() = error()
    override val verificationLevel: Nothing get() = error()
    override val defaultMessageNotifications: Nothing get() = error()
    override val explicitContentFilter: Nothing get() = error()
    override val roles: Nothing get() = error()
    override val emojis: Nothing get() = error()
    override val features: Nothing get() = error()
    override val mfaLevel: Nothing get() = error()
    override val applicationId: Nothing get() = error()
    override val widgetEnabled: Nothing get() = error()
    override val widgetChannelId: Nothing get() = error()
    override val systemChannelId: Nothing get() = error()
    override val systemChannelFlags: Nothing get() = error()
    override val rulesChannelId: Nothing get() = error()
    override val joinedAt: Nothing get() = error()
    override val large: Nothing get() = error()
    override val memberCount: Nothing get() = error()
    override val voiceStates: Nothing get() = error()
    override val members: Nothing get() = error()
    override val channels: Nothing get() = error()
    override val threads: Nothing get() = error()
    override val maxPresences: Nothing get() = error()
    override val maxMembers: Nothing get() = error()
    override val vanityUrlCode: Nothing get() = error()
    override val description: Nothing get() = error()
    override val banner: Nothing get() = error()
    override val premiumTier: Nothing get() = error()
    override val premiumSubscriptionCount: Nothing get() = error()
    override val preferredLocale: Nothing get() = error()
    override val publicUpdatesChannelId: Nothing get() = error()
    override val maxVideoChannelUsers: Nothing get() = error()
    override val approximateMemberCount: Nothing get() = error()
    override val approximatePresenceCount: Nothing get() = error()
    override val welcomeScreen: Nothing get() = error()
    override val nsfwLevel: Nothing get() = error()
    override val stageInstances: Nothing get() = error()
    override val stickers: Nothing get() = error()
    override val guildScheduledEvents: Nothing get() = error()
    override val premiumProgressBarEnabled: Nothing get() = error()

    private fun error(): Nothing = throw IllegalStateException("Guild is unavailable")

    @KiscordUnstableAPI
    public object SpecSerializer : KSerializer<GuildSpec> {
        @OptIn(ExperimentalSerializationApi::class)
        override val descriptor: SerialDescriptor =
            SerialDescriptor("kiscord.api.GuildSpec", Guild.serializer().descriptor)

        override fun serialize(encoder: Encoder, value: GuildSpec) {
            when (value) {
                is Guild -> Guild.serializer().serialize(encoder, value)
                is UnavailableGuild -> serializer().serialize(encoder, value)
                else -> throw IllegalArgumentException("Unknown guild implementation: ${value::class}")
            }
        }

        override fun deserialize(decoder: Decoder): GuildSpec {
            when (decoder) {
                is JsonDecoder -> {
                    val json = decoder.decodeJsonElement() as JsonObject
                    val deserializer: DeserializationStrategy<GuildSpec> =
                        when (json["unavailable"]?.jsonPrimitive?.boolean) {
                            true -> serializer()
                            else -> Guild.serializer()
                        }
                    return decoder.json.decodeFromJsonElement(deserializer, json)
                }
                // TODO: ETF
                else -> throw IllegalStateException("Unsupported decoder: $decoder")
            }
        }
    }

    @KiscordUnstableAPI
    public object SpecSetSerializer : KSerializer<Set<GuildSpec>> by SetSerializer(SpecSerializer)
}
