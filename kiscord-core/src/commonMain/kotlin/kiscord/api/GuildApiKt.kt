/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

@file:JvmMultifileClass
@file:JvmName(name = "GuildApiKt")

package kiscord.api

import kotlinx.coroutines.flow.*
import kotlin.jvm.*

public fun GuildApi.listAllGuildMembers(guildId: Snowflake, limitPerChunk: Int = 1000): Flow<GuildMember> = flow {
    check(limitPerChunk in 1..1000) { "limitPerChunk should be positive number less then a 1000" }
    var chunk = listGuildMembers(guildId, limit = limitPerChunk)
    var highestMemberId = Snowflake(0UL)
    while (true) {
        chunk.forEach { member ->
            val user = member.user ?: throw IllegalStateException("User shouldn't be null")
            if (user.id > highestMemberId) {
                highestMemberId = user.id
            }
            emit(member)
        }
        if (chunk.size < limitPerChunk) break
        chunk = listGuildMembers(guildId, limit = limitPerChunk, after = highestMemberId)
    }
}
