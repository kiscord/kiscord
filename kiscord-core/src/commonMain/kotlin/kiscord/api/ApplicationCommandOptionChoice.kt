/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.api

import kiscord.builder.*
import kotlinx.serialization.*
import kotlinx.serialization.builtins.*
import kotlinx.serialization.descriptors.*
import kotlinx.serialization.encoding.*
import kotlinx.serialization.json.*

@Serializable(with = ApplicationCommandOptionChoice.Serializer::class)
public class ApplicationCommandOptionChoice private constructor(
    public val name: String,
    public val kind: Kind,
    public val value: Any,
    public val nameLocalizations: Map<Locale, String>,
) {
    public constructor(name: String, value: String, nameLocalizations: Map<Locale, String> = emptyMap()) :
        this(name, Kind.String, value, nameLocalizations)

    public constructor(name: String, value: Long, nameLocalizations: Map<Locale, String> = emptyMap()) :
        this(name, Kind.Long, value, nameLocalizations)

    public constructor(name: String, value: Double, nameLocalizations: Map<Locale, String> = emptyMap()) :
        this(name, Kind.Double, value, nameLocalizations)

    public enum class Kind {
        String,
        Long,
        Double,
    }

    public val string: String
        get() {
            check(kind == Kind.String) { "Invalid choice kind" }
            return value as String
        }
    public val long: Long
        get() {
            check(kind == Kind.Long) { "Invalid choice kind" }
            return value as Long
        }
    public val double: Double
        get() {
            check(kind == Kind.Double) { "Invalid choice kind" }
            return value as Double
        }

    override fun toString(): String = "$name = $value"

    override fun equals(other: Any?): Boolean = other is ApplicationCommandOptionChoice &&
        name == other.name && kind == other.kind && value == other.value && nameLocalizations == other.nameLocalizations

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + kind.hashCode()
        result = 31 * result + value.hashCode()
        result = 31 * result + nameLocalizations.hashCode()
        return result
    }

    internal object Serializer : KSerializer<ApplicationCommandOptionChoice> {
        override val descriptor: SerialDescriptor =
            buildClassSerialDescriptor("kiscord.api.ApplicationCommandOptionChoice") {
                element("name", String.serializer().descriptor)
                element("value", JsonPrimitive.serializer().descriptor)
            }

        override fun deserialize(decoder: Decoder): ApplicationCommandOptionChoice {
            val composite = decoder.beginStructure(descriptor)
            var name: String? = null
            var value: JsonPrimitive? = null
            while (true) {
                when (val index = composite.decodeElementIndex(descriptor)) {
                    0 -> name = composite.decodeStringElement(descriptor, 0)
                    1 -> value = composite.decodeSerializableElement(descriptor, 1, JsonPrimitive.serializer())
                    CompositeDecoder.DECODE_DONE -> break
                    else -> error("Unexpected index: $index")
                }
            }
            composite.endStructure(descriptor)

            if (name == null) throw SerializationException("Missing name")
            if (value == null) throw SerializationException("Missing value")

            if (value.isString) return ApplicationCommandOptionChoice(name, value.content)
            value.longOrNull?.let { return ApplicationCommandOptionChoice(name, it) }
            value.doubleOrNull?.let { return ApplicationCommandOptionChoice(name, it) }
            throw SerializationException("Unknown primitive kind for value: $value")
        }

        override fun serialize(encoder: Encoder, value: ApplicationCommandOptionChoice) {
            val composite = encoder.beginStructure(descriptor)
            composite.encodeStringElement(descriptor, 0, value.name)
            when (value.kind) {
                Kind.String -> composite.encodeStringElement(descriptor, 1, value.string)
                Kind.Long -> composite.encodeLongElement(descriptor, 1, value.long)
                Kind.Double -> composite.encodeDoubleElement(descriptor, 1, value.double)
            }
            composite.endStructure(descriptor)
        }
    }
}

public val ApplicationCommandOptionChoice.number: Number
    get() = when (kind) {
        ApplicationCommandOptionChoice.Kind.Long -> long
        ApplicationCommandOptionChoice.Kind.Double -> double
        else -> throw IllegalArgumentException("Unable to decode choice as number")
    }

@BuilderDsl
public fun ApplicationCommandOption.Builder.choice(name: String, value: String) {
    choicesOrCreate().add(ApplicationCommandOptionChoice(name, value))
}

@BuilderDsl
public fun ApplicationCommandOption.Builder.choice(name: String, value: Long) {
    choicesOrCreate().add(ApplicationCommandOptionChoice(name, value))
}

@BuilderDsl
public fun ApplicationCommandOption.Builder.choice(name: String, value: Double) {
    choicesOrCreate().add(ApplicationCommandOptionChoice(name, value))
}


@BuilderDsl
public fun Interaction.Response.Data.Autocomplete.Builder.choice(name: String, value: String) {
    choices.add(ApplicationCommandOptionChoice(name, value))
}

@BuilderDsl
public fun Interaction.Response.Data.Autocomplete.Builder.choice(name: String, value: Long) {
    choices.add(ApplicationCommandOptionChoice(name, value))
}

@BuilderDsl
public fun Interaction.Response.Data.Autocomplete.Builder.choice(name: String, value: Double) {
    choices.add(ApplicationCommandOptionChoice(name, value))
}
