/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.api

import kiscord.serialization.*
import kotlinx.serialization.*
import kotlinx.serialization.builtins.*
import kotlinx.serialization.descriptors.*
import kotlinx.serialization.encoding.*
import kotlinx.serialization.json.*

@OptIn(ExperimentalSerializationApi::class)
internal object AutoModerationRuleSerializer : KSerializer<AutoModerationRule> {
    override val descriptor: SerialDescriptor = buildClassSerialDescriptor("kiscord.api.AutoModerationRule") {
        element("id", Snowflake.serializer().descriptor)
        element("guild_id", Snowflake.serializer().descriptor)
        element("name", String.serializer().descriptor)
        element("creator_id", Snowflake.serializer().descriptor)
        element("event_type", AutoModerationRule.EventType.serializer().descriptor)
        element("trigger_type", AutoModerationRule.TriggerType.serializer().descriptor)
        element("trigger_metadata", AutoModerationRule.TriggerMetadata.serializer().descriptor)
        element("actions", ListSerializer(AutoModerationAction.serializer()).descriptor)
        element("enabled", Boolean.serializer().descriptor)
        element("exempt_roles", ListSerializer(Snowflake.serializer()).descriptor)
        element("exempt_channels", ListSerializer(Snowflake.serializer()).descriptor)
    }

    override fun serialize(encoder: Encoder, value: AutoModerationRule): Unit = encoder.encodeStructure(descriptor) {
        encodeSerializableElement(descriptor, 0, Snowflake.serializer(), value.id)
        encodeSerializableElement(descriptor, 1, Snowflake.serializer(), value.guildId)
        encodeSerializableElement(descriptor, 2, String.serializer(), value.name)
        encodeSerializableElement(descriptor, 3, Snowflake.serializer(), value.creatorId)
        encodeSerializableElement(descriptor, 4, AutoModerationRule.EventType.serializer(), value.eventType)
        encodeSerializableElement(descriptor, 5, AutoModerationRule.TriggerType.serializer(), value.triggerType)
        when (val triggerMetadata = value.triggerMetadata) {
            is AutoModerationRule.TriggerMetadata.Keyword -> {
                check(value.triggerType == AutoModerationRule.TriggerType.Keyword) { "Invalid trigger metadata type ${triggerMetadata::class} for rule with trigger of type ${value.triggerType}" }
                encodeSerializableElement(
                    descriptor, 6,
                    AutoModerationRule.TriggerMetadata.Keyword.serializer(), triggerMetadata
                )
            }

            is AutoModerationRule.TriggerMetadata.Spam -> {
                check(value.triggerType == AutoModerationRule.TriggerType.Spam) { "Invalid trigger metadata type ${triggerMetadata::class} for rule with trigger of type ${value.triggerType}" }
                encodeSerializableElement(
                    descriptor, 6,
                    AutoModerationRule.TriggerMetadata.Spam.serializer(), triggerMetadata
                )
            }

            is AutoModerationRule.TriggerMetadata.KeywordPreset -> {
                check(value.triggerType == AutoModerationRule.TriggerType.KeywordPreset) { "Invalid trigger metadata type ${triggerMetadata::class} for rule with trigger of type ${value.triggerType}" }
                encodeSerializableElement(
                    descriptor, 6,
                    AutoModerationRule.TriggerMetadata.KeywordPreset.serializer(), triggerMetadata
                )
            }

            is AutoModerationRule.TriggerMetadata.MentionSpam -> {
                check(value.triggerType == AutoModerationRule.TriggerType.MentionSpam) { "Invalid trigger metadata type ${triggerMetadata::class} for rule with trigger of type ${value.triggerType}" }
                encodeSerializableElement(
                    descriptor, 6,
                    AutoModerationRule.TriggerMetadata.MentionSpam.serializer(), triggerMetadata
                )
            }
        }
        encodeSerializableElement(descriptor, 7, ListSerializer(AutoModerationAction.serializer()), value.actions)
        encodeSerializableElement(descriptor, 8, Boolean.serializer(), value.enabled)
        encodeSerializableElement(descriptor, 9, ListSerializer(Snowflake.serializer()), value.exemptRoles)
        encodeSerializableElement(descriptor, 10, ListSerializer(Snowflake.serializer()), value.exemptChannels)
    }

    override fun deserialize(decoder: Decoder): AutoModerationRule = decoder.decodeStructure(descriptor) {
        var id: Snowflake? = null
        var guildId: Snowflake? = null
        var name: String? = null
        var creatorId: Snowflake? = null
        var eventType: AutoModerationRule.EventType? = null
        var triggerType: AutoModerationRule.TriggerType? = null
        var triggerMetadata: AutoModerationRule.TriggerMetadata? = null
        var triggerMetadataJson: JsonObject? = null
        var actions: List<AutoModerationAction>? = null
        var enabled: Boolean? = null
        var exemptRoles: List<Snowflake>? = null
        var exemptChannels: List<Snowflake>? = null

        loop@ while (true) {
            when (val index = decodeElementIndex(descriptor)) {
                CompositeDecoder.DECODE_DONE -> break@loop
                0 -> id = decodeSerializableElement(descriptor, index, Snowflake.serializer())
                1 -> guildId = decodeSerializableElement(descriptor, index, Snowflake.serializer())
                2 -> name = decodeSerializableElement(descriptor, index, String.serializer())
                3 -> creatorId = decodeSerializableElement(descriptor, index, Snowflake.serializer())
                4 -> eventType = decodeSerializableElement(descriptor, index, AutoModerationRule.EventType.serializer())
                5 -> triggerType =
                    decodeSerializableElement(descriptor, index, AutoModerationRule.TriggerType.serializer())

                6 -> {
                    if (triggerType == null) {
                        check(this is JsonDecoder)
                        triggerMetadataJson = decodeSerializableElement(descriptor, index, JsonObject.serializer())
                        continue
                    }
                    triggerMetadata = when (triggerType) {
                        AutoModerationRule.TriggerType.Keyword -> decodeSerializableElement(
                            descriptor, index,
                            AutoModerationRule.TriggerMetadata.Keyword.serializer()
                        )

                        AutoModerationRule.TriggerType.Spam -> decodeSerializableElement(
                            descriptor, index,
                            AutoModerationRule.TriggerMetadata.Spam.serializer()
                        )

                        AutoModerationRule.TriggerType.KeywordPreset -> decodeSerializableElement(
                            descriptor, index,
                            AutoModerationRule.TriggerMetadata.KeywordPreset.serializer()
                        )

                        AutoModerationRule.TriggerType.MentionSpam -> decodeSerializableElement(
                            descriptor, index,
                            AutoModerationRule.TriggerMetadata.MentionSpam.serializer()
                        )
                    }
                }

                7 -> actions =
                    decodeSerializableElement(descriptor, index, ListSerializer(AutoModerationAction.serializer()))

                8 -> enabled = decodeSerializableElement(descriptor, index, Boolean.serializer())
                9 -> exemptRoles = decodeSerializableElement(descriptor, index, ListSerializer(Snowflake.serializer()))
                10 -> exemptChannels =
                    decodeSerializableElement(descriptor, index, ListSerializer(Snowflake.serializer()))

                else -> throw UnknownFieldException(index)
            }
        }

        if (triggerMetadataJson != null) {
            check(triggerMetadata == null && this is JsonDecoder)
            triggerMetadata = when (triggerType) {
                AutoModerationRule.TriggerType.Keyword -> json.decodeFromJsonElement(
                    AutoModerationRule.TriggerMetadata.Keyword.serializer(), triggerMetadataJson
                )

                AutoModerationRule.TriggerType.Spam -> json.decodeFromJsonElement(
                    AutoModerationRule.TriggerMetadata.Spam.serializer(), triggerMetadataJson
                )

                AutoModerationRule.TriggerType.KeywordPreset -> json.decodeFromJsonElement(
                    AutoModerationRule.TriggerMetadata.KeywordPreset.serializer(), triggerMetadataJson
                )

                AutoModerationRule.TriggerType.MentionSpam -> json.decodeFromJsonElement(
                    AutoModerationRule.TriggerMetadata.MentionSpam.serializer(), triggerMetadataJson
                )

                null -> throw SerializationException("Unsupported trigger metadata for trigger type $triggerType")
            }
        }

        val missingFields = mutableListOf<String>()
        if (id == null) missingFields += "id"
        if (guildId == null) missingFields += "guild_id"
        if (name == null) missingFields += "name"
        if (creatorId == null) missingFields += "creator_id"
        if (eventType == null) missingFields += "event_type"
        if (triggerType == null) missingFields += "trigger_type"
        if (triggerMetadata == null) missingFields += "trigger_metadata"
        if (actions == null) missingFields += "actions"
        if (enabled == null) missingFields += "enabled"
        if (exemptRoles == null) missingFields += "exempt_roles"
        if (exemptChannels == null) missingFields += "exempt_channels"

        if (missingFields.isNotEmpty()) {
            throw MissingFieldException(missingFields, AutoModerationActionSerializer.descriptor.serialName)
        }

        AutoModerationRule(
            id = id!!,
            guildId = guildId!!,
            name = name!!,
            creatorId = creatorId!!,
            eventType = eventType!!,
            triggerType!!,
            triggerMetadata = triggerMetadata!!,
            actions = actions!!,
            enabled = enabled!!,
            exemptRoles = exemptRoles!!,
            exemptChannels = exemptChannels!!,
        )
    }
}
