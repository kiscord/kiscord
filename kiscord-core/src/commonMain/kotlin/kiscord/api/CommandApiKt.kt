/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

@file:JvmMultifileClass
@file:JvmName(name = "CommandApiKt")

package kiscord.api

import kiscord.*
import kiscord.builder.*
import kotlin.jvm.*

/**
 * Respond to the interaction with [response]
 *
 * Must be called in [KiscordCall] context
 *
 * @see createInteractionResponse
 */
@DiscordApiDsl
public suspend fun Interaction.respond(response: Interaction.Response): Boolean {
    return kiscord().command.createInteractionResponse(id, token, response)
}

/**
 * Respond to the interaction with [response builder][block]
 *
 * Must be called in [KiscordCall] context
 *
 * @see createInteractionResponse
 */
@DiscordApiDsl
public suspend inline fun Interaction.respond(@BuilderDsl block: Interaction.Response.Builder.() -> Unit): Boolean {
    return respond(Interaction.Response(block))
}

/**
 * Respond to the interaction with [response][Interaction.Response] with specified [type][Interaction.Response.Type] and optional [response data builder][block]
 *
 * Must be called in [KiscordCall] context
 *
 * @see createInteractionResponse
 */
@DiscordApiDsl
public suspend inline fun Interaction.respond(
    type: Interaction.Response.Type,
    @BuilderDsl crossinline block: Interaction.Response.Data.Message.Builder.() -> Unit = {}
): Boolean = respond {
    type(type)
    data(Interaction.Response.Data.Message(block))
}

/**
 * Must be called in [KiscordCall] context
 *
 * @see createInteractionResponse
 */
@DiscordApiDsl
public suspend inline fun Interaction.autocomplete(
    result: Interaction.Response.Data.Autocomplete
): Boolean = respond {
    type(Interaction.Response.Type.ApplicationCommandAutocompleteResult)
    data(result)
}

/**
 * Must be called in [KiscordCall] context
 *
 * @see createInteractionResponse
 */
@DiscordApiDsl
public suspend inline fun Interaction.autocomplete(
    @BuilderDsl crossinline block: Interaction.Response.Data.Autocomplete.Builder.() -> Unit = {}
): Boolean = autocomplete(Interaction.Response.Data.Autocomplete(block))

/**
 * Edits the initial Interaction response with [message]
 *
 * Must be called in [KiscordCall] context
 *
 * @see editOriginalInteractionResponse
 */
@DiscordApiDsl
public suspend fun Interaction.editOriginal(message: EditMessage): Message {
    return kiscord().command.editOriginalInteractionResponse(applicationId, token, message)
}

/**
 * Edits the initial Interaction response. with [message builder][block]
 *
 * Must be called in [KiscordCall] context
 *
 * @see editOriginalInteractionResponse
 */
@DiscordApiDsl
public suspend inline fun Interaction.editOriginal(@BuilderDsl block: EditMessage.Builder.() -> Unit): Message {
    return editOriginal(EditMessage(block))
}

/**
 * Deletes the initial Interaction response.
 *
 * Must be called in [KiscordCall] context
 *
 * @see deleteOriginalInteractionResponse
 */
@DiscordApiDsl
public suspend fun Interaction.deleteOriginal(): Boolean {
    return kiscord().command.deleteOriginalInteractionResponse(applicationId, token)
}

/**
 * Creates followup message.
 *
 * Must be called in [KiscordCall] context
 *
 * @see createFollowupMessage
 */
@DiscordApiDsl
public suspend fun Interaction.followUp(message: CreateMessage): Message {
    return kiscord().command.createFollowupMessage(applicationId, token, message)
}

/**
 * Creates followup message.
 *
 * Must be called in [KiscordCall] context
 *
 * @see createFollowupMessage
 */
@DiscordApiDsl
public suspend inline fun Interaction.followUp(@BuilderDsl block: CreateMessage.Builder.() -> Unit): Message {
    return followUp(CreateMessage(block))
}

/**
 * Edits the followup message.
 *
 * Must be called in [KiscordCall] context
 *
 * @see createFollowupMessage
 */
@DiscordApiDsl
public suspend fun Interaction.editFollowUp(messageId: Snowflake, message: EditMessage): Message {
    return kiscord().command.editFollowupMessage(applicationId, token, messageId, message)
}

/**
 * Edits the followup message.
 *
 * Must be called in [KiscordCall] context
 *
 * @see createFollowupMessage
 */
@DiscordApiDsl
public suspend inline fun Interaction.editFollowUp(
    messageId: Snowflake,
    @BuilderDsl block: EditMessage.Builder.() -> Unit
): Message {
    return editFollowUp(messageId, EditMessage(block))
}

/**
 * Deletes the followup message.
 *
 * Must be called in [KiscordCall] context
 *
 * @see createFollowupMessage
 */
@DiscordApiDsl
public suspend fun Interaction.deleteFollowUp(messageId: Snowflake): Boolean {
    return kiscord().command.deleteFollowupMessage(applicationId, token, messageId)
}

@BuilderDsl
public fun CommandApi.ApplicationCommandInfo.Builder.nameLocalization(vararg nameLocalizations: Pair<Locale, String>) {
    this.nameLocalizations = this.nameLocalizations.map { it.orEmpty() + nameLocalizations }
        .or { nameLocalizations.toMap() }
}

@BuilderDsl
public fun CommandApi.ApplicationCommandInfo.Builder.descriptionLocalization(vararg descriptionLocalizations: Pair<Locale, String>) {
    this.descriptionLocalizations = this.descriptionLocalizations.map { it.orEmpty() + descriptionLocalizations }
        .or { descriptionLocalizations.toMap() }
}
