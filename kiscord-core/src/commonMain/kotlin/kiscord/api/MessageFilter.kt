/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.api

import kiscord.builder.*
import kotlinx.datetime.*

/**
 * General-purpose interface for filtering messages.
 */
public fun interface MessageFilter {
    /**
     * Main filter predicate
     */
    public fun isAcceptable(message: Message): Boolean

    /**
     * Optional message id predicate. Not guaranteed to be called though, so [isAcceptable] must be implemented correctly anyway.
     */
    public fun isAcceptableId(messageId: Snowflake): Boolean = true

    @KiscordUnstableAPI
    public fun isStricterThan(filter: MessageFilter): Boolean = false

    @KiscordUnstableAPI
    public operator fun not(): MessageFilter = NegateFilter(this)

    @BuilderDsl
    public interface Builder : MutableSet<MessageFilter>, BuilderFor<MessageFilter>

    public companion object : BuilderSpec<MessageFilter, Builder> {
        override fun builder(): Builder = builder(Mode.And)
        public fun builder(mode: Mode): Builder = MessageFilterBuilderImpl(mode)
        public inline fun composite(mode: Mode = Mode.And, block: Builder.() -> Unit): MessageFilter =
            builder(mode).apply(block).build()
    }

    public object All : MessageFilter {
        override fun isAcceptable(message: Message): Boolean = true
        override fun isAcceptableId(messageId: Snowflake): Boolean = true
        override fun toString(): String = "all"
        override fun not(): MessageFilter = None
    }

    public object None : MessageFilter {
        override fun isAcceptable(message: Message): Boolean = false
        override fun isAcceptableId(messageId: Snowflake): Boolean = false
        override fun toString(): String = "none"
        override fun not(): MessageFilter = All
    }

    public enum class Mode {
        And {
            override val filter: MessageFilter get() = All
            override fun not(): Mode = Or
        },
        Or {
            override val filter: MessageFilter get() = None
            override fun not(): Mode = And
        },
        ;

        internal abstract val filter: MessageFilter
        internal abstract operator fun not(): Mode
    }
}

private class FromIdFilter(private val fromId: Snowflake, private val inclusive: Boolean = true) : MessageFilter {
    override fun isAcceptable(message: Message): Boolean = isAcceptableId(message.id)

    override fun isAcceptableId(messageId: Snowflake): Boolean = when (inclusive) {
        true -> messageId >= fromId
        false -> messageId > fromId
    }

    override fun isStricterThan(filter: MessageFilter): Boolean {
        if (filter !is FromIdFilter) return false
        return when (val compareResult = fromId.compareTo(filter.fromId)) {
            0 -> !inclusive && filter.inclusive
            else -> compareResult > 0
        }
    }

    override fun not(): MessageFilter = ToIdFilter(fromId, !inclusive)

    override fun hashCode(): Int = fromId.hashCode() * 31 + inclusive.hashCode()

    override fun equals(other: Any?): Boolean {
        return other is FromIdFilter && fromId == other.fromId && inclusive == other.inclusive
    }

    override fun toString(): String {
        return formatComparisonFilter("id", ">", fromId.toString(), inclusive)
    }
}

private class ToIdFilter(private val toId: Snowflake, private val inclusive: Boolean = true) : MessageFilter {
    override fun isAcceptable(message: Message): Boolean = isAcceptableId(message.id)

    override fun isAcceptableId(messageId: Snowflake): Boolean = when (inclusive) {
        true -> messageId <= toId
        false -> messageId < toId
    }

    override fun isStricterThan(filter: MessageFilter): Boolean {
        if (filter !is ToIdFilter) return false
        return when (val compareResult = toId.compareTo(filter.toId)) {
            0 -> !inclusive && filter.inclusive
            else -> compareResult < 0
        }
    }

    override fun not(): MessageFilter = FromIdFilter(toId, !inclusive)

    override fun hashCode(): Int = toId.hashCode() * 31 + inclusive.hashCode()

    override fun equals(other: Any?): Boolean {
        return other is ToIdFilter && toId == other.toId && inclusive == other.inclusive
    }

    override fun toString(): String {
        return formatComparisonFilter("id", "<", toId.toString(), inclusive)
    }
}

private class FromDateFilter(private val fromDate: Instant, private val inclusive: Boolean = true) : MessageFilter {
    override fun isAcceptable(message: Message): Boolean = when (inclusive) {
        true -> message.timestamp >= fromDate
        false -> message.timestamp > fromDate
    }

    override fun isAcceptableId(messageId: Snowflake): Boolean = when (inclusive) {
        true -> messageId.time >= fromDate
        false -> messageId.time > fromDate
    }

    override fun isStricterThan(filter: MessageFilter): Boolean {
        if (filter !is FromDateFilter) return false
        return when (val compareResult = fromDate.compareTo(filter.fromDate)) {
            0 -> !inclusive && filter.inclusive
            else -> compareResult > 0
        }
    }

    override fun not(): MessageFilter = ToDateFilter(fromDate, !inclusive)

    override fun hashCode(): Int = fromDate.hashCode() * 31 + inclusive.hashCode()

    override fun equals(other: Any?): Boolean {
        return other is FromDateFilter && fromDate == other.fromDate && inclusive == other.inclusive
    }

    override fun toString(): String {
        return formatComparisonFilter("date", ">", fromDate.toString(), inclusive)
    }
}

private class ToDateFilter(private val toDate: Instant, private val inclusive: Boolean = true) : MessageFilter {
    override fun isAcceptable(message: Message): Boolean = when (inclusive) {
        true -> message.timestamp <= toDate
        false -> message.timestamp < toDate
    }

    override fun isAcceptableId(messageId: Snowflake): Boolean = when (inclusive) {
        true -> messageId.time <= toDate
        false -> messageId.time < toDate
    }

    override fun isStricterThan(filter: MessageFilter): Boolean {
        if (filter !is ToDateFilter) return false
        return when (val compareResult = toDate.compareTo(filter.toDate)) {
            0 -> !inclusive && filter.inclusive
            else -> compareResult < 0
        }
    }

    override fun not(): MessageFilter = FromDateFilter(toDate, !inclusive)

    override fun hashCode(): Int = toDate.hashCode() * 31 + inclusive.hashCode()

    override fun equals(other: Any?): Boolean {
        return other is ToDateFilter && toDate == other.toDate && inclusive == other.inclusive
    }

    override fun toString(): String {
        return formatComparisonFilter("date", "<", toDate.toString(), inclusive)
    }
}

private class NegateFilter(private val filter: MessageFilter) : MessageFilter {
    override fun isAcceptable(message: Message): Boolean = !filter.isAcceptable(message)
    override fun isAcceptableId(messageId: Snowflake): Boolean = !filter.isAcceptableId(messageId)
    override fun hashCode(): Int = filter.hashCode().inv()
    override fun equals(other: Any?): Boolean = other is NegateFilter && filter == other.filter
    override fun not(): MessageFilter = filter
    override fun toString(): String = "!($filter)"
}

private class CompositeMessageFilter(val filters: Set<MessageFilter>, val mode: MessageFilter.Mode) :
    MessageFilter {
    override fun isAcceptable(message: Message): Boolean = when (mode) {
        MessageFilter.Mode.And -> filters.all { filter -> filter.isAcceptable(message) }
        MessageFilter.Mode.Or -> filters.any { filter -> filter.isAcceptable(message) }
    }

    override fun isAcceptableId(messageId: Snowflake): Boolean = when (mode) {
        MessageFilter.Mode.And -> filters.all { filter -> filter.isAcceptableId(messageId) }
        MessageFilter.Mode.Or -> filters.any { filter -> filter.isAcceptableId(messageId) }
    }

    override fun not(): MessageFilter = CompositeMessageFilter(
        filters.mapTo(mutableSetOf()) { it.not() }, when (mode) {
            MessageFilter.Mode.And -> MessageFilter.Mode.Or
            MessageFilter.Mode.Or -> MessageFilter.Mode.And
        }
    )

    override fun hashCode(): Int = filters.hashCode() * 31 + mode.hashCode()

    override fun equals(other: Any?): Boolean {
        return other is CompositeMessageFilter && filters == other.filters && mode == other.mode
    }

    override fun toString(): String = filters.joinToString(
        prefix = "(", postfix = ")",
        separator = when (mode) {
            MessageFilter.Mode.And -> " && "
            MessageFilter.Mode.Or -> " || "
        }
    )
}

private class MessageFilterBuilderImpl(
    val mode: MessageFilter.Mode
) : MessageFilter.Builder, MutableSet<MessageFilter> by HashSet() {
    override fun build(): MessageFilter {
        val saneFilters = mutableSetOf<MessageFilter>()

        fun processMode(filter: MessageFilter) {
            if (filter is CompositeMessageFilter && filter.mode == mode) {
                filter.filters.forEach { processMode(it) }
                return
            }

            if (filter === mode.filter) return
            val invFilter = !mode.filter
            if (filter == invFilter) {
                saneFilters.clear()
                saneFilters += invFilter
                return
            }

            val iterator = saneFilters.iterator()
            while (iterator.hasNext()) {
                val candidate = iterator.next()
                if (
                    mode == MessageFilter.Mode.And && filter.isStricterThan(candidate) ||
                    mode == MessageFilter.Mode.Or && candidate.isStricterThan(filter)
                ) {
                    iterator.remove()
                } else if (
                    mode == MessageFilter.Mode.And && candidate.isStricterThan(filter) ||
                    mode == MessageFilter.Mode.Or && filter.isStricterThan(candidate) ||
                    filter == candidate || candidate == invFilter
                ) {
                    // No need to apply provided filter, as the higher one already applied
                    return
                }
            }

            saneFilters += filter
        }

        forEach { processMode(it) }

        return when (saneFilters.size) {
            0 -> mode.filter
            1 -> saneFilters.single()
            else -> CompositeMessageFilter(saneFilters, mode)
        }
    }
}

private fun formatComparisonFilter(name: String, op: String, value: String, inclusive: Boolean): String =
    "$name $op${if (inclusive) "=" else ""} $value"

@BuilderDsl
public fun MessageFilter.Builder.fromId(id: Snowflake, inclusive: Boolean = true) {
    add(FromIdFilter(id, inclusive))
}

@BuilderDsl
public fun MessageFilter.Builder.toId(id: Snowflake, inclusive: Boolean = true) {
    add(ToIdFilter(id, inclusive))
}

@BuilderDsl
public fun MessageFilter.Builder.fromDate(date: Instant, inclusive: Boolean = true) {
    add(FromDateFilter(date, inclusive))
}

@BuilderDsl
public fun MessageFilter.Builder.toDate(date: Instant, inclusive: Boolean = true) {
    add(ToDateFilter(date, inclusive))
}

@BuilderDsl
public fun MessageFilter.Builder.negate(filter: MessageFilter) {
    add(!filter)
}

@BuilderDsl
public inline fun MessageFilter.Builder.negate(block: MessageFilter.Builder.() -> Unit) {
    negate(MessageFilter(block))
}

@BuilderDsl
public inline fun MessageFilter.Builder.and(block: MessageFilter.Builder.() -> Unit) {
    add(MessageFilter.builder(MessageFilter.Mode.And).apply(block).build())
}

@BuilderDsl
public inline fun MessageFilter.Builder.or(block: MessageFilter.Builder.() -> Unit) {
    add(MessageFilter.builder(MessageFilter.Mode.Or).apply(block).build())
}
