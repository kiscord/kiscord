/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

@file:OptIn(ExperimentalSerializationApi::class, InternalSerializationApi::class)

package kiscord.api

import kiscord.api.*
import kiscord.api.AuditLogChangeKey.*
import kotlinx.serialization.*
import kotlinx.serialization.builtins.*
import kotlinx.serialization.descriptors.*
import kotlinx.serialization.encoding.*
import kotlinx.serialization.json.*

/**
 * [Discord API Reference](https://discord.com/developers/docs/resources/audit-log#audit-log-entry-object)
 */
@Serializable
public data class AuditLogEntry(
    /**
     * id of the affected entity (webhook, user, role, etc.)
     */
    @Required
    @SerialName("target_id")
    public val targetId: String? = null,
    /**
     * changes made to the target_id
     */
    @Serializable(with = ChangesSerializer::class)
    public val changes: List<AuditLogChange<@Contextual Any>>? = null,
    /**
     * the user who made the changes
     */
    @Required
    @SerialName("user_id")
    public val userId: Snowflake? = null,
    /**
     * id of the entry
     */
    @Required
    public val id: Snowflake,
    /**
     * type of action that occurred
     */
    @Required
    @SerialName("action_type")
    public val actionType: AuditLogEvent,
    /**
     * the reason for the change
     */
    public val reason: String? = null
) {
    private object ChangesSerializer : KSerializer<List<AuditLogChange<Any>>?> {
        private val rawSerializer = ListSerializer(AuditLogChange.serializer(JsonElement.serializer())).nullable
        override val descriptor: SerialDescriptor get() = rawSerializer.descriptor

        override fun serialize(encoder: Encoder, value: List<AuditLogChange<Any>>?): Nothing =
            throw NotImplementedError()

        override fun deserialize(decoder: Decoder): List<AuditLogChange<Any>>? {
            check(decoder is JsonDecoder)
            val entries = rawSerializer.deserialize(decoder)
            return entries?.map { entry ->
                val valueSerializer = entry.key.deduceSerializer()
                val newValue = entry.newValue?.let { decoder.json.decodeFromJsonElement(valueSerializer, it) }
                val oldValue = entry.oldValue?.let { decoder.json.decodeFromJsonElement(valueSerializer, it) }
                AuditLogChange(key = entry.key, newValue = newValue, oldValue = oldValue)
            }
        }
    }
}

private object IntOrString : DeserializationStrategy<Any> {
    override val descriptor: SerialDescriptor = buildSerialDescriptor("IntOrString", PolymorphicKind.SEALED) {
        element("int", Int.serializer().descriptor, isOptional = true)
        element("string", String.serializer().descriptor, isOptional = true)
    }

    override fun deserialize(decoder: Decoder): Any {
        check(decoder is JsonDecoder)
        val json = decoder.decodeJsonElement().jsonPrimitive
        return when {
            json.isString -> json.content
            else -> json.content.toInt()
        }
    }
}

private fun AuditLogChangeKey.deduceSerializer(): DeserializationStrategy<Any> = when (this) {
    // String
    Name,
    Description,
    IconHash,
    SplashHash,
    DiscoverySplashHash,
    BannerHash,
    Region,
    PreferredLocale,
    VanityUrlCode,
    Topic,
    Code,
    Nick,
    AvatarHash,
    Tags,
    Asset,
    -> String.serializer()
    // Snowflake
    OwnerId,
    AfkChannelId,
    RulesChannelId,
    PublicUpdatesChannelId,
    WidgetChannelId,
    SystemChannelId,
    ApplicationId,
    ChannelId,
    InviterId,
    Id,
    GuildId,
    -> Snowflake.serializer()
    // Integer
    PruneDeleteDays,
    Position,
    Bitrate,
    RateLimitPerUser,
    Color,
    MaxUses,
    Uses,
    MaxAge,
    ExpireBehavior,
    ExpireGracePeriod,
    UserLimit,
    -> Int.serializer()
    // Boolean
    WidgetEnabled,
    NSFW,
    Hoist,
    Mentionable,
    Temporary,
    Deaf,
    Mute,
    EnableEmoticons,
    Available,
    Archived,
    Locked,
    -> Boolean.serializer()
    AfkTimeout -> Guild.AfkTimeout.serializer()
    MfaLevel -> Guild.MFALevel.serializer()
    VerificationLevel -> Guild.VerificationLevel.serializer()
    ExplicitContentFilter -> Guild.ExplicitContentFilterLevel.serializer()
    DefaultMessageNotifications -> Guild.DefaultMessageNotificationLevel.serializer()
    AddRole, RemoveRole -> Role.serializer()
    PermissionOverwrites -> ListSerializer(PermissionOverwrite.serializer())
    Permissions, Allow, Deny -> PermissionSet.serializer()
    Type -> IntOrString
    PrivacyLevel -> StageInstance.PrivacyLevel.serializer()
    FormatType -> Sticker.FormatType.serializer()
    AuditLogChangeKey.AutoArchiveDuration, DefaultAutoArchiveDuration -> AutoArchiveDuration.serializer()
}
