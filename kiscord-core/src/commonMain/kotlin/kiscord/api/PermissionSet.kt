/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.api

import kiscord.*
import kotlinx.serialization.*
import kotlinx.serialization.descriptors.*
import kotlinx.serialization.encoding.*
import org.kodein.log.*
import kotlin.jvm.*

/**
 * Efficiently stores a permission set
 */
@Serializable(with = PermissionSet.Serializer::class)
public class PermissionSet @KiscordUnstableAPI constructor(
    internal val flags: ULong
) : AbstractSet<Permission>() {
    init {
        if (BuildConfig.DEBUG) {
            var unknownValues = flags and UnknownPermissionReporter.reported.inv()
            while (unknownValues != 0UL) {
                val bit = unknownValues.takeLowestOneBit()
                UnknownPermissionReporter.report(bit)
                val bitMask = bit.inv()
                unknownValues = unknownValues and bitMask
            }
        }
    }

    /** Constructs set from any [Iterable] */
    public constructor(flags: Iterable<Permission>) : this(flags.flags)

    /** Constructs set from array */
    public constructor(vararg flags: Permission) : this(flags.flags)

    /** Constructs set from permission string */
    public constructor(flags: String) : this(flags.toULong())

    /** @suppress */
    override val size: Int get() = flags.countOneBits()

    /** Check permission in the set */
    override fun contains(element: Permission): Boolean {
        val flag = Permission.enumToValue(element)
        return (flags and flag) == flag
    }

    /** Check all permissions in the set */
    override fun containsAll(elements: Collection<Permission>): Boolean {
        val mask = elements.flags
        return (flags and mask) == mask
    }

    /** Check set contains any permission */
    override fun isEmpty(): Boolean = flags == 0UL

    /** @suppress */
    override fun iterator(): Iterator<Permission> = PermissionIterator(flags and Permission.ALL_MASK)

    /** @suppress */
    override fun hashCode(): Int = flags.hashCode()

    /** @suppress */
    override fun equals(other: Any?): Boolean = when (other) {
        is PermissionSet -> flags == other.flags
        else -> super.equals(other)
    }

    private class PermissionIterator(private var set: ULong) : Iterator<Permission> {
        override fun hasNext() = set != 0UL
        override fun next(): Permission {
            if (set == 0UL) throw NoSuchElementException("No more permissions")
            val flag = set.takeLowestOneBit()
            set = set and flag.inv()
            return Permission.valueToEnum(flag)
                ?: throw IllegalStateException("Unknown permission flag: 0x${flag.toString(16).padStart(8, '0')}")
        }
    }

    public companion object {
        /**
         * Empty permission set
         */
        public val Empty: PermissionSet = PermissionSet(0UL)

        @JvmStatic
        internal fun of(flags: ULong): PermissionSet = when (flags) {
            0UL -> Empty
            else -> PermissionSet(flags)
        }
    }

    internal object Serializer : KSerializer<PermissionSet> {
        override val descriptor: SerialDescriptor =
            PrimitiveSerialDescriptor("kiscord.api.PermissionSet", PrimitiveKind.STRING)

        override fun serialize(encoder: Encoder, value: PermissionSet) = encoder.encodeString(value.flags.toString())
        override fun deserialize(decoder: Decoder) = PermissionSet(decoder.decodeString().toULong())
    }

    internal object UnknownPermissionReporter {
        fun report(permission: ULong) {
            reported = reported or permission
            val permissionHex = permission.toString(16)
            logger.warning { "Unknown permission 0x$permissionHex, please report this to ${Kiscord.NAME} team" }
        }

        var reported: ULong = Permission.ALL_MASK
            private set

        private val logger by lazy { Kiscord.DefaultLoggerFactory.newLogger("kiscord.api", "PermissionSet") }
    }
}
