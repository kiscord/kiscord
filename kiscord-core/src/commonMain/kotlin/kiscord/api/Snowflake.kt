/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.api

import kotlinx.datetime.*
import kotlinx.serialization.*
import kotlinx.serialization.descriptors.*
import kotlinx.serialization.encoding.*
import kotlin.jvm.*

/**
 * Discord utilizes Twitter's snowflake format for uniquely identifiable descriptors (IDs).
 * These IDs are guaranteed to be unique across all of Discord,
 * except in some unique scenarios in which child objects share their parent's ID.
 *
 * [External API Reference](https://discord.com/developers/docs/reference#snowflakes)
 *
 * @property id Packed 64-bit snowflake representation
 * @see [discordEpoch]
 * @see [unixEpoch]
 * @see [workerID]
 * @see [processID]
 * @see [increment]
 */
@Serializable(with = Snowflake.Serializer::class)
public class Snowflake constructor(public val id: ULong) : Comparable<Snowflake> {
    /**
     * Parses snowflake encoded as string
     * @see [toString]
     */
    public constructor(id: String) : this(id.toULong())

    /**
     * Encodes snowflake as string
     */
    override fun toString(): String = id.toString()

    /** @suppress */
    override fun hashCode(): Int = id.hashCode()

    /** @suppress */
    override fun equals(other: Any?): Boolean = other is Snowflake && id == other.id

    internal object Serializer : KSerializer<Snowflake> {
        override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor("kiscord.storage.Snowflake", PrimitiveKind.STRING)

        override fun deserialize(decoder: Decoder) = Snowflake(decoder.decodeString())
        override fun serialize(encoder: Encoder, value: Snowflake) = encoder.encodeString(value.toString())
    }

    override fun compareTo(other: Snowflake): Int = id.compareTo(other.id)

    public companion object {
        /**
         * Number of milliseconds between unix epoch and discord epoch
         */
        internal const val DISCORD_EPOCH: ULong = 1420070400000UL
    }
}

/**
 * Number of milliseconds since discord epoch (first second of 2015)
 */
public val Snowflake.discordEpoch: ULong get() = id shr 22

/**
 * Number of milliseconds since unix epoch (first second of 1970)
 */
public val Snowflake.unixEpoch: ULong get() = discordEpoch + Snowflake.DISCORD_EPOCH

/**
 * Internal worker ID who generates that snowflake
 */
public val Snowflake.workerID: Byte get() = ((id and 0x3E0000u) shr 17).toByte()

/**
 * Internal process ID who generates that snowflake
 */
public val Snowflake.processID: Byte get() = ((id and 0x1F000u) shr 12).toByte()

/**
 * For every ID that is generated on that process this value increments
 */
public val Snowflake.increment: Short get() = (id and 0xFFFu).toShort()

/**
 * Time of generation given snowflake
 */
public val Snowflake.time: Instant get() = Instant.fromEpochMilliseconds(unixEpoch.toLong())

/**
 * @return [discordEpoch]
 */
@JvmSynthetic
public operator fun Snowflake.component1(): ULong = discordEpoch

/**
 * @return [workerID]
 */
@JvmSynthetic
public operator fun Snowflake.component2(): Byte = workerID

/**
 * @return [processID]
 */
@JvmSynthetic
public operator fun Snowflake.component3(): Byte = processID

/**
 * @return [increment]
 */
@JvmSynthetic
public operator fun Snowflake.component4(): Short = increment
