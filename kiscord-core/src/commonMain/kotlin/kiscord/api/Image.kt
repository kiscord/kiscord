/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.api

import io.ktor.http.*
import kiscord.*
import kiscord.api.Image.Format.*
import kiscord.internal.*
import kiscord.api.Sticker.FormatType as StickerFormatType

/**
 * [Discord API Reference](https://discord.com/developers/docs/reference#image-formatting)
 */
public sealed class Image {
    public abstract val type: Type
    public open val supportedFormats: Set<Format> get() = type.formats
    public open val recommendedFormat: Format get() = supportedFormats.first()

    public fun url(format: Format = recommendedFormat): Url {
        check(format in type.formats) { "Invalid format $format for $type" }
        return buildUrl(format)
    }

    protected abstract fun buildUrl(format: Format): Url

    public val url: Url get() = url()

    override fun toString(): String = url.toString()

    override fun hashCode(): Int {
        return type.hashCode() * 31 + url.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        return other is Image && type == other.type && url == other.url
    }

    public data class CustomEmoji(
        public val emojiId: Snowflake
    ) : Image() {
        override val type: Type get() = Type.CustomEmoji

        override fun buildUrl(format: Format): Url =
            Url("${BuildConfig.DISCORD_CDN_URL}/emojis/$emojiId.${format.extension}")
    }

    public data class GuildIcon(
        public val guildId: Snowflake,
        public val guildIcon: String
    ) : Image() {
        override val type: Type get() = Type.GuildIcon
        override val recommendedFormat: Format
            get() = if (guildIcon.startsWith("a_")) GIF else PNG

        override fun buildUrl(format: Format): Url =
            Url("${BuildConfig.DISCORD_CDN_URL}/icons/$guildId/$guildIcon.${format.extension}")
    }

    public data class GuildSplash(
        public val guildId: Snowflake,
        public val guildSplash: String
    ) : Image() {
        override val type: Type get() = Type.GuildSplash

        override fun buildUrl(format: Format): Url =
            Url("${BuildConfig.DISCORD_CDN_URL}/splashes/$guildId/$guildSplash.${format.extension}")
    }

    public data class GuildDiscoverySplash(
        public val guildId: Snowflake,
        public val guildDiscoverySplash: String
    ) : Image() {
        override val type: Type get() = Type.GuildDiscoverySplash

        override fun buildUrl(format: Format): Url =
            Url("${BuildConfig.DISCORD_CDN_URL}/discovery-splashes/$guildId/$guildDiscoverySplash.${format.extension}")
    }

    public data class GuildBanner(
        public val guildId: Snowflake,
        public val guildBanner: String
    ) : Image() {
        override val type: Type get() = Type.GuildBanner

        override fun buildUrl(format: Format): Url =
            Url("${BuildConfig.DISCORD_CDN_URL}/banners/$guildId/$guildBanner.${format.extension}")
    }

    public data class UserBanner(
        public val userId: Snowflake,
        public val userBanner: String
    ) : Image() {
        override val type: Type get() = Type.UserBanner
        override val recommendedFormat: Format
            get() = if (userBanner.startsWith("a_")) GIF else PNG

        override fun buildUrl(format: Format): Url =
            Url("${BuildConfig.DISCORD_CDN_URL}/banners/$userId/$userBanner.${format.extension}")
    }

    public data class DefaultUserAvatar(
        public val userId: Snowflake
    ) : Image() {
        override val type: Type get() = Type.DefaultUserAvatar

        override fun buildUrl(format: Format): Url {
            return Url("${BuildConfig.DISCORD_CDN_URL}/embed/avatars/${userId.id.shr(22) % 6u}.${format.extension}")
        }
    }

    public data class LegacyDefaultUserAvatar(
        public val userDiscriminator: Int
    ) : Image() {
        override val type: Type get() = Type.DefaultUserAvatar

        override fun buildUrl(format: Format): Url {
            return Url("${BuildConfig.DISCORD_CDN_URL}/embed/avatars/${userDiscriminator % 5}.${format.extension}")
        }
    }

    public data class UserAvatar(
        public val userId: Snowflake,
        public val userAvatar: String
    ) : Image() {
        override val type: Type get() = Type.UserAvatar
        override val recommendedFormat: Format
            get() = if (userAvatar.startsWith("a_")) GIF else PNG

        override fun buildUrl(format: Format): Url =
            Url("${BuildConfig.DISCORD_CDN_URL}/avatars/$userId/$userAvatar.${format.extension}")
    }

    public data class GuildMemberAvatar(
        public val guildId: Snowflake,
        public val userId: Snowflake,
        public val memberAvatar: String
    ) : Image() {
        override val type: Type get() = Type.GuildMemberAvatar
        override val recommendedFormat: Format
            get() = if (memberAvatar.startsWith("a_")) GIF else PNG

        override fun buildUrl(format: Format): Url =
            Url("${BuildConfig.DISCORD_CDN_URL}/guilds/${guildId}/users/$userId/avatars/$memberAvatar.${format.extension}")
    }

    public data class ApplicationIcon(
        public val applicationId: Snowflake,
        public val icon: String
    ) : Image() {
        override val type: Type get() = Type.ApplicationIcon

        override fun buildUrl(format: Format): Url =
            Url("${BuildConfig.DISCORD_CDN_URL}/app-icons/$applicationId/$icon.${format.extension}")
    }

    public data class ApplicationCover(
        public val applicationId: Snowflake,
        public val coverImage: String
    ) : Image() {
        override val type: Type get() = Type.ApplicationCover

        override fun buildUrl(format: Format): Url =
            Url("${BuildConfig.DISCORD_CDN_URL}/app-icons/$applicationId/$coverImage.${format.extension}")
    }

    public data class ApplicationAsset(
        public val applicationId: Snowflake,
        public val assetId: String
    ) : Image() {
        public constructor(applicationId: Snowflake, assetId: Snowflake) : this(applicationId, assetId.toString())

        override val type: Type get() = Type.ApplicationAsset

        override fun buildUrl(format: Format): Url =
            Url("${BuildConfig.DISCORD_CDN_URL}/app-assets/$applicationId/$assetId.${format.extension}")
    }

    public data class AchievementIcon(
        public val applicationId: Snowflake,
        public val achievementId: String,
        public val iconHash: String
    ) : Image() {
        override val type: Type get() = Type.AchievementIcon

        override fun buildUrl(format: Format): Url =
            Url("${BuildConfig.DISCORD_CDN_URL}/app-assets/$applicationId/achievements/$achievementId/icons/$iconHash.${format.extension}")
    }

    public data class StickerPackBanner(
        public val stickerPackBannerAssetId: Snowflake
    ) : Image() {
        override val type: Type get() = Type.StickerPackBanner

        override fun buildUrl(format: Format): Url =
            Url("${BuildConfig.DISCORD_CDN_URL}/app-assets/710982414301790216/store/$stickerPackBannerAssetId.${format.extension}")
    }

    public data class TeamIcon(public val teamId: Snowflake, public val teamIcon: String) : Image() {
        override val type: Type get() = Type.TeamIcon

        override fun buildUrl(format: Format): Url =
            Url("${BuildConfig.DISCORD_CDN_URL}/team-icons/$teamId/$teamIcon.${format.extension}")
    }

    public data class Sticker(
        public val stickerId: Snowflake,
        public val stickerFormatType: StickerFormatType
    ) : Image() {
        override val type: Type get() = Type.Sticker
        override val supportedFormats: Set<Format>
            get() = when (stickerFormatType) {
                StickerFormatType.PNG, StickerFormatType.APNG -> setOf(PNG)
                StickerFormatType.LOTTIE -> setOf(Lottie)
            }

        override fun buildUrl(format: Format): Url =
            Url("${BuildConfig.DISCORD_CDN_URL}/stickers/$stickerId.${format.extension}")
    }

    public data class RoleIcon(
        public val roleId: Snowflake,
        public val roleIcon: String
    ) : Image() {
        override val type: Type get() = Type.RoleIcon
        override val recommendedFormat: Format
            get() = if (roleIcon.startsWith("a_")) GIF else PNG

        override fun buildUrl(format: Format): Url =
            Url("${BuildConfig.DISCORD_CDN_URL}/role-icons/$roleId/$roleIcon.${format.extension}")
    }

    /**
     * Image type with bounded formats
     */
    public enum class Type(public val formats: Set<Format>) {
        CustomEmoji(PNG, JPEG, WebP, GIF),
        GuildIcon(PNG, JPEG, WebP, GIF),
        GuildSplash(PNG, JPEG, WebP),
        GuildDiscoverySplash(PNG, JPEG, WebP),
        GuildBanner(PNG, JPEG, WebP),
        UserBanner(PNG, JPEG, WebP, GIF),
        DefaultUserAvatar(PNG),
        UserAvatar(PNG, JPEG, WebP, GIF),
        GuildMemberAvatar(PNG, JPEG, WebP, GIF),
        ApplicationIcon(PNG, JPEG, WebP),
        ApplicationCover(PNG, JPEG, WebP),
        ApplicationAsset(PNG, JPEG, WebP),
        AchievementIcon(PNG, JPEG, WebP),
        StickerPackBanner(PNG, JPEG, WebP),
        TeamIcon(PNG, JPEG, WebP),
        Sticker(PNG, Lottie),
        RoleIcon(PNG, JPEG, WebP),
        ;

        constructor(vararg formats: Format) : this(formats.toSet().toEnumSet())
    }

    public enum class Format(public val extensions: List<String>) {
        PNG("png"),
        JPEG("jpg", "jpeg"),
        GIF("gif"),
        WebP("webp"),
        Lottie("json"),
        ;

        constructor(vararg extensions: String) : this(extensions.toList())

        /**
         * Primary format extension
         */
        public val extension: String get() = extensions.first()
    }
}


public fun Image.isSupports(format: Image.Format): Boolean = format in type.formats

public val Emoji.image: Image.CustomEmoji?
    get() = id?.let { Image.CustomEmoji(it) }
public val GuildSpec.iconImage: Image.GuildIcon?
    get() = icon?.let { Image.GuildIcon(id, it) }
public val GuildSpec.splashImage: Image.GuildSplash?
    get() = splash?.let { Image.GuildSplash(id, it) }
public val GuildSpec.discoverySplashImage: Image.GuildDiscoverySplash?
    get() = discoverySplash?.let { Image.GuildDiscoverySplash(id, it) }
public val GuildSpec.bannerImage: Image.GuildBanner?
    get() = banner?.let { Image.GuildBanner(id, it) }
public val UserSpec.avatarImage: Image
    get() = avatar?.let { Image.UserAvatar(id, it) } ?: run {
        if (discriminator == "0") {
            Image.DefaultUserAvatar(id)
        } else {
            Image.LegacyDefaultUserAvatar(discriminator.toInt())
        }
    }
public val Application.iconImage: Image.ApplicationIcon?
    get() = icon?.let { Image.ApplicationIcon(id, it) }
public val Application.coverImage: Image.ApplicationCover?
    get() = cover?.let { Image.ApplicationCover(id, it) }
public val Sticker.image: Image.Sticker
    get() = Image.Sticker(id, formatType)
public val StickerPack.bannerImage: Image.StickerPackBanner?
    get() = bannerAssetId?.let { Image.StickerPackBanner(it) }
public val Role.iconImage: Image.RoleIcon?
    get() = icon.orNull?.let { Image.RoleIcon(id, it) }

public fun Application.asset(id: String): Image.ApplicationAsset =
    Image.ApplicationAsset(this.id, id)

public fun Application.asset(id: Snowflake): Image.ApplicationAsset =
    Image.ApplicationAsset(this.id, id)

public val Team.iconImage: Image.TeamIcon?
    get() = icon?.let { Image.TeamIcon(id, it) }
