/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

@file:OptIn(ExperimentalSerializationApi::class)

package kiscord.api

import kiscord.serialization.UnknownFieldException
import kotlinx.serialization.*
import kotlinx.serialization.builtins.*
import kotlinx.serialization.descriptors.*
import kotlinx.serialization.encoding.*
import kotlinx.serialization.json.*

@Serializable(with = InteractionDataOption.Serializer::class)
public class InteractionDataOption private constructor(
    public val name: String,
    public val type: ApplicationCommandOption.Type,
    private val value: Any?,
    public val focused: Boolean = false,
) {
    public val string: String
        get() {
            check(type == ApplicationCommandOption.Type.String)
            return value as String
        }

    public val integer: Int
        get() {
            check(type == ApplicationCommandOption.Type.Integer)
            return value as Int
        }

    public val number: Double
        get() {
            check(type == ApplicationCommandOption.Type.Number)
            return value as Double
        }

    public val boolean: Boolean
        get() {
            check(type == ApplicationCommandOption.Type.Boolean)
            return value as Boolean
        }

    public val snowflake: Snowflake
        get() {
            check(
                type == ApplicationCommandOption.Type.User
                    || type == ApplicationCommandOption.Type.Channel
                    || type == ApplicationCommandOption.Type.Role
                    || type == ApplicationCommandOption.Type.Mentionable
                    || type == ApplicationCommandOption.Type.Attachment
            )
            return value as Snowflake
        }

    @Suppress("UNCHECKED_CAST")
    public val options: List<InteractionDataOption>
        get() {
            check(type == ApplicationCommandOption.Type.SubCommand || type == ApplicationCommandOption.Type.SubCommandGroup)
            return value as List<InteractionDataOption>
        }

    public object Serializer : KSerializer<InteractionDataOption> {
        // We're unable to use cool builder, 'cause there is no way to pass buildable value to the builder itself for options field
        override val descriptor: SerialDescriptor = object : SerialDescriptor {
            override val kind: SerialKind get() = StructureKind.OBJECT
            override val serialName: String get() = "kiscord.api.InteractionDataOption"
            override val elementsCount: Int get() = 5
            override fun getElementAnnotations(index: Int): List<Annotation> = emptyList()
            override fun isElementOptional(index: Int): Boolean = index >= 2

            override fun getElementName(index: Int): String = when (index) {
                0 -> "name"
                1 -> "type"
                2 -> "value"
                3 -> "options"
                4 -> "focused"
                else -> throw IndexOutOfBoundsException("$index >= $elementsCount")
            }

            override fun getElementIndex(name: String): Int = when (name) {
                "name" -> 0
                "type" -> 1
                "value" -> 2
                "options" -> 3
                "focused" -> 4
                else -> CompositeDecoder.UNKNOWN_NAME
            }

            override fun getElementDescriptor(index: Int): SerialDescriptor = when (index) {
                0 -> String.serializer().descriptor // name
                1 -> ApplicationCommandOption.Type.serializer().descriptor // type
                2 -> JsonPrimitive.serializer().descriptor // value
                3 -> listSerialDescriptor(this) // options
                4 -> Boolean.serializer().descriptor // focused
                else -> throw IndexOutOfBoundsException("$index >= $elementsCount")
            }
        }

        override fun serialize(encoder: Encoder, value: InteractionDataOption): Unit =
            encoder.encodeStructure(descriptor) {
                encodeStringElement(descriptor, 0, value.name)
                encodeSerializableElement(descriptor, 1, ApplicationCommandOption.Type.serializer(), value.type)
                when (value.type) {
                    ApplicationCommandOption.Type.String -> encodeStringElement(descriptor, 2, value.string)
                    ApplicationCommandOption.Type.Integer -> encodeIntElement(descriptor, 2, value.integer)
                    ApplicationCommandOption.Type.Number -> encodeDoubleElement(descriptor, 2, value.number)
                    ApplicationCommandOption.Type.Boolean -> encodeBooleanElement(descriptor, 2, value.boolean)
                    ApplicationCommandOption.Type.User,
                    ApplicationCommandOption.Type.Channel,
                    ApplicationCommandOption.Type.Role,
                    ApplicationCommandOption.Type.Mentionable,
                    ApplicationCommandOption.Type.Attachment
                    -> encodeSerializableElement(descriptor, 2, Snowflake.serializer(), value.snowflake)
                    ApplicationCommandOption.Type.SubCommand, ApplicationCommandOption.Type.SubCommandGroup ->
                        encodeSerializableElement(descriptor, 3, ListSerializer(Serializer), value.options)
                }
                if (shouldEncodeElementDefault(descriptor, 4) || value.focused) {
                    encodeBooleanElement(descriptor, 4, value.focused)
                }
            }

        override fun deserialize(decoder: Decoder): InteractionDataOption = decoder.decodeStructure(descriptor) {
            var name: String? = null
            var type: ApplicationCommandOption.Type? = null
            var value: Any? = null
            var focused = false

            loop@ while (true) {
                when (val index = decodeElementIndex(descriptor)) {
                    0 -> name = decodeStringElement(descriptor, index)
                    1 -> type = decodeSerializableElement(descriptor, index, ApplicationCommandOption.Type.serializer())
                    2 -> value = when (type) {
                        ApplicationCommandOption.Type.String -> decodeStringElement(descriptor, index)
                        ApplicationCommandOption.Type.Integer -> decodeIntElement(descriptor, index)
                        ApplicationCommandOption.Type.Number -> decodeDoubleElement(descriptor, index)
                        ApplicationCommandOption.Type.Boolean -> decodeBooleanElement(descriptor, index)
                        ApplicationCommandOption.Type.User,
                        ApplicationCommandOption.Type.Channel,
                        ApplicationCommandOption.Type.Role,
                        ApplicationCommandOption.Type.Mentionable,
                        ApplicationCommandOption.Type.Attachment ->
                            decodeSerializableElement(descriptor, index, Snowflake.serializer())
                        ApplicationCommandOption.Type.SubCommand, ApplicationCommandOption.Type.SubCommandGroup ->
                            throw IllegalStateException("Subcommands cannot have a value!")
                        null -> decodeSerializableElement(descriptor, index, JsonElement.serializer())
                    }
                    3 -> value = when (type) {
                        ApplicationCommandOption.Type.SubCommand, ApplicationCommandOption.Type.SubCommandGroup ->
                            decodeSerializableElement(descriptor, index, ListSerializer(Serializer))
                        null -> decodeSerializableElement(descriptor, index, JsonElement.serializer())
                        else -> throw IllegalStateException("Only subcommands can have options!")
                    }
                    4 -> focused = decodeBooleanElement(descriptor, index)
                    CompositeDecoder.DECODE_DONE -> break@loop
                    else -> throw UnknownFieldException(index)
                }
            }

            val missingFields = mutableListOf<String>()

            if (value is JsonElement) {
                check(this is JsonDecoder)
                value = when (type) {
                    ApplicationCommandOption.Type.String -> json.decodeFromJsonElement(String.serializer(), value)
                    ApplicationCommandOption.Type.Integer -> json.decodeFromJsonElement(Int.serializer(), value)
                    ApplicationCommandOption.Type.Number -> json.decodeFromJsonElement(Double.serializer(), value)
                    ApplicationCommandOption.Type.Boolean -> json.decodeFromJsonElement(Boolean.serializer(), value)
                    ApplicationCommandOption.Type.User,
                    ApplicationCommandOption.Type.Channel,
                    ApplicationCommandOption.Type.Role,
                    ApplicationCommandOption.Type.Mentionable,
                    ApplicationCommandOption.Type.Attachment ->
                        json.decodeFromJsonElement(Snowflake.serializer(), value)
                    ApplicationCommandOption.Type.SubCommand, ApplicationCommandOption.Type.SubCommandGroup ->
                        json.decodeFromJsonElement(ListSerializer(Serializer), value)
                    null -> null
                }
            }

            if (name == null) missingFields += "name"
            when (type) {
                null -> missingFields += "type"
                ApplicationCommandOption.Type.String,
                ApplicationCommandOption.Type.Integer,
                ApplicationCommandOption.Type.Number,
                ApplicationCommandOption.Type.Boolean,
                ApplicationCommandOption.Type.User,
                ApplicationCommandOption.Type.Channel,
                ApplicationCommandOption.Type.Role,
                ApplicationCommandOption.Type.Mentionable,
                ApplicationCommandOption.Type.Attachment ->
                    if (value == null) missingFields += "value"
                ApplicationCommandOption.Type.SubCommand, ApplicationCommandOption.Type.SubCommandGroup ->
                    if (value == null) missingFields += "options"
            }
            if (missingFields.isNotEmpty()) throw MissingFieldException(missingFields, descriptor.serialName)
            InteractionDataOption(name!!, type!!, value, focused)
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || this::class != other::class) return false

        other as InteractionDataOption

        if (name != other.name) return false
        if (type != other.type) return false
        if (value != other.value) return false
        if (focused != other.focused) return false

        return true
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + type.hashCode()
        result = 31 * result + (value?.hashCode() ?: 0)
        result = 31 * result + focused.hashCode()
        return result
    }

    override fun toString(): String {
        return "$name = $value"
    }
}
