/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.api

import kiscord.builder.*

@BuilderDsl
public inline fun MutableCollection<in Component>.button(block: Component.Button.Builder.() -> Unit) {
    add(Component.Button(block))
}

@BuilderDsl
public inline fun MutableCollection<in Component>.actionRow(block: Component.ActionRow.Builder.() -> Unit) {
    add(Component.ActionRow(block))
}

@BuilderDsl
public inline fun MutableCollection<in Component>.selectMenu(block: Component.SelectMenu.Builder.() -> Unit) {
    add(Component.SelectMenu(block))
}

@BuilderDsl
public inline fun MutableCollection<in Component>.textInput(block: Component.TextInput.Builder.() -> Unit) {
    add(Component.TextInput(block))
}


@BuilderDsl
public inline fun Component.ActionRow.Builder.button(block: Component.Button.Builder.() -> Unit) {
    components.add(Component.Button(block))
}

@BuilderDsl
public inline fun Component.ActionRow.Builder.actionRow(block: Component.ActionRow.Builder.() -> Unit) {
    components.add(Component.ActionRow(block))
}

@BuilderDsl
public inline fun Component.ActionRow.Builder.selectMenu(block: Component.SelectMenu.Builder.() -> Unit) {
    components.add(Component.SelectMenu(block))
}

@BuilderDsl
public inline fun Component.ActionRow.Builder.textInput(block: Component.TextInput.Builder.() -> Unit) {
    components.add(Component.TextInput(block))
}
