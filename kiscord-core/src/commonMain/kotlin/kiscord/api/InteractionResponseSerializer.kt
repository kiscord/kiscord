/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.api

import kotlinx.serialization.*
import kotlinx.serialization.descriptors.*
import kotlinx.serialization.encoding.*

@OptIn(ExperimentalSerializationApi::class)
internal object InteractionResponseSerializer : KSerializer<Interaction.Response> {
    override val descriptor: SerialDescriptor = buildClassSerialDescriptor("kiscord.api.Interaction.Response") {
        element("type", Interaction.Response.Type.serializer().descriptor)
        element("data", Interaction.Response.Data.serializer().descriptor)
    }

    override fun deserialize(decoder: Decoder): Interaction.Response {
        throw NotImplementedError("Deserializing for interaction response not supported")
    }

    override fun serialize(encoder: Encoder, value: Interaction.Response) = encoder.encodeStructure(descriptor) {
        encodeSerializableElement(descriptor, 0, Interaction.Response.Type.serializer(), value.type)
        when (value.data) {
            is Interaction.Response.Data.Message ->
                encodeSerializableElement(descriptor, 1, Interaction.Response.Data.Message.serializer(), value.data)

            is Interaction.Response.Data.Autocomplete ->
                encodeSerializableElement(
                    descriptor,
                    1,
                    Interaction.Response.Data.Autocomplete.serializer(),
                    value.data
                )

            is Interaction.Response.Data.Modal ->
                encodeSerializableElement(descriptor, 1, Interaction.Response.Data.Modal.serializer(), value.data)

            null -> {
                if (shouldEncodeElementDefault(descriptor, 1)) {
                    encodeNullableSerializableElement(descriptor, 1, Interaction.Response.Data.serializer(), null)
                }
            }
        }
    }
}
