/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.api

import io.ktor.http.*
import kiscord.serialization.*
import kotlinx.serialization.*
import kotlinx.serialization.builtins.*
import kotlinx.serialization.descriptors.*
import kotlinx.serialization.encoding.*

@OptIn(ExperimentalSerializationApi::class)
internal object ComponentSerializer : KSerializer<Component> {
    // We're unable to use cool builder, 'cause there is no way to pass buildable value to the builder itself for components field
    override val descriptor: SerialDescriptor = object : SerialDescriptor {
        override val kind: SerialKind get() = StructureKind.OBJECT
        override val serialName: String get() = "kotlinx.serialization.Sealed<kiscord.api.Component>"
        override val elementsCount: Int get() = 16
        override fun getElementAnnotations(index: Int): List<Annotation> = emptyList()
        override fun isElementOptional(index: Int): Boolean = index != 0

        override fun getElementName(index: Int): String = when (index) {
            0 -> "type"
            1 -> "custom_id"
            2 -> "disabled"
            3 -> "style"
            4 -> "label"
            5 -> "emoji"
            6 -> "url"
            7 -> "options"
            8 -> "placeholder"
            9 -> "min_values"
            10 -> "max_values"
            11 -> "components"
            12 -> "min_length"
            13 -> "max_length"
            14 -> "value"
            15 -> "required"
            16 -> "channel_types"
            else -> throw IndexOutOfBoundsException("$index >= $elementsCount")
        }

        override fun getElementIndex(name: String): Int = when (name) {
            "type" -> 0
            "custom_id" -> 1
            "disabled" -> 2
            "style" -> 3
            "label" -> 4
            "emoji" -> 5
            "url" -> 6
            "options" -> 7
            "placeholder" -> 8
            "min_values" -> 9
            "max_values" -> 10
            "components" -> 11
            "min_length" -> 12
            "max_length" -> 13
            "value" -> 14
            "required" -> 15
            "channel_types" -> 16
            else -> CompositeDecoder.UNKNOWN_NAME
        }

        override fun getElementDescriptor(index: Int): SerialDescriptor = when (index) {
            0 -> Component.Type.serializer().descriptor // type
            1 -> String.serializer().descriptor // custom_id
            2 -> Boolean.serializer().descriptor // disabled
            3 -> Int.serializer().descriptor // style
            4 -> String.serializer().descriptor // label
            5 -> Emoji.serializer().descriptor // emoji
            6 -> UrlSerializer.descriptor // url
            7 -> listSerialDescriptor(Component.SelectMenu.Option.serializer().descriptor) // options
            8 -> String.serializer().descriptor // placeholder
            9 -> Int.serializer().descriptor // min_values
            10 -> Int.serializer().descriptor // max_values
            11 -> listSerialDescriptor(this) // components
            12 -> Int.serializer().descriptor // min_length
            13 -> Int.serializer().descriptor // max_length
            14 -> String.serializer().descriptor // value
            15 -> Boolean.serializer().descriptor // value
            16 -> setSerialDescriptor(Channel.Type.serializer().descriptor)
            else -> throw IndexOutOfBoundsException("$index >= $elementsCount")
        }
    }

    override fun deserialize(decoder: Decoder): Component = decoder.decodeStructure(descriptor) {
        var type: Component.Type? = null
        var customId: String? = null
        var disabled: Boolean? = null
        var style: Int? = null
        var label: String? = null
        var emoji: Emoji? = null
        var url: Url? = null
        var options: List<Component.SelectMenu.Option>? = null
        var placeholder: String? = null
        var minValues = 1
        var maxValues = 1
        var components: List<Component>? = null
        var minLength: Int? = null
        var maxLength: Int? = null
        var value: String? = null
        var required: Boolean? = null
        var channelTypes: Set<Channel.Type>? = null

        loop@ while (true) {
            when (val index = decodeElementIndex(descriptor)) {
                0 -> type = decodeSerializableElement(descriptor, index, Component.Type.serializer())
                1 -> customId = decodeStringElement(descriptor, index)
                2 -> disabled = decodeBooleanElement(descriptor, index)
                3 -> style = decodeIntElement(descriptor, index)
                4 -> label = decodeStringElement(descriptor, index)
                5 -> emoji = decodeSerializableElement(descriptor, index, Emoji.serializer())
                6 -> url = decodeSerializableElement(descriptor, index, UrlSerializer)
                7 -> options = decodeSerializableElement(
                    descriptor,
                    index,
                    ListSerializer(Component.SelectMenu.Option.serializer())
                )

                8 -> placeholder = decodeStringElement(descriptor, index)
                9 -> minValues = decodeIntElement(descriptor, index)
                10 -> maxValues = decodeIntElement(descriptor, index)
                11 -> components = decodeSerializableElement(descriptor, index, ListSerializer(ComponentSerializer))
                12 -> minLength = decodeIntElement(descriptor, index)
                13 -> maxLength = decodeIntElement(descriptor, index)
                14 -> value = decodeStringElement(descriptor, index)
                15 -> required = decodeBooleanElement(descriptor, index)
                16 -> channelTypes = decodeNullableSerializableElement(
                    descriptor,
                    index,
                    SetSerializer(Channel.Type.serializer()).nullable
                )

                CompositeDecoder.DECODE_DONE -> break@loop
                else -> throw UnknownFieldException(index)
            }
        }

        val missingFields = mutableListOf<String>()

        when (type) {
            Component.Type.ActionRow -> {
                if (components == null) missingFields += "components"
                if (missingFields.isNotEmpty()) throw MissingFieldException(
                    missingFields,
                    "kiscord.api.Component.ActionRow"
                )

                Component.ActionRow(
                    components = components!!,
                )
            }

            Component.Type.Button -> {
                if (style == null) missingFields += "style"
                if (missingFields.isNotEmpty()) throw MissingFieldException(
                    missingFields,
                    "kiscord.api.Component.Button"
                )

                Component.Button(
                    customId = customId,
                    disabled = disabled ?: false,
                    style = Component.Button.Style.valueToEnum(style!!)!!,
                    label = label,
                    emoji = emoji,
                    url = url,
                )
            }

            Component.Type.SelectMenu -> {
                if (customId == null) missingFields += "custom_id"
                if (missingFields.isNotEmpty()) throw MissingFieldException(
                    missingFields,
                    "kiscord.api.Component.SelectMenu"
                )

                Component.SelectMenu(
                    customId = customId!!,
                    disabled = disabled ?: false,
                    options = options,
                    channelTypes = channelTypes,
                    placeholder = placeholder,
                    minValues = minValues,
                    maxValues = maxValues,
                )
            }

            Component.Type.TextInput -> {
                if (customId == null) missingFields += "custom_id"
                if (style == null) missingFields += "style"
                if (label == null) missingFields += "label"
                if (missingFields.isNotEmpty()) throw MissingFieldException(
                    missingFields,
                    "kiscord.api.Component.TextInput"
                )

                Component.TextInput(
                    customId = customId!!,
                    style = Component.TextInput.Style.valueToEnum(style!!)!!,
                    label = label!!,
                    minLength = minLength,
                    maxLength = maxLength,
                    required = required ?: true,
                    value = value,
                    placeholder = placeholder,
                )
            }

            null -> throw MissingFieldException(listOf("type"), descriptor.serialName)
        }
    }

    override fun serialize(encoder: Encoder, value: Component) = encoder.encodeStructure(descriptor) {
        val type = value.type
        encodeSerializableElement(descriptor, 0, Component.Type.serializer(), type)
        when (type) {
            Component.Type.ActionRow -> {
                val actionRow = value as Component.ActionRow
                encodeSerializableElement(descriptor, 11, ListSerializer(ComponentSerializer), actionRow.components)
            }

            Component.Type.Button -> {
                val button = value as Component.Button
                if (shouldEncodeElementDefault(descriptor, 1) || button.customId != null) {
                    encodeNullableSerializableElement(descriptor, 1, String.serializer(), button.customId)
                }
                if (shouldEncodeElementDefault(descriptor, 2) || button.disabled) {
                    encodeBooleanElement(descriptor, 2, button.disabled)
                }
                encodeIntElement(descriptor, 3, Component.Button.Style.enumToValue(button.style))
                if (shouldEncodeElementDefault(descriptor, 4) || button.label != null) {
                    encodeNullableSerializableElement(descriptor, 4, String.serializer(), button.label)
                }
                if (shouldEncodeElementDefault(descriptor, 5) || button.emoji != null) {
                    encodeNullableSerializableElement(descriptor, 5, Emoji.serializer(), button.emoji)
                }
                if (shouldEncodeElementDefault(descriptor, 6) || button.url != null) {
                    encodeNullableSerializableElement(descriptor, 6, UrlSerializer, button.url)
                }
            }

            Component.Type.SelectMenu -> {
                val selectMenu = value as Component.SelectMenu
                encodeStringElement(descriptor, 1, selectMenu.customId)
                if (shouldEncodeElementDefault(descriptor, 2) || selectMenu.disabled) {
                    encodeBooleanElement(descriptor, 2, selectMenu.disabled)
                }
                if (shouldEncodeElementDefault(descriptor, 7) || selectMenu.options != null) {
                    encodeNullableSerializableElement(
                        descriptor, 7, ListSerializer(Component.SelectMenu.Option.serializer()), selectMenu.options
                    )
                }
                if (shouldEncodeElementDefault(descriptor, 8) || selectMenu.placeholder != null) {
                    encodeNullableSerializableElement(descriptor, 8, String.serializer(), selectMenu.placeholder)
                }
                if (shouldEncodeElementDefault(descriptor, 9) || selectMenu.minValues != 1) {
                    encodeIntElement(descriptor, 9, selectMenu.minValues)
                }
                if (shouldEncodeElementDefault(descriptor, 10) || selectMenu.maxValues != 1) {
                    encodeIntElement(descriptor, 10, selectMenu.maxValues)
                }
                if (shouldEncodeElementDefault(descriptor, 16) || selectMenu.channelTypes != null) {
                    encodeNullableSerializableElement(
                        descriptor, 16, SetSerializer(Channel.Type.serializer()), selectMenu.channelTypes
                    )
                }
            }

            Component.Type.TextInput -> {
                val textInput = value as Component.TextInput
                encodeStringElement(descriptor, 1, textInput.customId)
                encodeIntElement(descriptor, 3, Component.TextInput.Style.enumToValue(textInput.style))
                encodeStringElement(descriptor, 4, textInput.label)
                if (shouldEncodeElementDefault(descriptor, 12) || textInput.minLength != null) {
                    encodeNullableSerializableElement(descriptor, 12, Int.serializer(), textInput.minLength)
                }
                if (shouldEncodeElementDefault(descriptor, 13) || textInput.maxLength != null) {
                    encodeNullableSerializableElement(descriptor, 13, Int.serializer(), textInput.maxLength)
                }
                if (shouldEncodeElementDefault(descriptor, 15) || !textInput.required) {
                    encodeBooleanElement(descriptor, 15, textInput.required)
                }
                if (shouldEncodeElementDefault(descriptor, 14) || textInput.value != null) {
                    encodeNullableSerializableElement(descriptor, 14, String.serializer(), textInput.value)
                }
                if (shouldEncodeElementDefault(descriptor, 8) || textInput.placeholder != null) {
                    encodeNullableSerializableElement(descriptor, 8, String.serializer(), textInput.placeholder)
                }
            }
        }
    }
}
