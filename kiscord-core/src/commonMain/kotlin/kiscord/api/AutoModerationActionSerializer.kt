/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.api

import kiscord.serialization.*
import kotlinx.serialization.*
import kotlinx.serialization.descriptors.*
import kotlinx.serialization.encoding.*
import kotlinx.serialization.json.*

@OptIn(ExperimentalSerializationApi::class)
internal object AutoModerationActionSerializer : KSerializer<AutoModerationAction> {
    override val descriptor: SerialDescriptor = buildClassSerialDescriptor("kiscord.api.AutoModerationAction") {
        element("type", AutoModerationAction.Type.serializer().descriptor)
        element("metadata", AutoModerationAction.Metadata.serializer().descriptor, isOptional = true)
    }

    override fun serialize(encoder: Encoder, value: AutoModerationAction): Unit = encoder.encodeStructure(descriptor) {
        encodeSerializableElement(descriptor, 0, AutoModerationAction.Type.serializer(), value.type)
        when (val metadata = value.metadata) {
            is AutoModerationAction.Metadata.SendAlertMessage -> {
                check(value.type == AutoModerationAction.Type.SendAlertMessage) { "Invalid metadata type ${metadata::class} for action of type ${value.type}" }
                encodeSerializableElement(
                    descriptor, 1,
                    AutoModerationAction.Metadata.SendAlertMessage.serializer(), metadata
                )
            }

            is AutoModerationAction.Metadata.Timeout -> {
                check(value.type == AutoModerationAction.Type.Timeout) { "Invalid metadata type ${metadata::class} for action of type ${value.type}" }
                encodeSerializableElement(
                    descriptor, 1,
                    AutoModerationAction.Metadata.Timeout.serializer(), metadata
                )
            }

            null -> {
                check(value.type == AutoModerationAction.Type.BlockMessage) { "Invalid null metadata for action of type ${value.type}" }
            }
        }
    }

    override fun deserialize(decoder: Decoder): AutoModerationAction = decoder.decodeStructure(descriptor) {
        var type: AutoModerationAction.Type? = null
        var metadata: AutoModerationAction.Metadata? = null
        var metadataJson: JsonObject? = null

        loop@ while (true) {
            when (val index = decodeElementIndex(descriptor)) {
                CompositeDecoder.DECODE_DONE -> break@loop
                0 -> type = decodeSerializableElement(descriptor, index, AutoModerationAction.Type.serializer())
                1 -> {
                    if (type == null) {
                        check(this is JsonDecoder)
                        metadataJson = decodeSerializableElement(descriptor, index, JsonObject.serializer())
                        continue
                    }
                    metadata = when (type) {
                        AutoModerationAction.Type.SendAlertMessage -> decodeSerializableElement(
                            descriptor, index,
                            AutoModerationAction.Metadata.SendAlertMessage.serializer()
                        )

                        AutoModerationAction.Type.Timeout -> decodeSerializableElement(
                            descriptor, index,
                            AutoModerationAction.Metadata.Timeout.serializer()
                        )

                        AutoModerationAction.Type.BlockMessage -> {
                            throw SerializationException("Metadata present for BlockMessage action type")
                        }
                    }
                }

                else -> throw UnknownFieldException(index)
            }
        }

        if (metadataJson != null) {
            check(metadata == null && this is JsonDecoder)
            metadata = when (type) {
                AutoModerationAction.Type.SendAlertMessage ->
                    json.decodeFromJsonElement(
                        AutoModerationAction.Metadata.SendAlertMessage.serializer(),
                        metadataJson
                    )

                AutoModerationAction.Type.Timeout ->
                    json.decodeFromJsonElement(
                        AutoModerationAction.Metadata.Timeout.serializer(),
                        metadataJson
                    )

                AutoModerationAction.Type.BlockMessage ->
                    throw SerializationException("Metadata present for BlockMessage action type")

                null -> throw SerializationException("Unsupported metadata for type $type")
            }
        }

        val missingFields = mutableListOf<String>()
        if (type == null) missingFields += "type"
        if (metadata == null && type != AutoModerationAction.Type.BlockMessage) missingFields += "metadata"

        if (missingFields.isNotEmpty()) {
            throw MissingFieldException(missingFields, descriptor.serialName)
        }

        AutoModerationAction(
            type = type!!,
            metadata = metadata,
        )
    }
}
