/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.api

import kiscord.builder.*
import kotlinx.coroutines.flow.*
import kotlinx.datetime.*

/**
 * List-like interface to transparently access cached and retrieve not-yet-received messages from Discord.
 *
 * Supports traversing chat history in backward direction (from newest to the oldest messages).
 * It potentially may traverse the whole chat history, so use with caution!
 */
public interface MessageList {
    public suspend fun get(id: Snowflake): Message

    public operator fun get(range: SnowflakeRange): MessageList {
        return subList(range.start, range.endInclusive, toInclusive = true)
    }

    public operator fun get(range: ClosedRange<Instant>): MessageList {
        return subList(range.start, range.endInclusive, toInclusive = true)
    }

    public fun subList(fromId: Snowflake? = null, toId: Snowflake? = null, toInclusive: Boolean = false): MessageList =
        filter {
            if (fromId != null) fromId(fromId)
            if (toId != null) toId(toId, toInclusive)
        }

    public fun subList(fromDate: Instant? = null, toDate: Instant? = null, toInclusive: Boolean = false): MessageList =
        filter {
            if (fromDate != null) fromDate(fromDate)
            if (toDate != null) toDate(toDate, toInclusive)
        }

    public fun filterBy(filter: MessageFilter): MessageList
    public fun asFlow(): Flow<Message>
    public operator fun iterator(): MessageIterator

    public interface MessageIterator {
        public suspend operator fun next(): Message
        public suspend operator fun hasNext(): Boolean
    }
}

public inline fun MessageList.filter(block: MessageFilter.Builder.() -> Unit): MessageList {
    return filterBy(MessageFilter(block))
}
