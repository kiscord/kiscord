/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

@file:JvmMultifileClass
@file:JvmName(name = "EmbedKt")

package kiscord.api

import kiscord.builder.*
import kotlin.jvm.*

@BuilderDsl
public fun Embed.Builder.field(name: String, value: String, inline: Boolean = false) {
    fieldsOrCreate() += Embed.Field.Builder(name, value, inline)
}

@BuilderDsl
public fun Embed.Image.Builder.attachment(attachment: Attachment) {
    url("attachment://${attachment.filename}")
}

public class EmbedValidationException internal constructor(message: String) : RuntimeException(message)

private const val MAX_TOTAL_LENGTH = 6000
private const val MAX_TITLE_LENGTH = 256
private const val MAX_DESCRIPTION_LENGTH = 2048
private const val MAX_FIELDS_COUNT = 25
private const val MAX_FIELD_NAME_LENGTH = 256
private const val MAX_FIELD_VALUE_LENGTH = 1024
private const val MAX_FOOTER_TEXT_LENGTH = 2048
private const val MAX_AUTHOR_NAME_LENGTH = 256

/**
 * Validate [Embed] by using [embed limits](https://discord.com/developers/docs/resources/channel#embed-limits).
 */
public fun Embed.validate(allowEmptyFields: Boolean = true) {
    var totalLength = 0

    fun checkSize(field: String, maxLength: Int, value: String?, allowEmpty: Boolean = true) {
        val length = value?.length ?: 0
        if (!allowEmpty && length == 0) throw EmbedValidationException("Field $field is cannot be empty")
        if (length > maxLength) throw EmbedValidationException("Field $field excess max length: $length > $maxLength")
        totalLength += length
    }

    checkSize("title", MAX_TITLE_LENGTH, title)
    checkSize("description", MAX_DESCRIPTION_LENGTH, description)
    checkSize("footer.text", MAX_FOOTER_TEXT_LENGTH, footer?.text)
    checkSize("author.name", MAX_AUTHOR_NAME_LENGTH, author?.name)

    fields?.let { fields ->
        if (fields.size > MAX_FIELDS_COUNT) throw EmbedValidationException("Embed fields limit outreached: ${fields.size} > $MAX_FIELDS_COUNT")

        fields.forEach { field ->
            checkSize("field.name", MAX_FIELD_NAME_LENGTH, field.name, allowEmptyFields)
            checkSize("field.value", MAX_FIELD_VALUE_LENGTH, field.value, allowEmptyFields)
        }
    }

    if (totalLength > MAX_TOTAL_LENGTH) throw EmbedValidationException("Embed excess max length: $totalLength > $MAX_TOTAL_LENGTH")
}
