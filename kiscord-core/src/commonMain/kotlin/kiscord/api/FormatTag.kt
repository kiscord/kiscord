/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.api

import kotlinx.datetime.*

public sealed class FormatTag {
    public class User(public val userId: Snowflake, public val nickname: Boolean = false) : FormatTag() {
        override fun hashCode(): Int = userId.hashCode() * 31 + nickname.hashCode()
        override fun equals(other: Any?): Boolean =
            other is User && userId == other.userId && nickname == other.nickname

        override fun toString(): String = "<@${if (nickname) "!" else ""}$userId>"
    }

    public class Channel(public val channelId: Snowflake) : FormatTag() {
        override fun hashCode(): Int = channelId.hashCode()
        override fun equals(other: Any?): Boolean = other is Channel && channelId == other.channelId
        override fun toString(): String = "<#$channelId>"
    }

    public class Role(public val roleId: Snowflake) : FormatTag() {
        override fun hashCode(): Int = roleId.hashCode()
        override fun equals(other: Any?): Boolean = other is Role && roleId == other.roleId
        override fun toString(): String = "<@&$roleId>"
    }

    public class StandardEmoji(
        public val emoji: String,
    ) : FormatTag() {
        override fun hashCode(): Int = emoji.hashCode()
        override fun equals(other: Any?): Boolean = other is StandardEmoji && emoji == other.emoji
        override fun toString(): String = emoji
    }

    public class CustomEmoji(
        public val emojiName: String,
        public val emojiId: Snowflake,
        public val animated: Boolean = false
    ) : FormatTag() {
        override fun hashCode(): Int = emojiName.hashCode() * 31 + emojiId.hashCode()
        override fun equals(other: Any?): Boolean =
            other is CustomEmoji && emojiName == other.emojiName && emojiId == other.emojiId && animated == other.animated

        override fun toString(): String = "<${if (animated) "a" else ""}:$emojiName:$emojiId>"
    }

    public class Timestamp(public val unixSeconds: Long, public val style: Style? = null) : FormatTag() {
        override fun hashCode(): Int = unixSeconds.hashCode() * 31 + style.hashCode()
        override fun equals(other: Any?): Boolean =
            other is Timestamp && unixSeconds == other.unixSeconds && style == other.style

        override fun toString(): String = "<t:$unixSeconds${if (style == null) "" else ":${style.tag}"}>"

        public enum class Style(public val tag: Char) {
            ShortTime('t'),
            LongTime('T'),
            ShortDate('d'),
            LongDate('D'),
            ShortDateTime('f'),
            LongDateTime('F'),
            RelativeTime('R'),
        }
    }
}

public fun User.tag(nickname: Boolean = false): FormatTag = FormatTag.User(userId = id, nickname = nickname)
public fun Channel.tag(): FormatTag = FormatTag.Channel(channelId = id)
public fun Role.tag(): FormatTag = FormatTag.Role(roleId = id)
public fun Emoji.tag(): FormatTag = when (id) {
    null -> FormatTag.StandardEmoji(
        emoji = name ?: throw IllegalArgumentException("Illegal emoji object with empty name")
    )
    else -> FormatTag.CustomEmoji(
        emojiName = name ?: throw IllegalArgumentException("Illegal emoji object with empty name"),
        emojiId = id,
        animated = animated == true
    )
}

public fun Instant.tag(style: FormatTag.Timestamp.Style? = null): FormatTag =
    FormatTag.Timestamp(unixSeconds = epochSeconds, style = style)
