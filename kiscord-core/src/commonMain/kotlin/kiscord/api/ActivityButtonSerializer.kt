/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.api

import io.ktor.http.*
import kiscord.serialization.*
import kotlinx.serialization.*
import kotlinx.serialization.builtins.*
import kotlinx.serialization.descriptors.*
import kotlinx.serialization.encoding.*
import kotlinx.serialization.json.*

@OptIn(ExperimentalSerializationApi::class)
internal object ActivityButtonSerializer : KSerializer<Activity.Button> {
    override val descriptor: SerialDescriptor = buildClassSerialDescriptor("kiscord.api.Activity.Button") {
        element("label", String.serializer().descriptor)
        element("url", UrlSerializer.descriptor)
    }

    override fun deserialize(decoder: Decoder): Activity.Button {
        check(decoder is JsonDecoder)
        return when (val json = decoder.decodeJsonElement()) {
            is JsonPrimitive -> {
                Activity.Button(
                    label = decoder.json.decodeFromJsonElement(String.serializer(), json),
                    url = Url("http://hidden-for-privacy/")
                )
            }

            is JsonObject -> {
                val label = json[descriptor.getElementName(0)]?.let {
                    decoder.json.decodeFromJsonElement(String.serializer(), it)
                }
                val url = json[descriptor.getElementName(1)]?.let {
                    decoder.json.decodeFromJsonElement(UrlSerializer, it)
                }
                if (label == null || url == null) {
                    throw MissingFieldException(
                        buildList {
                            if (label == null) add(descriptor.getElementName(0))
                            if (url == null) add(descriptor.getElementName(1))
                        },
                        descriptor.serialName
                    )
                }
                Activity.Button(label = label, url = url)
            }

            is JsonArray -> throw SerializationException("Unsupported json type for ${descriptor.serialName}: JsonArray")
        }
    }

    override fun serialize(encoder: Encoder, value: Activity.Button) = encoder.encodeStructure(descriptor) {
        encodeStringElement(descriptor, 0, value.label)
        encodeSerializableElement(descriptor, 1, UrlSerializer, value.url)
    }
}
