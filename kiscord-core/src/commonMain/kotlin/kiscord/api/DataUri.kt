/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.api

import io.ktor.http.*
import io.ktor.util.*
import io.ktor.utils.io.charsets.*
import kotlinx.serialization.*
import kotlinx.serialization.descriptors.*
import kotlinx.serialization.encoding.*
import kotlinx.serialization.encoding.Encoder
import kotlin.jvm.*

@Serializable(with = DataUri.Serializer::class)
public sealed class DataUri(public val contentType: ContentType?) {
    public class Text @JvmOverloads constructor(
        public val text: String,
        contentType: ContentType = DefaultTextType
    ) : DataUri(contentType) {
        override fun toString(): String {
            val charset = contentType?.charset() ?: Charsets.UTF_8
            val contentType = contentType?.withCharset(charset)?.toString() ?: ""
            return "data:$contentType,${text.encodeURLQueryComponent(encodeFull = true, charset = charset)}"
        }

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other == null || this::class != other::class) return false
            other as Text
            if (contentType != other.contentType) return false
            return text == other.text
        }

        override fun hashCode(): Int {
            return 31 * contentType.hashCode() + text.hashCode()
        }
    }

    public class Binary @JvmOverloads constructor(
        public val data: ByteArray,
        contentType: ContentType = DefaultBinaryType
    ) : DataUri(contentType) {
        override fun toString(): String = "data:${contentType ?: ""};base64,${data.encodeBase64()}"

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other == null || this::class != other::class) return false
            other as Binary
            if (contentType != other.contentType) return false
            return data contentEquals other.data
        }

        override fun hashCode(): Int {
            return 31 * contentType.hashCode() + data.contentHashCode()
        }
    }

    internal object Serializer : KSerializer<DataUri> {
        override val descriptor: SerialDescriptor =
            PrimitiveSerialDescriptor("kiscord.api.DataUri", PrimitiveKind.STRING)

        override fun serialize(encoder: Encoder, value: DataUri) = encoder.encodeString(value.toString())
        override fun deserialize(decoder: Decoder) = parse(decoder.decodeString())
    }

    public companion object {
        private val DefaultTextType get() = ContentType.Text.Plain
        private val DefaultBinaryType get() = ContentType.Application.OctetStream

        private const val DATA_PREFIX = "data:"

        public fun parse(uri: String): DataUri {
            require(uri.startsWith(DATA_PREFIX)) { "Isn't valid data uri" }
            val dataBound = uri.indexOf(',', DATA_PREFIX.length)
            require(dataBound >= 5) { "Isn't valid data uri" }

            val mediaType = uri.substring(5, dataBound)
            val base64 = mediaType.endsWith(";base64")
            val contentType: ContentType

            return if (base64) {
                contentType =
                    mediaType.removeSuffix(";base64").takeUnless { it.isEmpty() }?.let { ContentType.parse(it) }
                        ?: DefaultBinaryType
                val data = uri.substring(dataBound + 1).decodeBase64Bytes()
                Binary(data, contentType)
            } else {
                contentType = mediaType.takeUnless { it.isEmpty() }?.let { ContentType.parse(it) } ?: DefaultTextType
                val charset = contentType.charset() ?: Charsets.UTF_8
                val text = uri.substring(dataBound + 1).decodeURLQueryComponent(charset = charset)
                Text(text, contentType)
            }
        }
    }
}
