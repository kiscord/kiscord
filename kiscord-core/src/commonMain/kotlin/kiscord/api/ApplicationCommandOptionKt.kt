/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

@file:JvmMultifileClass
@file:JvmName(name = "ApplicationCommandOptionKt")

package kiscord.api

import kiscord.builder.*
import kotlin.jvm.*

@BuilderDsl
public fun ApplicationCommandOption.Builder.nameLocalization(vararg nameLocalizations: Pair<Locale, String>) {
    this.nameLocalizations = this.nameLocalizations.map { it.orEmpty() + nameLocalizations }
        .or { nameLocalizations.toMap() }
}

@BuilderDsl
public fun ApplicationCommandOption.Builder.descriptionLocalization(vararg descriptionLocalizations: Pair<Locale, String>) {
    this.descriptionLocalizations = this.descriptionLocalizations.map { it.orEmpty() + descriptionLocalizations }
        .or { descriptionLocalizations.toMap() }
}
