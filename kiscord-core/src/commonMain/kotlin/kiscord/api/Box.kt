/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

@file:Suppress("UNCHECKED_CAST", "NOTHING_TO_INLINE")

package kiscord.api

import kotlinx.serialization.*
import kotlinx.serialization.descriptors.*
import kotlinx.serialization.encoding.*
import kotlin.jvm.*

/**
 * [Box] is generic container for type [T], which can be marked as absent (empty). Behaves similarly to Java's Optional,
 * but allows nullable types as well.
 *
 * Primary usage is to mark properties in kotlinx.serialization as nullable and optional at the same time, to be able to serialize nulls
 * when schema requires it (for example in many request bodies for patch/edit requests) and still have some "default" value
 * (in this case [Box.absent]) to omit property in serialization process.
 *
 * ```kotlin
 * @Serializable
 * data class SomePojo(
 *     // Now we can omit property as it is optional,
 *     // but serialize nulls as-well.
 *     val property: Box<String?> = Box.absent
 * )
 * ```
 */
@JvmInline
@Serializable(with = Box.Serializer::class)
public value class Box<out T> @PublishedApi internal constructor(@PublishedApi internal val _value: Any?) {
    /**
     * Creates absent (empty) [Box]
     */
    public constructor() : this(Absent)

    /**
     * Returns underlying value of type [T] or throws [NoSuchElementException] if this box is absent.
     *
     * @return [T] if box isn't absent
     * @throws NoSuchElementException if box is absent
     */
    public val value: T
        get() {
            if (_value === Absent) {
                throw NoSuchElementException("no value present")
            }
            return _value as T
        }

    /**
     * @return `false` if box is absent
     * @see isAbsent
     */
    public val isPresent: Boolean get() = _value !== Absent

    /**
     * @return `true` if box is absent
     * @see isPresent
     */
    public val isAbsent: Boolean get() = _value === Absent

    @PublishedApi
    internal object Absent {
        override fun toString(): String = "Absent"
    }

    public companion object {
        public val absent: Box<Nothing> = Box(Absent)
        public val nulled: Box<Nothing?> = Box(null)

        public inline fun <T> of(value: T): Box<T> = Box(value)
        public inline fun <T : Any> maybe(value: T?): Box<T> = when (value) {
            null -> absent
            else -> Box(value)
        }
    }

    override fun toString(): String = "Box($_value)"

    @OptIn(ExperimentalSerializationApi::class)
    public open class Serializer<T>(private val valueSerializer: KSerializer<T>) : KSerializer<Box<T>> {
        final override val descriptor: SerialDescriptor = BoxDescriptor(valueSerializer.descriptor)

        private class BoxDescriptor(private val descriptor: SerialDescriptor) : SerialDescriptor {
            override val serialName: String = "kiscord.api.Box<${descriptor.serialName}>"
            override val isInline: Boolean get() = true
            override val kind: SerialKind get() = descriptor.kind

            override val elementsCount: Int get() = 1
            override fun getElementAnnotations(index: Int): List<Annotation> = emptyList()
            override fun getElementDescriptor(index: Int): SerialDescriptor = when (index) {
                0 -> descriptor
                else -> throw IndexOutOfBoundsException()
            }

            override fun getElementIndex(name: String): Int = throw NotImplementedError()
            override fun getElementName(index: Int): String = throw NotImplementedError()
            override fun isElementOptional(index: Int): Boolean = false
        }

        final override fun serialize(encoder: Encoder, value: Box<T>) {
            encoder.encodeInline(descriptor).encodeSerializableValue(valueSerializer, value.value)
        }

        final override fun deserialize(decoder: Decoder): Box<T> {
            return Box(decoder.decodeInline(descriptor).decodeSerializableValue(valueSerializer))
        }
    }
}

public inline infix fun <T, R> Box<T>.map(block: (T) -> R): Box<R> {
    val value = _value
    return if (value === Box.Absent) Box.absent
    else Box(block(value as T))
}

public inline infix fun <T> Box<T>.or(another: T): Box<T> = when {
    isPresent -> this
    else -> Box(another)
}

public inline infix fun <T> Box<T>.or(another: Box<T>): Box<T> = when {
    isPresent -> this
    else -> another
}

public inline infix fun <T> Box<T>.or(another: () -> T): Box<T> = when {
    isPresent -> this
    else -> Box(another())
}

public inline infix fun <T : Any> Box<T?>.computeIfNull(another: () -> T): T {
    val value = _value
    return if (value === Box.Absent || value == null) another()
    else value as T
}

public inline infix fun <T> Box<T>.getOr(another: T): T {
    val value = _value
    return if (value === Box.Absent) another
    else value as T
}

public inline infix fun <T> Box<T>.getOr(another: () -> T): T {
    val value = _value
    return if (value === Box.Absent) another()
    else value as T
}

public val <T> Box<T>.orNull: T?
    inline get() = getOr(null)
