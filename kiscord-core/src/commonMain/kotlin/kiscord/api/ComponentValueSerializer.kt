/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.api

import kiscord.serialization.UnknownFieldException
import kotlinx.serialization.*
import kotlinx.serialization.builtins.*
import kotlinx.serialization.descriptors.*
import kotlinx.serialization.encoding.*

@OptIn(ExperimentalSerializationApi::class)
internal object ComponentValueSerializer : KSerializer<Component.Value> {
    // We're unable to use cool builder, 'cause there is no way to pass buildable value to the builder itself for components field
    override val descriptor: SerialDescriptor = object : SerialDescriptor {
        override val kind: SerialKind get() = StructureKind.OBJECT
        override val serialName: String get() = "kotlinx.serialization.Sealed<kiscord.api.Component.Value>"
        override val elementsCount: Int get() = 4
        override fun getElementAnnotations(index: Int): List<Annotation> = emptyList()
        override fun isElementOptional(index: Int): Boolean = index != 0

        override fun getElementName(index: Int): String = when (index) {
            0 -> "type"
            1 -> "custom_id"
            2 -> "value"
            3 -> "components"
            else -> throw IndexOutOfBoundsException("$index >= $elementsCount")
        }

        override fun getElementIndex(name: String): Int = when (name) {
            "type" -> 0
            "custom_id" -> 1
            "value" -> 2
            "components" -> 3
            else -> CompositeDecoder.UNKNOWN_NAME
        }

        override fun getElementDescriptor(index: Int): SerialDescriptor = when (index) {
            0 -> Component.Type.serializer().descriptor // type
            1 -> String.serializer().descriptor // custom_id
            2 -> String.serializer().descriptor // value
            3 -> listSerialDescriptor(this) // components
            else -> throw IndexOutOfBoundsException("$index >= $elementsCount")
        }
    }

    override fun deserialize(decoder: Decoder): Component.Value = decoder.decodeStructure(descriptor) {
        var type: Component.Type? = null
        var customId: String? = null
        var value: String? = null
        var components: List<Component.Value>? = null

        loop@ while (true) {
            when (val index = decodeElementIndex(descriptor)) {
                0 -> type = decodeSerializableElement(descriptor, index, Component.Type.serializer())
                1 -> customId = decodeStringElement(descriptor, index)
                2 -> value = decodeStringElement(descriptor, index)
                3 -> components = decodeSerializableElement(descriptor, index, ListSerializer(ComponentValueSerializer))
                CompositeDecoder.DECODE_DONE -> break@loop
                else -> throw UnknownFieldException(index)
            }
        }

        val missingFields = mutableListOf<String>()

        when (type) {
            Component.Type.ActionRow -> {
                if (components == null) missingFields += "components"
                if (missingFields.isNotEmpty()) throw MissingFieldException(
                    missingFields,
                    "kiscord.api.Component.ActionRow.Value"
                )

                Component.ActionRow.Value(
                    components = components!!,
                )
            }
            Component.Type.TextInput -> {
                if (customId == null) missingFields += "custom_id"
                if (missingFields.isNotEmpty()) throw MissingFieldException(
                    missingFields,
                    "kiscord.api.Component.TextInput.Value"
                )

                Component.TextInput.Value(
                    customId = customId!!,
                    value = value,
                )
            }
            null -> throw MissingFieldException(listOf("type"), descriptor.serialName)
            else -> throw NotImplementedError("Type $type not supported yet")
        }
    }

    override fun serialize(encoder: Encoder, value: Component.Value) = throw NotImplementedError()
}
