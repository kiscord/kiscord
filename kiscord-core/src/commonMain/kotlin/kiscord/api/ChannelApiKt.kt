/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

@file:JvmMultifileClass
@file:JvmName(name = "ChannelApiKt")
@file:OptIn(SerializableMultiPartDsl::class)

package kiscord.api

import kiscord.*
import kiscord.internal.*
import kiscord.ktor.*
import kotlinx.coroutines.*
import kotlin.jvm.*
import kotlin.time.*
import kotlin.time.Duration.Companion.seconds

/**
 * Create a reaction for the message. This endpoint requires the 'READ_MESSAGE_HISTORY'
 * permission to be present on the current user. Additionally, if nobody else has reacted to the
 * message using this emoji, this endpoint requires the 'ADD_REACTIONS' permission to be present on
 * the current user. Returns a 204 empty response on success.
 *
 * [Discord API Reference](https://discord.com/developers/docs/resources/channel#create-reaction)
 * @param channelId Channel id to retrieve
 * @param messageId Message id to retrieve
 * @param emoji Unicode emoji
 */
@DiscordApiDsl
public suspend inline fun ChannelApi.createReaction(
    channelId: Snowflake,
    messageId: Snowflake,
    emoji: String
): Boolean = createReaction(channelId, messageId, Emoji(name = emoji))

/**
 * Delete a reaction the current user has made for the message
 *
 * [Discord API Reference](https://discord.com/developers/docs/resources/channel#delete-own-reaction)
 * @param channelId Channel id to retrieve
 * @param messageId Message id to retrieve
 * @param emoji Unicode emoji
 */
@DiscordApiDsl
public suspend inline fun ChannelApi.deleteOwnReaction(
    channelId: Snowflake,
    messageId: Snowflake,
    emoji: String
): Boolean = deleteOwnReaction(channelId, messageId, Emoji(name = emoji))

/**
 * Deletes another user's reaction. This endpoint requires the 'MANAGE_MESSAGES' permission to
 * be present on the current user.
 *
 * [Discord API Reference](https://discord.com/developers/docs/resources/channel#delete-user-reaction)
 * @param channelId Channel id to manage
 * @param messageId Message id to manage
 * @param emoji Unicode emoji
 * @param userId User id to manage
 */
@DiscordApiDsl
public suspend inline fun ChannelApi.deleteUserReaction(
    channelId: Snowflake,
    messageId: Snowflake,
    emoji: String,
    userId: Snowflake
): Boolean = deleteUserReaction(channelId, messageId, Emoji(name = emoji), userId)

/**
 * Get a list of users that reacted with this emoji.
 *
 * [Discord API Reference](https://discord.com/developers/docs/resources/channel#get-reactions)
 * @param channelId Channel id to manage
 * @param messageId Message id to manage
 * @param emoji Unicode emoji
 */
@DiscordApiDsl
public suspend fun ChannelApi.getReactions(
    channelId: Snowflake,
    messageId: Snowflake,
    emoji: String
): List<User> = getReactions(channelId, messageId, Emoji(name = emoji))

/**
 * Deletes all the reactions for a given emoji on a message. This endpoint requires the
 * MANAGE_MESSAGES permission to be present on the current user.
 *
 * [Discord API Reference](https://discord.com/developers/docs/resources/channel#delete-all-reactions-for-emoji)
 * @param channelId Channel id to manage
 * @param messageId Message id to manage
 * @param emoji Unicode emoji
 */
@DiscordApiDsl
public suspend fun ChannelApi.deleteAllReactionsForEmoji(
    channelId: Snowflake,
    messageId: Snowflake,
    emoji: String
): Boolean = deleteAllReactionsForEmoji(channelId, messageId, Emoji(name = emoji))

/**
 * Post a typing indicator for the specified channel all time while [block] executed.
 *
 * [Discord API Reference](https://discord.com/developers/docs/resources/channel#trigger-typing-indicator)
 * @param channelId Channel id to trigger
 * @param block Code to execute
 */
@DiscordApiDsl
@OptIn(ExperimentalTime::class)
public suspend fun <R> ChannelApi.triggerTypingIndicator(
    channelId: Snowflake,
    block: suspend () -> R
): R = coroutineScope {
    val timerJob = launch {
        while (isActive) {
            triggerTypingIndicator(channelId)
            delay(5.seconds)
        }
    }

    try {
        block()
    } finally {
        timerJob.cancelAndJoin()
    }
}

@DiscordApiDsl
public suspend fun ChannelApi.createMessageWithTypingIndicator(
    channelId: Snowflake,
    block: suspend CreateMessage.Builder.() -> Unit
): Message {
    val message = triggerTypingIndicator(channelId) {
        val builder = CreateMessage.Builder()
        block(builder)
        builder.build()
    }
    return createMessage(channelId, message)
}

/**
 * Retrieves [MessageList] for a specified channel.
 *
 * Must be called in [KiscordCall] context.
 */
public suspend fun Channel.messages(): MessageList {
    val kiscord = kiscord()
    return kiscord.attributes[MessageListProvider].getMessageList(kiscord, id)
}
