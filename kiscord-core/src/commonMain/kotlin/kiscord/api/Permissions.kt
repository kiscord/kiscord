/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

@file:JvmName("Permissions")

package kiscord.api

import kotlin.jvm.*

internal val Iterable<Permission>.flags: ULong
    get() = when (this) {
        is PermissionSet -> flags
        else -> fold(0UL) { c, r -> c or Permission.enumToValue(r) }
    }

/**
 * Returns [PermissionSet] that contains all permissions of the original collection.
 */
public fun Iterable<Permission>.toPermissionSet(): PermissionSet = PermissionSet.of(flags)

internal val Array<out Permission>.flags: ULong
    get() = fold(0UL) { c, r -> c or Permission.enumToValue(r) }

/**
 * Returns [PermissionSet] that contains all permissions of the original array.
 */
public fun Array<out Permission>.toPermissionSet(): PermissionSet = PermissionSet.of(flags)

/**
 * Returns an empty permission set.
 */
public fun permissionSetOf(): PermissionSet = PermissionSet.Empty

/**
 * Returns a new set of the given [permissions].
 */
public fun permissionSetOf(vararg permissions: Permission): PermissionSet = permissions.toPermissionSet()

/**
 * Returns a set containing all permissions of the original set and then the given [permission] if it isn't already in this set.
 */
public operator fun PermissionSet.plus(permission: Permission): PermissionSet =
    PermissionSet.of(flags or Permission.enumToValue(permission))

/**
 * Returns a set containing all permissions of the original set and then the given [permissions] array, which aren't already in this set.
 */
public operator fun PermissionSet.plus(permissions: Array<out Permission>): PermissionSet =
    PermissionSet.of(flags or permissions.flags)

/**
 * Returns a set containing all permissions of the original set and then the given [permissions] collection, which aren't already in this set.
 */
public operator fun PermissionSet.plus(permissions: Iterable<Permission>): PermissionSet =
    PermissionSet.of(flags or permissions.flags)

/**
 * Returns a set containing all permissions of the original set and then the given [permissions] collection, which aren't already in this set.
 */
public operator fun PermissionSet.plus(permissions: PermissionSet): PermissionSet =
    PermissionSet.of(flags or permissions.flags)

/**
 * Returns a set containing all permissions of the original set except the given [permission].
 */
public operator fun PermissionSet.minus(permission: Permission): PermissionSet =
    PermissionSet.of(flags and Permission.enumToValue(permission).inv())

/**
 * Returns a set containing all permissions of the original set except the permissions contained in the given [permissions] array.
 */
public operator fun PermissionSet.minus(permissions: Array<out Permission>): PermissionSet =
    PermissionSet.of(flags and permissions.flags.inv())

/**
 * Returns a set containing all permissions of the original set except the permissions contained in the given [permissions] collection.
 */
public operator fun PermissionSet.minus(permissions: Iterable<Permission>): PermissionSet =
    PermissionSet.of(flags and permissions.flags.inv())

/**
 * Returns a set containing all permissions of the original set except the permissions contained in the given [permissions] collection.
 */
public operator fun PermissionSet.minus(permissions: PermissionSet): PermissionSet =
    PermissionSet.of(flags and permissions.flags.inv())
