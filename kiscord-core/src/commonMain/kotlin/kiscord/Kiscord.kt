/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

@file:OptIn(SerializableMultiPartDsl::class)

package kiscord

import io.ktor.client.*
import io.ktor.client.engine.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.plugins.cookies.*
import io.ktor.client.request.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import io.ktor.util.pipeline.*
import io.ktor.utils.io.core.*
import kiscord.api.*
import kiscord.builder.*
import kiscord.ktor.*
import kiscord.serialization.*
import kotlinx.coroutines.*
import org.kodein.log.*
import kotlin.coroutines.*
import kotlin.time.*

/**
 * Entrypoint for all Discord's API endpoints
 * @see auditLog
 * @see channel
 * @see command
 * @see emoji
 * @see gateway
 * @see guild
 * @see guildTemplate
 * @see guildScheduledEvent
 * @see invite
 * @see oAuth2
 * @see stage
 * @see sticker
 * @see user
 * @see voice
 * @see webhook
 */
@DiscordApiDsl
public class Kiscord private constructor(
    public val httpClient: HttpClient,
    override val attributes: TypedAttributes
) : PluginScope, Closeable, CoroutineScope {
    override val coroutineContext: CoroutineContext get() = httpClient.coroutineContext

    override fun close() {
        httpClient.close()
    }

    override fun toString(): String {
        return "Kiscord(httpClient=$httpClient, attributes=$attributes)"
    }

    @BuilderDsl
    public interface Builder : PluggableBuilder<Kiscord>

    private class BuilderImpl : PluggableBuilderImpl<Kiscord>(), Builder {
        override fun create(attributes: TypedAttributes): Kiscord {
            val httpClient = httpClientFactory.create(ConfigurationChain {
                developmentMode = DEBUG

                install(HttpCookies)
                install(KiscordBranding)
                install(KiscordAuthentication) {
                    apiUrl = discordApiUrl
                    defaultToken = token
                }
                install(SerializableMultiPartCoder) {
                    textCodec {
                        json(DiscordSerialFormat.Json)
                    }
                }
                install(ContentNegotiation) {
                    json(DiscordSerialFormat.Json)
                }
                install(DefaultRequest) {
                    url.takeFrom(discordApiUrl)
                }
                install(HttpRequestRetry) {
                    retryIf { _, response -> response.status == HttpStatusCode.TooManyRequests || response.status.value in 500..599 }
                    delayMillis(respectRetryAfterHeader = true) {
                        val response = response ?: return@delayMillis 0L
                        val after = response.headers["X-RateLimit-Reset-After"] ?: return@delayMillis 0L
                        after.toDouble().toDuration(DurationUnit.SECONDS).inWholeMilliseconds
                    }
                }
                install(HttpTimeout)
            })

            attributes[HttpClientFactoryAttr] = null

            return Kiscord(httpClient, attributes)
        }
    }

    public companion object : BuilderSpec<Kiscord, Builder> {
        private val ProductionUrl: Url = Url("${BuildConfig.DISCORD_API_URL}/v${BuildConfig.DISCORD_API_VERSION}/")

        internal val TokenAttr = TypedAttribute("Kiscord:Api:Token", Token.Default)
        internal val DiscordApiUrlAttr = TypedAttribute("Kiscord:Api:DiscordApiUrl", ProductionUrl)
        internal val HttpClientFactoryAttr =
            TypedAttribute<HttpClientFactory>("Kiscord:Api:HttpClientFactory", HttpClientFactory.Default)
        internal val LoggerFactoryAttr =
            TypedAttribute("Kiscord:Api:LoggerFactory") { DefaultLoggerFactory }

        public var DefaultLoggerFactory: LoggerFactory
            get() = DefaultKiscordLoggerFactory
            set(value) {
                DefaultKiscordLoggerFactory = value
            }

        /**
         * Library name, i.e. `Kiscord`
         */
        public const val NAME: String = BuildConfig.NAME

        /**
         * Library version, e.g. 1.0.0
         */
        public const val VERSION: String = BuildConfig.VERSION

        /**
         * Library homepage url
         */
        public const val URL: String = BuildConfig.URL

        /**
         * Discord API version, used by Kiscord
         */
        public const val API_VERSION: Int = BuildConfig.DISCORD_API_VERSION

        /**
         * Kotlin version, used at compile time of Kiscord
         */
        public const val KOTLIN_VERSION: String = BuildConfig.KOTLIN_VERSION

        /**
         * Kotlin platform, one of `JVM`, `JS` or `Native`
         */
        public val KOTLIN_PLATFORM: String = kiscord.internal.KotlinPlatform

        /**
         * Is debug mode enabled in currently used build
         * @suppress
         */
        @KiscordUnstableAPI
        public const val DEBUG: Boolean = BuildConfig.DEBUG

        override fun builder(): Builder = BuilderImpl()
    }

}

@BuilderDsl
public inline fun Kiscord(
    engineFactory: HttpClientEngineFactory<*>,
    block: Kiscord.Builder.() -> Unit = {}
): Kiscord = Kiscord.builder().apply {
    httpClient(engineFactory)
    block()
}.build()

@BuilderDsl
public inline fun Kiscord(
    engine: HttpClientEngine,
    block: Kiscord.Builder.() -> Unit = {}
): Kiscord = Kiscord.builder().apply {
    httpClient(engine)
    block()
}.build()

@BuilderDsl
public inline fun Kiscord(
    block: Kiscord.Builder.() -> Unit = {}
): Kiscord {
    return Kiscord.builder().apply(block).build()
}

public var Kiscord.Builder.token: Token by Kiscord.TokenAttr
public val Kiscord.token: Token by Kiscord.TokenAttr

public var Kiscord.Builder.discordApiUrl: Url by Kiscord.DiscordApiUrlAttr
public val Kiscord.discordApiUrl: Url by Kiscord.DiscordApiUrlAttr

public var Kiscord.Builder.httpClientFactory: HttpClientFactory by Kiscord.HttpClientFactoryAttr

public var Kiscord.Builder.loggerFactory: LoggerFactory by Kiscord.LoggerFactoryAttr
public val Kiscord.loggerFactory: LoggerFactory by Kiscord.LoggerFactoryAttr

@BuilderDsl
public fun Kiscord.Builder.token(token: Token) {
    this.token = token
}

@BuilderDsl
public inline fun Kiscord.Builder.discordApiUrl(block: URLBuilder.() -> Unit) {
    discordApiUrl = URLBuilder().takeFrom(discordApiUrl).apply(block).build()
}

@BuilderDsl
public fun Kiscord.Builder.httpClient(block: HttpClientConfig<*>.() -> Unit) {
    httpClientFactory = httpClientFactory.withConfig(block)
}

@BuilderDsl
public fun Kiscord.Builder.httpClient(
    engine: HttpClientEngine,
    block: HttpClientConfig<*>.() -> Unit = {}
) {
    httpClientFactory = httpClientFactory.withNewRoot(HttpClientFactory.Engine(engine, block))
}

@BuilderDsl
public fun <T : HttpClientEngineConfig> Kiscord.Builder.httpClient(
    engineFactory: HttpClientEngineFactory<T>,
    block: HttpClientConfig<T>.() -> Unit = {}
) {
    httpClientFactory = httpClientFactory.withNewRoot(HttpClientFactory.EngineFactory(engineFactory, block))
}

@PublishedApi
internal suspend fun kiscordCall(): KiscordCall {
    return coroutineContext[KiscordCall] ?: throw IllegalStateException("Not in kiscord call")
}

@PublishedApi
internal suspend fun kiscord(): Kiscord = kiscordCall().client

internal var DefaultKiscordLoggerFactory: LoggerFactory = LoggerFactory.default
