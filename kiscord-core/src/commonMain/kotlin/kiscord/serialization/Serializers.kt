/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.serialization

import io.ktor.http.*
import kiscord.api.*
import kotlinx.datetime.*
import kotlinx.datetime.serializers.*
import kotlinx.serialization.*
import kotlinx.serialization.builtins.*
import kotlinx.serialization.descriptors.*
import kotlinx.serialization.encoding.*
import kotlin.time.*
import kotlin.time.Duration.Companion.seconds

/**
 * @suppress
 */
@KiscordUnstableAPI
public object UrlSerializer : KSerializer<Url> {
    override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor("io.ktor.http.Url", PrimitiveKind.STRING)
    override fun deserialize(decoder: Decoder): Url = Url(decoder.decodeString())
    override fun serialize(encoder: Encoder, value: Url): Unit = encoder.encodeString(value.toString())
}

/**
 * @suppress
 */
@KiscordUnstableAPI
public object InstantEpochMillisecondsSerializer : KSerializer<Instant> {
    override val descriptor: SerialDescriptor =
        PrimitiveSerialDescriptor("kotlinx.datetime.Instant", PrimitiveKind.LONG)

    override fun deserialize(decoder: Decoder): Instant = Instant.fromEpochMilliseconds(decoder.decodeLong())
    override fun serialize(encoder: Encoder, value: Instant): Unit = encoder.encodeLong(value.toEpochMilliseconds())
}

/**
 * @suppress
 */
@KiscordUnstableAPI
public object InstantEpochSecondsSerializer : KSerializer<Instant> {
    override val descriptor: SerialDescriptor =
        PrimitiveSerialDescriptor("kotlinx.datetime.Instant", PrimitiveKind.LONG)

    override fun deserialize(decoder: Decoder): Instant = Instant.fromEpochSeconds(decoder.decodeLong())
    override fun serialize(encoder: Encoder, value: Instant): Unit = encoder.encodeLong(value.epochSeconds)
}

/**
 * @suppress
 */
@KiscordUnstableAPI
public object BoxInstantIso8601Serializer : Box.Serializer<Instant>(InstantIso8601Serializer)

/**
 * @suppress
 */
@KiscordUnstableAPI
public object BoxNullableInstantIso8601Serializer : Box.Serializer<Instant?>(InstantIso8601Serializer.nullable)

@KiscordUnstableAPI
public object DurationSecondsSerializer : KSerializer<Duration> {
    override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor("kotlin.time.Duration", PrimitiveKind.LONG)
    override fun serialize(encoder: Encoder, value: Duration): Unit = encoder.encodeLong(value.inWholeSeconds)

    override fun deserialize(decoder: Decoder): Duration = decoder.decodeLong().seconds
}
