package kiscord.serialization

import kotlinx.serialization.*

@PublishedApi
internal class UnknownFieldException
internal constructor(message: String?) : SerializationException(message) {
    constructor(index: Int) : this("An unknown field for index $index")
}
