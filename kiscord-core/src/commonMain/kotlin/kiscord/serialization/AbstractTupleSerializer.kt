/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.serialization

import kiscord.api.*
import kotlinx.serialization.*
import kotlinx.serialization.descriptors.*
import kotlinx.serialization.encoding.*

/**
 * @suppress
 */
@KiscordUnstableAPI
public abstract class AbstractTupleSerializer<R>(name: String, vararg serializers: KSerializer<*>) : KSerializer<R> {
    final override val descriptor: SerialDescriptor =
        TupleDescriptor(name, Array(serializers.size) { serializers[it].descriptor })

    protected fun <T> CompositeDecoder.decodeTupleElement(
        descriptor: SerialDescriptor,
        index: Int,
        deserializer: DeserializationStrategy<T>
    ): T {
        require(decodeElementIndex(descriptor) == index) { "Unexpected element index" }
        return decodeSerializableElement(descriptor, index, deserializer)
    }

    @OptIn(ExperimentalSerializationApi::class)
    private class TupleDescriptor(
        override val serialName: String,
        private val descriptors: Array<out SerialDescriptor>
    ) : SerialDescriptor {
        override val kind get() = StructureKind.LIST
        override val elementsCount get() = descriptors.size
        override fun getElementIndex(name: String) = name.toInt()
        override fun getElementName(index: Int) = index.toString()
        override fun getElementDescriptor(index: Int) = descriptors[index]
        override fun getElementAnnotations(index: Int) = emptyList<Annotation>()
        override fun isElementOptional(index: Int) = false
    }
}
