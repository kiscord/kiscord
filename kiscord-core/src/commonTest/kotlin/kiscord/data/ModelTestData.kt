/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.data

import kotlinx.serialization.*

abstract class ModelTestData<S, T> : TestData<ModelTestData.ModelWithSource<S, T>> {
    abstract val serializer: KSerializer<T>

    private val _all = mutableSetOf<ModelWithSource<S, T>>()
    override val all: Collection<ModelWithSource<S, T>> get() = _all

    protected fun produce(source: S, model: T): ModelWithSource<S, T> {
        return ModelWithSource(source, model).also { _all += it }
    }

    data class ModelWithSource<out S, out T>(val source: S, val model: T)
}

typealias StringModelTestData<T> = ModelTestData<String, T>
