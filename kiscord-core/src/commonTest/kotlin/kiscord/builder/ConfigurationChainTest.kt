/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.builder

import kotlin.test.*

class ConfigurationChainTest {
    @Test
    fun simple() {
        var step = false
        val chain = ConfigurationChain<Unit> { step = true }

        chain(Unit)
        assertTrue(step)
    }

    @Test
    fun chained() {
        var step0 = false
        var step1 = false
        var step2 = false
        var chain = ConfigurationChain<Unit> { step0 = true }
        chain = chain + { step1 = true }
        chain = chain + { step2 = true }

        chain(Unit)
        assertTrue(step0)
        assertTrue(step1)
        assertTrue(step2)
    }

    @Test
    fun empty() {
        var step0 = false
        var step1 = false
        var chain = ConfigurationChain<Unit>()
        chain = chain + { step0 = true }
        chain = chain + { step1 = true }

        chain(Unit)
        assertTrue(step0)
        assertTrue(step1)
    }
}
