/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.api

import kotlin.test.*

class PermissionSetTest {
    @Test
    fun testBasicSets() {
        enumValues<Permission>().forEach { permission ->
            val set = PermissionSet(Permission.enumToValue(permission))

            assertEquals(1, set.size)
            assertTrue("Permission $permission should be in the set") {
                permission in set
            }
            assertEquals(permission, set.single())
        }
    }

    @Test
    fun testNeighborSets() {
        val values = enumValues<Permission>()
        values.forEachIndexed { i, permission ->
            val neighbor = values[(i + 1) % values.size]

            val permissionFlag = Permission.enumToValue(permission)
            val neighborFlag = Permission.enumToValue(neighbor)
            val set = PermissionSet(permissionFlag or neighborFlag)

            assertEquals(2, set.size)
            assertTrue("Permission $permission should be in the set") {
                permission in set
            }
            assertTrue("Permission $neighbor should be in the set") {
                neighbor in set
            }

            assertEquals(setOf(permission, neighbor), set)
        }
    }

    @Test
    fun testDispersed() {
        val values = enumValues<Permission>()

        values.forEachIndexed { i, permission ->
            val dispersed = values[values.size - i - 1]

            if (permission == dispersed) return@forEachIndexed

            val permissionFlag = Permission.enumToValue(permission)
            val dispersedFlag = Permission.enumToValue(dispersed)
            val set = PermissionSet(permissionFlag or dispersedFlag)

            assertEquals(2, set.size)
            assertTrue("Permission $permission should be in the set") {
                permission in set
            }
            assertTrue("Permission $dispersed should be in the set") {
                dispersed in set
            }

            assertEquals(setOf(permission, dispersed), set)
        }
    }
}
