/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.api

import kiscord.data.*

class EmojiTest : JsonModelTest<Emoji>() {
    override val testData get() = Data

    companion object Data : StringModelTestData<Emoji>() {
        override val serializer get() = Emoji.serializer()

        val Emoji1 = produce(
            """
            {
                "id": "41771983429993937",
                "name": "LUL",
                "roles": [
                    "41771983429993000",
                    "41771983429993111"
                ],
                "user": {
                    "id": "96008815106887111",
                    "username": "Luigi",
                    "discriminator": "0002",
                    "global_name": null,
                    "avatar": "5500909a3274e1812beb4e8de6631111"
                },
                "require_colons": true,
                "managed": false,
                "animated": false
            }
        """.trimIndent(), Emoji(
                id = Snowflake(41771983429993937u),
                name = "LUL",
                roles = setOf(
                    Snowflake(41771983429993000u),
                    Snowflake(41771983429993111u)
                ),
                user = User(
                    id = Snowflake(96008815106887111u),
                    username = "Luigi",
                    discriminator = "0002",
                    avatar = "5500909a3274e1812beb4e8de6631111"
                ),
                requireColons = true,
                managed = false,
                animated = false
            )
        )
    }
}
