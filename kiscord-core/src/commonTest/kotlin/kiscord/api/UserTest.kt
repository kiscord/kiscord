/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.api

import kiscord.data.*

class UserTest : JsonModelTest<User>() {
    override val testData get() = Data

    companion object Data : StringModelTestData<User>() {
        override val serializer get() = User.serializer()

        val Nelly = produce(
            """
            {
                "id": "80351110224678912",
                "username": "Nelly",
                "discriminator": "1337",
                "global_name": "Nelly",
                "avatar": "8342729096ea3675442027381ff50dfe",
                "verified": true,
                "email": "nelly@discord.com",
                "flags": 64,
                "premium_type": 1
            }
        """.trimIndent(), User(
                id = Snowflake(80351110224678912u),
                username = "Nelly",
                discriminator = "1337",
                globalName = "Nelly",
                avatar = "8342729096ea3675442027381ff50dfe",
                verified = true,
                email = Box.of("nelly@discord.com"),
                flags = setOf(User.Flag.HypeSquadOnlineHouse1),
                premiumType = User.PremiumType.NitroClassic
            )
        )
    }
}
