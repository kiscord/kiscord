/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.api

import kiscord.api.Activity.Type.*
import kotlin.test.*

class ActivityTest {
    private val data = mapOf(
        "Playing Rocket League" to Activity("Rocket League", Game),
        "Streaming Rocket League" to Activity("Rocket League", Streaming),
        "Listening to Spotify" to Activity("Spotify", Listening),
        "Watching Game of Thrones" to Activity("Game of Thrones", Watching),
        "Competing in Rocket League" to Activity("Rocket League", Competing),
    )

    @Test
    fun testTypeFormat() {
        data.forEach { (displayName, bean) ->
            assertEquals(displayName, bean.displayName)
        }
    }
}
