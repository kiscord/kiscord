/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.api

import io.ktor.client.engine.mock.*
import io.ktor.client.request.*
import io.ktor.http.*
import kiscord.test.*
import kotlin.test.*

class GatewayApiTest : ApiMockTest() {
    override suspend fun MockRequestHandleScope.proceed(request: HttpRequestData) = when (request.url.encodedPath) {
        "/api/gateway" -> respondJson("""
            {
                "url": "wss://gateway.discord.gg/"
            }
        """.trimIndent())
        "/api/gateway/bot" -> respondJson("""
            {
                "url": "wss://gateway.discord.gg/",
                "shards": 9,
                "session_start_limit": {
                    "total": 1000,
                    "remaining": 999,
                    "reset_after": 14400000
                }
            }
        """.trimIndent())
        else -> respondError(HttpStatusCode.NotFound)
    }

    @Test
    fun testNormalGateway() = withApi {
        val expected = GetGateway(url = Url("wss://gateway.discord.gg/"))
        assertEquals(expected, gateway.getGateway())
    }

    @Test
    fun testShardedGateway() = withApi {
        val expected = GetGateway.Bot(
                url = Url("wss://gateway.discord.gg/"),
                shards = 9,
                sessionStartLimit = GetGateway.Bot.SessionStartLimit(
                        total = 1000,
                        remaining = 999,
                        resetAfter = 14400000
                )
        )

        assertEquals(expected, gateway.getGatewayBot())
    }
}
