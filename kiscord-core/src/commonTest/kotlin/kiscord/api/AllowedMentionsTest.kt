/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.api

import kiscord.builder.*
import kiscord.data.*

class AllowedMentionsTest : JsonModelTest<AllowedMentions>() {
    override val testData get() = Data

    companion object Data : StringModelTestData<AllowedMentions>() {
        override val serializer get() = AllowedMentions.serializer()

        val ParseNone = produce("""
            {
                "parse": [
                ]
            }
        """.trimIndent(), AllowedMentions {
            parse()
        })

        val ParseAll = produce("""
            {
                "parse": [
                    "roles",
                    "users",
                    "everyone"
                ]
            }
        """.trimIndent(), AllowedMentions {
            parse(AllowedMentions.Type.Roles, AllowedMentions.Type.Users, AllowedMentions.Type.Everyone)
        })

        val OnlyUsers = produce("""
            {
                "users": [
                    "123",
                    "124"
                ]
            }
        """.trimIndent(), AllowedMentions {
            users(Snowflake(123U), Snowflake(124U))
        })

        val OnlyRoles = produce("""
            {
                "roles": [
                    "123",
                    "124"
                ]
            }
        """.trimIndent(), AllowedMentions {
            roles(Snowflake(123U), Snowflake(124U))
        })

        val UsersAndRoles = produce("""
            {
                "roles": [
                    "123",
                    "124"
                ],
                "users": [
                    "223",
                    "224"
                ]
            }
        """.trimIndent(), AllowedMentions {
            roles(Snowflake(123U), Snowflake(124U))
            users(Snowflake(223U), Snowflake(224U))
        })
    }
}
