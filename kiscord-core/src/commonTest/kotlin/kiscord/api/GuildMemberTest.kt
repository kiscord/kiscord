/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.api

import kiscord.data.*
import kotlinx.datetime.*

class GuildMemberTest : JsonModelTest<GuildMember>() {
    override val testData get() = Data

    companion object Data : StringModelTestData<GuildMember>() {
        override val serializer get() = GuildMember.serializer()

        val Nelly = produce(
            """
            {
                "user": {
                    "id": "80351110224678912",
                    "username": "Nelly",
                    "discriminator": "1337",
                    "global_name": "Nelly",
                    "avatar": "8342729096ea3675442027381ff50dfe"
                },
                "nick": "NOT API SUPPORT",
                "roles": [
                    "41771983429993000"
                ],
                "joined_at": "2015-04-26T06:26:56.936Z",
                "deaf": false,
                "mute": false
            }
        """.trimIndent(), GuildMember(
                user = User(
                    id = Snowflake(80351110224678912u),
                    username = "Nelly",
                    discriminator = "1337",
                    globalName = "Nelly",
                    avatar = "8342729096ea3675442027381ff50dfe"
                ),
                nick = Box.of("NOT API SUPPORT"),
                roles = setOf(Snowflake(41771983429993000u)),
                joinedAt = LocalDateTime(
                    year = 2015, month = Month.APRIL, dayOfMonth = 26,
                    hour = 6, minute = 26, second = 56, nanosecond = 936000000
                ).toInstant(TimeZone.UTC),
                deaf = false,
                mute = false
            )
        )
    }
}
