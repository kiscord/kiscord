/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.api

import io.ktor.client.engine.mock.*
import io.ktor.client.request.*
import io.ktor.http.*
import kiscord.test.*
import kotlin.test.*

class UserApiTest : ApiMockTest() {
    override suspend fun MockRequestHandleScope.proceed(request: HttpRequestData) = when (request.url.encodedPath) {
        "/api/users/@me", "/api/users/${UserTest.Nelly.model.id}" -> respondJson(UserTest.Nelly.source)
        "/api/users/@me/guilds/41771983423143937" -> respondNoContent()
        else -> respondError(HttpStatusCode.NotFound)
    }

    @Test
    fun testGetCurrentUser() = withApi {
        assertEquals(UserTest.Nelly.model, user.getCurrentUser())
    }

    @Test
    fun testGetUser() = withApi {
        assertEquals(UserTest.Nelly.model, user.getUser(UserTest.Nelly.model.id))
    }

    @Test
    fun testLeaveGuild() = withApi {
        assertTrue(user.leaveGuild(Snowflake(41771983423143937u)))
    }

    @Test
    fun testModifyUser() = withApi {
        val actual = user.modifyCurrentUser {
            username = UserTest.Nelly.model.username
        }
        assertEquals(UserTest.Nelly.model, actual)
    }
}
