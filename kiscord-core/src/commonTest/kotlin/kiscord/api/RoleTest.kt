/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.api

import kiscord.data.*

class RoleTest : JsonModelTest<Role>() {
    override val testData get() = Data

    companion object Data : StringModelTestData<Role>() {
        override val serializer get() = Role.serializer()

        val Role1 = produce(
            """
            {
                "id": "41771983423143936",
                "name": "WE DEM BOYZZ!!!!!!",
                "color": 3447003,
                "hoist": true,
                "position": 1,
                "permissions": "66321471",
                "managed": false,
                "mentionable": false
            }
        """.trimIndent(), Role(
                id = Snowflake(41771983423143936u),
                name = "WE DEM BOYZZ!!!!!!",
                color = 3447003,
                hoist = true,
                position = 1,
                permissions = PermissionSet(66321471u),
                managed = false,
                mentionable = false
            )
        )
    }
}
