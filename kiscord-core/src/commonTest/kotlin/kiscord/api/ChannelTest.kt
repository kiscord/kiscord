/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.api

import kiscord.data.*

class ChannelTest : JsonModelTest<Channel>() {
    override val testData get() = Data

    companion object Data : StringModelTestData<Channel>() {
        override val serializer get() = Channel.serializer()

        val GuildText = produce(
            """
            {
                "id": "41771983423143937",
                "type": 0,
                "guild_id": "41771983423143937",
                "position": 6,
                "permission_overwrites": [
                ],
                "name": "general",
                "topic": "24/7 chat about how to gank Mike #2",
                "nsfw": true,
                "last_message_id": "155117677105512449",
                "rate_limit_per_user": 2,
                "parent_id": "399942396007890945"
            }
        """.trimIndent(), Channel(
                id = Snowflake(41771983423143937u),
                type = Channel.Type.GuildText,
                guildId = Snowflake(41771983423143937u),
                position = 6,
                permissionOverwrites = emptySet(),
                name = Box.of("general"),
                topic = Box.of("24/7 chat about how to gank Mike #2"),
                nsfw = true,
                lastMessageId = Box.of(Snowflake(155117677105512449u)),
                rateLimitPerUser = 2,
                parentId = Box.of(Snowflake(399942396007890945u))
            )
        )

        val DM = produce(
            """
            {
                "id": "319674150115610528",
                "type": 1,
                "last_message_id": "3343820033257021450",
                "recipients": [
                    {
                        "id": "82198898841029460",
                        "username": "test",
                        "discriminator": "9999",
                        "global_name": "Test",
                        "avatar": "33ecab261d4681afa4d85a04691c4a01"
                    }
                ]
            }
        """.trimIndent(), Channel(
                id = Snowflake(319674150115610528u),
                type = Channel.Type.DM,
                lastMessageId = Box.of(Snowflake(3343820033257021450u)),
                recipients = setOf(
                    User(
                        id = Snowflake(82198898841029460u),
                        username = "test",
                        discriminator = "9999",
                        globalName = "Test",
                        avatar = "33ecab261d4681afa4d85a04691c4a01"
                    )
                )
            )
        )

        val GuildVoice = produce(
            """
            {
                "id": "155101607195836416",
                "type": 2,
                "guild_id": "41771983423143937",
                "position": 5,
                "permission_overwrites": [
                ],
                "name": "ROCKET CHEESE",
                "nsfw": false,
                "bitrate": 64000,
                "user_limit": 0
            }
        """.trimIndent(), Channel(
                id = Snowflake(155101607195836416u),
                type = Channel.Type.GuildVoice,
                guildId = Snowflake(41771983423143937u),
                position = 5,
                permissionOverwrites = emptySet(),
                name = Box.of("ROCKET CHEESE"),
                nsfw = false,
                bitrate = 64000,
                userLimit = 0
            )
        )

        val GroupDM = produce(
            """
            {
                "id": "319674150115710528",
                "type": 3,
                "name": "Some test channel",
                "last_message_id": "3343820033257021450",
                "recipients": [
                    {
                        "id": "82198898841029460",
                        "username": "test",
                        "discriminator": "9999",
                        "global_name": "Test",
                        "avatar": "33ecab261d4681afa4d85a04691c4a01"
                    },
                    {
                        "id": "82198810841029460",
                        "username": "test2",
                        "discriminator": "9999",
                        "global_name": "Test2",
                        "avatar": "33ecab261d4681afa4d85a10691c4a01"
                    }
                ],
                "owner_id": "82198810841029460"
            }
        """.trimIndent(), Channel(
                id = Snowflake(319674150115710528u),
                type = Channel.Type.GroupDM,
                name = Box.of("Some test channel"),
                lastMessageId = Box.of(Snowflake(3343820033257021450u)),
                recipients = setOf(
                    User(
                        id = Snowflake(82198898841029460u),
                        username = "test",
                        discriminator = "9999",
                        globalName = "Test",
                        avatar = "33ecab261d4681afa4d85a04691c4a01"
                    ),
                    User(
                        id = Snowflake(82198810841029460u),
                        username = "test2",
                        discriminator = "9999",
                        globalName = "Test2",
                        avatar = "33ecab261d4681afa4d85a10691c4a01"
                    )
                ),
                ownerId = Snowflake(82198810841029460u)
            )
        )

        val GuildCategory = produce(
            """
            {
                "id": "399942396007890945",
                "type": 4,
                "guild_id": "290926798629997250",
                "position": 0,
                "permission_overwrites": [
                ],
                "name": "Test",
                "nsfw": false
            }
        """.trimIndent(), Channel(
                id = Snowflake(399942396007890945u),
                type = Channel.Type.GuildCategory,
                guildId = Snowflake(290926798629997250u),
                position = 0,
                permissionOverwrites = setOf(),
                name = Box.of("Test"),
                nsfw = false
            )
        )

        val GuildAnnouncement = produce(
            """
            {
                "id": "41771983423143932",
                "type": 5,
                "guild_id": "41771983423143937",
                "position": 6,
                "permission_overwrites": [
                ],
                "name": "important-news",
                "topic": "Rumors about Half Life 3",
                "nsfw": true,
                "last_message_id": "155117677105512449",
                "parent_id": "399942396007890945"
            }
        """.trimIndent(), Channel(
                id = Snowflake(41771983423143932u),
                type = Channel.Type.GuildAnnouncement,
                guildId = Snowflake(41771983423143937u),
                position = 6,
                permissionOverwrites = emptySet(),
                name = Box.of("important-news"),
                topic = Box.of("Rumors about Half Life 3"),
                nsfw = true,
                lastMessageId = Box.of(Snowflake(155117677105512449u)),
                parentId = Box.of(Snowflake(399942396007890945u))
            )
        )
    }
}
