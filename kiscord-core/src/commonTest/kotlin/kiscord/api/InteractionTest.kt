/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.api

import kiscord.data.*

class InteractionTest : JsonModelTest<Interaction>() {
    override val testData get() = Data

    companion object Data : StringModelTestData<Interaction>() {
        override val serializer get() = Interaction.serializer()

        val Ping = produce(
            """
            {
                "id": "123",
                "application_id": "456",
                "type": 1,
                "token": "foobar",
                "version": 1
            }
        """.trimIndent(), Interaction(
                id = Snowflake(123UL),
                applicationId = Snowflake(456UL),
                type = Interaction.Type.Ping,
                token = "foobar",
                version = 1
            )
        )

        val Command = produce(
            """
            {
                "id": "123",
                "application_id": "456",
                "type": 2,
                "data": {
                    "id": "789",
                    "name": "test",
                    "type": 1
                },
                "token": "foobar",
                "version": 1
            }
        """.trimIndent(), Interaction(
                id = Snowflake(123UL),
                applicationId = Snowflake(456UL),
                type = Interaction.Type.ApplicationCommand,
                data = Interaction.Data.Command(
                    id = Snowflake(789UL),
                    name = "test",
                    type = ApplicationCommand.Type.ChatInput
                ),
                token = "foobar",
                version = 1
            )
        )
    }
}
