/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.api

import io.ktor.http.*
import io.ktor.utils.io.charsets.*
import io.ktor.utils.io.core.*
import kiscord.data.*
import kotlinx.serialization.*

class DataUriTest : JsonModelTest<DataUri>() {
    override val testData: ModelTestData<String, DataUri> get() = Data

    companion object Data : StringModelTestData<DataUri>() {
        override val serializer: KSerializer<DataUri> get() = DataUri.serializer()

        val text1 = produce(
            "\"data:text/plain; charset=UTF-8,A%20brief%20note\"",
            DataUri.Text("A brief note", contentType = ContentType.Text.Plain.withCharset(Charsets.UTF_8))
        )

        val binary1 = produce(
            "\"data:application/octet-stream;base64,Zm9v\"",
            DataUri.Binary("foo".toByteArray(), ContentType.Application.OctetStream)
        )
    }
}
