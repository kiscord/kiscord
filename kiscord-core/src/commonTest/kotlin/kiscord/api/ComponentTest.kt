/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.api

import kiscord.data.*

class ComponentTest : JsonModelTest<Component>() {
    override val testData get() = Data

    companion object Data : StringModelTestData<Component>() {
        override val serializer get() = Component.serializer()

        val Button = produce(
            """
            {
                "type": 2,
                "custom_id": "button_one",
                "disabled": true,
                "style": 4,
                "label": "Label",
                "emoji": {
                    "id": "123",
                    "name": "kappa"
                }
            }
        """.trimIndent(), Component.Button(
                style = Component.Button.Style.Danger,
                label = "Label",
                emoji = Emoji(id = Snowflake(123UL), name = "kappa"),
                customId = "button_one",
                disabled = true,
            )
        )

        val SelectMenu = produce(
            """
            {
                "type": 3,
                "custom_id": "menu_one",
                "options": [
                    {
                        "label": "Option One",
                        "value": "value_one"
                    }
                ],
                "placeholder": "Choice some",
                "min_values": 5,
                "max_values": 10
            }
        """.trimIndent(), Component.SelectMenu(
                customId = "menu_one",
                options = listOf(
                    Component.SelectMenu.Option(
                        label = "Option One",
                        value = "value_one"
                    )
                ),
                placeholder = "Choice some",
                minValues = 5,
                maxValues = 10,
            )
        )

        val ActionRow = produce(
            """
            {
                "type": 1,
                "components": [
                    {
                        "type": 2,
                        "custom_id": "button_one",
                        "disabled": true,
                        "style": 4,
                        "label": "Label",
                        "emoji": {
                            "id": "123",
                            "name": "kappa"
                        }
                    },
                    {
                        "type": 3,
                        "custom_id": "menu_one",
                        "options": [
                            {
                                "label": "Option One",
                                "value": "value_one"
                            }
                        ],
                        "placeholder": "Choice some",
                        "min_values": 5,
                        "max_values": 10
                    }
                ]
            }
        """.trimIndent(), Component.ActionRow(
                components = listOf(Button.model, SelectMenu.model)
            )
        )
    }
}
