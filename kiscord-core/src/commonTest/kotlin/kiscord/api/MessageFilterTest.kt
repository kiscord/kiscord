/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.api

import kiscord.builder.*
import kotlinx.datetime.*
import kotlin.test.*

class MessageFilterTest {
    data class Data<T : Any>(
        val first: T,
        val second: T,
        val result: Result = Result.Equals,
        val firstInclusive: Boolean = true,
        val secondInclusive: Boolean = true
    )

    enum class Result {
        Equals {
            override fun assert(first: MessageFilter, second: MessageFilter) {
                assertFalse(first.isStricterThan(second), "Filters $first and $second should be the same")
                assertFalse(second.isStricterThan(first), "Filters $first and $second should be the same")
                assertEquals(first, second, "Filters $first and $second should be the same")
            }
        },
        Softer {
            override fun assert(first: MessageFilter, second: MessageFilter) {
                assertFalse(first.isStricterThan(second), "Filter $first should be softer than $second")
                assertTrue(second.isStricterThan(first), "Filter $first should be softer than $second")
                assertNotEquals(first, second, "Filter $first should be softer than $second")
            }
        },
        Stricter {
            override fun assert(first: MessageFilter, second: MessageFilter) {
                assertTrue(first.isStricterThan(second), "Filter $first should be stricter than $second")
                assertFalse(second.isStricterThan(first), "Filter $first should be stricter than $second")
                assertNotEquals(first, second, "Filter $first should be stricter than $second")
            }
        };

        abstract fun assert(first: MessageFilter, second: MessageFilter)
    }

    @Test
    fun testFromIdPriority() {
        setOf(
            Data(Snowflake(1UL), Snowflake(2UL), Result.Softer),
            Data(Snowflake(2UL), Snowflake(1UL), Result.Stricter),
            Data(Snowflake(1UL), Snowflake(1UL), Result.Equals),
            Data(Snowflake(1UL), Snowflake(1UL), Result.Softer, secondInclusive = false),
            Data(Snowflake(1UL), Snowflake(1UL), Result.Stricter, firstInclusive = false),
        ).forEach { data ->
            val first = MessageFilter.composite { fromId(data.first, data.firstInclusive) }
            val second = MessageFilter.composite { fromId(data.second, data.secondInclusive) }
            data.result.assert(first, second)
        }
    }

    @Test
    fun testToIdPriority() {
        setOf(
            Data(Snowflake(1UL), Snowflake(2UL), Result.Stricter),
            Data(Snowflake(2UL), Snowflake(1UL), Result.Softer),
            Data(Snowflake(1UL), Snowflake(1UL), Result.Equals),
            Data(Snowflake(1UL), Snowflake(1UL), Result.Softer, secondInclusive = false),
            Data(Snowflake(1UL), Snowflake(1UL), Result.Stricter, firstInclusive = false),
        ).forEach { data ->
            val first = MessageFilter.composite { toId(data.first, data.firstInclusive) }
            val second = MessageFilter.composite { toId(data.second, data.secondInclusive) }
            data.result.assert(first, second)
        }
    }

    @Test
    fun testFromDatePriority() {
        setOf(
            Data(Instant.fromEpochSeconds(1), Instant.fromEpochSeconds(2), Result.Softer),
            Data(Instant.fromEpochSeconds(2), Instant.fromEpochSeconds(1), Result.Stricter),
            Data(Instant.fromEpochSeconds(1), Instant.fromEpochSeconds(1), Result.Equals),
            Data(Instant.fromEpochSeconds(1), Instant.fromEpochSeconds(1), Result.Softer, secondInclusive = false),
            Data(Instant.fromEpochSeconds(1), Instant.fromEpochSeconds(1), Result.Stricter, firstInclusive = false),
        ).forEach { data ->
            val first = MessageFilter.composite { fromDate(data.first, data.firstInclusive) }
            val second = MessageFilter.composite { fromDate(data.second, data.secondInclusive) }
            data.result.assert(first, second)
        }
    }

    @Test
    fun testToDatePriority() {
        setOf(
            Data(Instant.fromEpochSeconds(1), Instant.fromEpochSeconds(2), Result.Stricter),
            Data(Instant.fromEpochSeconds(2), Instant.fromEpochSeconds(1), Result.Softer),
            Data(Instant.fromEpochSeconds(1), Instant.fromEpochSeconds(1), Result.Equals),
            Data(Instant.fromEpochSeconds(1), Instant.fromEpochSeconds(1), Result.Softer, secondInclusive = false),
            Data(Instant.fromEpochSeconds(1), Instant.fromEpochSeconds(1), Result.Stricter, firstInclusive = false),
        ).forEach { data ->
            val first = MessageFilter.composite { toDate(data.first, data.firstInclusive) }
            val second = MessageFilter.composite { toDate(data.second, data.secondInclusive) }
            data.result.assert(first, second)
        }
    }

    @Test
    fun testBuilder() {
        setOf(
            Data(MessageFilter.composite {
                fromId(Snowflake(2UL))
            }, MessageFilter.composite {
                fromId(Snowflake(1UL))
                fromId(Snowflake(2UL))
            }),
            Data(MessageFilter.composite {
                fromId(Snowflake(1UL), inclusive = false)
            }, MessageFilter.composite {
                fromId(Snowflake(1UL))
                fromId(Snowflake(1UL), inclusive = false)
            }),
            Data(MessageFilter.composite {
                toId(Snowflake(1UL))
            }, MessageFilter.composite {
                toId(Snowflake(2UL))
                toId(Snowflake(1UL))
            }),
            Data(MessageFilter.composite {
                toId(Snowflake(1UL), inclusive = false)
            }, MessageFilter.composite {
                toId(Snowflake(1UL))
                toId(Snowflake(1UL), inclusive = false)
            }),
            Data(MessageFilter.composite {
                fromId(Snowflake(1UL))
            }, MessageFilter.composite {
                add(MessageFilter.All)
                fromId(Snowflake(1UL))
            }),
            Data(MessageFilter.All, MessageFilter.composite {
                or {
                    add(MessageFilter.All)
                    fromId(Snowflake(1UL))
                }
            }),
            Data(MessageFilter.None, MessageFilter.composite {
                add(MessageFilter.None)
                fromId(Snowflake(1UL))
            }),
            Data(MessageFilter.composite {
                fromId(Snowflake(1UL))
            }, MessageFilter.composite {
                or {
                    add(MessageFilter.None)
                    fromId(Snowflake(1UL))
                }
            }),
        ).forEach { data ->
            data.result.assert(data.first, data.second)
        }
    }
}
