/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.test

import io.ktor.client.engine.mock.*
import io.ktor.client.request.*
import io.ktor.http.*
import io.ktor.utils.io.*
import io.ktor.utils.io.charsets.*
import io.ktor.utils.io.core.*

fun MockRequestHandleScope.respondJson(
    content: String,
    statusCode: HttpStatusCode = HttpStatusCode.OK,
    headers: Headers = Headers.Empty
): HttpResponseData {
    val combinedHeaders = Headers.build {
        appendAll(headers)
        if (HttpHeaders.ContentType !in this) {
            set(HttpHeaders.ContentType, ContentType.Application.Json.withCharset(Charsets.UTF_8).toString())
        }
    }
    val charset = combinedHeaders[HttpHeaders.ContentType]?.let { ContentType.parse(it).charset() } ?: Charsets.UTF_8
    val bytes = content.toByteArray(charset)

    return respond(ByteReadChannel(bytes), statusCode, combinedHeaders)
}


fun MockRequestHandleScope.respondNoContent(headers: Headers = Headers.Empty) =
    respond(ByteReadChannel.Empty, HttpStatusCode.NoContent, headers)

