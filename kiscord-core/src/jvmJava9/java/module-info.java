/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

module kiscord.core {
    requires kotlin.stdlib;
    requires kotlin.stdlib.jdk8;
    requires io.ktor.client.content.negotiation;
    requires io.ktor.client.core;
    requires io.ktor.serialization.kotlinx.json;
    requires io.ktor.serialization.kotlinx;
    requires io.ktor.serialization;
    requires kotlinx.datetime;
    requires kotlinx.serialization.core;
    requires kotlinx.serialization.json;

    exports kiscord;
    exports kiscord.api;
    exports kiscord.builder;
    exports kiscord.ktor;
    exports kiscord.serialization;
}
