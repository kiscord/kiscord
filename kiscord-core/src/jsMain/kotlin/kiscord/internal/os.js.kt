@file:JsModule("os")

package kiscord.internal

@JsName("platform")
internal external fun nodePlatform(): String
