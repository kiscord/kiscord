/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.interactions

import kiscord.*
import kiscord.api.*
import org.kodein.log.*

@KiscordUnstableAPI
public sealed class ApplicationCommandInitializer(
    public val commands: Set<CommandHandle>,
    public val mode: CommandRegistrar.CommandRegistrationMode
) {
    protected abstract suspend fun getCommands(clientId: Snowflake, client: Kiscord): List<ApplicationCommand>
    protected abstract suspend fun createCommand(
        clientId: Snowflake,
        client: Kiscord,
        command: CommandApi.ApplicationCommandInfo
    ): ApplicationCommand

    protected abstract suspend fun editCommand(
        clientId: Snowflake,
        client: Kiscord,
        commandId: Snowflake,
        command: CommandApi.ApplicationCommandInfo
    ): ApplicationCommand

    protected abstract suspend fun deleteCommand(
        clientId: Snowflake,
        client: Kiscord,
        commandId: Snowflake
    ): Boolean

    protected abstract val loggerMeta: Map<String, Any>

    public suspend fun initialize(
        clientId: Snowflake,
        client: Kiscord
    ) {
        val logger = client.loggerFactory.newLogger("kiscord.interactions", "ApplicationCommandInitializer")

        val actualCommands = getCommands(clientId, client).toMutableList()
        if (mode == CommandRegistrar.CommandRegistrationMode.None) {
            logger.log(level = Logger.Level.DEBUG, meta = loggerMeta) {
                "Due to registration mode, command initialization skipped"
            }
            return
        }
        val toCreate = mutableListOf<CommandApi.ApplicationCommandInfo>()
        val toEdit = mutableMapOf<ApplicationCommand, CommandApi.ApplicationCommandInfo>()
        commands.forEach { command ->
            val actualCommand = actualCommands.find { it.name == command.applicationCommandInfo.name }
            if (actualCommand == null) {
                toCreate += command.applicationCommandInfo
                return@forEach
            } else if (!command.applicationCommandInfo.isSameAs(actualCommand)) {
                toEdit[actualCommand] = command.applicationCommandInfo
            }
            actualCommands -= actualCommand
        }
        toCreate.forEach { command ->
            logger.log(level = Logger.Level.INFO, meta = loggerMeta) {
                "Creating new command /${command.name}"
            }
            val result = createCommand(clientId, client, command)
            logger.log(level = Logger.Level.INFO, meta = loggerMeta) {
                "Command /${command.name} created with id ${result.id}"
            }
        }
        toEdit.forEach { (actualCommand, command) ->
            logger.log(level = Logger.Level.INFO, meta = loggerMeta) {
                "Editing existing command with id ${actualCommand.id} /${command.name}"
            }
            val result = editCommand(clientId, client, actualCommand.id, command)
            if (command.isSameAs(result)) {
                logger.log(level = Logger.Level.INFO, meta = loggerMeta) {
                    "Editing existing command with id ${actualCommand.id} /${command.name} successful"
                }
            } else {
                logger.log(level = Logger.Level.WARNING, meta = loggerMeta) {
                    "Editing existing command with id ${actualCommand.id} /${command.name} returns another command, WAT?\nExpected $command\n... but got $result"
                }
            }
        }
        if (mode == CommandRegistrar.CommandRegistrationMode.Permissive) return
        // Any leftover commands don't match any of the expected commands, so delete it
        actualCommands.forEach { command ->
            logger.log(level = Logger.Level.INFO, meta = loggerMeta) {
                "Deleting existing command with id ${command.id} /${command.name}"
            }
            val result = deleteCommand(clientId, client, command.id)
            if (result) {
                logger.log(level = Logger.Level.INFO, meta = loggerMeta) {
                    "Existing command with id ${command.id} /${command.name} successfully deleted"
                }
            } else {
                logger.log(level = Logger.Level.WARNING, meta = loggerMeta) {
                    "Deleting existing command with id ${command.id} /${command.name} failed"
                }
            }
        }
    }

    private fun CommandApi.ApplicationCommandInfo.isSameAs(command: ApplicationCommand): Boolean {
        return name == command.name
            && nameLocalizations.orNull == command.nameLocalizations.orNull
            && description == command.description
            && descriptionLocalizations.orNull == command.descriptionLocalizations.orNull
            && options == command.options
            && dmPermission.orNull == command.dmPermission
            && defaultMemberPermissions.orNull == command.defaultMemberPermissions
            && type == command.type
    }

    public class Global(
        commands: Set<CommandHandle>,
        mode: CommandRegistrar.CommandRegistrationMode,
    ) : ApplicationCommandInitializer(commands, mode) {
        override val loggerMeta: Map<String, Any> = mapOf("scope" to "Global")

        override fun toString(): String {
            return "ApplicationCommandInitializer.Global(mode=$mode, commands=$commands)"
        }

        override suspend fun getCommands(clientId: Snowflake, client: Kiscord): List<ApplicationCommand> {
            return client.command.getGlobalApplicationCommands(clientId, withLocalizations = true)
        }

        override suspend fun createCommand(
            clientId: Snowflake,
            client: Kiscord,
            command: CommandApi.ApplicationCommandInfo
        ): ApplicationCommand {
            return client.command.createGlobalApplicationCommand(clientId, command)
        }

        override suspend fun editCommand(
            clientId: Snowflake,
            client: Kiscord,
            commandId: Snowflake,
            command: CommandApi.ApplicationCommandInfo
        ): ApplicationCommand {
            return client.command.editGlobalApplicationCommand(clientId, commandId, command)
        }

        override suspend fun deleteCommand(
            clientId: Snowflake,
            client: Kiscord,
            commandId: Snowflake
        ): Boolean {
            return client.command.deleteGlobalApplicationCommand(clientId, commandId)
        }
    }

    public class Guild(
        commands: Set<CommandHandle>,
        mode: CommandRegistrar.CommandRegistrationMode,
        public val guildId: Snowflake
    ) : ApplicationCommandInitializer(commands, mode) {
        override val loggerMeta: Map<String, Any> = mapOf("scope" to "Guild #$guildId")

        override fun toString(): String {
            return "ApplicationCommandInitializer.Guild(guildId=$guildId, mode=$mode, commands=$commands)"
        }

        override suspend fun getCommands(clientId: Snowflake, client: Kiscord): List<ApplicationCommand> {
            return client.command.getGuildApplicationCommands(clientId, guildId, withLocalizations = true)
        }

        override suspend fun createCommand(
            clientId: Snowflake,
            client: Kiscord,
            command: CommandApi.ApplicationCommandInfo
        ): ApplicationCommand {
            return client.command.createGuildApplicationCommand(clientId, guildId, command)
        }

        override suspend fun editCommand(
            clientId: Snowflake,
            client: Kiscord,
            commandId: Snowflake,
            command: CommandApi.ApplicationCommandInfo
        ): ApplicationCommand {
            return client.command.editGuildApplicationCommand(clientId, guildId, commandId, command)
        }

        override suspend fun deleteCommand(
            clientId: Snowflake,
            client: Kiscord,
            commandId: Snowflake
        ): Boolean {
            return client.command.deleteGuildApplicationCommand(clientId, guildId, commandId)
        }
    }
}
