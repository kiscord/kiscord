/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.interactions

import kiscord.api.*
import kiscord.builder.*

@BuilderDsl
public interface ComponentHandle<PredicateData : Any> {
    @KiscordUnstableAPI
    public val predicate: InteractionPredicate<PredicateData, Interaction.Data.Component>

    @KiscordUnstableAPI
    public val handler: suspend InteractionCall<Interaction.Data.Component>.(PredicateData) -> Unit

    @BuilderDsl
    public infix fun handle(block: suspend InteractionCall<Interaction.Data.Component>.(PredicateData) -> Unit): ComponentHandle<PredicateData>
}
