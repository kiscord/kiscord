/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.interactions

import kiscord.api.*

public sealed interface InteractionPredicate<out PredicateData : Any, in Data : Interaction.Data> {
    public fun isMatching(interaction: Interaction, data: Data): PredicateData?

    public class ExactId<in Data : Interaction.Data>(
        public val customId: String
    ) : InteractionPredicate<String, Data> {
        override fun isMatching(interaction: Interaction, data: Data): String? {
            return if (data.customId == customId) customId else null
        }
    }

    public class RegexId<in Data : Interaction.Data>(
        public val regexId: Regex
    ) : InteractionPredicate<MatchResult, Data> {
        override fun isMatching(interaction: Interaction, data: Data): MatchResult? {
            return data.customId?.let { regexId.matchEntire(it) }
        }
    }

    public class Button<out PredicateData : kotlin.Any>(private val another: InteractionPredicate<PredicateData, Interaction.Data.Component>) :
        InteractionPredicate<PredicateData, Interaction.Data.Component> {
        override fun isMatching(interaction: Interaction, data: Interaction.Data.Component): PredicateData? {
            if (data.componentType != Component.Type.Button) return null
            return another.isMatching(interaction, data)
        }
    }

    public class SelectMenu<out PredicateData : kotlin.Any>(private val another: InteractionPredicate<PredicateData, Interaction.Data.Component>) :
        InteractionPredicate<PredicateData, Interaction.Data.Component> {
        override fun isMatching(interaction: Interaction, data: Interaction.Data.Component): PredicateData? {
            if (data.componentType != Component.Type.SelectMenu) return null
            return another.isMatching(interaction, data)
        }
    }

    public object Any : InteractionPredicate<Unit, Interaction.Data> {
        override fun isMatching(interaction: Interaction, data: Interaction.Data): Unit = Unit
    }
}

private val Interaction.Data.customId: String?
    get() = when (this) {
        is Interaction.Data.Component -> customId
        is Interaction.Data.ModalSubmit -> customId
        else -> null
    }
