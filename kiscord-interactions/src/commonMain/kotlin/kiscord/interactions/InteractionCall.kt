/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.interactions

import kiscord.*
import kiscord.api.*
import kiscord.builder.*
import kotlinx.coroutines.*
import kotlin.coroutines.*

public interface InteractionCall<out Data : Interaction.Data> : KiscordCall {
    public val interaction: Interaction
    public val data: Data
    public val result: Deferred<InteractionResult>

    public suspend fun respondWith(result: InteractionResult)

    @OptIn(ExperimentalStdlibApi::class)
    public companion object Key :
        AbstractCoroutineContextKey<KiscordCall, InteractionCall<*>>(KiscordCall, { it as? InteractionCall<*> })
}

public suspend fun InteractionCall<*>.respondWith(result: Interaction.Response): Unit =
    respondWith(InteractionResult(result))

@BuilderDsl
public suspend fun InteractionCall<*>.message(
    type: Interaction.Response.Type? = null,
    block: suspend Interaction.Response.Data.Message.Builder.() -> Unit
) {
    respondWith(Interaction.Response {
        type(
            when {
                type != null -> type
                this@message.data is Interaction.Data.Component -> Interaction.Response.Type.UpdateMessage
                else -> Interaction.Response.Type.ChannelMessageWithSource
            }
        )
        data = Interaction.Response.Data.Message {
            block()
        }
    })
}

@BuilderDsl
public suspend fun InteractionCall<*>.deferredMessage(
    type: Interaction.Response.Type? = null,
    block: suspend EditMessage.Builder.() -> Unit
) {
    respondWith(InteractionResult(
        Interaction.Response(
            type = when {
                type != null -> type
                data is Interaction.Data.Component -> Interaction.Response.Type.DeferredUpdateMessage
                else -> Interaction.Response.Type.DeferredChannelMessageWithSource
            }
        )
    ) {
        val builder = EditMessage.builder()
        builder.block()
        client.command.editOriginalInteractionResponse(
            interaction.applicationId, interaction.token, builder.build()
        )
    })
}

@BuilderDsl
public suspend fun InteractionCall<*>.modal(block: suspend Interaction.Response.Data.Modal.Builder.() -> Unit) {
    respondWith(Interaction.Response {
        type(Interaction.Response.Type.Modal)
        data(Interaction.Response.Data.Modal {
            block()
        })
    })
}

@BuilderDsl
public suspend fun InteractionCall<*>.autocomplete(block: suspend Interaction.Response.Data.Autocomplete.Builder.() -> Unit) {
    respondWith(Interaction.Response {
        type(Interaction.Response.Type.ApplicationCommandAutocompleteResult)
        data(Interaction.Response.Data.Autocomplete {
            block()
        })
    })
}
