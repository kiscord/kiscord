/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.interactions

import kiscord.api.*
import kiscord.builder.*

@BuilderDsl
public interface ComponentRegistrar {
    @BuilderDsl
    public fun <PredicateData : Any> component(predicate: InteractionPredicate<PredicateData, Interaction.Data.Component>): ComponentHandle<PredicateData>
}

@BuilderDsl
public fun ComponentRegistrar.component(): ComponentHandle<Unit> {
    return component(InteractionPredicate.Any)
}

@BuilderDsl
public fun ComponentRegistrar.component(customId: String): ComponentHandle<String> {
    return component(InteractionPredicate.ExactId(customId))
}

@BuilderDsl
public fun ComponentRegistrar.component(regexId: Regex): ComponentHandle<MatchResult> {
    return component(InteractionPredicate.RegexId(regexId))
}


@BuilderDsl
public fun <PredicateData : Any> ComponentRegistrar.button(predicate: InteractionPredicate<PredicateData, Interaction.Data.Component>): ComponentHandle<PredicateData> {
    return component(InteractionPredicate.Button(predicate))
}

@BuilderDsl
public fun ComponentRegistrar.button(): ComponentHandle<Unit> {
    return button(InteractionPredicate.Any)
}

@BuilderDsl
public fun ComponentRegistrar.button(customId: String): ComponentHandle<String> {
    return button(InteractionPredicate.ExactId(customId))
}

@BuilderDsl
public fun ComponentRegistrar.button(regexId: Regex): ComponentHandle<MatchResult> {
    return button(InteractionPredicate.RegexId(regexId))
}

@BuilderDsl
public fun <PredicateData : Any> ComponentRegistrar.selectMenu(predicate: InteractionPredicate<PredicateData, Interaction.Data.Component>): ComponentHandle<PredicateData> {
    return component(InteractionPredicate.SelectMenu(predicate))
}

@BuilderDsl
public fun ComponentRegistrar.selectMenu(): ComponentHandle<Unit> {
    return selectMenu(InteractionPredicate.Any)
}

@BuilderDsl
public fun ComponentRegistrar.selectMenu(customId: String): ComponentHandle<String> {
    return selectMenu(InteractionPredicate.ExactId(customId))
}

@BuilderDsl
public fun ComponentRegistrar.selectMenu(regexId: Regex): ComponentHandle<MatchResult> {
    return selectMenu(InteractionPredicate.RegexId(regexId))
}
