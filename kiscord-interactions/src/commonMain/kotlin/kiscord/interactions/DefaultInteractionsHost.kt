/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.interactions

import kiscord.*
import kiscord.api.*
import kiscord.builder.*
import kotlinx.coroutines.*
import org.kodein.log.*

@KiscordUnstableAPI
public class DefaultInteractionsHost private constructor(
    private val logger: Logger,
    builder: Builder,
) : InteractionsHost {
    override val pipeline: GatewayInteractionsPipeline = GatewayInteractionsPipeline()
    override val commandInitializers: List<ApplicationCommandInitializer>

    init {
        commandInitializers = buildList {
            val globalRegistrar = builder.globalCommands
            add(ApplicationCommandInitializer.Global(globalRegistrar.commands.values.toSet(), globalRegistrar.mode))

            builder.guildCommands.mapTo(this) { (guildId, registrar) ->
                ApplicationCommandInitializer.Guild(registrar.commands.values.toSet(), registrar.mode, guildId)
            }
        }

        commandInitializers.forEach { initializer ->
            val commands = initializer.commands
            when (initializer) {
                is ApplicationCommandInitializer.Global -> {
                    pipeline.intercept(GatewayInteractionsPipeline.GlobalCommands) { interaction ->
                        if (interaction !is Interaction) return@intercept
                        if (interaction.type != Interaction.Type.ApplicationCommand
                            && interaction.type != Interaction.Type.ApplicationCommandAutocomplete
                        ) return@intercept
                        val data = interaction.data
                        if (data !is Interaction.Data.Command) return@intercept
                        val command = commands.find { it.applicationCommandInfo.name == data.name } ?: return@intercept
                        val context = PureInteractionCall(context, data)
                        val result = context.handleCommand(command, interaction)
                        if (result != null) {
                            proceedWith(result)
                            return@intercept
                        }
                    }
                }
                is ApplicationCommandInitializer.Guild -> {
                    val guildId = initializer.guildId
                    pipeline.intercept(GatewayInteractionsPipeline.GuildCommands) { interaction ->
                        if (interaction !is Interaction) return@intercept
                        if (interaction.type != Interaction.Type.ApplicationCommand
                            && interaction.type != Interaction.Type.ApplicationCommandAutocomplete
                        ) return@intercept
                        val data = interaction.data
                        if (data !is Interaction.Data.Command) return@intercept
                        if (interaction.guildId != guildId) return@intercept
                        val command = commands.find { it.applicationCommandInfo.name == data.name } ?: return@intercept
                        val context = PureInteractionCall(context, data)
                        val result = context.handleCommand(command, interaction)
                        if (result != null) {
                            proceedWith(result)
                            return@intercept
                        }
                        logger.warning { "Unhandled interaction $interaction" }
                    }
                }
            }
        }

        val components = builder.components.toList()
        pipeline.intercept(GatewayInteractionsPipeline.Components) { interaction ->
            if (interaction !is Interaction) return@intercept
            if (interaction.type != Interaction.Type.MessageComponent) return@intercept
            val data = interaction.data
            if (data !is Interaction.Data.Component) return@intercept
            val context = PureInteractionCall(context, data)
            components.forEach { handle ->
                val interactionResult = context.handleComponent(handle, interaction)
                if (interactionResult != null) {
                    proceedWith(interactionResult)
                    return@intercept
                }
            }
            logger.warning { "Unhandled interaction $interaction" }
        }

        val modals = builder.modals.toList()
        pipeline.intercept(GatewayInteractionsPipeline.Components) { interaction ->
            if (interaction !is Interaction) return@intercept
            if (interaction.type != Interaction.Type.ModalSubmit) return@intercept
            val data = interaction.data
            if (data !is Interaction.Data.ModalSubmit) return@intercept
            val context = PureInteractionCall(context, data)
            modals.forEach { handle ->
                val interactionResult = context.handleModal(handle, interaction)
                if (interactionResult != null) {
                    proceedWith(interactionResult)
                    return@intercept
                }
            }
            logger.warning { "Unhandled interaction $interaction" }
        }
    }

    public class DefaultCommandRegistrar(
        internal val commands: MutableMap<CommandApi.ApplicationCommandInfo, DefaultCommandHandle> = mutableMapOf()
    ) : CommandRegistrar, MutableCollection<CommandApi.ApplicationCommandInfo> by commands.keys {
        override var mode: CommandRegistrar.CommandRegistrationMode = CommandRegistrar.CommandRegistrationMode.Strict

        override fun clear() {
            commands.clear()
        }

        override fun command(info: CommandApi.ApplicationCommandInfo): CommandHandle {
            return commands.getOrPut(info) { DefaultCommandHandle(info) }
        }
    }

    @KiscordUnstableAPI
    public class Builder private constructor(
        private val logger: Logger,
        internal val globalCommands: DefaultCommandRegistrar
    ) : InteractionsHost.Builder, CommandRegistrar by globalCommands,
        BuilderFor<DefaultInteractionsHost> {
        public constructor(logger: Logger) : this(logger, DefaultCommandRegistrar())

        internal val guildCommands = mutableMapOf<Snowflake, DefaultCommandRegistrar>()
        internal val components = mutableListOf<ComponentHandle<*>>()
        internal val modals = mutableListOf<ModalHandle<*>>()

        override fun guild(guildId: Snowflake): CommandRegistrar {
            return guildCommands.getOrPut(guildId) { DefaultCommandRegistrar() }
        }

        override fun <PredicateData : Any> component(predicate: InteractionPredicate<PredicateData, Interaction.Data.Component>): ComponentHandle<PredicateData> {
            return DefaultComponentHandle(predicate).also { components += it }
        }

        override fun <PredicateData : Any> modal(predicate: InteractionPredicate<PredicateData, Interaction.Data.ModalSubmit>): ModalHandle<PredicateData> {
            return DefaultModalHandle(predicate).also { modals += it }
        }

        override fun build(): DefaultInteractionsHost = DefaultInteractionsHost(
            logger = logger,
            builder = this,
        )
    }

    @KiscordUnstableAPI
    public data class DefaultCommandHandle(
        override val applicationCommandInfo: CommandApi.ApplicationCommandInfo
    ) : CommandHandle {
        @KiscordUnstableAPI
        override var handler: suspend InteractionCall<Interaction.Data.Command>.() -> Unit =
            EMPTY_HANDLER
            private set

        override fun handle(block: suspend InteractionCall<Interaction.Data.Command>.() -> Unit): DefaultCommandHandle {
            handler = block
            return this
        }

        @KiscordUnstableAPI
        override var autocompleteHandler: suspend InteractionCall<Interaction.Data.Command>.() -> Unit =
            EMPTY_HANDLER
            private set

        override fun autocompleteHandle(block: suspend InteractionCall<Interaction.Data.Command>.() -> Unit): CommandHandle {
            autocompleteHandler = block
            return this
        }

        private companion object {
            val EMPTY_HANDLER: suspend InteractionCall<Interaction.Data.Command>.() -> Unit =
                { throw IllegalArgumentException("Unhandled command: $data") }
        }
    }

    public data class DefaultComponentHandle<PredicateData : Any>(
        override val predicate: InteractionPredicate<PredicateData, Interaction.Data.Component>,
    ) : ComponentHandle<PredicateData> {
        @KiscordUnstableAPI
        override var handler: suspend InteractionCall<Interaction.Data.Component>.(PredicateData) -> Unit =
            EMPTY_HANDLER
            private set

        override fun handle(block: suspend InteractionCall<Interaction.Data.Component>.(PredicateData) -> Unit): DefaultComponentHandle<PredicateData> {
            handler = block
            return this
        }

        private companion object {
            val EMPTY_HANDLER: suspend InteractionCall<Interaction.Data.Component>.(Any) -> Unit =
                { throw IllegalArgumentException("Unhandled component: $data") }
        }
    }

    public data class DefaultModalHandle<PredicateData : Any>(
        override val predicate: InteractionPredicate<PredicateData, Interaction.Data.ModalSubmit>,
    ) : ModalHandle<PredicateData> {
        @KiscordUnstableAPI
        override var handler: suspend InteractionCall<Interaction.Data.ModalSubmit>.(PredicateData) -> Unit =
            EMPTY_HANDLER
            private set

        override fun handle(block: suspend InteractionCall<Interaction.Data.ModalSubmit>.(PredicateData) -> Unit): DefaultModalHandle<PredicateData> {
            handler = block
            return this
        }

        private companion object {
            val EMPTY_HANDLER: suspend InteractionCall<Interaction.Data.ModalSubmit>.(Any) -> Unit =
                { throw IllegalArgumentException("Unhandled modal: $data") }
        }
    }
}

private class PureInteractionCall<out Data : Interaction.Data>(
    val call: InteractionCall<*>,
    override val data: Data,
) : InteractionCall<Data> {
    override val client: Kiscord get() = call.client
    override val attributes: TypedAttributes get() = call.attributes
    override val interaction: Interaction get() = call.interaction
    override val result: Deferred<InteractionResult> get() = call.result
    override suspend fun respondWith(result: InteractionResult) = call.respondWith(result)
}

@OptIn(ExperimentalCoroutinesApi::class)
private suspend fun InteractionCall<Interaction.Data.Command>.handleCommand(
    handle: CommandHandle, interaction: Interaction
): InteractionResult? {
    when (val type = interaction.type) {
        Interaction.Type.ApplicationCommand -> handle.handler(this)
        Interaction.Type.ApplicationCommandAutocomplete -> handle.autocompleteHandler(this)
        else -> throw IllegalArgumentException("Invalid interaction type: $type")
    }
    if (!result.isCompleted) {
        return null
    }
    return result.getCompleted()
}

@OptIn(ExperimentalCoroutinesApi::class)
private suspend fun <PredicateData : Any> InteractionCall<Interaction.Data.Component>.handleComponent(
    handle: ComponentHandle<PredicateData>, interaction: Interaction
): InteractionResult? {
    val predicateData = handle.predicate.isMatching(interaction, data) ?: return null
    handle.handler(this, predicateData)
    if (!result.isCompleted) {
        return null
    }
    return result.getCompleted()
}

@OptIn(ExperimentalCoroutinesApi::class)
private suspend fun <PredicateData : Any> InteractionCall<Interaction.Data.ModalSubmit>.handleModal(
    handle: ModalHandle<PredicateData>, interaction: Interaction
): InteractionResult? {
    val predicateData = handle.predicate.isMatching(interaction, data) ?: return null
    handle.handler(this, predicateData)
    if (!result.isCompleted) {
        error("interaction not handled: $interaction")
    }
    return result.getCompleted()
}
