/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.interactions

import kiscord.api.*
import kiscord.builder.*

@BuilderDsl
public interface CommandRegistrar : MutableCollection<CommandApi.ApplicationCommandInfo> {
    @BuilderDsl
    public var mode: CommandRegistrationMode

    @BuilderDsl
    public fun command(info: CommandApi.ApplicationCommandInfo): CommandHandle

    /**
     * Command registration mode, which describes how to create / update / delete commands in case of conflicts
     */
    public enum class CommandRegistrationMode {
        /**
         * Remove any unregistered commands from the Discord API
         */
        Strict,

        /**
         * Allows to any unregistered command exists in the API
         */
        Permissive,

        /**
         * Don't make any changes in the API, even if command doesn't exist
         */
        None,
    }
}

@BuilderDsl
public fun CommandRegistrar.mode(mode: CommandRegistrar.CommandRegistrationMode) {
    this.mode = mode
}

@BuilderDsl
public inline fun CommandRegistrar.command(
    block: CommandApi.ApplicationCommandInfo.Builder.() -> Unit
): CommandHandle = command(CommandApi.ApplicationCommandInfo(block))
