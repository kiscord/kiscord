/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.interactions.server.security

import java.util.*

internal actual inline val defaultFactory: SignatureVerifierFactory get() = JvmSignatureVerifierFactoryLoader

private object JvmSignatureVerifierFactoryLoader : SignatureVerifierFactory {
    private val serviceLoader: ServiceLoader<SignatureVerifierFactory> by lazy {
        ServiceLoader.load(SignatureVerifierFactory::class.java)
    }

    override val isSupported: Boolean get() = serviceLoader.any { it.isSupported }

    override fun createVerifier(verifyKey: String): SignatureVerifier {
        val factory = serviceLoader.sortedByDescending {
            it.priority
        }.firstOrNull { factory ->
            factory.isSupported
        } ?: throw NotImplementedError("No applicable signature verifiers available in the classpath")
        return factory.createVerifier(verifyKey)
    }
}
