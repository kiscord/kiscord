/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.interactions.server.security

import kiscord.interactions.server.*
import java.math.*
import java.security.*
import java.security.spec.*
import kotlin.experimental.*

public class JdkSignatureVerifierFactory : SignatureVerifierFactory {
    override val isSupported: Boolean by lazy {
        try {
            Signature.getInstance(ALGORITHM)
            KeyFactory.getInstance(ALGORITHM)
            true
        } catch (e: NoSuchAlgorithmException) {
            false
        }
    }

    override val priority: Int get() = 100

    override fun createVerifier(verifyKey: String): SignatureVerifier {
        check(isSupported) { "JdkSignatureVerifierFactory supported only since Java 15" }
        return JdkSignatureVerifier(parsePublicKey(verifyKey))
    }

    private class JdkSignatureVerifier(verifyKey: PublicKey) : SignatureVerifier {
        private val localSignature = ThreadLocal.withInitial {
            Signature.getInstance(ALGORITHM).apply {
                initVerify(verifyKey)
            }
        }

        override fun isSignatureValid(timestamp: ByteArray, body: ByteArray, signature: ByteArray): Boolean {
            return localSignature.get().run {
                update(timestamp)
                update(body)
                verify(signature)
            }
        }
    }

    internal companion object {
        const val ALGORITHM = "Ed25519"
    }
}


@Suppress("Since15")
private fun parsePublicKey(publicKey: String): PublicKey {
    val pk = publicKey.decodeHex()
    val xIsOdd = pk.last() < 0
    pk[pk.lastIndex] = pk[pk.lastIndex] and 0x7F
    pk.reverse()
    return KeyFactory.getInstance(JdkSignatureVerifierFactory.ALGORITHM)
        .generatePublic(EdECPublicKeySpec(NamedParameterSpec.ED25519, EdECPoint(xIsOdd, BigInteger(1, pk))))
}
