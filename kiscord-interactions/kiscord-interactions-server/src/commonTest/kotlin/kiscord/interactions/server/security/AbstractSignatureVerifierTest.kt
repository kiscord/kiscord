/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.interactions.server.security

import kiscord.interactions.server.*
import kotlin.test.*

// test vectors taken from RFC8032 (https://datatracker.ietf.org/doc/html/rfc8032#section-7.1)
abstract class AbstractSignatureVerifierTest {
    abstract val signatureVerifierFactory: SignatureVerifierFactory
    private fun test(publicKey: String, signature: String, body: ByteArray): Boolean {
        val verifier = signatureVerifierFactory.createVerifier(publicKey)
        return verifier.isSignatureValid(ByteArray(0), body, signature.decodeHex())
    }

    @Test
    fun testVector1() {
        assertTrue(
            test(
                "d75a980182b10ab7d54bfed3c964073a0ee172f3daa62325af021a68f707511a",
                "e5564300c360ac729086e2cc806e828a84877f1eb8e5d974d873e065224901555fb8821590a33bacc61e39701cf9b46bd25bf5f0595bbe24655141438e7a100b",
                ByteArray(0)
            )
        )
    }
    @Test
    fun testVector2() {
        assertTrue(
            test(
                "3d4017c3e843895a92b70aa74d1b7ebc9c982ccf2ec4968cc0cd55f12af4660c",
                "92a009a9f0d4cab8720e820b5f642540a2b27b5416503f8fb3762223ebdb69da085ac1e43e15996e458f3613d0f11d8c387b2eaeb4302aeeb00d291612bb0c00",
                byteArrayOf(0x72)
            )
        )
    }
    @Test
    fun testVector3() {
        assertTrue(
            test(
                "fc51cd8e6218a1a38da47ed00230f0580816ed13ba3303ac5deb911548908025",
                "6291d657deec24024827e69c3abe01a30ce548a284743a445e3680d7db5ac3ac18ff9b538d16f290ae67f760984dc6594a7c15e9716ed28dc027beceea1ec40a",
                byteArrayOf(0xafu.toByte(), 0x82u.toByte())
            )
        )
    }
}
