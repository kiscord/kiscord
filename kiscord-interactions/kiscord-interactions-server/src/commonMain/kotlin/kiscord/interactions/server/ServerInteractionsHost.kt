/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.interactions.server

import kiscord.*
import kiscord.api.*
import kiscord.builder.*
import kiscord.interactions.*
import kiscord.interactions.server.security.*
import org.kodein.log.*

public interface ServerInteractionsHost : InteractionsHost {
    public val loggerFactory: LoggerFactory
    public val clientId: Snowflake
    public val clientSecret: String
    public val clientConfiguration: ConfigurationChain<Kiscord.Builder>
    public val signatureVerifierFactory: SignatureVerifierFactory

    public interface Builder : InteractionsHost.Builder {
        public val loggerFactory: LoggerFactory
        public var clientId: Snowflake
        public var clientSecret: String
        public var signatureVerifierFactory: SignatureVerifierFactory

        public fun clientConfiguration(block: Kiscord.Builder.() -> Unit)

        public fun build(): ServerInteractionsHost
    }

    public companion object {
        public fun builder(loggerFactory: LoggerFactory): Builder =
            DefaultServerInteractionsHost.Builder(
                loggerFactory = loggerFactory,
                builder = DefaultInteractionsHost.Builder(
                    logger = loggerFactory.newLogger("kiscord.interactions.server", "ServerInteractionsHost")
                )
            )
    }
}
