/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.interactions.server

import io.ktor.http.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.utils.io.*
import io.ktor.utils.io.charsets.*
import io.ktor.utils.io.core.*
import kiscord.*
import kiscord.api.*
import kiscord.builder.*
import kiscord.interactions.*
import kiscord.serialization.*
import kotlinx.coroutines.*

@KtorDsl
public fun Route.discordInteractionsWebhook(client: Kiscord, host: ServerInteractionsHost) {
    val application = runBlocking {
        when (val tokenType = client.token.type) {
            Token.Type.ClientCredentials, Token.Type.Bearer -> client.oAuth2.getCurrentAuthorizationInformation().application
            Token.Type.Bot -> client.oAuth2.getCurrentBotApplicationInformation()
            else -> throw IllegalArgumentException("Token type $tokenType not supported")
        }
    }

    val verifyKey = application.verifyKey
        ?: throw IllegalStateException("Unable to extract public key from current authorization")

    val signatureVerifier = host.signatureVerifierFactory.createVerifier(verifyKey)

    client.launch {
        host.commandInitializers.forEach { initializer ->
            initializer.initialize(application.id, client)
        }
    }

    val pipeline = host.pipeline

    post {
        val headers = call.request.headers
        val signature = headers["X-Signature-Ed25519"]?.decodeHex()
            ?: return@post call.respond(HttpStatusCode.Unauthorized)
        val timestamp = headers["X-Signature-Timestamp"]?.toByteArray(Charsets.ISO_8859_1)
            ?: return@post call.respond(HttpStatusCode.Unauthorized)
        val rawBody = call.receive<ByteArray>()
        if (!signatureVerifier.isSignatureValid(timestamp, rawBody, signature)) {
            return@post call.respond(HttpStatusCode.Unauthorized)
        }


        val rawBodyText = Charsets.UTF_8.newDecoder().decode(ByteReadPacket(rawBody))

        val interaction = DiscordSerialFormat.Json.decodeFromString(Interaction.serializer(), rawBodyText)

        if (interaction.type == Interaction.Type.Ping) {
            return@post call.respondText(ContentType.Application.Json) {
                DiscordSerialFormat.Json.encodeToString(
                    Interaction.Response.serializer(),
                    Interaction.Response(Interaction.Response.Type.Pong)
                )
            }
        }

        val interactionCall = KtorServerInteractionCall(client, interaction, coroutineContext.job)

        val result = withContext(interactionCall) {
            pipeline.execute(interactionCall, interaction)
        }

        when (result) {
            is InteractionResult -> {
                call.respondText(ContentType.Application.Json) {
                    DiscordSerialFormat.Json.encodeToString(Interaction.Response.serializer(), result.response)
                }

                result.deferred?.let { deferred ->
                    launch { deferred(KtorServerKiscordCall(client)) }
                }
            }

            is Interaction.Response -> {
                call.respondText(ContentType.Application.Json) {
                    DiscordSerialFormat.Json.encodeToString(Interaction.Response.serializer(), result)
                }
            }

            else -> {
                call.respond(HttpStatusCode.InternalServerError)
            }
        }
    }
}

@KtorDsl
public inline fun Route.discordInteractionsWebhook(
    client: Kiscord,
    block: ServerInteractionsHost.Builder.() -> Unit
): Unit = discordInteractionsWebhook(
    client, ServerInteractionsHost.builder(loggerFactory = Kiscord.DefaultLoggerFactory).apply(block).build()
)

@KtorDsl
public fun Route.discordInteractionsWebhook(
    host: ServerInteractionsHost
): Kiscord {
    val clientId = host.clientId
    val clientSecret = host.clientSecret
    val client = Kiscord {
        token(Token.clientCredentials(clientId, clientSecret, "applications.commands.update"))
        loggerFactory = host.loggerFactory
        host.clientConfiguration(this)
    }

    discordInteractionsWebhook(client, host)

    return client
}

@KtorDsl
public inline fun Route.discordInteractionsWebhook(
    block: ServerInteractionsHost.Builder.() -> Unit
): Kiscord = discordInteractionsWebhook(
    ServerInteractionsHost.builder(loggerFactory = Kiscord.DefaultLoggerFactory).apply(block).build()
)

@KtorDsl
public fun Route.discordInteractionsWebhook(
    path: String,
    host: ServerInteractionsHost
): Kiscord = createRouteFromPath(path).discordInteractionsWebhook(host)

@KtorDsl
public inline fun Route.discordInteractionsWebhook(
    path: String,
    block: ServerInteractionsHost.Builder.() -> Unit
): Kiscord = createRouteFromPath(path).discordInteractionsWebhook(block)

private class KtorServerInteractionCall(
    override val client: Kiscord,
    override val interaction: Interaction,
    job: Job? = null,
) : InteractionCall<Interaction.Data> {
    override val data: Interaction.Data
        get() = interaction.data ?: throw IllegalStateException("Interaction doesn't have a data!")
    override val attributes: TypedAttributes by lazy { TypedAttributes() }
    private val _result = CompletableDeferred<InteractionResult>(job)
    override val result: Deferred<InteractionResult> get() = _result
    override suspend fun respondWith(result: InteractionResult) {
        _result.complete(result)
    }
}

private class KtorServerKiscordCall(
    override val client: Kiscord
) : KiscordCall {
    override val attributes: TypedAttributes by lazy { TypedAttributes() }
}

internal fun String.decodeHex(): ByteArray {
    check(length % 2 == 0) { "Must have an even length" }
    return ByteArray(length / 2) {
        val msb = get(it * 2).digitToInt(16)
        val lsb = get(it * 2 + 1).digitToInt(16)
        ((msb shl 4) or lsb).toByte()
    }
}
