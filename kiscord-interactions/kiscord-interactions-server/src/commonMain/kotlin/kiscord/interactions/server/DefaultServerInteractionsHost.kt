/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.interactions.server

import kiscord.*
import kiscord.api.*
import kiscord.builder.*
import kiscord.interactions.*
import kiscord.interactions.server.security.*
import org.kodein.log.*

@PublishedApi
internal class DefaultServerInteractionsHost(
    host: InteractionsHost,
    override val loggerFactory: LoggerFactory,
    override val clientId: Snowflake,
    override val clientSecret: String,
    override val clientConfiguration: ConfigurationChain<Kiscord.Builder>,
    override val signatureVerifierFactory: SignatureVerifierFactory
) : ServerInteractionsHost, InteractionsHost by host {
    class Builder<out P> internal constructor(
        override val loggerFactory: LoggerFactory,
        private val builder: P
    ) : ServerInteractionsHost.Builder, InteractionsHost.Builder by builder,
        BuilderFor<DefaultServerInteractionsHost>
        where P : InteractionsHost.Builder, P : BuilderFor<InteractionsHost> {

        override lateinit var clientId: Snowflake
        override lateinit var clientSecret: String
        private var clientConfiguration = ConfigurationChain<Kiscord.Builder>()
        override var signatureVerifierFactory: SignatureVerifierFactory = SignatureVerifierFactory

        override fun clientConfiguration(block: Kiscord.Builder.() -> Unit) {
            clientConfiguration += block
        }

        override fun build(): DefaultServerInteractionsHost {
            val host = builder.build()
            check(::clientId.isInitialized) { "Client ID must be initialized!" }
            check(::clientSecret.isInitialized) { "Client secret must be initialized!" }
            return DefaultServerInteractionsHost(
                host,
                loggerFactory,
                clientId,
                clientSecret,
                clientConfiguration,
                signatureVerifierFactory
            )
        }
    }
}
