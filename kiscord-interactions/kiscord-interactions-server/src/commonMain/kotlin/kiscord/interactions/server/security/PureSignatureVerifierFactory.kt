/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.interactions.server.security

import io.github.andreypfau.curve25519.ed25519.*
import kiscord.interactions.server.*


internal class PureSignatureVerifierFactory : SignatureVerifierFactory {
    class Verifier(private val publicKey: Ed25519PublicKey) : SignatureVerifier {
        override fun isSignatureValid(timestamp: ByteArray, body: ByteArray, signature: ByteArray): Boolean {
            return publicKey.verify(timestamp + body, signature)
        }
    }

    override val isSupported: Boolean get() = true

    override val priority: Int get() = -100

    override fun createVerifier(verifyKey: String): SignatureVerifier {
        val publicKey = Ed25519PublicKey(verifyKey.decodeHex())
        return Verifier(publicKey)
    }
}
