# Module kiscord-interactions-server

[Ktor](https://ktor.io) server plugin to run interaction-based app as Ktor server with webhooks.

See also [kiscord-interactions](https://kiscord.aur.rocks/docs/kiscord-interactions/index.html) module.

# Package kiscord.interactions.server

Bindings and helper functions for [Ktor](https://ktor.io) server.
