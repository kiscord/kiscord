plugins {
    alias(libs.plugins.kotlin.multiplatform)
    alias(libs.plugins.kotlin.serialization)
    alias(libs.plugins.kotlinx.atomicfu)
    alias(libs.plugins.dokka)
    alias(libs.plugins.licenser)
    `maven-publish`
    signing
}

apply(from = rootProject.file("gradle/multiplatform-bom.gradle.kts"))

kotlin {
    sourceSets.configureEach {
        languageSettings {
            optIn("kiscord.api.KiscordUnstableAPI")
        }
    }

    jvm {
        withJava()
    }

    // Linux
    linuxX64()

    // Darwin
    iosArm64()
    iosX64()
    macosArm64()
    macosX64()
}

dependencies {
    commonMainApi(project(":kiscord-interactions"))
    commonMainApi(libs.ktor.server.core)
    commonMainImplementation(libs.curve25519.kotlin)

    "jvmTestRuntimeOnly"(libs.slf4j.simple)
}

apply(from = rootProject.file("gradle/java9-module-info.gradle.kts"))
