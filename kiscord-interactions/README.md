# Module kiscord-interactions

Base module
for [kiscord-gateway-interactions](https://kiscord.aur.rocks/docs/kiscord-gateway/kiscord-gateway-interactions/index.html)
and [kiscord-interactions-server](https://kiscord.aur.rocks/docs/kiscord-interactions/kiscord-interactions-server/index.html).

# Package kiscord.interactions

Building blocks for describing interaction-based app.
