plugins {
    `java-platform`
    `maven-publish`
    signing
}

javaPlatform {
    allowDependencies()
}

dependencies {
    constraints {
        api(libs.kotlinx.atomicfu)
        api(libs.kotlinx.atomicfu.jvm)
        api(libs.kotlinx.atomicfu.js)
        api(libs.kotlinx.datetime)
        api(libs.kotlinx.html)
        api(libs.logback.classic)
        api(libs.logback.core)
        api(libs.slf4j.api)
        api(libs.slf4j.simple)
    }

    api(platform(project(":kiscord-bom")))
    api(platform(libs.kotlin.bom))
    api(platform(libs.ktor.bom))
    api(platform(libs.kotlinx.coroutines.bom))
    api(platform(libs.kotlinx.serialization.bom))
    api(platform(libs.junit5.bom))
    api(platform(libs.jackson.bom))
}

publishing {
    publications {
        register<MavenPublication>("platform") {
            from(components["javaPlatform"])
        }
    }
}
