pluginManagement {
    resolutionStrategy {
        eachPlugin {
            when (requested.id.id) {
                "kotlinx-atomicfu" -> useModule("org.jetbrains.kotlinx:atomicfu-gradle-plugin:${requested.version}")
            }
        }
    }
}

plugins {
    id("com.mooltiverse.oss.nyx") version (System.getenv("NYX_TOOL_VERSION") ?: "3.0.1")
    id("org.gradle.toolchains.foojay-resolver-convention") version "0.8.0"
}

rootProject.name = "kiscord"

include(":kiscord-bom")
include(":kiscord-codegen")
include(":kiscord-core")
include(":kiscord-gateway")
include(":kiscord-gateway:kiscord-gateway-events")
include(":kiscord-gateway:kiscord-gateway-interactions")
include(":kiscord-interactions")
include(":kiscord-interactions:kiscord-interactions-server")
include(":kiscord-platform")
include(":kiscord-samples")
include(":kiscord-scripting")
include(":kiscord-scripting-runner")
