import org.jetbrains.kotlin.gradle.dsl.*

plugins {
    alias(libs.plugins.kotlin.jvm)
    alias(libs.plugins.licenser)
}

apply(from = rootProject.file("gradle/multiplatform-bom.gradle.kts"))

dependencies {
    implementation(libs.bundles.swagger.core)
    implementation(libs.bundles.swagger.parser)
    implementation(libs.jackson.module.kotlin)
    implementation(libs.jetbrains.markdown)
    implementation(libs.kotlin.stdlib.jdk8)
    implementation(libs.kotlinpoet)
    implementation(libs.clikt)
    implementation(libs.kotlinx.html)
    implementation(libs.kotlinx.serialization.core)
    implementation(libs.ktor.client.cio)

    runtimeOnly(libs.slf4j.simple)
}

val kiscordJavaTarget = JvmTarget.fromTarget(ext["kiscord.java.target"] as String)

kotlin {
    explicitApi = null

    jvmToolchain {
        languageVersion = JavaLanguageVersion.of(JavaVersion.toVersion(kiscordJavaTarget.target).majorVersion)
    }

    compilerOptions {
        jvmTarget = kiscordJavaTarget
    }

    sourceSets.configureEach {
        languageSettings {
            optIn("kotlin.RequiresOptIn")
            optIn("kotlin.ExperimentalStdlibApi")
        }
    }
}

java {
    targetCompatibility = JavaVersion.toVersion(kiscordJavaTarget.target)
    sourceCompatibility = JavaVersion.toVersion(kiscordJavaTarget.target)

    toolchain {
        languageVersion = JavaLanguageVersion.of(JavaVersion.toVersion(kiscordJavaTarget.target).majorVersion)
    }
}
