/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.apiSpider

import io.swagger.v3.oas.models.*
import org.intellij.markdown.*
import org.intellij.markdown.ast.*
import org.intellij.markdown.flavours.*
import org.intellij.markdown.flavours.commonmark.*
import org.intellij.markdown.parser.*
import java.nio.file.*
import kotlin.streams.*

private val blacklistPaths = setOf(
    "game_sdk"
)

private val blacklistEndpoints: Set<(Endpoint) -> Boolean> = setOf(
    { it.path == "/guilds/{guild.id}/members/@me/nick" && it.method == PathItem.HttpMethod.PATCH },
)

private val pathRegex = Regex("##? (.+) % (GET|POST|DELETE|PUT|PATCH|PUT/PATCH) (.+)")
private val paramRegex = Regex("\\{([\\w.]+)#[^}]+}")

private tailrec fun Path.isSubPathOf(path: Path): Boolean {
    if (this == path) return true
    if (parent == null) return false
    return parent.isSubPathOf(path)
}

fun crawlApiDocs(path: Path): ApiSnapshot {
    val flavour: MarkdownFlavourDescriptor = CommonMarkFlavourDescriptor()
    val parser = MarkdownParser(flavour)

    val docsPath = path.resolve("docs")
    val blacklistPaths = blacklistPaths.map { docsPath.resolve(it) }

    val mdFiles = Files.find(docsPath, 10, { it, _ ->
        Files.isRegularFile(it) && Files.isReadable(it) && it.extension == "md"
    }).asSequence().filter { p ->
        blacklistPaths.none { p.isSubPathOf(it) }
    }.map { md: Path ->
        val mdPath = docsPath.relativize(md).toString().removeSuffix(".md").lowercase().replace('_', '-')

        val text = md.toFile().readText()
        val tree = parser.buildMarkdownTreeFromString(text).enrich(text)

        ParsedMarkdown(mdPath, tree)
    }.toList()

    val endpoints = mdFiles.flatMap { (mdPath, tree) ->
        val endpointTag = ReferencePage.entries.find { it.path == mdPath }?.tag ?: ReferencePage.Tag.Unknown

        tree.deepSequence.filter { node ->
            node.type == MarkdownElementTypes.ATX_2 || node.type == MarkdownElementTypes.ATX_1
        }.mapNotNull { node ->
            pathRegex.matchEntire(node.text)?.destructured
        }.flatMap collector@{ (title, methods, path) ->
            if (title.endsWith("(deprecated)")) return@collector emptySequence<Endpoint>()

            val pathFixed = paramRegex.replace(path, "{$1}").removeSuffix("/")
            val operationId = title.lowercase().replace(' ', '-')

            val url = "https://discord.com/developers/docs/${mdPath}#${operationId}"

            if (endpointTag == ReferencePage.Tag.Unknown) {
                println("Unable to guess endpoint tag for operation with id $operationId with path $path")
            }

            methods.splitToSequence('/').map { method ->
                Endpoint(
                    path = pathFixed,
                    method = enumValueOf(method),
                    title = title,
                    tag = endpointTag,
                    url = url
                )
            }.filterNot { endpoint -> blacklistEndpoints.any { it.invoke(endpoint) } }
        }
    }.toSet()

    val structures = mdFiles.flatMapTo(mutableSetOf()) { (mdPath, tree) ->
        val referencePage = ReferencePage.entries.find { it.path == mdPath } ?: return@flatMapTo emptySequence()

        tree.deepSequenceWithTrackedPath { it.type.titleLevel }.filter { (_, node) ->
            node.type == MarkdownElementTypes.PARAGRAPH
        }.map { (path, node) ->
            path.map { it.text.trimStart('#').substringBeforeLast('%').trim() } to
                node.text.lines()
        }.filter { (_, lines) ->
            lines.size > 1 && lines.all { line -> line.startsWith('|') && line.endsWith('|') }
        }.mapNotNull { (path, lines) ->
            val parsedLines = lines.map { line -> line.trim('|').split('|').map { it.trim() } }
            val header = parsedLines[0]
            val fieldIndex = header.searchIndex("Field", "Name") ?: return@mapNotNull null
            val typeIndex = header.searchIndex("Type") ?: return@mapNotNull null
            val descriptionIndex = header.searchIndex("Description")
            val structureRef =
                "${referencePage.pageUrl}#${path.takeLast(2).joinToString("-") { it.toFragmentSlug() }}"
            val fields = parsedLines.subList(2, lines.size).mapNotNullTo(mutableSetOf()) { line ->
                var name = line[fieldIndex].trimEnd('*', ' ', '\\')
                if (name.endsWith("(deprecated)")) return@mapNotNullTo null
                if (descriptionIndex != null && "**Deprecated**" in line[descriptionIndex]) return@mapNotNullTo null
                if (descriptionIndex != null && "(deprecated)" in line[descriptionIndex]) return@mapNotNullTo null
                val type = line[typeIndex]
                val isRequired = !name.endsWith('?')
                if (!isRequired) {
                    name = name.removeSuffix("?")
                }
                val isNullable = type.startsWith('?')
                StructureDef.Field(
                    name = name,
                    required = isRequired,
                    nullable = isNullable
                )
            }
            StructureDef(path = structureRef, title = path.last(), fields = fields)
        }
    }

    return ApiSnapshot(
        kind = ApiSnapshot.Kind.Reference,
        name = "Discord API",
        endpoints = endpoints,
        structures = structures
    )
}

private fun Iterable<String>.searchIndex(vararg candidates: String): Int? {
    val lower = map { it.lowercase() }
    candidates.forEach { candidate ->
        val i = lower.indexOf(candidate.lowercase())
        if (i >= 0) return i
    }
    return null
}

private data class ParsedMarkdown(
    val path: String,
    val tree: EnrichedASTNode
)

val Path.extension: String get() = fileName.toString().substringAfterLast(".")

private interface ASTNodeGeneric<out T : ASTNodeGeneric<T>> : ASTNode, Iterable<T> {
    override val children: List<T>

    override val parent: T?
}

private class EnrichedASTNode(
    node: ASTNode,
    private val originalText: String,
    override val parent: EnrichedASTNode? = null
) :
    ASTNode by node, ASTNodeGeneric<EnrichedASTNode> {
    override val children: List<EnrichedASTNode> by lazy {
        node.children.map { EnrichedASTNode(it, originalText, this) }
    }

    override fun iterator() = children.iterator()

    val text: String by lazy { originalText.substring(startOffset, endOffset) }

    override fun toString(): String {
        return "$type: $text"
    }
}

private val <T : ASTNodeGeneric<T>> T.deepSequence: Sequence<T>
    get() = sequence {
        yield(this@deepSequence)
        yieldAll(children.asSequence().flatMap { it.deepSequence })
    }

private fun <T : ASTNodeGeneric<T>> T.deepSequenceWithTrackedPath(pathExtractor: (T) -> Int): Sequence<Pair<List<T>, T>> {
    val stack: MutableList<T> = ArrayList()
    return deepSequence.map { node ->
        val currentPath = stack.toList()
        val nodeLevel = pathExtractor(node)
        if (nodeLevel > 0) {
            stack.removeAll { pathExtractor(it) >= nodeLevel }
            stack.add(node)
        }
        currentPath to node
    }
}

private val IElementType.titleLevel: Int
    get() = when (this) {
        MarkdownElementTypes.ATX_1 -> 1
        MarkdownElementTypes.ATX_2 -> 2
        MarkdownElementTypes.ATX_3 -> 3
        MarkdownElementTypes.ATX_4 -> 4
        MarkdownElementTypes.ATX_5 -> 5
        MarkdownElementTypes.ATX_6 -> 6
        else -> 0
    }

private fun ASTNode.enrich(text: String): EnrichedASTNode {
    check(parent == null)
    if (this is EnrichedASTNode) return this
    return EnrichedASTNode(this, text, null)
}

private val fragmentSlugToRemove = Regex("[/]")
private val fragmentSlugInvalid = Regex("[^a-z0-9]")
private val fragmentSlugDashes = Regex("-{2,}")
private fun String.toFragmentSlug(): String {
    return lowercase()
        .replace(fragmentSlugToRemove, "")
        .replace(fragmentSlugInvalid, "-")
        .replace(fragmentSlugDashes, "-")
}
