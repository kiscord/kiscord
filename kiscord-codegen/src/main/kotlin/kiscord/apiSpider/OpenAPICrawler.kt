/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.apiSpider

import io.ktor.http.*
import io.swagger.v3.oas.models.*
import io.swagger.v3.oas.models.media.*
import io.swagger.v3.parser.util.*
import kiscord.codegen.poet.*
import kiscord.codegen.swagger.*

fun crawlOpenAPI(openapi: OpenAPI, resolver: KiscordOpenAPIResolver): ApiSnapshot {
    val endpoints = openapi.paths.flatMapTo(mutableSetOf()) { (path, item) ->
        item.readOperationsMap().map { (method, operation) ->
            val pathForUrl = path.replace('{', '-').replace('}', '-')
            val url = "/exploring/openapi#${method.name.lowercase()}-${pathForUrl.encodeURLPath()}"

            val endpointTag = operation.tags.map {
                try {
                    ReferencePage.Tag.valueOf(it)
                } catch (_: IllegalArgumentException) {
                    ReferencePage.Tag.Unknown
                }
            }.firstOrNull { it != ReferencePage.Tag.Unknown } ?: ReferencePage.Tag.Unknown

            if (endpointTag == ReferencePage.Tag.Unknown) {
                println("Unable to guess endpoint tag for operation with id ${operation.operationId}")
            }

            Endpoint(
                path = path,
                method = method,
                title = operation.summary,
                tag = endpointTag,
                url = url
            )
        }
    }

    val structures = openapi.components.schemas.mapNotNullTo(mutableSetOf()) { (_, schema) ->
        if (!schema.enum.isNullOrEmpty()) return@mapNotNullTo null
        val structureRef =
            schema.codegenRef ?: schema.externalDocs?.url ?: return@mapNotNullTo null
        if (structureRef == "kiscord://suppress") return@mapNotNullTo null
        val fields = schema.extractFields(resolver)
        if (fields.isEmpty()) return@mapNotNullTo null
        StructureDef(
            path = structureRef,
            title = schema.title ?: error(""),
            description = schema.description ?: "",
            fields = fields
        )
    }

    return ApiSnapshot(
        kind = ApiSnapshot.Kind.OpenAPI,
        name = "Kiscord",
        endpoints = endpoints,
        structures = structures
    )
}

private fun Schema<*>.extractFields(resolver: KiscordOpenAPIResolver): Set<StructureDef.Field> {
    val ref = `$ref`
    if (ref != null) {
        return resolver.cache.loadRef(ref, RefUtils.computeRefFormat(ref), Schema::class.java)?.extractFields(resolver)
            .orEmpty()
    }
    return buildSet {
        when (this@extractFields) {
            is ObjectSchema -> {
                val required = required.orEmpty()
                properties?.forEach { (name, property) ->
                    val hacks = property.codegenStructureHacks.orEmpty()
                    val isRequired = "required" in hacks || name in required
                    val isNullable = "nullable" in hacks || property.nullable == true
                    val isSynthetic = "synthetic" in hacks
                    add(
                        StructureDef.Field(
                            name = name,
                            required = isRequired,
                            nullable = isNullable,
                            synthetic = isSynthetic
                        )
                    )
                }
            }
            is ComposedSchema -> {
                oneOf?.flatMapTo(this) { it.extractFields(resolver) }
                allOf?.flatMapTo(this) { it.extractFields(resolver) }
            }
        }
    }
}
