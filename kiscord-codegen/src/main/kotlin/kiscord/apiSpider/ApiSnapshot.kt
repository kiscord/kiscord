/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.apiSpider

import io.swagger.v3.oas.models.*

data class ApiSnapshot(
    val kind: Kind,
    val name: String,
    val endpoints: Set<Endpoint>,
    val structures: Set<StructureDef>
) {
    fun purify(): ApiSnapshot {
        val endpoints = endpoints.filterTo(mutableSetOf()) { endpoint ->
            excludedEndpoints.none { it.match(endpoint) }
        }

        return copy(endpoints = endpoints)
    }

    private data class Ignore(
        val method: PathItem.HttpMethod,
        val path: String,
        val title: String? = null
    ) {
        fun match(endpoint: Endpoint): Boolean {
            if (method != endpoint.method || path != endpoint.path) return false
            if (title != null && title != endpoint.title) return false
            return true
        }
    }

    enum class Kind(val title: String, val icon: String, val canBeOmitted: Boolean = false) {
        Reference("Discord API Reference", "discord", canBeOmitted = true),
        OpenAPI("OpenAPI Specification by Kiscord", "openapi"),
    }


    companion object {
        private val excludedEndpoints = setOf(
            Ignore(PathItem.HttpMethod.POST, "/webhooks/{webhook.id}/{webhook.token}/github"),
            Ignore(PathItem.HttpMethod.POST, "/webhooks/{webhook.id}/{webhook.token}/slack"),
            Ignore(PathItem.HttpMethod.POST, "/users/@me/channels", "Create Group DM"),
        )
    }
}
