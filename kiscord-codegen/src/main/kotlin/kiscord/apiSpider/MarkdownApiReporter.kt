/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.apiSpider

import kotlinx.html.*
import kotlinx.html.stream.*
import java.io.*
import java.text.*
import java.util.*

object MarkdownApiReporter : ApiReporter<MarkdownApiReporter.MarkdownOptions> {
    data class MarkdownOptions(
        override val endpoints: Boolean = true,
        override val structures: Boolean = true,
    ) : ApiReporter.Options

    val Default = MarkdownOptions()

    override fun report(destination: File, snapshots: Collection<ApiSnapshot>) {
        report(destination, snapshots, Default)
    }

    override fun report(destination: File, snapshots: Collection<ApiSnapshot>, options: MarkdownOptions) {
        val md = buildString {
            appendLine(
                """
                ---
                layout: post
                title: "API Spider"
                date: 2021-08-01 22:00:00 +0800
                last_modified_at: ${SimpleDateFormat("yyyy-MM-dd HH:mm:ss XX").format(Date())}
                category: exploring
                author: ci
                short-description: Check what parts of Discord implemented in Kiscord
                ---
                """.trimIndent()
            )

            val tagConsumer = appendHTML()
            tagConsumer.link(href = "{{ site.baseurl }}/css/apiSpider.css", rel = "stylesheet")
            if (options.endpoints) tagConsumer.reportEndpoints(snapshots)
            if (options.structures) tagConsumer.reportStructures(snapshots)
            tagConsumer.finalize()
        }
        destination.writeText(md)
    }

    private fun <T> T.addHeader(
        title: String,
        idPrefix: String = ""
    ) where T : HTMLTag, T : CommonAttributeGroupFacadeFlowHeadingPhrasingContent {
        val id = "$idPrefix${title.lowercase().replace('.', '-')}"
        this.id = id
        +title

        a(classes = "anchor") {
            href = "#$id"
            this.title = "Anchor link to this section."
            i(classes = "icon-link")
        }
    }

    fun TagConsumer<*>.reportEndpoints(snapshots: Collection<ApiSnapshot>) {
        val endpoints = snapshots.flatMapTo(TreeSet()) { snapshot ->
            snapshot.endpoints.map { endpoint -> endpoint.copy(url = null) }
        }
        val refinedSnapshots = snapshots.filterNot { snapshot ->
            endpoints.all { endpoint ->
                snapshot.endpoints.any { it.isSame(endpoint) }
            }
        }.takeIf { it.isNotEmpty() } ?: snapshots.filterNot { it.kind.canBeOmitted }
        val tags =
            refinedSnapshots.flatMapTo(EnumSet.noneOf(ReferencePage.Tag::class.java)) { snapshot -> snapshot.endpoints.map { it.tag } }

        h1 {
            addHeader("Endpoints")
        }
        p { +"This sections represent status of REST methods, implemented by Kiscord." }
        p {
            snapshots.forEachIndexed { i, snapshot ->
                if (i != 0) br
                +"Total operations in %s: %d (%.0f%%)".format(
                    snapshot.name,
                    snapshot.endpoints.size,
                    snapshot.endpoints.size * 100.0 / endpoints.size
                )
            }
        }

        table(classes = "endpoints gridlike sticky-header") {
            tags.forEach { tag ->
                val endpointsWithTag = endpoints.filter { it.tag == tag }
                tr {
                    th(classes = "endpoint-path") {
                        +tag.displayName
                    }
                    refinedSnapshots.forEach { snapshot ->
                        th(classes = "min-size") { +snapshot.name }
                    }
                }
                endpointsWithTag.forEach { endpoint ->
                    tr(classes = "endpoint") {
                        td(classes = "endpoint-path") {
                            title = endpoint.title
                            div(classes = "flex-container") {
                                span(classes = "http-method ${endpoint.method.name.lowercase()}") { +endpoint.method.name }
                                span(classes = "endpoint-path") {
                                    endpoint.path.split('/').forEach { part ->
                                        if (part.isNotBlank()) {
                                            span { +"/" }
                                            span(classes = "non_breakable") { +part }
                                        }
                                    }
                                }
                                snapshots.forEach link@{ snapshot ->
                                    val e = snapshot.endpoints.find { it.isSame(endpoint) }
                                    if (e?.url == null) return@link
                                    a(
                                        classes = "reference-link icon-${snapshot.kind.icon}",
                                        href = e.url,
                                        target = "_blank",
                                    ) {
                                        title = snapshot.kind.title
                                    }
                                }
                            }
                        }
                        refinedSnapshots.forEach { snapshot ->
                            val isPresent = snapshot.endpoints.any { it.isSame(endpoint) }
                            if (isPresent) {
                                td(classes = "checkmark green") {
                                    title = "Operation implemented by Kiscord"
                                    i(classes = "icon-check-circle")
                                }
                            } else {
                                td(classes = "checkmark red") {
                                    title = "Operation not implemented by Kiscord"
                                    i(classes = "icon-times-circle")
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    fun TagConsumer<*>.reportStructures(snapshots: Collection<ApiSnapshot>) {
        h1 {
            addHeader("Structures")
        }
        p { +"This sections represent all structures, defined by Kiscord." }
        p {
            +"Questing mark before the name means field is optional."
            br
            +"Questing mark after the name means field is nullable."
        }

        val structures = snapshots.filter { !it.kind.canBeOmitted }.flatMap { it.structures }.sortedBy { it.title }
        val structuresMap = snapshots.flatMap { snapshot -> snapshot.structures.map { it to snapshot } }.toMap()
        val refinedStructuresMap = structuresMap.filterValues { it.kind.canBeOmitted }

        structures.forEach { structure ->
            val refined = refinedStructuresMap.filterKeys { it.path == structure.path }
            val reports = mutableListOf<StructureFieldReport>()
            structure.fields.forEach { field ->
                reports += reportStructureField(field, refined.keys.singleOrNull())
            }
            refined.keys.flatMap { it.fields }.filter { field ->
                structure.fields.none { it.name == field.name }
            }.forEach { field ->
                reports += reportStructureField(field, null)
            }

            val implementedCount = reports.count { it.state == StructureFieldReport.State.Implemented }
            val implementedRatio = implementedCount.toFloat() / reports.size
            val isAllImplemented = implementedCount == reports.size

            details {
                open = !isAllImplemented

                summary {
                    h3(classes = "structure-header") {
                        addHeader(structure.title, idPrefix = "structure-")
                        a(classes = "anchor") {
                            href = structure.path
                            title = "Discord API Reference"
                            target = "_blank"
                            i(classes = "icon-discord")
                        }
                        span(classes = "structure-ratio") { +"%.0f%%".format(implementedRatio * 100f) }
                    }
                }

                if (structure.description.isNotBlank()) {
                    p {
                        +structure.description
                    }
                }

                table(classes = "structure gridlike") {
                    reports.forEach { (field, state) ->
                        tr {
                            td {
                                title = field.title
                                if (!field.required) +"?"
                                +field.name
                                if (field.nullable) +"?"
                            }
                            when (state) {
                                StructureFieldReport.State.Implemented -> td(classes = "checkmark green icon-cell") {
                                    title = "Property implemented by Kiscord"
                                    i(classes = "icon-check-circle min-size")
                                }
                                StructureFieldReport.State.Broken -> td(classes = "checkmark yellow icon-cell") {
                                    title = "Property partially implemented by Kiscord"
                                    i(classes = "icon-times-circle min-size")
                                }
                                StructureFieldReport.State.Missing -> td(classes = "checkmark red icon-cell") {
                                    title = "Property doesn't implemented by Kiscord"
                                    i(classes = "icon-times-circle min-size")
                                }
                                StructureFieldReport.State.Extra -> td(classes = "checkmark yellow icon-cell") {
                                    title = "Property missing in API Reference, deprecated and/or removed?"
                                    i(classes = "icon-times-circle min-size")
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    data class StructureFieldReport(
        val field: StructureDef.Field,
        val state: State
    ) {
        enum class State {
            Implemented,
            Missing,
            Broken,
            Extra,
        }
    }

    private fun reportStructureField(
        field: StructureDef.Field,
        referenceStructure: StructureDef?
    ): StructureFieldReport = when {
        field.synthetic -> StructureFieldReport(field, StructureFieldReport.State.Implemented)
        referenceStructure == null ->
            StructureFieldReport(field, StructureFieldReport.State.Missing)
        field in referenceStructure.fields ->
            StructureFieldReport(field, StructureFieldReport.State.Implemented)
        referenceStructure.fields.any { it.name == field.name } ->
            StructureFieldReport(field, StructureFieldReport.State.Broken)
        else -> StructureFieldReport(field, StructureFieldReport.State.Extra)
    }

    private val StructureDef.Field.title: String
        get() = when {
            required && nullable -> "Required, but can be null"
            required -> "Required and cannot be null"
            nullable -> "Optional, but can be null"
            else -> "Optional and cannot be null"
        }
}
