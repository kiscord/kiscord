/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.apiSpider

import io.swagger.v3.oas.models.*
import kotlinx.html.*
import kotlinx.html.stream.*
import java.io.*
import java.util.*

object HtmlApiReporter : ApiReporter<HtmlApiReporter.HtmlOptions> {
    data class HtmlOptions(
        override val endpoints: Boolean = true,
        override val structures: Boolean = true,
        val border: Int? = 1,
        val splitPathAtSlash: Boolean = false,
        val operationPresent: FlowOrPhrasingContent.() -> Unit,
        val operationMissing: FlowOrPhrasingContent.() -> Unit,
    ) : ApiReporter.Options

    val Default = HtmlOptions(
        operationPresent = {
            img(classes = "method-icon", alt = "V", src = "./positive.svg") {
                width = "24"
                height = "24"
            }
        },
        operationMissing = {
            img(classes = "method-icon", alt = "X", src = "./negative.svg") {
                width = "24"
                height = "24"
            }
        }
    )

    fun TagConsumer<*>.reportEndpoints(snapshots: Collection<ApiSnapshot>, options: HtmlOptions) {
        val paths = snapshots.flatMap { it.endpoints }.mapTo(TreeSet()) { it.path }
        val httpMethods = snapshots.flatMapTo(EnumSet.noneOf(PathItem.HttpMethod::class.java)) { snapshot ->
            snapshot.endpoints.map { it.method }
        }

        h1 { +"Endpoints" }
        span { +"This sections represent status of REST methods, implemented by Kiscord" }
        table(classes = "endpoints sticky-header") {
            options.border?.let { attributes["border"] = it.toString() }

            thead {
                tr {
                    th(classes = "endpoint-path") { +"Path" }
                    th(classes = "endpoint-source") { +"Source" }
                    httpMethods.forEach { method ->
                        th(classes = "http-method") {
                            +method.name
                        }
                    }
                }
            }
            tbody {
                paths.forEach { path ->
                    snapshots.forEachIndexed { snapshotIndex, snapshot ->
                        val endpoints = snapshot.endpoints.filter { it.path == path }

                        val definedMethods = snapshots.flatMap { it.endpoints }
                            .filter { it.path == path }
                            .mapTo(mutableSetOf()) { it.method }

                        tr(classes = "endpoint") {
                            if (snapshotIndex == 0) td(classes = "endpoint-path") {
                                rowSpan = "${snapshots.size}"
                                if (options.splitPathAtSlash) {
                                    path.split('/').forEach { part ->
                                        if (part.isNotBlank()) {
                                            span { +"/" }
                                            span(classes = "non_breakable") { +part }
                                        }
                                    }
                                } else {
                                    +path
                                }
                            }
                            td(classes = "endpoint-source") { +snapshot.name }
                            httpMethods.forEach { method ->
                                val endpoint = endpoints.find { it.method == method }
                                when {
                                    method !in definedMethods -> {
                                        if (snapshotIndex == 0) {
                                            td(classes = "http-method blank") { rowSpan = "${snapshots.size}" }
                                        }
                                    }
                                    endpoint == null -> {
                                        td(classes = "http-method missing") {
                                            title = "Method missing"
                                            options.operationMissing(this)
                                        }
                                    }
                                    else -> {
                                        td(classes = "http-method present") {
                                            title = "Method present"
                                            options.operationPresent(this)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        br()
        snapshots.forEach { snapshot ->
            p { +"Total operations in ${snapshot.name}: ${snapshot.endpoints.size}" }
        }
    }

    fun TagConsumer<*>.reportStructures() {
        h1 { +"Structures" }
        span { +"Structures doesn't supported in html reporter for now" }
    }

    fun TagConsumer<*>.report(snapshots: Collection<ApiSnapshot>, options: HtmlOptions) {
        html {
            head {
                meta(charset = "utf-8")
                meta(name = "viewport", content = "width=device-width, initial-scale=1")
                title("Kiscord API Spider report")
                link(href = "./style.css", rel = "stylesheet")
            }
            body {
                if (options.endpoints) reportEndpoints(snapshots, options)
                if (options.structures) reportStructures()
            }
        }
    }

    override fun report(destination: File, snapshots: Collection<ApiSnapshot>) {
        report(destination, snapshots, Default)
    }

    override fun report(destination: File, snapshots: Collection<ApiSnapshot>, options: HtmlOptions) {
        val html = buildString {
            val tagConsumer = appendHTML()
            tagConsumer.report(snapshots, options)
            tagConsumer.finalize()
        }

        destination.resolve("index.html").writeText(html)

        copyResources(destination)
    }

    fun copyResources(destination: File) {
        val classLoader = HtmlApiReporter::class.java.classLoader

        reportTemplateFiles.forEach { file ->
            classLoader.getResourceAsStream("kiscord/apiSpider/reportTemplate/$file")!!.use { input: InputStream ->
                destination.resolve(file).outputStream().use { output: OutputStream ->
                    input.copyTo(output)
                }
            }
        }
    }

    private val reportTemplateFiles = setOf(
        "style.css",
        "positive.svg",
        "negative.svg"
    )
}
