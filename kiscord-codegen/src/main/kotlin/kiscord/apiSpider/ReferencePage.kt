/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.apiSpider

import io.ktor.http.*

enum class ReferencePage(val path: String, val title: String, val tag: Tag = Tag.Unknown) {
    Application("resources/application", "Application Resource", Tag.Application),
    ApplicationCommands("interactions/application-commands", "Application Commands", Tag.Command),
    ApplicationRoleConnectionMetadata(
        "resources/application-role-connection-metadata",
        "Application Role Connection Metadata",
        Tag.Command
    ),
    AuditLog("resources/audit-log", "Audit Logs Resource", Tag.AuditLog),
    AutoModeration("resources/auto-moderation", "Auto Moderation", Tag.AutoModeration),
    Channel("resources/channel", "Channels Resource", Tag.Channel),
    Emoji("resources/emoji", "Emoji Resource", Tag.Emoji),
    EntitlementResource("monetization/entitlements", "Entitlement Resource", Tag.Application),
    Gateway("topics/gateway", "Gateways", Tag.Gateway),
    GatewayEvents("topics/gateway-events", "Gateway Events", Tag.Gateway),
    Guild("resources/guild", "Guild Resource", Tag.Guild),
    GuildScheduledEvent("resources/guild-scheduled-event", "Guild Scheduled Event", Tag.GuildScheduledEvent),
    GuildTemplate("resources/guild-template", "Guild Template Resource", Tag.GuildTemplate),
    InteractionsReceivingAndResponding("interactions/receiving-and-responding", "Interactions", Tag.Command),
    Invite("resources/invite", "Invite Resource", Tag.Invite),
    MessageComponents("interactions/message-components", "Message Components"),
    OAuth2("topics/oauth2", "OAuth2", Tag.OAuth2),
    Permissions("topics/permissions", "Permissions"),
    SKUResource("monetization/skus", "SKU Resource", Tag.Application),
    StageInstance("resources/stage-instance", "Stage Instance Resource", Tag.Stage),
    Sticker("resources/sticker", "Sticker Resource", Tag.Sticker),
    Teams("topics/teams", "Teams"),
    User("resources/user", "Users Resource", Tag.User),
    Voice("resources/voice", "Voice Resource", Tag.Voice),
    Webhook("resources/webhook", "Webhook Resource", Tag.Webhook),
    ;

    val pageUrl get() = Url("https://discord.com/developers/docs/$path")

    enum class Tag(val displayName: String) {
        Unknown("Unknown operations"),
        Application("Application operations"),
        AuditLog("Audit Log operations"),
        AutoModeration("Auto Moderation operations"),
        Channel("Channel operations"),
        Command("Command operations"),
        Emoji("Emoji operations"),
        Gateway("Gateway operations"),
        Guild("Guild operations"),
        GuildScheduledEvent("Guild Scheduled Event operations"),
        GuildTemplate("Guild Template operations"),
        Invite("Invite operations"),
        OAuth2("OAuth2 operations"),
        Stage("Stage operations"),
        Sticker("Sticker operations"),
        User("User operations"),
        Voice("Voice operations"),
        Webhook("Webhook operations"),
    }
}
