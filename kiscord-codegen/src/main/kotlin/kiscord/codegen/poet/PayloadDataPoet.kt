/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.codegen.poet

import com.fasterxml.jackson.module.kotlin.*
import com.squareup.kotlinpoet.*
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import io.swagger.v3.oas.models.media.*
import kiscord.codegen.*

interface PayloadDataPoet : ClassPoet {
    val schema: Schema<*>

    fun mapping(snap: PoetSnap): Map<EnumPoet.EnumValue, TypeName?> {
        val opcodePoet = snap.resolvePoet(opcodeClass) as EnumPoet<*>
        return opcodePoet.enumValues.associateWith { enumValue ->
            enumValue.extensions["x-kiscord-codegen-data-ref"]?.let {
                val schema = codegenMapper.convertValue<Schema<*>>(it)
                snap.resolveType(schema)?.copy(nullable = false)
            }
        }
    }

    fun eventType(snap: PoetSnap): ClassName {
        return ClassName(GatewayEventPoet.PACKAGE, "${selfType.simpleName}Event")
    }

    fun populateEvent(snap: PoetSnap, builder: TypeSpec.Builder) {
        val opcode = mapping(snap).entries.single { it.value == selfType }.key.identifier

        builder.addSuperinterface(GATEWAY_PAYLOAD_EVENT.parameterizedBy(getDataType(snap)))
            .addProperty(
                PropertySpec.builder("opcode", GATEWAY_OPCODE)
                    .addModifiers(KModifier.OVERRIDE)
                    .getter(
                        FunSpec.getterBuilder().addStatement("return %T.%L", opcodeClass, opcode).build()
                    )
                    .build()
            )
    }

    fun getCatchFunName(snap: PoetSnap): String = "on${selfType.simpleName}"
    fun getDataType(snap: PoetSnap): TypeName = selfType

    class Object(
        schema: Schema<Any?>, selfName: ClassName, metadata: PoetSnap.PoetResolveMetadata
    ) : ObjectPoet<Schema<Any?>>(schema, selfName, metadata), PayloadDataPoet {
        override fun process(snap: PoetSnap, sink: PoetSink, builder: TypeSpec.Builder) {
            super.process(snap, sink, builder)
            populatePayloadData(builder, snap)
            if (isSingleton(snap)) {
                builder.addFunction(
                    FunSpec.builder("toString")
                        .addModifiers(KModifier.OVERRIDE)
                        .returns(STRING)
                        .addStatement("return %S", selfType.simpleName)
                        .build()
                )
            }
        }
    }

    class ValueClass(
        schema: Schema<out Any?>, selfName: ClassName
    ) : ValueClassPoet<Any?>(schema, selfName), PayloadDataPoet {
        override fun process(snap: PoetSnap, sink: PoetSink, builder: TypeSpec.Builder) {
            super.process(snap, sink, builder)
            populatePayloadData(builder, snap)
        }
    }

    companion object {
        val opcodeClass = ClassName("kiscord.api.gateway", "GatewayOpcode")
    }
}

private fun PayloadDataPoet.populatePayloadData(builder: TypeSpec.Builder, snap: PoetSnap) {
    val opcode = mapping(snap).entries.single { it.value == selfType }.key.identifier

    builder.addSuperinterface(GATEWAY_PAYLOAD_DATA).addProperty(
        PropertySpec.builder("opcode", PayloadDataPoet.opcodeClass)
            .addModifiers(KModifier.OVERRIDE)
            .getter(
                FunSpec.getterBuilder()
                    .addStatement("return %T.%L", PayloadDataPoet.opcodeClass, opcode)
                    .build()
            )
            .build()
    )
}
