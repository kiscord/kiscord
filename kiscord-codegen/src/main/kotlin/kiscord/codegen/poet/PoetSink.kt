/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.codegen.poet

import com.squareup.kotlinpoet.*
import kiscord.codegen.*
import java.nio.file.*

sealed class PoetSink {
    abstract val isRoot: Boolean

    abstract fun resolveClassName(name: String): ClassName
    abstract fun addType(typeSpec: TypeSpec, target: Target = Target.Default)
    abstract fun addProperty(propertySpec: PropertySpec, target: Target = Target.Default)
    abstract fun addFunction(funSpec: FunSpec, target: Target = Target.Default)
    abstract fun addAnnotation(annotationSpec: AnnotationSpec, target: Target = Target.Default)
    abstract fun addAliasedImport(member: ClassName, `as`: String)
    abstract fun addAliasedImport(member: MemberName, `as`: String)
    abstract fun addImport(packageName: String, target: Target = Target.Default, vararg names: String)
    abstract fun addUseSerializers(serializer: ClassName)
    abstract fun addOptIn(optIn: ClassName, target: Target = Target.Default)

    abstract fun toFileSpecs(): Collection<FileSpec>

    enum class Target {
        Default,
        TopLevel,
        ExternalFile,
        Extra;
    }

    companion object {
        val typeNameComparator = compareBy<TypeName?> {
            it.toString()
        }

        val funSpecComparator = compareBy<FunSpec> {
            it.name
        } then compareBy(typeNameComparator) {
            it.receiverType
        } then compareBy {
            it.parameters.joinToString()
        } then compareBy(typeNameComparator) {
            it.returnType
        }

        val propertySpecComparator = compareBy<PropertySpec> {
            it.name
        } then compareBy(typeNameComparator) {
            it.receiverType
        } then compareBy(typeNameComparator) {
            it.type
        }

        val typeSpecComparator: Comparator<TypeSpec> = compareBy { it.name }
    }

    class RootFile(private val builder: FileSpec.Builder) : PoetSink() {
        constructor(className: ClassName) : this(FileSpec.builder(className.packageName, className.simpleName))

        init {
            builder.addAnnotation(GENERATED)
        }

        override val isRoot: Boolean get() = true

        private val sinks = mutableSetOf<PoetSink>()
        private val typeSpecs = mutableListOf<TypeSpec>()
        private val funSpecs = mutableListOf<FunSpec>()

        private val extraSink by lazy {
            ExtraFile(
                FileSpec.builder(builder.packageName, "${builder.name}Codegen")
                    .addAnnotation(GENERATED)
                    .addAnnotation(
                        AnnotationSpec.builder(JvmMultifileClass::class)
                            .useSiteTarget(AnnotationSpec.UseSiteTarget.FILE).build()
                    )
                    .addAnnotation(
                        AnnotationSpec.builder(JvmName::class)
                            .addMember("name = %S", "${builder.name}Kt")
                            .useSiteTarget(AnnotationSpec.UseSiteTarget.FILE).build()
                    )
                    .indent("    ")
            ).also {
                sinks += it
            }
        }

        init {
            builder.indent("    ")
        }

        override fun resolveClassName(name: String) = ClassName(builder.packageName, name)

        override fun addType(typeSpec: TypeSpec, target: Target) {
            when (target) {
                Target.Extra -> extraSink.addType(typeSpec, target)
                Target.ExternalFile -> {
                    val className =
                        ClassName(builder.packageName, typeSpec.name ?: error("external file should have name"))
                    RootFile(className).also { file ->
                        file.addType(typeSpec)
                        sinks.add(file)
                    }
                }
                else -> typeSpecs.add(typeSpec)
            }
        }

        override fun addProperty(propertySpec: PropertySpec, target: Target) {
            when (target) {
                Target.Extra -> extraSink.addProperty(propertySpec, target)
                Target.ExternalFile -> error("unable to create external file for property")
                else -> builder.addProperty(propertySpec)
            }
        }

        override fun addFunction(funSpec: FunSpec, target: Target) {
            when (target) {
                Target.Extra -> extraSink.addFunction(funSpec, target)
                Target.ExternalFile -> throw UnsupportedOperationException()
                else -> funSpecs += funSpec
            }
        }

        override fun addAnnotation(annotationSpec: AnnotationSpec, target: Target) {
            when (target) {
                Target.Extra -> extraSink.addAnnotation(annotationSpec, target)
                Target.ExternalFile -> throw UnsupportedOperationException()
                else -> builder.addAnnotation(annotationSpec)
            }
        }

        override fun addAliasedImport(member: ClassName, `as`: String) {
            builder.addAliasedImport(member, `as`)
        }

        override fun addAliasedImport(member: MemberName, `as`: String) {
            builder.addAliasedImport(member, `as`)
        }

        override fun addImport(packageName: String, target: Target, vararg names: String) {
            when (target) {
                Target.Extra -> extraSink.addImport(packageName, target, *names)
                Target.ExternalFile -> throw UnsupportedOperationException()
                else -> builder.addImport(packageName, *names)
            }
        }

        override fun toFileSpecs(): Collection<FileSpec> {
            typeSpecs.sortedWith(typeSpecComparator).forEach { builder.addType(it) }
            typeSpecs.clear()

            funSpecs.sortedWith(funSpecComparator).forEach { builder.addFunction(it) }
            funSpecs.clear()

            if (useSerializers.isNotEmpty()) {
                builder.addAnnotation(
                    AnnotationSpec.builder(ClassName("kotlinx.serialization", "UseSerializers"))
                        .addMember("%L", useSerializers.map { CodeBlock.of("%T::class", it) }.joinToCode())
                        .build()
                )
            }

            if (optIn.isNotEmpty()) {
                builder.addAnnotation(
                    AnnotationSpec.builder(ClassName("kotlin", "OptIn"))
                        .addMember("%L", optIn.map { CodeBlock.of("%T::class", it) }.joinToCode())
                        .build()
                )
            }

            val specs = mutableSetOf<FileSpec>()
            specs += builder.build()
            sinks.flatMapTo(specs) { it.toFileSpecs() }
            return specs
        }

        private val useSerializers: MutableSet<ClassName> = mutableSetOf()

        override fun addUseSerializers(serializer: ClassName) {
            useSerializers += serializer
        }

        private val optIn: MutableSet<ClassName> = mutableSetOf()

        override fun addOptIn(optIn: ClassName, target: Target) {
            when (target) {
                Target.Extra -> extraSink.addOptIn(optIn, target)
                Target.Default, Target.TopLevel -> this.optIn += optIn
                else -> throw UnsupportedOperationException()
            }
        }
    }

    class ExtraFile(private val builder: FileSpec.Builder) : PoetSink() {
        override val isRoot: Boolean get() = true

        private val functions = mutableListOf<FunSpec>()
        private val properties = mutableListOf<PropertySpec>()

        override fun resolveClassName(name: String) = throw IllegalStateException("Extra file cannot contains types")

        override fun addType(typeSpec: TypeSpec, target: Target) =
            throw IllegalStateException("Extra file cannot contains types")

        override fun addProperty(propertySpec: PropertySpec, target: Target) {
            require(target == Target.Extra)
            properties += propertySpec
        }

        override fun addFunction(funSpec: FunSpec, target: Target) {
            require(target == Target.Extra)
            functions += funSpec
        }

        override fun addAnnotation(annotationSpec: AnnotationSpec, target: Target) {
            require(target == Target.Extra)
            builder.addAnnotation(annotationSpec)
        }

        override fun addAliasedImport(member: ClassName, `as`: String) {
            builder.addAliasedImport(member, `as`)
        }

        override fun addAliasedImport(member: MemberName, `as`: String) {
            builder.addAliasedImport(member, `as`)
        }

        override fun addImport(packageName: String, target: Target, vararg names: String) {
            require(target == Target.Extra)
            builder.addImport(packageName, *names)
        }

        override fun toFileSpecs(): Collection<FileSpec> {
            properties.sortedWith(propertySpecComparator).forEach(builder::addProperty)
            properties.clear()

            functions.sortedWith(funSpecComparator).forEach(builder::addFunction)
            functions.clear()

            if (useSerializers.isNotEmpty()) {
                builder.addAnnotation(
                    AnnotationSpec.builder(ClassName("kotlinx.serialization", "UseSerializers"))
                        .addMember("%L", useSerializers.map { CodeBlock.of("%T::class", it) }.joinToCode())
                        .build()
                )
            }

            if (optIn.isNotEmpty()) {
                builder.addAnnotation(
                    AnnotationSpec.builder(ClassName("kotlin", "OptIn"))
                        .addMember("%L", optIn.map { CodeBlock.of("%T::class", it) }.joinToCode())
                        .build()
                )
            }

            return setOf(builder.build())
        }

        private val useSerializers: MutableSet<ClassName> = mutableSetOf()

        override fun addUseSerializers(serializer: ClassName) {
            useSerializers += serializer
        }

        private val optIn: MutableSet<ClassName> = mutableSetOf()

        override fun addOptIn(optIn: ClassName, target: Target) {
            when (target) {
                Target.Extra, Target.Default -> this.optIn += optIn
                else -> throw UnsupportedOperationException()
            }
        }
    }

    class InnerClass(private val builder: TypeSpec.Builder, val typeName: ClassName, val parent: PoetSink) :
        PoetSink() {
        override val isRoot: Boolean get() = false

        override fun resolveClassName(name: String) = typeName.nestedClass(name)

        override fun addType(typeSpec: TypeSpec, target: Target) {
            when (target) {
                Target.TopLevel, Target.Extra, Target.ExternalFile -> parent.addType(typeSpec, target)
                Target.Default -> builder.addType(typeSpec)
            }
        }

        override fun addProperty(propertySpec: PropertySpec, target: Target) {
            when (target) {
                Target.TopLevel, Target.Extra, Target.ExternalFile -> parent.addProperty(propertySpec, target)
                Target.Default -> builder.addProperty(propertySpec)
            }
        }

        override fun addFunction(funSpec: FunSpec, target: Target) {
            when (target) {
                Target.TopLevel, Target.Extra -> parent.addFunction(funSpec, target)
                Target.ExternalFile -> throw UnsupportedOperationException()
                Target.Default -> builder.addFunction(funSpec)
            }
        }

        override fun addAnnotation(annotationSpec: AnnotationSpec, target: Target) {
            when (target) {
                Target.TopLevel, Target.Extra -> parent.addAnnotation(annotationSpec, target)
                Target.ExternalFile -> throw UnsupportedOperationException()
                Target.Default -> builder.addAnnotation(annotationSpec)
            }
        }

        override fun addAliasedImport(member: ClassName, `as`: String) {
            parent.addAliasedImport(member, `as`)
        }

        override fun addAliasedImport(member: MemberName, `as`: String) {
            parent.addAliasedImport(member, `as`)
        }

        override fun addImport(packageName: String, target: Target, vararg names: String) {
            parent.addImport(packageName, target, *names)
        }

        override fun addUseSerializers(serializer: ClassName) {
            parent.addUseSerializers(serializer)
        }

        override fun addOptIn(optIn: ClassName, target: Target) {
            parent.addOptIn(optIn, target)
        }

        override fun toFileSpecs() = throw UnsupportedOperationException()
    }
}

fun PoetSink.writeTo(path: Path) {
    toFileSpecs().forEach { it.writeTo(path) }
}
