/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.codegen.poet

import com.squareup.kotlinpoet.*
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import kiscord.codegen.*

class TupleSerializerPoet(private val arity: Int) : ClassPoet {
    override val selfType = ClassName("kiscord.serialization", "Tuple${arity}Serializer")
    override val category: String? get() = null

    override fun build(snap: PoetSnap, sink: PoetSink): TypeSpec {
        val resultType = TypeVariableName("R")

        val builder = TypeSpec.classBuilder(selfType)
            .addAnnotation(GENERATED)
            .addAnnotation(KISCORD_UNSTABLE_API)
            .addModifiers(KModifier.ABSTRACT)
            .superclass(ABSTRACT_TUPLE_SERIALIZER.parameterizedBy(resultType))
            .addSuperclassConstructorParameter("name")
            .addTypeVariable(resultType)
            .addKdoc("@suppress")

        val constructor = FunSpec.constructorBuilder()
            .addParameter("name", STRING)

        val toResult = FunSpec.builder("toResult")
            .addModifiers(KModifier.PROTECTED, KModifier.ABSTRACT)
            .returns(resultType)

        val serializeFun = FunSpec.builder("serialize")
            .addModifiers(KModifier.OVERRIDE, KModifier.FINAL)
            .addParameter("encoder", ENCODER)
            .addParameter("value", resultType)
            .addStatement("val output = encoder.beginCollection(descriptor, %L)", arity)

        val deserializeFun = FunSpec.builder("deserialize")
            .addModifiers(KModifier.OVERRIDE, KModifier.FINAL)
            .addParameter("decoder", DECODER)
            .returns(resultType)
            .addStatement("val input = decoder.beginStructure(descriptor)")
            .addStatement("val size = input.decodeCollectionSize(descriptor)")
            .beginControlFlow("require(size == -1 || size == %L)", arity)
            .addStatement("%P", "Invalid tuple size: expected $arity, got \$size")
            .endControlFlow()

        val deserializeResultArgs = mutableListOf<CodeBlock>()

        repeat(arity) { i ->
            val type = TypeVariableName("T$i")
            val serializerType = KSERIALIZER.parameterizedBy(type)
            val serializerName = "serializer$i"
            val valueName = "value$i"

            builder.addTypeVariable(type)
                .addSuperclassConstructorParameter(serializerName)
                .addProperty(
                    PropertySpec.builder(serializerName, serializerType)
                        .addModifiers(KModifier.PRIVATE)
                        .initializer(serializerName)
                        .build()
                )
            constructor.addParameter(ParameterSpec.builder(serializerName, serializerType).build())
            toResult.addParameter(valueName, type)

            builder.addProperty(
                PropertySpec.builder(valueName, type)
                    .addModifiers(KModifier.PROTECTED, KModifier.ABSTRACT)
                    .receiver(resultType)
                    .build()
            )

            serializeFun.addStatement(
                "output.encodeSerializableElement(descriptor, %L, %L, value.%L)",
                i, serializerName, valueName
            )

            deserializeFun.addStatement(
                "val %L = input.decodeTupleElement(descriptor, %L, %L)",
                valueName, i, serializerName
            )

            deserializeResultArgs += CodeBlock.of("%L", valueName)
        }

        serializeFun.addStatement("output.endStructure(descriptor)")

        deserializeFun.addStatement("input.endStructure(descriptor)")
            .addStatement("return toResult(%L)", deserializeResultArgs.joinToCode())

        builder.primaryConstructor(constructor.build())
            .addFunction(toResult.build())
            .addFunction(serializeFun.build())
            .addFunction(deserializeFun.build())

        return builder.build()
    }

    companion object {
        private val poets: MutableMap<Int, TupleSerializerPoet> = mutableMapOf()
        operator fun get(arity: Int): TupleSerializerPoet = poets.computeIfAbsent(arity, ::TupleSerializerPoet)

        fun processPoets(snap: PoetSnap, sink: PoetSink) {
            poets.values.forEach { poet ->
                sink.addType(poet.build(snap, sink))
            }
        }
    }
}
