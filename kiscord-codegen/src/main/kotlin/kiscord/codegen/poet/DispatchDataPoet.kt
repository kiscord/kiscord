/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.codegen.poet

import com.fasterxml.jackson.module.kotlin.*
import com.squareup.kotlinpoet.*
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import io.swagger.v3.oas.models.media.*
import kiscord.codegen.*

interface DispatchDataPoet : PayloadDataPoet {
    override fun mapping(snap: PoetSnap): Map<EnumPoet.EnumValue, TypeName?> {
        val eventNamePoet = snap.resolvePoet(eventNameType) as GatewayEventNamePoet
        return eventNamePoet.mapping(snap)
    }

    override fun eventType(snap: PoetSnap): ClassName {
        return ClassName(GatewayEventPoet.PACKAGE, "${eventName(snap)}Event")
    }

    fun eventName(snap: PoetSnap): String {
        return mapping(snap).entries.single { it.value == selfType }.key.identifier
    }

    override fun populateEvent(snap: PoetSnap, builder: TypeSpec.Builder) {
        val eventName =
            CodeBlock.of("%T.%L", EVENT_NAME, mapping(snap).entries.single { it.value == selfType }.key.identifier)

        builder.addSuperinterface(GATEWAY_DISPATCH_EVENT.parameterizedBy(getDataType(snap)))
            .addKdoc("Event for [%L] with %L payload", eventName, getDataType(snap).kdocTag)
            .addProperty(
                PropertySpec.builder("eventName", EVENT_NAME)
                    .addModifiers(KModifier.OVERRIDE)
                    .getter(
                        FunSpec.getterBuilder().addStatement("return %L", eventName).build()
                    )
                    .build()
            )
    }

    override fun getCatchFunName(snap: PoetSnap): String {
        val eventName = mapping(snap).entries.single { it.value == selfType }.key.identifier
        return "on$eventName"
    }

    override fun getDataType(snap: PoetSnap): TypeName {
        return schema.extensions["x-kiscord-codegen-event-data"]?.let {
            val dataSchema = codegenMapper.convertValue<Schema<*>>(it)
            snap.resolveType(dataSchema)
        } ?: super.getDataType(snap)
    }

    class Object(
        schema: Schema<Any?>, selfName: ClassName, metadata: PoetSnap.PoetResolveMetadata
    ) : ObjectPoet<Schema<Any?>>(schema, selfName, metadata), DispatchDataPoet {
        override fun process(snap: PoetSnap, sink: PoetSink, builder: TypeSpec.Builder) {
            super.process(snap, sink, builder)
            builder.generatePayload(snap, selfType, this)
            if (isSingleton(snap)) {
                builder.addFunction(
                    FunSpec.builder("toString")
                        .addModifiers(KModifier.OVERRIDE)
                        .returns(STRING)
                        .addStatement("return %S", selfType.simpleName)
                        .build()
                )
            }
        }
    }

    class Delegate(schema: ComposedSchema, selfType: ClassName, private val forUpdate: Boolean = false) :
        SchemaPoet<Any, ComposedSchema>(schema, selfType),
        DispatchDataPoet {
        override fun createBuilder(snap: PoetSnap) = TypeSpec.classBuilder(selfType).addAnnotation(GENERATED)

        override fun process(snap: PoetSnap, sink: PoetSink, builder: TypeSpec.Builder) {
            val childSchema = snap.resolve(schema.oneOf.single())
            var childType = snap.resolveType(childSchema) ?: throw IllegalStateException()
            val childPoet = snap.resolvePoet(childType as ClassName, childSchema, includeSpec = true)
            if (forUpdate && childPoet is ObjectPoet<*>) {
                childType = childPoet.updateDataType
            }

            builder
                .primaryConstructor(
                    FunSpec.constructorBuilder()
                        .addParameter("value", childType)
                        .build()
                )
                .addProperty(
                    PropertySpec.builder("value", childType)
                        .initializer("value")
                        .build()
                )
                .addFunction(FunSpec.builder("toString")
                    .addModifiers(KModifier.OVERRIDE)
                    .returns(STRING)
                    .addStatement("return %P", "${selfType.simpleName}(value=\$value)")
                    .build())

            val serializer = snap.resolvePoet(childType, childSchema, includeSpec = false).serializerInstance(childType)

            val deSer = TypeSpec.objectBuilder("Serializer")
                .addModifiers(KModifier.INTERNAL)
                .addAnnotation(GENERATED)
                .addSuperinterface(KSERIALIZER.parameterizedBy(selfType))
                .addProperty(
                    PropertySpec.builder("serializer", KSERIALIZER.parameterizedBy(childType))
                        .addModifiers(KModifier.PRIVATE)
                        .initializer(serializer)
                        .build()
                )
                .addProperty(
                    PropertySpec.builder("descriptor", SERIAL_DESCRIPTOR)
                        .addModifiers(KModifier.OVERRIDE)
                        .initializer(
                            "serializer.descriptor.%M(%S)",
                            WITH_SERIAL_NAME,
                            selfType.toString()
                        )
                        .build()
                )
                .addFunction(
                    FunSpec.builder("serialize")
                        .addModifiers(KModifier.OVERRIDE)
                        .addParameter("encoder", ENCODER)
                        .addParameter("value", selfType)
                        .addStatement("serializer.serialize(encoder, value.value)")
                        .build()
                )
                .addFunction(
                    FunSpec.builder("deserialize")
                        .addModifiers(KModifier.OVERRIDE)
                        .addParameter("decoder", DECODER)
                        .returns(selfType)
                        .addStatement("return %T(serializer.deserialize(decoder))", selfType)
                        .build()
                )
            builder.addType(deSer.build())

            builder.addAnnotation(
                AnnotationSpec.builder(SERIALIZABLE)
                    .addMember("with = %T::class", selfType.nestedClass("Serializer"))
                    .build()
            )

            sink.addOptIn(INTERNAL_SERIALIZATION_API)

            builder.generatePayload(snap, selfType, this)
        }
    }

    companion object {
        val eventNameType = ClassName("kiscord.api.gateway", "EventName")
    }
}

private fun <T> TypeSpec.Builder.generatePayload(
    snap: PoetSnap,
    selfType: ClassName,
    poet: T
) where T : DispatchDataPoet, T : SchemaPoet<*, *> {
    val eventNamePoet = snap.resolvePoet(DispatchDataPoet.eventNameType) as GatewayEventNamePoet
    val eventName = eventNamePoet.mapping(snap).entries.single { it.value == selfType }.key.identifier
    val dataType = poet.getDataType(snap)

    addSuperinterface(GATEWAY_DISPATCH_DATA)
    addProperty(
        PropertySpec.builder("eventName", EVENT_NAME)
            .addModifiers(KModifier.OVERRIDE)
            .getter(
                FunSpec.getterBuilder()
                    .addStatement("return %T", EVENT_NAME.nestedClass(eventName))
                    .build()
            )
            .build()
    )

    val passthrough = poet.schema.extensions["x-kiscord-codegen-dispatch-passthrough"] == true

    val dispatch = FunSpec.builder("dispatch")
        .addModifiers(KModifier.INTERNAL)

    if (passthrough) {
        dispatch.returns(dataType)
        when (poet) {
            is DispatchDataPoet.Delegate -> dispatch.addStatement("return value")
            is DispatchDataPoet.Object -> dispatch.addStatement("return this")
        }
    } else {
        dispatch.addModifiers(KModifier.SUSPEND, KModifier.INLINE)
            .addParameter("context", PIPELINE_CONTEXT.parameterizedBy(STAR, GATEWAY_CALL))
            .returns(dataType.copy(nullable = true))
            .addStatement("return context.%M(this)", HANDLE_PAYLOAD)
            .addAnnotation(
                AnnotationSpec.builder(Suppress::class)
                    .addMember("%S", "RedundantNullableReturnType")
                    .addMember("%S", "RedundantSuppression")
                    .build()
            )
    }

    addFunction(dispatch.build())
}

