/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.codegen.poet

import com.squareup.kotlinpoet.*
import io.swagger.v3.oas.models.media.*

class SealedPoet(
    schema: ComposedSchema, selfName: ClassName, metadata: PoetSnap.PoetResolveMetadata
) : ObjectPoet<ComposedSchema>(schema, selfName, metadata) {
    override fun isSingleton(snap: PoetSnap): Boolean = false
    override fun isSealedParent(snap: PoetSnap): Boolean = true

    override fun createBuilder(snap: PoetSnap): TypeSpec.Builder =
        super.createBuilder(snap).addModifiers(KModifier.SEALED)

    override fun resolveMembers(snap: PoetSnap): List<SealedMember> {
        val allMembers = schema.oneOf.map { snap.resolvePoet(it) }.filterIsInstance<ObjectPoet<*>>().map {
            it.resolveMembers(snap)
        }.map { members -> members.associate { it.name to it.schema } }
        val allProperties = allMembers.flatMapTo(mutableSetOf()) { it.keys }

        return allProperties.mapNotNull { name ->
            if (!allMembers.all { name in it.keys }) return@mapNotNull null
            val schema = allMembers.firstNotNullOf { it[name] }
            SealedMember(name, schema, snap)
        }
    }

    override fun createInnerPoets(snap: PoetSnap) {
        schema.oneOf.forEach { s ->
            snap.createPoetIfNeeded(s, PoetSnap.PoetResolveMetadata(sealedParent = selfType))
        }
    }

    override fun isRequiredMember(snap: PoetSnap, name: String): Boolean = true

    inner class SealedMember(name: String, schema: Schema<*>, snap: PoetSnap) :
        ObjectMember(name, schema, false, snap) {
        override val isReadOnly: Boolean get() = false

        override fun asPropertySpec(withSpec: Boolean, kind: Kind, snap: PoetSnap, sink: PoetSink): PropertySpec =
            super.asPropertySpec(withSpec, kind, snap, sink).toBuilder()
                .addModifiers(KModifier.ABSTRACT)
                .initializer(null).build()
    }

    class ChildPoet(
        schema: ObjectSchema, selfName: ClassName, metadata: PoetSnap.PoetResolveMetadata
    ) : ObjectPoet<ObjectSchema>(schema, selfName, metadata) {
        override fun createBuilder(snap: PoetSnap): TypeSpec.Builder {
            return super.createBuilder(snap).superclass(metadata.sealedParent!!)
        }

        override fun resolveParents(snap: PoetSnap, includeFake: Boolean): Map<Schema<*>, TypePoet?> {
            val poet = snap.resolvePoet(metadata.sealedParent!!) as SealedPoet
            return mapOf(poet.schema to poet)
        }
    }
}
