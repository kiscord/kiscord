/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.codegen.poet

import com.fasterxml.jackson.databind.node.*
import com.squareup.kotlinpoet.*
import kiscord.codegen.*

class ValueResolver(private val snap: PoetSnap) {
    private fun <T> resolveCollection(
        kind: ObjectPoet.Kind,
        type: ParameterizedTypeName,
        value: Any?,
        block: (Boolean, TypeName, Collection<CodeBlock>) -> T
    ): T {
        var elementType = type.typeArguments.single()
        require(elementType is ClassName)
        val elementPoet = snap.resolvePoet(elementType)
        val mutable = kind == ObjectPoet.Kind.Builder
        if (mutable && elementPoet is BuildablePoet && elementPoet.withBuilder) {
            elementType = elementPoet.builderType
        }
        if (elementPoet is ManyResolver) {
            return block(mutable, elementType, elementPoet.resolveValues(value))
        } else if (value is Collection<*>) {
            return block(mutable, elementType, value.map { resolve(elementType, it) })
        } else if (value is ArrayNode && value.isEmpty) {
            return block(mutable, elementType, emptySet())
        }
        throw IllegalStateException("Don't know how to resolve element value $value :(")
    }

    fun resolve(type: TypeName, value: Any?, kind: ObjectPoet.Kind = ObjectPoet.Kind.Value): CodeBlock {
        if (type.isNullable && value == null) return CodeBlock.of("null")
        return resolveInternal(type.copy(nullable = false), value, kind)
    }

    private fun resolveInternal(type: TypeName, value: Any?, kind: ObjectPoet.Kind): CodeBlock = when (type) {
        STRING -> when (value) {
            "%LIBRARY%", "%BROWSER%", "%DEVICE%" -> CodeBlock.of("%T.NAME", KISCORD)
            "%OS%" -> CodeBlock.of("%T.OS_NAME", KISCORD)
            else -> CodeBlock.of("%S", value as String?)
        }
        CHAR -> when (value as Char?) {
            is Char -> CodeBlock.of("'%L'", value)
            null -> CodeBlock.of("null")
            else -> throw IllegalArgumentException("wtf")
        }
        DOUBLE -> CodeBlock.of("%L", value as Double?)
        FLOAT -> CodeBlock.of("%Lf", value as Float?)
        LONG -> CodeBlock.of("%LL", value as Long?)
        INT -> CodeBlock.of("%L", value as Int?)
        SHORT -> CodeBlock.of("%L", value as Short?)
        BYTE -> CodeBlock.of("%L", value as Byte?)
        BOOLEAN -> CodeBlock.of("%L", value as Boolean?)
        INSTANT -> when(value) {
            "%NOW%" -> CodeBlock.of("%T.now()", ClassName("kotlinx.datetime", "Clock", "System"))
            else -> CodeBlock.of("%T.parse(%S)", INSTANT, value as String?)
        }
        is ParameterizedTypeName -> when (type.rawType) {
            SET -> if (value == "%ALL%") CodeBlock.of("%T.ALL", type.typeArguments.single())
            else resolveCollection(kind, type, value) { mutable, elementType, values ->
                if (mutable) {
                    CodeBlock.of("%M<%T>(%L)", MUTABLE_SET_OF, elementType, values.joinToCode())
                } else if (values.isEmpty()) {
                    CodeBlock.of("%M<%T>()", EMPTY_SET, elementType)
                } else {
                    CodeBlock.of("%M<%T>(%L)", SET_OF, elementType, values.joinToCode())
                }
            }
            MUTABLE_SET -> resolveCollection(kind, type, value) { _, elementType, values ->
                CodeBlock.of("%M<%T>(%L)", MUTABLE_SET_OF, elementType, values.joinToCode())
            }
            LIST -> resolveCollection(kind, type, value) { mutable, elementType, values ->
                if (mutable) {
                    CodeBlock.of("%M<%T>(%L)", MUTABLE_LIST_OF, elementType, values.joinToCode())
                } else if (values.isEmpty()) {
                    CodeBlock.of("%M<%T>()", EMPTY_LIST, elementType)
                } else {
                    CodeBlock.of("%M<%T>(%L)", LIST_OF, elementType, values.joinToCode())
                }
            }
            MUTABLE_LIST -> resolveCollection(kind, type, value) { _, elementType, values ->
                CodeBlock.of("%M<%T>(%L)", MUTABLE_LIST_OF, elementType, values.joinToCode())
            }
            BOX -> CodeBlock.of("%T.of(%L)", BOX, resolve(type.typeArguments[0], value, kind))
            else -> throw IllegalArgumentException("Unable to resolve value for type $type")
        }
        else -> {
            val poet = snap.resolvePoet(type.copy(nullable = false))
            require(poet is MonoResolver) {
                "Poet $poet doesn't implement MonoResolver"
            }
            poet.resolveValue(value)
        }
    }

    companion object {
        private val EMPTY_SET = MemberName("kotlin.collections", "emptySet")
        private val SET_OF = MemberName("kotlin.collections", "setOf")
        private val MUTABLE_SET_OF = MemberName("kotlin.collections", "mutableSetOf")
        private val EMPTY_LIST = MemberName("kotlin.collections", "emptyList")
        private val LIST_OF = MemberName("kotlin.collections", "listOf")
        private val MUTABLE_LIST_OF = MemberName("kotlin.collections", "mutableListOf")
    }

    interface MonoResolver {
        fun resolveValue(value: Any?): CodeBlock
    }

    interface ManyResolver {
        fun resolveValues(value: Any?): Collection<CodeBlock>
    }
}
