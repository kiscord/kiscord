/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.codegen.poet

import com.squareup.kotlinpoet.*
import io.swagger.v3.oas.models.media.*
import kiscord.codegen.*

open class ValueClassPoet<out T>(schema: Schema<out T>, selfType: ClassName) :
    SchemaPoet<T, Schema<out T>>(schema, selfType) {
    val serializationPolicy = schema.codegenSerializationPolicy ?: SerializationPolicy()

    override fun createBuilder(snap: PoetSnap): TypeSpec.Builder =
        TypeSpec.classBuilder(selfType).addModifiers(KModifier.VALUE).addAnnotation(GENERATED)

    override fun process(snap: PoetSnap, sink: PoetSink, builder: TypeSpec.Builder) {
        var valueType: TypeName = serializationPolicy.valueTypeData?.let { snap.resolveType(it) } ?: when (schema) {
            is IntegerSchema -> {
                when (val format = schema.format) {
                    "int64" -> LONG
                    "int32", null -> INT
                    else -> throw IllegalArgumentException("Unknown format for integer: $format")
                }
            }
            is BooleanSchema -> BOOLEAN
            else -> throw IllegalArgumentException("Unknown schema: $schema")
        }

        if (schema.nullable == true) {
            valueType = valueType.copy(nullable = true)
        }

        val valueTypeName = serializationPolicy.valueTypeName ?: "value"

        builder
            .addProperty(
                PropertySpec.builder(valueTypeName, valueType)
                    .initializer(valueTypeName).build()
            )
            .primaryConstructor(
                FunSpec.constructorBuilder()
                    .addParameter(
                        ParameterSpec.builder(valueTypeName, valueType)
                            .build()
                    ).build()
            )
            .addAnnotation(SERIALIZABLE)
            .addAnnotation(JVM_INLINE)
    }
}
