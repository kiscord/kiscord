/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.codegen.poet

import com.squareup.kotlinpoet.*
import io.swagger.v3.oas.models.media.*

class TuplePoet(
    schema: ArraySchema, selfName: ClassName, metadata: PoetSnap.PoetResolveMetadata
) : ObjectPoet<ArraySchema>(schema, selfName, metadata) {
    init {
        check(schema.minItems == schema.maxItems) { "Inconsistent tuple" }
    }

    override fun isSingleton(snap: PoetSnap): Boolean = false
    override val isTuple: Boolean get() = true

    override fun resolveMembers(snap: PoetSnap): List<ObjectMember> {
        val tupleValues = schema.codegenTupleValues ?: throw IllegalStateException("Missing elements")
        val generalSchema = snap.resolve(schema.items)

        check(schema.minItems == tupleValues.size) { "Inconsistent tuple" }
        return tupleValues.mapIndexed { i, data ->
            val valueSchema = ComposedSchema()
            valueSchema.addOneOfItem(generalSchema)
            data.description?.let { valueSchema.description = it }
            ObjectMember(data.name ?: "value$i", valueSchema, false, snap)
        }
    }

    override fun isRequiredMember(snap: PoetSnap, name: String) = true

    data class TupleValue(
        val name: String? = null,
        val description: String? = null
    )

    override fun createInnerPoets(snap: PoetSnap) {
        val members = resolveMembers(snap)
        val arity = members.size
        TupleSerializerPoet[arity]
    }
}
