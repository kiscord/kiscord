/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.codegen.poet

import com.squareup.kotlinpoet.*
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import kiscord.codegen.*
import kotlin.experimental.*

object GatewayEventPoet {
    const val PACKAGE = "kiscord.gateway.events"
    const val DEFINITION = "Definition"

    @OptIn(ExperimentalTypeInference::class)
    fun build(snap: PoetSnap): List<PoetSink> {
        fun generateEventHolder(poet: PayloadDataPoet): PoetSink {
            val dataType = poet.getDataType(snap)
            val eventType = poet.eventType(snap)

            val sink = PoetSink.RootFile(eventType)

            val safeBuild = FunSpec.builder("safeBuild")
                .addModifiers(KModifier.OVERRIDE, KModifier.INTERNAL)
                .addParameter("data", ANY.copy(nullable = true))
                .returns(eventType.copy(nullable = true))
                .addStatement("val refinedData = data as? %T ?: return null", dataType)
                .addStatement("return %T(refinedData)", eventType)

            if (dataType is ParameterizedTypeName) {
                safeBuild.addAnnotation(
                    AnnotationSpec.builder(ClassName("kotlin", "Suppress"))
                        .addMember("%S", "UNCHECKED_CAST")
                        .build()
                )
            }

            val builder = TypeSpec.classBuilder(eventType)
                .addAnnotation(GENERATED)
                .primaryConstructor(
                    FunSpec.constructorBuilder()
                        .addModifiers(KModifier.INTERNAL)
                        .addParameter("data", dataType)
                        .build()
                )
                .addProperty(
                    PropertySpec.builder("data", dataType)
                        .addModifiers(KModifier.OVERRIDE)
                        .initializer("data")
                        .build()
                )
                .addType(
                    TypeSpec.companionObjectBuilder(DEFINITION)
                        .superclass(GATEWAY_EVENT_DEFINITION.parameterizedBy(eventType, dataType))
                        .addFunction(safeBuild.build())
                        .build()
                )
                .addFunction(FunSpec.builder("hashCode")
                    .addModifiers(KModifier.OVERRIDE)
                    .returns(INT)
                    .addStatement("return data.hashCode()")
                    .build())
                .addFunction(FunSpec.builder("equals")
                    .addModifiers(KModifier.OVERRIDE)
                    .addParameter("other", ANY.copy(nullable = true))
                    .returns(BOOLEAN)
                    .addStatement("return other is %T && data == other.data", eventType)
                    .build())
                .addFunction(FunSpec.builder("toString")
                    .addModifiers(KModifier.OVERRIDE)
                    .returns(STRING)
                    .addStatement("return %P", "${eventType.simpleName}(data=\$data)")
                    .build())

            poet.populateEvent(snap, builder)

            sink.addType(builder.build())

            val catchFun = FunSpec.builder(poet.getCatchFunName(snap))
                .addKdoc(
                    "Listen for %L and call [handler] with data of type %L\n\n",
                    eventType.kdocTag,
                    dataType.kdocTag
                )
                .addKdoc("@param concurrent if true execute handler in separate coroutine for each event\n")
                .addKdoc("@param handler Event handler for data of type %L\n", dataType.kdocTag)
                .addKdoc("@see %L\n", eventType.kdocTag(wraparound = false))
                .addAnnotation(GENERATED)
                .addAnnotation(GATEWAY_EVENT_DSL)
                .receiver(GATEWAY_EVENT_SINK)
                .returns(DISPOSABLE_HANDLE)
                .addParameter(ParameterSpec.builder("concurrent", BOOLEAN).defaultValue("false").build())
                .addParameter("handler", GATEWAY_EVENT_HANDLER.parameterizedBy(eventType, dataType))
                .addStatement("return subscribe(%T, concurrent, handler)", eventType)

            if (dataType is ClassName) catchFun.addKdoc("@see %L\n", dataType.kdocTag(wraparound = false))

            sink.addFunction(catchFun.build())

            return sink
        }

        val payloads = snap.resolvePayloadData()

        val events = payloads.map { poet ->
            generateEventHolder(poet)
        }

        val definitionBody = CodeBlock.builder()
            .beginControlFlow("return when(this)")

        payloads.filterIsInstance<DispatchDataPoet>().forEach { poet ->
            definitionBody.addStatement("%T.%L -> %T", EVENT_NAME, poet.eventName(snap), poet.eventType(snap))
        }

        definitionBody.addStatement("else -> null").endControlFlow()

        val definitionMapperPoet = PoetSink.RootFile(ClassName(PACKAGE, "EventDefinitionMapper"))

        definitionMapperPoet.addProperty(
            PropertySpec.builder(
                "definition",
                GATEWAY_EVENT_DEFINITION.parameterizedBy(STAR, STAR).copy(nullable = true)
            )
                .addModifiers(KModifier.INTERNAL)
                .addAnnotation(GENERATED)
                .receiver(EVENT_NAME)
                .getter(
                    FunSpec.getterBuilder()
                        .addCode(definitionBody.build())
                        .build()
                )
                .build()
        )

        return events + definitionMapperPoet
    }
}
