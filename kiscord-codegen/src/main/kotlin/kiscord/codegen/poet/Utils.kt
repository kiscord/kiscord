/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

@file:Suppress("UNCHECKED_CAST")

package kiscord.codegen.poet

import com.fasterxml.jackson.module.kotlin.*
import com.squareup.kotlinpoet.*
import io.ktor.http.*
import io.swagger.v3.oas.models.*
import io.swagger.v3.oas.models.media.*
import io.swagger.v3.oas.models.parameters.*
import kiscord.codegen.*

val ClassName.isTopLevel: Boolean
    get() = simpleNames.size == 1

fun String.toPascalCase(vararg delimiters: Char) =
    split(*delimiters).joinToString("") { it.replaceFirstChar { c -> c.titlecase() } }

fun String.toCamelCase(vararg delimiters: Char) = toPascalCase(*delimiters).replaceFirstChar { it.lowercase() }
fun String.toKiscordCase() = toCamelCase('_', '-', '.')

private val nullableSerializer = MemberName("kotlinx.serialization.builtins", "nullable")

fun TypePoet.serializerInstance(
    type: TypeName
): CodeBlock {
    if (type.isNullable) {
        return CodeBlock.of("%L.%M", serializerInstance(type.copy(nullable = false)), nullableSerializer)
    }

    return when (val t = serializerTypeFor(selfType)) {
        null -> serializerInstanceForSelf()
        else -> {
            val metadata = t.tag() ?: SerializerMetadata()
            if (metadata.isDefaultSerializer) {
                serializerInstanceForSelf()
            } else if (metadata.isObject) {
                CodeBlock.of("%T", t)
            } else {
                CodeBlock.of("%T()", t)
            }
        }
    }
}

data class SerializerMetadata (
    val isObject: Boolean = false,
    val isDefaultSerializer: Boolean = false,
)

val Schema<*>.codegenCategory get() = extensions?.get("x-kiscord-codegen-category") as String?
val Schema<*>.codegenExternal get() = extensions?.get("x-kiscord-codegen-external") as Boolean?
val Operation.codegenExternal get() = extensions?.get("x-kiscord-codegen-external") as Boolean?
val Schema<*>.codegenCustomUrlEncoder get() = extensions?.get("x-kiscord-codegen-custom-url-encoder") as Boolean?
val Schema<*>.codegenPackage get() = extensions?.get("x-kiscord-codegen-package") as String?
val Schema<*>.codegenName get() = extensions?.get("x-kiscord-codegen-name") as String?
val Schema<*>.codegenGenerateSpec get() = extensions?.get("x-kiscord-codegen-generate-spec") as Boolean?
val Schema<*>.codegenGenerateBuilder get() = extensions?.get("x-kiscord-codegen-generate-builder") as Boolean?
val Schema<*>.codegenTupleValues
    get() = extensions?.get("x-kiscord-codegen-tuple-values")
        ?.let { codegenMapper.convertValue<List<TuplePoet.TupleValue>>(it) }
val Schema<*>.codegenKind get() = extensions?.get("x-kiscord-codegen-kind") as String?
val Schema<*>.codegenSensitiveData get() = extensions?.get("x-kiscord-codegen-sensitive-data") as Boolean?
val <T> Schema<out T>.codegenDefault get() = extensions?.get("x-kiscord-codegen-default") as T?
val <T> Schema<out T>.codegenBuilderDefault get() = extensions?.get("x-kiscord-codegen-builder-default") as T?
val Parameter.codegenSchema get() = extensions?.get("x-kiscord-codegen-parameter-schema") as Schema<*>? ?: schema
val Schema<*>.codegenRef
    get() = extensions?.get("x-kiscord-codegen-ref") as String?
val Schema<*>.codegenStructureHacks
    get() = extensions?.get("x-kiscord-codegen-structure-hacks")
        ?.let { codegenMapper.convertValue<List<String>>(it) }

val Schema<*>.codegenGenerateUpdateSerializer
    get() = extensions?.get("x-kiscord-codegen-generate-update-serializer") as Boolean?

val Schema<*>.enumReference
    get() = extensions?.get("x-enum-reference")?.let {
        codegenMapper.convertValue<Schema<*>>(it)
    }

val Schema<*>.enumValues
    get() = extensions?.get("x-enum-values")?.let {
        codegenMapper.convertValue<List<EnumPoet.EnumValue>>(it)
    }
val Schema<*>.enumIsBitmask get() = extensions?.get("x-enum-is-bitmask") as Boolean?
val Schema<*>.enumExtraData
    get() = extensions?.get("x-enum-extra-data")?.let {
        codegenMapper.convertValue<Map<String, Schema<*>>>(it)
    }
val Schema<*>.codegenSerializationPolicy
    get() = extensions?.get("x-kiscord-codegen-serialization-policy")
        ?.let { codegenMapper.convertValue<SerializationPolicy>(it) }

val Schema<*>.codegenTransientProperties
    get() = extensions?.get("x-kiscord-codegen-transient-properties")
        ?.let { codegenMapper.convertValue<Map<String, Schema<*>>>(it) }

val Schema<*>.deprecationMessage
    get() = extensions?.get("x-deprecation-message") as String?

fun ExternalDocumentation.writeTo(builder: CodeBlock.Builder) {
    builder.add("[${description.replace(' ', '·')}](%L)\n", url)
}

fun ExternalDocumentation.writeTo(builder: TypeSpec.Builder) {
    builder.addKdoc(buildCodeBlock { writeTo(this) })
}

fun ExternalDocumentation.writeTo(builder: PropertySpec.Builder) {
    builder.addKdoc(buildCodeBlock { writeTo(this) })
}

fun ExternalDocumentation.writeTo(builder: FunSpec.Builder) {
    builder.addKdoc(buildCodeBlock { writeTo(this) })
}

private val primitiveTypes = setOf(INT, LONG, SHORT, CHAR, BYTE, STRING, FLOAT, DOUBLE, BOOLEAN)

val TypeName.isPrimitive: Boolean get() = this in primitiveTypes

inline fun <T : TypeName, reified R : Any> T.withTag(tag: R): T {
    val tags = tags.toMutableMap()
    tags[tag::class] = tag
    return copy(tags = tags) as T
}

fun ContentType.toCodeBlock(): CodeBlock = buildCodeBlock {
    val primary = contentType.replaceFirstChar { it.titlecase() }
    var secondary = contentSubtype.replaceFirstChar { it.titlecase() }
    if (match(ContentType.Image.Any)) {
        secondary = secondary.uppercase()
    }

    add("%T.%L", CONTENT_TYPE.nestedClass(primary), secondary)
    parameters.forEach { (name, value) ->
        add("\n.withParameter(%S, %S)", name, value)
    }
}

data class Plurals(
    val singular: String? = null
)

val Schema<*>.codegenPlurals
    get() = extensions?.get("x-kiscord-codegen-plurals")
        ?.let { codegenMapper.convertValue<Plurals>(it) }

val TypeName.kdocTag: CodeBlock
    get() = kdocTag(wraparound = true)

fun TypeName.kdocTag(wraparound: Boolean): CodeBlock = when (this) {
    is ClassName, is TypeVariableName -> if (wraparound) CodeBlock.of("[%T]", this) else CodeBlock.of("%T", this)
    is ParameterizedTypeName -> CodeBlock.of(
        "[%T]<%L>",
        rawType,
        typeArguments.map { it.kdocTag }.joinToCode(separator = ",·")
    )

    is LambdaTypeName -> {
        val builder = CodeBlock.builder()
        receiver?.let { receiver ->
            builder.add("%L.", receiver.kdocTag)
        }
        builder.add(parameters.map { parameter ->
            if (parameter.name.isBlank()) {
                parameter.type.kdocTag
            } else {
                CodeBlock.of("%L:·%L", parameter.name, parameter.type.kdocTag)
            }
        }.joinToCode(prefix = "(", separator = ",·", suffix = ")"))
        builder.add("·->·%L", returnType.kdocTag)
        builder.build()
    }

    is WildcardTypeName -> when {
        inTypes.size == 1 -> CodeBlock.of("in·%L", inTypes[0].kdocTag)
        outTypes == STAR.outTypes -> CodeBlock.of("*")
        else -> CodeBlock.of("out·%L", outTypes[0].kdocTag)
    }

    is Dynamic -> CodeBlock.of("dynamic")
}
