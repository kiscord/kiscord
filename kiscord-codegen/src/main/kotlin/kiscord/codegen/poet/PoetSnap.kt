/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.codegen.poet

import com.fasterxml.jackson.module.kotlin.*
import com.squareup.kotlinpoet.*
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import io.ktor.http.*
import io.swagger.v3.oas.models.*
import io.swagger.v3.oas.models.media.*
import io.swagger.v3.oas.models.parameters.*
import io.swagger.v3.oas.models.tags.*
import io.swagger.v3.parser.*
import io.swagger.v3.parser.util.*
import kiscord.codegen.*
import kiscord.codegen.poet.fake.*
import java.nio.file.*
import kotlin.reflect.*

class PoetSnap(val cache: ResolverCache) {
    private val poets = mutableSetOf<TypePoet>()
    private val operations = mutableSetOf<OperationPoet>()
    private val tags = mutableSetOf<Tag>()

    val valueResolver = ValueResolver(this)

    init {
        poets += PrimitiveFakePoet.validTypes.map(::PrimitiveFakePoet)
        poets += PrimitiveArrayFakePoet.validTypes.map(::PrimitiveArrayFakePoet)
        poets += BoxPoet
        poets += DurationSecondsPoet
        poets += FileDataFakePoet
        poets += GuildSpecPoet
        poets += InstantEpochMillisecondsPoet
        poets += InstantEpochSecondsPoet
        poets += InstantISOPoet
        poets += UrlPoet
    }

    fun addPoet(poet: TypePoet) {
        poets += poet
    }

    inline fun <reified T : Any> resolve(value: T, noinline refProvider: T.() -> String?): T {
        return resolve(T::class, value, refProvider)
    }

    fun <T : Any> resolve(type: KClass<T>, value: T, refProvider: T.() -> String?): T {
        val ref = refProvider(value)
        if (ref != null) {
            val refValue = cache.loadRef(ref, RefUtils.computeRefFormat(ref), type.java)
                ?: throw IllegalArgumentException("Invalid ref: $ref in $value")
            return resolve(type, refValue, refProvider)
        }
        return value
    }

    fun resolve(schema: Schema<*>): Schema<*> = resolve(schema) { `$ref` }
    fun resolve(requestBody: RequestBody): RequestBody = resolve(requestBody) { `$ref` }

    fun resolveType(schema: Schema<*>, forceNullable: Boolean = false): TypeName? {
        val resolved = resolve(schema)
        val type = resolveTypeInternal(resolved)
        if (forceNullable || resolved.nullable == true) {
            return type?.copy(nullable = true)
        }
        return type
    }

    private fun resolveTypeInternal(schema: Schema<*>): TypeName? {
        fun collectionType(collectionSchema: Schema<*>, itemSchema: Schema<*>): TypeName? {
            val itemType = resolveType(itemSchema) ?: return null
            if (collectionSchema.enumIsBitmask == true || collectionSchema.uniqueItems == true) {
                return SET.parameterizedBy(itemType)
            }
            return LIST.parameterizedBy(itemType)
        }

        schema.codegenClassName?.let { return it }

        if (schema.enumIsBitmask == true && schema.enumReference != null) {
            collectionType(schema, schema.enumReference!!)?.let { return it }
        }

        return when (schema) {
            is StringSchema -> STRING
            is IntegerSchema -> when (schema.format) {
                "int32", null -> INT
                "int64" -> LONG
                else -> null
            }
            is NumberSchema -> when (schema.format) {
                "float", null -> FLOAT
                "double" -> DOUBLE
                else -> null
            }
            is BooleanSchema -> BOOLEAN
            is ArraySchema -> collectionType(schema, schema.items)
            is ComposedSchema -> when {
                (schema.oneOf?.size ?: 0) == 1 && schema.allOf.isNullOrEmpty() && schema.anyOf.isNullOrEmpty() -> {
                    return resolveType(schema.oneOf.single(), forceNullable = schema.nullable == true)
                }
                else -> null
            }
            else -> schema.additionalProperties?.let { it ->
                val valueType = resolveType(codegenMapper.convertValue(it))
                val keyType = schema.extensions["x-kiscord-codegen-additional-property-key"]?.let {
                    resolveType(codegenMapper.convertValue(it))
                } ?: schema.propertyNames?.let { resolveType(it) } ?: STRING

                when {
                    valueType != null -> MAP.parameterizedBy(keyType, valueType)
                    else -> null
                }
            }
        }
    }

    fun resolvePoet(typeName: TypeName, schema: Schema<*>? = null, includeSpec: Boolean = false): TypePoet {
        check(!typeName.isNullable)
        poets.singleOrNull {
            it.selfType == typeName
                || (it is BuildablePoet && includeSpec && it.withSpec && it.specType == typeName)
                || (it is FakeTypeNamePoet && it.fakeSelfType == typeName)
        }?.let { return it }
        return when (typeName) {
            is ParameterizedTypeName -> when (typeName.rawType) {
                SET -> {
                    val itemType = typeName.typeArguments[0]
                    val itemPoet = resolvePoet(itemType)
                    if (schema != null && schema.enumIsBitmask == true && itemPoet is EnumPoet<*>) {
                        EnumSetFakePoet(schema, itemPoet)
                    } else {
                        CollectionFakePoet.OfSet(itemType, itemPoet)
                    }
                }
                LIST -> {
                    val itemType = typeName.typeArguments[0]
                    CollectionFakePoet.OfList(itemType, resolvePoet(itemType))
                }
                else -> FakePoet(typeName)
            }
            else -> FakePoet(typeName)
        }
    }

    fun resolvePoet(schema: Schema<*>): TypePoet? {
        val type = resolveType(schema)?.copy(nullable = false) ?: return null
        return resolvePoet(type, schema)
    }

    fun resolveDirectInners(parent: ClassPoet) = resolveDirectInners(parent.selfType)

    fun resolveDirectInners(typeName: ClassName): List<ClassPoet> {
        return poets.filterIsInstance<ClassPoet>().filter {
            it.selfType.isChildOf(typeName)
        }.sortedBy { it.selfType }
    }

    fun resolveOperations(tag: Tag): Set<OperationPoet> {
        return operations.filterTo(mutableSetOf()) { operation ->
            val tags = operation.operation.tags ?: error("empty tags on operation $operation not allowed")
            tag.name in tags
        }
    }

    fun resolvePayloadData(): Set<PayloadDataPoet> {
        return poets.filterIsInstanceTo(mutableSetOf())
    }

    private val processedSchemas = mutableSetOf<Schema<*>>()

    data class PoetResolveMetadata(
        val isRequestBody: Boolean = false,
        val isMultipartData: Boolean = false,
        val sealedParent: ClassName? = null,
    )

    fun createPoetIfNeeded(schema: Schema<*>, metadata: PoetResolveMetadata = PoetResolveMetadata()) {
        if (!processedSchemas.add(schema)) return
        try {
            schema.`$ref`?.let { ref ->
                cache.loadRef(ref, RefUtils.computeRefFormat(ref), Schema::class.java)
                    ?.let { return createPoetIfNeeded(it, metadata) }
            }
            val className = schema.codegenClassName
            if (className != null && schema.codegenExternal != true) {
                if (poets.all { it.selfType != className }) {
                    val poet = schema.toPoet(metadata = metadata)
                    poets += poet
                    poet.createInnerPoets(this)
                }
            }
            schema.properties?.values.orEmpty()
                .forEach { createPoetIfNeeded(it) }
            schema.enumReference?.let {
                createPoetIfNeeded(it)
            }
            when (schema) {
                is ArraySchema -> createPoetIfNeeded(schema.items)
            }
        } finally {
            processedSchemas.remove(schema)
        }
    }

    fun read(openapi: OpenAPI) {
        openapi.paths.flatMapTo(operations) { (path, item) ->
            item.readOperationsMap().map { (method, operation) ->
                OperationPoet(operation, method, path, item)
            }
        }

        operations.forEach {
            it.operation.parameters?.forEach { parameter ->
                parameter.codegenSchema?.let { schema ->
                    createPoetIfNeeded(schema)
                }
            }
            it.operation.requestBody?.let { body -> resolve(body) }?.let { requestBody ->
                requestBody.content.forEach { (contentType, mediaType) ->
                    val isMultipartData = ContentType.parse(contentType) == ContentType.MultiPart.FormData
                    createPoetIfNeeded(
                        mediaType.schema,
                        PoetResolveMetadata(isRequestBody = true, isMultipartData = isMultipartData)
                    )
                }
            }
            it.operation.responses.forEach response@{ (code, response) ->
                if (code == "default") return@response
                response?.content?.values?.mapNotNull { mediaType -> mediaType.schema }?.forEach { schema ->
                    createPoetIfNeeded(schema)
                }
            }
        }

        openapi.components.schemas.values.forEach { createPoetIfNeeded(it) }

        tags += openapi.tags

        cache.resolutionCache.values.filterIsInstance<Schema<*>>().forEach { createPoetIfNeeded(it) }
    }

    fun writeTo(path: Path, category: String?) {
        poets.filterIsInstance<ClassPoet>().filter {
            !it.isExternal && it.category == category && it.selfType.isTopLevel
        }.sortedBy { it.selfType }.forEach {
            it.writeTo(this, path)
        }

        when (category) {
            null -> {
                tags.forEach { tag ->
                    ApiPoet(tag).writeTo(this, path)
                }

                val tupleSink = PoetSink.RootFile(ClassName("kiscord.serialization", "TupleSerializers"))
                TupleSerializerPoet.processPoets(this, tupleSink)
                tupleSink.writeTo(path)

                EnumSerializerPoet.writeTo(this, path)
            }
            "gateway-events" -> {
                GatewayEventPoet.build(this).forEach { sink ->
                    sink.writeTo(path)
                }
            }
        }
    }

    tailrec fun resolveParameter(parameter: Parameter): Parameter {
        val ref = parameter.`$ref`
        if (ref != null) {
            val refParameter = cache.loadRef(ref, RefUtils.computeRefFormat(ref), Parameter::class.java)
                ?: throw IllegalArgumentException("Invalid ref: $ref")
            return resolveParameter(refParameter)
        }
        return parameter
    }
}
