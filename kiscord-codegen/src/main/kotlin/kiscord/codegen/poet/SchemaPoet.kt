/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.codegen.poet

import com.squareup.kotlinpoet.*
import io.swagger.v3.oas.models.media.*
import kiscord.codegen.*
import kiscord.codegen.poet.fake.*

abstract class SchemaPoet<out T, out S : Schema<out T>>(val schema: S, override val selfType: ClassName) : ClassPoet {
    override val category: String? get() = schema.codegenCategory
    override val isExternal: Boolean get() = schema.codegenExternal == true
    override val urlEncoder: MemberName?
        get() {
            return if (schema.codegenCustomUrlEncoder == true) MemberName(selfType.packageName, "encodeAsUrlComponent")
            else null
        }

    private var created = false

    private val companionLazy = lazy {
        createCompanionBuilder()
    }

    val companion by companionLazy

    protected open fun createCompanionBuilder(name: String? = null): TypeSpec.Builder =
        TypeSpec.companionObjectBuilder(name)
            .addAnnotation(GENERATED)

    abstract fun createBuilder(snap: PoetSnap): TypeSpec.Builder

    abstract fun process(snap: PoetSnap, sink: PoetSink, builder: TypeSpec.Builder)

    final override fun build(snap: PoetSnap, sink: PoetSink): TypeSpec {
        check(!created) { "Poet $selfType already consumed" }
        created = true

        val builder = createBuilder(snap)

        schema.description?.let {
            builder.addKdoc("%L\n\n", it)
        }

        schema.externalDocs?.writeTo(builder)

        process(snap, sink, builder)

        val selfSink = PoetSink.InnerClass(builder, selfType, sink)

        snap.resolveDirectInners(this).forEach {
            it.addTo(snap, selfSink)
        }

        if (companionLazy.isInitialized()) {
            builder.addType(companion.build())
        }

        serializerTypeForUse(snap, RecursivePoetVisitorImpl()).forEach { serializer ->
            sink.addUseSerializers(serializer)
        }

        return builder.build()
    }

    override fun toString() = "SchemaPoet($selfType)"
}

private class RecursivePoetVisitorImpl : RecursivePoetVisitor {
    private val visited = mutableSetOf<TypePoet>()

    override fun <T : TypePoet> visit(poet: T, block: (T) -> Unit) {
        if (visited.add(poet)) {
            block(poet)
        }
    }
}

fun Schema<*>.toPoet(
    selfName: ClassName = codegenClassName ?: throw IllegalArgumentException("Missing package name for $this"),
    metadata: PoetSnap.PoetResolveMetadata
): TypePoet = when (this) {
    is StringSchema -> when {
        !enum.isNullOrEmpty() -> when (val t = codegenKind) {
            "gateway-event-name" -> GatewayEventNamePoet(this, selfName)
            null -> EnumPoet.StringSchema(this, selfName)
            else -> throw IllegalArgumentException("Unsupported kind: $type $t")
        }
        codegenExternal == true -> SchemaFakePoet(this, selfName)
        else -> throw IllegalArgumentException("Unsupported schema: $type")
    }
    is IntegerSchema -> when {
        !enum.isNullOrEmpty() -> when (format) {
            "int32" -> EnumPoet.IntSchema(this, selfName)
            "int64" -> {
                val policy = codegenSerializationPolicy
                if (policy != null && policy.unsigned) {
                    EnumPoet.ULongSchema(this, selfName)
                } else {
                    EnumPoet.LongSchema(this, selfName)
                }
            }
            else -> throw IllegalArgumentException("Unsupported schema: $type $format")
        }
        codegenKind == "payload-data" -> PayloadDataPoet.ValueClass(this, selfName)
        else -> ValueClassPoet(this, selfName)
    }
    is ObjectSchema -> when (codegenKind) {
        "dispatch-data" -> DispatchDataPoet.Object(this, selfName, metadata)
        "payload-data" -> PayloadDataPoet.Object(this, selfName, metadata)
        else -> when {
            metadata.isMultipartData -> MultipartObjectPoet(this, selfName, metadata)
            metadata.sealedParent != null -> SealedPoet.ChildPoet(this, selfName, metadata)
            else -> ObjectPoet(this, selfName, metadata)
        }
    }
    is ArraySchema -> when (minItems) {
        null -> throw IllegalArgumentException("Unsupported schema: $type")
        maxItems -> TuplePoet(this, selfName, metadata)
        else -> throw IllegalArgumentException("Unsupported schema: $type")
    }
    is ComposedSchema -> when {
        codegenKind == "dispatch-data" || codegenKind == "dispatch-data-update" -> when {
            !allOf.isNullOrEmpty() -> DispatchDataPoet.Object(this, selfName, metadata)
            else -> DispatchDataPoet.Delegate(this, selfName, codegenKind == "dispatch-data-update")
        }
        codegenExternal == true -> SchemaFakePoet(this, selfName)
        !allOf.isNullOrEmpty() && anyOf.isNullOrEmpty() && oneOf.isNullOrEmpty() -> ObjectPoet(this, selfName, metadata)
        oneOf?.size == 1 && anyOf.isNullOrEmpty() && allOf.isNullOrEmpty() -> ObjectPoet(this, selfName, metadata)
        (oneOf?.size ?: 0) > 1 && anyOf.isNullOrEmpty() && allOf.isNullOrEmpty() -> SealedPoet(this, selfName, metadata)
        else -> throw IllegalArgumentException("Unsupported composed schema: $codegenKind")
    }
    is BooleanSchema -> when(codegenKind) {
        "payload-data" -> PayloadDataPoet.ValueClass(this, selfName)
        else -> throw IllegalArgumentException("Unsupported schema: $type $this")
    }
    else -> throw IllegalArgumentException("Unsupported schema: $type $this")
}

val Schema<*>.codegenClassName: ClassName?
    get() {
        val packageName = codegenPackage ?: return null
        val name = codegenName ?: title ?: return null
        val names = name.split('.')
        return ClassName(packageName, names.first(), *names.subList(1, names.size).toTypedArray())
    }

fun ClassName.isChildOf(parent: ClassName): Boolean {
    if (packageName != parent.packageName) return false
    return simpleNames.dropLast(1) == parent.simpleNames
}

