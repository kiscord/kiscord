/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.codegen.poet

import com.fasterxml.jackson.annotation.*
import com.squareup.kotlinpoet.*
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import io.swagger.v3.oas.models.*
import io.swagger.v3.oas.models.media.*
import kiscord.codegen.*

sealed class EnumPoet<T : Any>(schema: Schema<T>, selfName: ClassName) : SchemaPoet<T, Schema<T>>(schema, selfName),
    ExtensionsForDTO, ValueResolver.MonoResolver {
    val serializationPolicy = schema.codegenSerializationPolicy ?: SerializationPolicy()

    abstract val enumValues: List<EnumValue>
    open val serializeByOrdinal: Boolean get() = false
    open val serializeByName: Boolean get() = false

    override fun serializerTypeFor(type: TypeName): ClassName? = when {
        type !is ParameterizedTypeName -> super.serializerTypeFor(type)
        type.rawType == SET && type.typeArguments[0] == selfType && schema.enumIsBitmask == true ->
            selfType.nestedClass(BITMASK_SERIALIZER).withTag(SerializerMetadata(isObject = true))

        type.rawType == SET && type.typeArguments[0] == selfType && serializationPolicy.nullOnUnknown ->
            selfType.nestedClass(SET_SERIALIZER).withTag(SerializerMetadata(isObject = true))

        type.rawType == BOX && schema.enumIsBitmask == true -> {
            val t = type.typeArguments[0]
            when {
                t is ParameterizedTypeName && t.rawType == SET && t.typeArguments[0] == selfType && serializationPolicy.optionalSerializer ->
                    selfType.nestedClass(OPTIONAL_BITMASK_SERIALIZER).withTag(SerializerMetadata(isObject = true))

                else -> super.serializerTypeFor(type)
            }
        }

        type.rawType == BOX && serializationPolicy.nullOnUnknown -> {
            val t = type.typeArguments[0]
            when {
                t is ParameterizedTypeName && t.rawType == SET && t.typeArguments[0] == selfType && serializationPolicy.optionalSerializer ->
                    selfType.nestedClass(OPTIONAL_SET_SERIALIZER).withTag(SerializerMetadata(isObject = true))

                else -> super.serializerTypeFor(type)
            }
        }

        else -> super.serializerTypeFor(type)
    }

    class IntSchema(schema: Schema<Number>, selfName: ClassName) : EnumPoet<Number>(schema, selfName),
        ValueResolver.ManyResolver {
        override val valueTypeName get() = INT
        override val valueTypeKind: EnumSerializerPoet.Kind get() = EnumSerializerPoet.Kind.AsInt

        override val enumValues = schema.enumValues ?: schema.enum.map {
            EnumValue(numericValue = it, identifier = "Enum$it")
        }
        override val EnumValue.valueLiteral: CodeBlock
            get() = valueLiteral(actualIntValue)
        override val Collection<EnumValue>.valueLiteral: CodeBlock
            get() = valueLiteral(map { it.actualIntValue }.reduce { acc, i -> acc or i })

        private fun valueLiteral(value: Int): CodeBlock {
            return when (val t = serializationPolicy.literalFormat) {
                "hex" -> CodeBlock.of("0x%L", value.toString(16).padStart(8, '0'))
                "decimal", null -> CodeBlock.of("%L", value)
                else -> throw IllegalArgumentException("Unknown enum value format: $t")
            }
        }

        override val serializeByOrdinal: Boolean
            get() {
                if (enumValues.isEmpty()) return true
                enumValues.forEachIndexed { index, enumValue ->
                    val value = enumValue.numericValue ?: throw IllegalArgumentException("Missing numericValue")
                    if (index != value.toInt())
                        return false
                }
                return true
            }

        override fun FunSpec.Builder.addMapping(s: String, value: EnumValue, enumIsBitmask: Boolean) {
            when {
                enumIsBitmask -> addStatement(s, value.identifier, value.actualIntValue.countTrailingZeroBits())
                else -> addStatement(s, value.identifier, value.valueLiteral)
            }
        }

        override fun resolveValue(value: Any?): CodeBlock {
            val int = (value as Number).toInt()
            val enumValue = enumValues.single { it.actualIntValue == int }
            return CodeBlock.of("%T.%N", selfType, enumValue.identifier)
        }

        override fun resolveValues(value: Any?): Collection<CodeBlock> {
            check(schema.enumIsBitmask == true) { "Enum doesn't support set" }
            var i = (value as Number).toInt()
            val values = mutableSetOf<CodeBlock>()
            while (i != 0) {
                val j = i.takeLowestOneBit()
                i = i and j.inv()
                values += resolveValue(j)
            }
            return values
        }

        override fun validateValues(enumValues: List<EnumValue>, enumIsBitmask: Boolean) {
            if (enumIsBitmask) {
                require(enumValues.all { it.actualIntValue.isPowerOfTwo })
            }
        }

        private val Int.isPowerOfTwo: Boolean get() = this > 0 && this and -this == this
    }

    class ULongSchema(schema: Schema<Number>, selfName: ClassName) : LongSchema(schema, selfName) {
        override val valueTypeName: ClassName get() = U_LONG
        override val serializerTypeName: ClassName get() = throw UnsupportedOperationException()

        override fun valueLiteral(value: Long): CodeBlock = unsignedValueLiteral(value.toULong())
        private fun unsignedValueLiteral(value: ULong): CodeBlock {
            return when (val t = serializationPolicy.literalFormat) {
                "hex" -> CodeBlock.of("0x%LUL", value.toString(16).padStart(16, '0'))
                "decimal", null -> CodeBlock.of("%LUL", value)
                else -> throw IllegalArgumentException("Unknown enum value format: $t")
            }
        }

        override fun FunSpec.Builder.addMapping(s: String, value: EnumValue, enumIsBitmask: Boolean) {
            when {
                enumIsBitmask -> addStatement(s, value.identifier, value.actualLongValue.countTrailingZeroBits())
                else -> addStatement(s, value.identifier, value.valueLiteral)
            }
        }
    }

    open class LongSchema(schema: Schema<Number>, selfName: ClassName) : EnumPoet<Number>(schema, selfName),
        ValueResolver.ManyResolver {
        override val valueTypeName get() = LONG
        override val valueTypeKind: EnumSerializerPoet.Kind
            get() = EnumSerializerPoet.Kind.AsLong

        override val enumValues = schema.enumValues ?: schema.enum.map {
            EnumValue(numericValue = it, identifier = "Enum$it")
        }
        override val EnumValue.valueLiteral: CodeBlock
            get() = valueLiteral(actualLongValue)
        override val Collection<EnumValue>.valueLiteral: CodeBlock
            get() = valueLiteral(map { it.actualLongValue }.reduce { acc, i -> acc or i })

        open fun valueLiteral(value: Long): CodeBlock = when (val t = serializationPolicy.literalFormat) {
            "hex" -> CodeBlock.of("0x%LL", value.toString(16).padStart(16, '0'))
            "decimal", null -> CodeBlock.of("%LL", value)
            else -> throw IllegalArgumentException("Unknown enum value format: $t")
        }

        override val serializeByOrdinal: Boolean
            get() {
                if (enumValues.isEmpty()) return true
                enumValues.forEachIndexed { index, enumValue ->
                    val value = enumValue.numericValue ?: throw IllegalArgumentException("Missing numericValue")
                    if (index.toLong() != value.toLong())
                        return false
                }
                return true
            }

        override fun FunSpec.Builder.addMapping(s: String, value: EnumValue, enumIsBitmask: Boolean) {
            when {
                enumIsBitmask -> addStatement(s, value.identifier, value.actualLongValue.countTrailingZeroBits())
                else -> addStatement(s, value.identifier, value.valueLiteral)
            }
        }

        override fun resolveValue(value: Any?): CodeBlock {
            val long = (value as Number).toLong()
            val enumValue = enumValues.single { it.actualLongValue == long }
            return CodeBlock.of("%T.%N", selfType, enumValue.identifier)
        }

        override fun resolveValues(value: Any?): Collection<CodeBlock> {
            check(schema.enumIsBitmask == true) { "Enum doesn't support set" }
            var i = (value as Number).toLong()
            val values = mutableSetOf<CodeBlock>()
            while (i != 0L) {
                val j = i.takeLowestOneBit()
                i = i and j.inv()
                values += resolveValue(j)
            }
            return values
        }

        override fun validateValues(enumValues: List<EnumValue>, enumIsBitmask: Boolean) {
            if (enumIsBitmask) {
                require(enumValues.all { it.actualLongValue.isPowerOfTwo })
            }
        }

        private val Long.isPowerOfTwo: Boolean get() = this > 0 && this and -this == this
    }

    open class StringSchema(schema: Schema<String>, selfName: ClassName) : EnumPoet<String>(schema, selfName) {
        override val valueTypeName get() = STRING
        override val valueTypeKind: EnumSerializerPoet.Kind
            get() = EnumSerializerPoet.Kind.AsString

        override val enumValues = schema.enumValues ?: schema.enum.map {
            EnumValue(stringValue = it, identifier = it)
        }
        override val serializeByName: Boolean
            get() = enumValues.isEmpty() || enumValues.all { enumValue ->
                enumValue.stringValue == enumValue.identifier
            }
        override val EnumValue.valueLiteral: CodeBlock
            get() = CodeBlock.of("%S", actualStringValue)
        override val setSerializerTypeName: ClassName
            get() = EnumSerializerPoet.getSetSerializer(EnumSerializerPoet.Kind.AsString)

        override fun FunSpec.Builder.addMapping(s: String, value: EnumValue, enumIsBitmask: Boolean) {
            addStatement(s, value.identifier, value.valueLiteral)
        }

        override fun resolveValue(value: Any?): CodeBlock {
            val string = value.toString()
            val enumValue = enumValues.single { it.stringValue == string }
            return CodeBlock.of("%T.%N", selfType, enumValue.identifier)
        }
    }

    abstract val valueTypeName: TypeName
    abstract val valueTypeKind: EnumSerializerPoet.Kind
    open val serializerTypeName: ClassName get() = EnumSerializerPoet.getSerializerFor(valueTypeKind)
    open val setSerializerTypeName: ClassName get() = EnumSerializerPoet.getBitmaskSerializerFor(valueTypeKind)
    abstract fun FunSpec.Builder.addMapping(s: String, value: EnumValue, enumIsBitmask: Boolean)
    abstract val EnumValue.valueLiteral: CodeBlock
    open val Collection<EnumValue>.valueLiteral: CodeBlock get() = throw UnsupportedOperationException()

    override fun createBuilder(snap: PoetSnap) = TypeSpec.enumBuilder(selfType).addAnnotation(GENERATED)

    override fun process(snap: PoetSnap, sink: PoetSink, builder: TypeSpec.Builder) {
        val enum = schema.enum as List<T>

        val extraData = schema.enumExtraData
        if (extraData != null) {
            val constructor = FunSpec.constructorBuilder()
            extraData.entries.forEach { (name, schema) ->
                val type = snap.resolveType(schema)
                    ?: throw IllegalStateException("Unresolved type for extra enum data $name")

                val parameterSpec = ParameterSpec.builder(name, type)
                if (schema.default != null) {
                    parameterSpec.defaultValue(snap.valueResolver.resolve(type, schema.default))
                }
                constructor.addParameter(parameterSpec.build())
                builder.addProperty(
                    PropertySpec.builder(name, type)
                        .initializer(name)
                        .build()
                )
                schema.description?.let {
                    builder.addKdoc("@property %N %L\n", name, it)
                }
            }
            builder.primaryConstructor(constructor.build())
        }

        require(enum.size == enumValues.size) { "Enum values size mismatch for $selfType" }

        val enumIsBitmask = schema.enumIsBitmask == true

        validateValues(enumValues, enumIsBitmask)

        val serializeByOrdinal = serializeByOrdinal
        val serializeByName = serializeByName

        val enumToValue = FunSpec.builder(ENUM_TO_VALUE)
            .addAnnotation(KISCORD_UNSTABLE_API)
            .addParameter("value", selfType)
            .returns(valueTypeName)

        when {
            serializeByOrdinal -> enumToValue.addStatement("return value.ordinal")
            serializeByName -> enumToValue.addStatement("return value.name")
            else -> enumToValue.beginControlFlow("return when(value)")
        }

        val valueToEnum = FunSpec.builder(VALUE_TO_ENUM)
            .addAnnotation(KISCORD_UNSTABLE_API)
            .addParameter("value", valueTypeName)
            .returns(selfType.copy(nullable = true))

        if (serializeByName) {
            valueToEnum.beginControlFlow("return try")
                .addStatement("enumValueOf<%T>(value)", selfType)
                .nextControlFlow("catch(_: %T)", ClassName("kotlin", "IllegalArgumentException"))
                .addStatement("null")
        } else {
            valueToEnum.beginControlFlow("return when(value)")
        }

        val maxIdentifierLength = enumValues.maxOf { it.identifier.length }
        val maxValueLength = enumValues.maxOf { it.valueLiteral.toString().length }.coerceAtLeast(4)
        enumValues.forEach { enumValue ->
            val enumType = TypeSpec.anonymousClassBuilder()
            processEnumValue(snap, sink, enumValue, enumType, extraData)
            builder.addEnumConstant(enumValue.identifier, enumType.build())

            if (!serializeByOrdinal && !serializeByName) enumToValue.addMapping(
                "%1N${" ".repeat(maxIdentifierLength - enumValue.identifier.length)} -> %2L",
                enumValue,
                false
            )
            if (!serializeByName) valueToEnum.addMapping(
                "%2L${" ".repeat(maxValueLength - enumValue.valueLiteral.toString().length)} -> %1N",
                enumValue,
                false
            )

            if (enumValue.numericValue != null && enumIsBitmask) {
                var shift = enumValue.shift
                if (shift == null) {
                    shift = enumValue.numericValue.toLong().countTrailingZeroBits()
                    enumValue.shift = shift
                }
                require(1L shl shift == enumValue.numericValue.toLong()) { "Bitmask shift mismatch" }
            }
        }

        if (!serializeByName) valueToEnum.addStatement("else${" ".repeat(maxValueLength - 4)} -> null")

        if (!serializeByOrdinal && !serializeByName) enumToValue.endControlFlow()
        valueToEnum.endControlFlow()

        if (serializationPolicy.enumToValue) {
            companion.addFunction(enumToValue.build())
        }
        if (serializationPolicy.valueToEnum) {
            companion.addFunction(valueToEnum.build())
        }

        if (serializationPolicy.allSet) {
            val allSet = PropertySpec.builder("ALL", SET.parameterizedBy(selfType))
                .initializer(
                    "%M(\n⇥%L⇤)",
                    ENUM_SET_OF,
                    enumValues.map { CodeBlock.of("%L", it.identifier) }.joinToCode(separator = ",\n", suffix = ",\n")
                )
                .build()

            companion.addProperty(allSet)
        }

        if (serializationPolicy.allMask && enumIsBitmask) {
            val allMask = PropertySpec.builder("ALL_MASK", valueTypeName)
                .addAnnotation(KISCORD_UNSTABLE_API)
                .addModifiers(KModifier.CONST)
                .initializer("%L", enumValues.valueLiteral)
                .build()

            companion.addProperty(allMask)
        }

        if (!serializationPolicy.serializer) return

        fun TypeSpec.Builder.populateSerializer() {
            addAnnotation(GENERATED)
            addAnnotation(KISCORD_UNSTABLE_API)

            addFunction(
                FunSpec.builder(ENUM_TO_VALUE)
                    .addModifiers(KModifier.OVERRIDE)
                    .returns(valueTypeName)
                    .addParameter("value", selfType)
                    .addStatement("return %T.%N(value)", selfType, ENUM_TO_VALUE)
                    .build()
            )

            addFunction(
                FunSpec.builder(VALUE_TO_ENUM)
                    .addModifiers(KModifier.OVERRIDE)
                    .returns(selfType.copy(nullable = true))
                    .addParameter("value", valueTypeName)
                    .addStatement("return %T.%N(value)", selfType, VALUE_TO_ENUM)
                    .build()
            )
        }

        if (enumIsBitmask) {
            sink.addOptIn(EXPERIMENTAL_SERIALIZATION_API, PoetSink.Target.TopLevel)

            // Generate set serializer
            val serializer = TypeSpec.objectBuilder(BITMASK_SERIALIZER).apply {
                superclass(setSerializerTypeName.parameterizedBy(selfType))
                addSuperclassConstructorParameter("%S", SET.parameterizedBy(selfType))

                val (atomicType, initializer) = when (valueTypeName) {
                    INT -> ATOMIC_INT to CodeBlock.of("%M(%L)", ATOMIC, 0)
                    LONG -> ATOMIC_LONG to CodeBlock.of("%M(%LL)", ATOMIC, 0)
                    else -> throw UnsupportedOperationException()
                }

                addProperty(
                    PropertySpec.builder("unknownValues", atomicType)
                        .addModifiers(KModifier.PRIVATE)
                        .initializer(initializer)
                        .build()
                )

                companion.addProperty(
                    PropertySpec.builder("logger", LOGGER)
                        .delegate(
                            "%M { %T.DefaultLoggerFactory.%M(%S, %S) }",
                            MemberName("kotlin", "lazy"),
                            KISCORD,
                            MemberName("org.kodein.log", "newLogger"),
                            selfType.packageName,
                            selfType.simpleNames.joinToString(separator = ".")
                        ).build()
                )

                addFunction(
                    FunSpec.builder("unknownValue")
                        .addModifiers(KModifier.OVERRIDE)
                        .addParameter("value", valueTypeKind.primitive)
                        .beginControlFlow("while (true)")
                        .addStatement("val oldValue = unknownValues.value")
                        .beginControlFlow("if (oldValue and value == value)")
                        .addStatement("return")
                        .endControlFlow()
                        .beginControlFlow("if (unknownValues.compareAndSet(oldValue, oldValue or value))")
                        .addStatement("break")
                        .endControlFlow()
                        .endControlFlow()
                        .beginControlFlow("logger.warning")
                        .addStatement("%L", valueTypeKind.unknownEnumValueMessage())
                        .endControlFlow()
                        .build()
                )

                populateSerializer()
            }.build()

            builder.addType(serializer)

            if (serializationPolicy.optionalSerializer) {
                builder.addType(
                    TypeSpec.objectBuilder(OPTIONAL_BITMASK_SERIALIZER)
                        .addAnnotation(KISCORD_UNSTABLE_API)
                        .addAnnotation(GENERATED)
                        .superclass(BOX.nestedClass("Serializer").parameterizedBy(SET.parameterizedBy(selfType)))
                        .addSuperclassConstructorParameter("%T", selfType.nestedClass(BITMASK_SERIALIZER))
                        .build()
                )
            }
        } else {
            // Generate regular serializer
            val serializer = TypeSpec.objectBuilder(SERIALIZER).apply {
                superclass(serializerTypeName.parameterizedBy(selfType))
                addSuperclassConstructorParameter("%S", selfType)

                populateSerializer()
            }.build()

            builder.addType(serializer)

            builder.addAnnotation(
                AnnotationSpec.builder(SERIALIZABLE)
                    .addMember("with = %T::class", selfType.nestedClass(SERIALIZER)).build()
            )

            if (serializationPolicy.nullOnUnknown) {
                val setSerializer = TypeSpec.objectBuilder(SET_SERIALIZER).apply {
                    superclass(setSerializerTypeName.parameterizedBy(selfType))
                    addSuperclassConstructorParameter("%S", selfType)
                    populateSerializer()
                }.build()

                builder.addType(setSerializer)

                if (serializationPolicy.optionalSerializer) {
                    builder.addType(
                        TypeSpec.objectBuilder(OPTIONAL_SET_SERIALIZER)
                            .addAnnotation(KISCORD_UNSTABLE_API)
                            .addAnnotation(GENERATED)
                            .superclass(BOX.nestedClass("Serializer").parameterizedBy(SET.parameterizedBy(selfType)))
                            .addSuperclassConstructorParameter("%T", selfType.nestedClass(SET_SERIALIZER))
                            .build()
                    )
                }
            }
        }
    }

    open fun validateValues(enumValues: List<EnumValue>, enumIsBitmask: Boolean) {}

    open fun processEnumValue(
        snap: PoetSnap,
        sink: PoetSink,
        enumValue: EnumValue,
        builder: TypeSpec.Builder,
        extraData: Map<String, Schema<*>>?
    ) {
        enumValue.description?.let {
            builder.addKdoc("%L\n\n", it)
        }
        enumValue.externalDocs?.writeTo(builder)
        extraData?.entries?.forEach { (name, schema) ->
            val extraType =
                snap.resolveType(schema) ?: throw IllegalStateException("Unresolved type for extra enum data $name")
            val actualValue = enumValue.extensions[name] ?: return@forEach
            builder.addSuperclassConstructorParameter(
                "%N = %L",
                name,
                snap.valueResolver.resolve(extraType, actualValue)
            )
        }
    }

    companion object {
        const val SERIALIZER = "Serializer"
        const val BITMASK_SERIALIZER = "BitmaskSerializer"
        const val SET_SERIALIZER = "SetSerializer"
        const val OPTIONAL_BITMASK_SERIALIZER = "Optional${BITMASK_SERIALIZER}"
        const val OPTIONAL_SET_SERIALIZER = "Optional${SET_SERIALIZER}"
        const val ENUM_TO_VALUE = "enumToValue"
        const val VALUE_TO_ENUM = "valueToEnum"
    }

    data class EnumValue(
        val identifier: String,
        val numericValue: Number? = null,
        val stringValue: String? = null,
        val description: String? = null,
        var shift: Int? = null,
        val externalDocs: ExternalDocumentation? = null,
        @get:JsonAnyGetter
        val extensions: MutableMap<String, Any?> = mutableMapOf()
    ) {
        @JsonAnySetter
        fun extension(name: String, value: Any?) {
            extensions[name] = value
        }

        val actualIntValue: Int get() = numericValue?.toInt() ?: identifier.toInt()
        val actualLongValue: Long get() = numericValue?.toLong() ?: identifier.toLong()
        val actualStringValue: String get() = stringValue ?: identifier
    }

    override fun <S : Schema<Any?>> generateExtensionsForDTO(
        poet: ObjectPoet<S>,
        member: ObjectPoet<S>.ObjectMember,
        kind: ObjectPoet.Kind,
        sink: PoetSink,
        snap: PoetSnap
    ) {
        if (kind != ObjectPoet.Kind.Builder) return
        if (member.propertyType !is ParameterizedTypeName
            || member.propertyType.rawType !in setOf(SET, LIST)
        ) return

        fun generateFun(parameterSpec: ParameterSpec) {
            val funBuilder = FunSpec.builder(member.propertyName)
            funBuilder.addAnnotation(GENERATED)
            funBuilder.addAnnotation(BUILDER_DSL)
            funBuilder.addKdoc("Adds `%N` to according set in the builder", member.propertyName)
            funBuilder.receiver(poet.builderType)
            funBuilder.addParameter(parameterSpec)
            if (member.required) {
                funBuilder.addStatement("this.%1N += %1N", member.propertyName)
            } else {
                funBuilder.addStatement("this.%1N = this.%1N.orEmpty() + %1N", member.propertyName)
            }
            sink.addFunction(funBuilder.build(), target = PoetSink.Target.Extra)
        }

        generateFun(ParameterSpec.builder(member.propertyName, selfType).build())
        generateFun(ParameterSpec.builder(member.propertyName, selfType).addModifiers(KModifier.VARARG).build())
        generateFun(ParameterSpec.builder(member.propertyName, COLLECTION.parameterizedBy(selfType)).build())
    }
}
