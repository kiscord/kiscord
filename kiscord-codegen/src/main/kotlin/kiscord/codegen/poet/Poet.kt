/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.codegen.poet

import com.squareup.kotlinpoet.*
import io.ktor.http.*
import java.nio.file.*

interface Poet<out T> {
    val category: String?

    fun build(snap: PoetSnap, sink: PoetSink): T
}

interface TypePoet : Poet<TypeSpec> {
    val selfType: TypeName
    val isValueClass: Boolean get() = false
    val isExternal: Boolean get() = false

    fun serializerTypeFor(type: TypeName): ClassName? = null
    fun serializerTypeForUse(snap: PoetSnap, visitor: RecursivePoetVisitor): Set<ClassName> = emptySet()

    fun serializerInstanceForSelf(): CodeBlock = CodeBlock.of("%T.serializer()", selfType)

    val urlEncoder: MemberName? get() = null

    fun isSubtypeOf(type: TypeName): Boolean = false
    fun toPartData(
        selfValue: CodeBlock,
        partName: CodeBlock,
        overrideContentType: ContentType? = null
    ): CodeBlock {
        throw NotImplementedError("Poet $this doesn't support encoding as part data")
    }

    fun createInnerPoets(snap: PoetSnap) {}
}

interface RecursivePoetVisitor {
    fun <T : TypePoet> visit(poet: T, block: (T) -> Unit)

    object Noop : RecursivePoetVisitor {
        override fun <T : TypePoet> visit(poet: T, block: (T) -> Unit) = Unit
    }
}

interface BuildablePoet : TypePoet {
    val withSpec: Boolean get() = false
    val withBuilder: Boolean get() = false
    val specType: TypeName
    val builderType: TypeName

    fun formatToBuilder(value: CodeBlock, preserveBuilder: Boolean = false): CodeBlock = value
    fun formatBuild(builder: CodeBlock): CodeBlock = builder
    fun formatCreateBuilder(): CodeBlock = CodeBlock.of("%T()", builderType)
}

interface ClassPoet : BuildablePoet {
    override val selfType: ClassName
    override val specType: ClassName get() = selfType
    override val builderType: ClassName get() = selfType
}

@JvmName("addTypeTo")
fun Poet<TypeSpec>.addTo(snap: PoetSnap, sink: PoetSink) {
    sink.addType(build(snap, sink))
}

@JvmName("addFunTo")
fun Poet<FunSpec>.addTo(snap: PoetSnap, sink: PoetSink) {
    sink.addFunction(build(snap, sink))
}

fun ClassPoet.toFileSpecs(snap: PoetSnap): Collection<FileSpec> {
    val sink = PoetSink.RootFile(selfType)
    addTo(snap, sink)
    return sink.toFileSpecs()
}

fun ClassPoet.writeTo(snap: PoetSnap, path: Path) {
    toFileSpecs(snap).forEach { it.writeTo(path) }
}
