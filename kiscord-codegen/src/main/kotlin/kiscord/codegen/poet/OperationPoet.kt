/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.codegen.poet

import com.fasterxml.jackson.module.kotlin.*
import com.squareup.kotlinpoet.*
import io.ktor.http.*
import io.swagger.v3.oas.annotations.enums.*
import io.swagger.v3.oas.models.*
import io.swagger.v3.oas.models.media.*
import io.swagger.v3.oas.models.parameters.*
import kiscord.codegen.*
import kiscord.codegen.poet.fake.*

class OperationPoet(
    val operation: Operation,
    val method: PathItem.HttpMethod,
    val path: String,
    val pathItem: PathItem
) : Poet<List<FunSpec.Builder>> {
    override val category: String? get() = null

    sealed class PathPart {
        class Constant(val part: String) : PathPart()
        class Variable(val name: String, val poet: TypePoet) : PathPart() {
            val propertyName = name.toKiscordCase()
        }
    }

    private fun Parameter.toSpec(snap: PoetSnap): ParameterSpec {
        val schema = codegenSchema

        val defaultValue = schema.default
        val required = required == true || defaultValue != null

        val parameterName = extensions?.get("x-kiscord-codegen-name") as String?
            ?: name.toKiscordCase()

        val parameterPoet = snap.resolvePoet(schema)
            ?: throw IllegalStateException("Couldn't resolve type for parameter $name in $method operation $path")

        val parameterType = parameterPoet.selfType.copy(nullable = !required)

        val spec = ParameterSpec.builder(parameterName, parameterType)

        if (defaultValue != null) {
            spec.defaultValue("%L", snap.valueResolver.resolve(parameterType, defaultValue))
        } else if (!required) {
            spec.defaultValue("null")
        }

        return spec.build()
    }

    fun resolveParameters(snap: PoetSnap): List<Parameter> {
        return (operation.parameters.orEmpty() + pathItem.parameters.orEmpty()).map { snap.resolveParameter(it) }
    }

    fun resolveParts(snap: PoetSnap): List<PathPart> {
        var parts: List<PathPart> = listOf(PathPart.Constant(path))
        resolveParameters(snap).filterIsInstance<PathParameter>().forEach { parameter ->
            parts = parts.flatMap {
                when (it) {
                    is PathPart.Constant -> {
                        val subparts = mutableListOf<PathPart>()
                        it.part.split("{${parameter.name}}").forEachIndexed { index, part ->
                            val poet = snap.resolvePoet(parameter.codegenSchema)
                                ?: throw IllegalArgumentException("Unable to resolve poet for ${parameter.schema}")
                            if (index != 0) subparts += PathPart.Variable(parameter.name, poet)
                            if (part.isNotBlank()) subparts += PathPart.Constant(part)
                        }
                        subparts
                    }
                    else -> listOf(it)
                }
            }
        }

        return parts
    }

    fun resolveOperationMethods(snap: PoetSnap): Set<String> = buildSet {
        val defaultOperationName = operation.extensions?.get("x-kiscord-codegen-name") as String?
            ?: operation.operationId.toKiscordCase()

        val body = operation.requestBody?.let { snap.resolve(it) }
        if (body == null) {
            this += defaultOperationName
        } else {
            body.content.entries.forEach { (_, mediaType) ->
                if (mediaType != null && mediaType.extensions != null) {
                    val operationNameForBody = mediaType.extensions["x-kiscord-codegen-operation-name"] as String?
                    if (operationNameForBody != null) this += operationNameForBody
                } else {
                    this += defaultOperationName
                }
            }
        }
    }

    override fun build(snap: PoetSnap, sink: PoetSink): List<FunSpec.Builder> = buildList {
        val responses = resolveResponses()

        when (responses.size) {
            0 -> throw IllegalStateException()
            1 -> {
            }
            else -> {
                val operationName = operation.extensions?.get("x-kiscord-codegen-name") as String?
                    ?: operation.operationId.toKiscordCase()

                val resultType = sink.resolveClassName("${operationName.replaceFirstChar { it.titlecase() }}Result")

                val resultBuilder = TypeSpec.classBuilder(resultType)
                    .addModifiers(KModifier.SEALED)
                    .addSuperinterface(RESPONSE_RESULT)
                    .addKdoc("Result type for [%1L]\n\n@see %1L", operationName)

                responses.forEach { response ->
                    val responseResultType = resultType.nestedClass(
                        response.name ?: throw IllegalStateException("Missing response name in $response")
                    )

                    fun enrichResultType(builder: TypeSpec.Builder) {
                        if (response.description != null) {
                            builder.addKdoc(response.description)
                        }
                        builder.addProperty(
                            PropertySpec.builder("isSuccess", BOOLEAN)
                                .addModifiers(KModifier.OVERRIDE)
                                .getter(
                                    FunSpec.getterBuilder()
                                        .addStatement("return %L", response.code.isSuccess())
                                        .build()
                                )
                                .build()
                        )
                    }

                    if (response is ResponseRoutine.Code) {
                        val responseResult = TypeSpec.objectBuilder(responseResultType)
                            .superclass(resultType)
                            .addModifiers(KModifier.DATA)
                        enrichResultType(responseResult)
                        resultBuilder.addType(responseResult.build())
                    } else {
                        val mediaType = response.returnType(snap, false)
                        val responseResult = TypeSpec.classBuilder(responseResultType)
                            .superclass(resultType)
                            .primaryConstructor(
                                FunSpec.constructorBuilder()
                                    .addParameter("value", mediaType)
                                    .build()
                            )
                            .addProperty(
                                PropertySpec.builder(
                                    "value", mediaType
                                ).initializer("value").build()
                            )
                        enrichResultType(responseResult)
                        resultBuilder.addType(responseResult.build())
                    }
                }

                if (operation.deprecated == true) {
                    resultBuilder.addAnnotation(
                        AnnotationSpec.builder(Deprecated::class)
                            .addMember("message = %S", "Deprecated in OpenAPI Schema")
                            .build()
                    )
                }

                sink.addType(resultBuilder.build())
            }
        }

        if (operation.codegenExternal == true) return@buildList

        val body = operation.requestBody?.let { snap.resolve(it) }
        if (body == null) {
            this += build(snap, sink, RequestBodyRoutine.Empty, responses)
        } else {
            if (body.extensions?.get("x-kiscord-codegen-fuse-multipart") == true) {
                val content = body.content
                val multipartContentType = content.keys.find { ContentType.MultiPart.FormData.match(it) }
                    ?: throw IllegalStateException("No any multipart data")
                val multipartMediaType = content[multipartContentType]!!
                val fusibleProperty = multipartMediaType.schema.codegenFusible.orEmpty().single()
                val fusibleContentType = ContentType.parse(fusibleProperty.contentType!!)
                this += build(
                    snap, sink,
                    RequestBodyRoutine.SimpleOrFused(
                        fusibleContentType,
                        content[fusibleProperty.contentType]!!,
                        multipartMediaType
                    ),
                    responses
                )
            } else {
                body.content.entries.mapTo(this) { (contentType, mediaType) ->
                    build(
                        snap,
                        sink,
                        RequestBodyRoutine.SimpleOrFused(ContentType.parse(contentType), mediaType),
                        responses
                    )
                }
            }
        }
    }

    fun resolveResponses(): List<ResponseRoutine> {
        val responseMap = operation.responses.entries.mapNotNull { (statusCode, response) ->
            val code = statusCode.toIntOrNull()?.let { HttpStatusCode.fromValue(it) } ?: return@mapNotNull null
            code to response
        }.toMap()

        val responses =
            responseMap.filter { (code, _) -> code.isSuccess() }.mapTo(mutableListOf()) { (code, response) ->
                val name = response.extensions?.get("x-kiscord-codegen-name") as String?
                val content = response.content ?: return@mapTo ResponseRoutine.Code(code, name, response.description)
                ResponseRoutine.Media(code, name, response.description, content)
            }

        if (responses.size == 1 && responses.single() is ResponseRoutine.Media && responseMap.count { (code, _) -> !code.isSuccess() } == 1) {
            val nullCode = responseMap.keys.single { !it.isSuccess() }
            responses.replaceAll {
                val media = it as ResponseRoutine.Media
                ResponseRoutine.NullableMedia(media, nullCode)
            }
        }

        return responses
    }

    fun build(
        snap: PoetSnap,
        sink: PoetSink,
        body: RequestBodyRoutine,
        responses: List<ResponseRoutine>
    ): FunSpec.Builder {
        val operationName = body.getOperationName(
            operation.extensions?.get("x-kiscord-codegen-name") as String?
                ?: operation.operationId.toKiscordCase()
        )

        val builder = FunSpec.builder(operationName)
            .addModifiers(KModifier.SUSPEND)
            .addAnnotation(GENERATED)
            .addAnnotation(DISCORD_API_DSL)

        (operation.description ?: operation.summary)?.let {
            builder.addKdoc("%L\n\n", it)
        }

        builder.addKdoc("[%L](%L)\n", "Kiscord·OpenAPI·Specification", "https://kiscord.aur.rocks/exploring/openapi#${method.name.lowercase()}-${path.replace(Regex("\\{([.a-z]+)}"), "-$1-")}")
        operation.externalDocs?.writeTo(builder)

        if (operation.deprecated == true) {
            builder.addAnnotation(
                AnnotationSpec.builder(Deprecated::class)
                    .addMember("message = %S", "Deprecated in OpenAPI Schema")
                    .build()
            )
        }

        val comparator = compareBy<Parameter, ParameterIn>(MostValuedParameterInComparator) { it.location }
        val parameters = resolveParameters(snap).sortedWith(comparator)

        parameters.forEach { parameter ->
            val spec = parameter.toSpec(snap)
            builder.addParameter(spec)
            parameter.description?.let {
                builder.addKdoc("@param %N %L\n", spec.name, it)
            }
        }

        val resultType = when (responses.size) {
            0 -> throw IllegalStateException()
            1 -> responses.single().returnType(snap, true)
            else -> sink.resolveClassName("${operationName.replaceFirstChar { it.titlecase() }}Result")
        }

        builder.returns(resultType)

        val requestCodeBlock = CodeBlock.builder()
        val receiveCodeBlock = CodeBlock.builder()

        val acceptMediaTypes = responses.flatMapTo(mutableSetOf()) { it.acceptMediaTypes }

        fun singleResponseRoutine(response: ResponseRoutine) {
            receiveCodeBlock.add(response.receive(snap, builder))
        }

        fun multipleResponseRoutine() {
            require(resultType is ClassName)

            receiveCodeBlock.beginControlFlow("when(response.status.value)")

            responses.forEach { response ->
                val responseResultType = resultType.nestedClass(
                    response.name ?: throw IllegalStateException("Missing response name in $response")
                )

                val receiver = if (response is ResponseRoutine.Code) {
                    CodeBlock.of("%T", responseResultType)
                } else {
                    CodeBlock.of("%T(%L)", responseResultType, response.receive(snap, builder))
                }
                receiveCodeBlock.addStatement("%L -> %L", response.code.value, receiver)
            }

            receiveCodeBlock.addStatement("else -> throw %T(response.status)", UNKNOWN_RESPONSE_EXCEPTION)
            receiveCodeBlock.unindent().add("}")
        }

        fun processParameter(parameter: Parameter, output: CodeBlock.Builder) {
            var hasDefaultCondition = false
            val parameterSchema = snap.resolve(parameter.codegenSchema)
            val parameterPoet = snap.resolvePoet(parameterSchema)
            val parameterType = parameterPoet?.selfType
            val parameterName =
                parameter.extensions?.get("x-kiscord-codegen-name") as String? ?: parameter.name.toKiscordCase()

            if (parameterSchema.default != null) {
                if (parameterType != null) {
                    val defaultValue = snap.valueResolver.resolve(parameterType, parameterSchema.default)
                    output.beginControlFlow("if (%N != %L)", parameterName, defaultValue)
                    hasDefaultCondition = true
                }
            } else if (!parameter.required) {
                output.beginControlFlow("if (%N != null)", parameterName)
                hasDefaultCondition = true
            }

            fun processParameterType(schema: Schema<*>, value: CodeBlock): CodeBlock {
                val type = snap.resolveType(schema)
                if (schema.type == "string" && type == STRING) {
                    return CodeBlock.of("%L", value)
                } else if (type is ClassName) {
                    when (snap.resolvePoet(type)) {
                        is EnumPoet.StringSchema -> return CodeBlock.of("%T.enumToValue(%L)", type, value)
                        is EnumPoet<*> -> return CodeBlock.of("%T.enumToValue(%L).toString()", type, value)
                    }
                } else if (schema is ArraySchema) {
                    when (parameter.style) {
                        Parameter.StyleEnum.FORM -> ","
                        Parameter.StyleEnum.SPACEDELIMITED -> " "
                        Parameter.StyleEnum.PIPEDELIMITED -> "|"
                        else -> null
                    }?.let { separator ->
                        return buildCodeBlock {
                            beginControlFlow("%L.joinToString(separator = %S)", value, separator)
                            addStatement("%L", processParameterType(schema.items, CodeBlock.of("it")))
                            unindent()
                            add("}")
                        }
                    }
                }
                return CodeBlock.of("%L.toString()", value)
            }

            val valueCodeBlock = processParameterType(parameterSchema, CodeBlock.of("%N", parameterName))

            when (parameter) {
                is QueryParameter -> output.add("parameters.append(%S, %L)\n", parameter.name, valueCodeBlock)
                is HeaderParameter -> output.add("headers[%S] = %L\n", parameter.name, valueCodeBlock)
                else -> throw IllegalArgumentException("Unsupported parameter: $parameter")
            }

            if (hasDefaultCondition) {
                output.endControlFlow()
            }
        }

        requestCodeBlock.beginControlFlow(
            "%N.%M", ApiPoet.CLIENT,
            MemberName("kiscord.ktor", "request")
        )
        requestCodeBlock.add(
            "method = %T.%N\n",
            HTTP_METHOD,
            method.name.lowercase().replaceFirstChar { it.titlecase() })

        acceptMediaTypes.forEach { accept ->
            requestCodeBlock.addStatement(
                "%M(%L)",
                MemberName("io.ktor.client.request", "accept"),
                accept.toCodeBlock()
            )
        }

        parameters.filterIsInstance<HeaderParameter>().forEach { processParameter(it, requestCodeBlock) }

        with(body) { process(snap, sink, operationName, builder, requestCodeBlock, resultType, parameters) }

        requestCodeBlock.beginControlFlow("url")

        val pathSegments = mutableListOf<CodeBlock>()

        resolveParts(snap).forEachIndexed { index, part ->
            when (part) {
                is PathPart.Constant -> {
                    val stringPart = when (index) {
                        0 -> part.part.trimStart('/')
                        else -> part.part
                    }
                    stringPart.split('/').filter { it.isNotBlank() }.mapTo(pathSegments) {
                        CodeBlock.of("%S", it.encodeURLPathPart())
                    }
                }
                is PathPart.Variable -> {
                    val encoder = part.poet.urlEncoder
                    if (encoder != null) {
                        pathSegments += CodeBlock.of("%L.%M()", part.propertyName, encoder)
                    } else {
                        pathSegments += CodeBlock.of("%L.toString()", part.propertyName)
                    }
                }
            }
        }

        requestCodeBlock.addStatement("%M(%L)", APPEND_ENCODED_PATH_SEGMENTS, pathSegments.joinToCode())

        parameters.filterIsInstance<QueryParameter>().forEach { processParameter(it, requestCodeBlock) }

        requestCodeBlock.endControlFlow()

        if (operation.security?.isEmpty() == true) {
            requestCodeBlock.add("%M()\n", ANONYMOUS)
        }

        when (responses.size) {
            0 -> throw IllegalStateException()
            1 -> singleResponseRoutine(responses.single())
            else -> multipleResponseRoutine()
        }

        requestCodeBlock.unindent().add("}")

        builder.addStatement(
            "return %L", CodeBlock.builder()
                .beginControlFlow("%L.execute·{·response·->", requestCodeBlock.build())
                .add("%L\n", receiveCodeBlock.build())
                .endControlFlow().build()
        )

        return builder
    }

    companion object {
        val ANONYMOUS = MemberName("kiscord.ktor", "anonymous")
        val BODY = MemberName("io.ktor.client.call", "body")
        val ENCODED_PATH = MemberName("io.ktor.http", "encodedPath")
        val RESPONSE_RESULT = ClassName("kiscord.api", "ResponseResult")
        val SET_BODY = MemberName("io.ktor.client.request", "setBody")
        val TAKE_FROM = MemberName("io.ktor.http", "takeFrom")
        val UNKNOWN_RESPONSE_EXCEPTION = ClassName("kiscord.api", "UnknownResponseException")
        val URL = MemberName("kiscord", "url")
        val HTTP_METHOD = ClassName("io.ktor.http", "HttpMethod")
        val APPEND_PATH_SEGMENTS = MemberName("io.ktor.http", "appendPathSegments")
        val APPEND_ENCODED_PATH_SEGMENTS = MemberName("io.ktor.http", "appendEncodedPathSegments")
    }

    sealed class RequestBodyRoutine {
        open fun getOperationName(defaultName: String): String = defaultName
        open fun OperationPoet.process(
            snap: PoetSnap,
            sink: PoetSink,
            operationName: String,
            builder: FunSpec.Builder,
            requestCodeBlock: CodeBlock.Builder,
            resultType: TypeName,
            parameters: List<Parameter>
        ) = Unit

        object Empty : RequestBodyRoutine()

        open class SimpleOrFused(
            val contentType: ContentType,
            val mediaType: MediaType,
            val fusibleMediaType: MediaType? = null
        ) : RequestBodyRoutine() {
            override fun getOperationName(defaultName: String): String {
                val operationNameForBody = mediaType.extensions?.get("x-kiscord-codegen-operation-name") as String?
                if (operationNameForBody != null) return operationNameForBody
                return super.getOperationName(defaultName)
            }

            override fun OperationPoet.process(
                snap: PoetSnap,
                sink: PoetSink,
                operationName: String,
                builder: FunSpec.Builder,
                requestCodeBlock: CodeBlock.Builder,
                resultType: TypeName,
                parameters: List<Parameter>
            ) {
                val requestSchema = snap.resolve(mediaType.schema)
                val requestType = snap.resolveType(requestSchema)
                    ?: throw IllegalStateException("Unable to resolve request body type for ${mediaType.schema}")
                val requestPoet = snap.resolvePoet(requestType.copy(nullable = false), requestSchema)
                builder.addParameter(ParameterSpec.builder("body", requestType).build())

                if (fusibleMediaType != null) {
                    val fusibleMediaSchema = snap.resolve(fusibleMediaType.schema)
                    val mediaSchema = snap.resolve(mediaType.schema)
                    val fusibleProperties = mediaSchema.codegenFusible.orEmpty()
                    val multipartProperties = fusibleMediaSchema.properties.toMutableMap()
                    check(fusibleProperties.isNotEmpty())
                    val guard = fusibleProperties.map { (name) ->
                        val fusiblePoet = snap.resolvePoet(multipartProperties[name]!!)!!
                        if (fusiblePoet is CollectionFakePoet) {
                            CodeBlock.of("!%N.%L.isNullOrEmpty()", BODY, name.toKiscordCase())
                        } else {
                            CodeBlock.of("%N.%L != null", BODY, name.toKiscordCase())
                        }
                    }.joinToCode(separator = " && ")

                    sink.addOptIn(ClassName("kiscord.ktor", "SerializableMultiPartDsl"), target = PoetSink.Target.Extra)
                    requestCodeBlock
                        .beginControlFlow("if (%L)", guard)
                        .beginControlFlow("%M", MemberName("kiscord.ktor", "multipartBody"))


                    fusibleMediaSchema.codegenFusible?.forEach { property ->
                        val fusiblePoet = snap.resolvePoet(multipartProperties.remove(property.name)!!)!!
                        requestCodeBlock.beginControlFlow("part")
                            .add("%L",
                                fusiblePoet.toPartData(
                                    CodeBlock.of("%N", "body"),
                                    partName = CodeBlock.of("%S", property.name),
                                    overrideContentType = property.contentType?.let { ContentType.parse(it) }
                                        ?: ContentType.Application.Json)
                            )
                            .endControlFlow()
                    }

                    multipartProperties.forEach { (name, schema) ->
                        val fusiblePoet = snap.resolvePoet(schema)!!
                        if (fusiblePoet is CollectionFakePoet) {
                            requestCodeBlock.beginControlFlow("%N.%N.forEachIndexed { index, value ->", "body", name)
                                .beginControlFlow("part")
                                .add(
                                    "%L",
                                    fusiblePoet.itemPoet.toPartData(
                                        CodeBlock.of("value"),
                                        partName = CodeBlock.of("%P", "$name[\$index]")
                                    )
                                )
                                .endControlFlow()
                                .endControlFlow()
                        } else {
                            requestCodeBlock.beginControlFlow("part")
                                .add(
                                    "%L",
                                    fusiblePoet.toPartData(
                                        CodeBlock.of("%N.%N", "body", name),
                                        partName = CodeBlock.of("%S", name)
                                    )
                                )
                                .endControlFlow()
                        }
                    }

                    requestCodeBlock.endControlFlow()
                        .nextControlFlow("else")
                        .addStatement("%M(%L)", MemberName("io.ktor.http", "contentType"), contentType.toCodeBlock())
                        .add("%M<%T>(body)\n", SET_BODY, requestType)
                        .endControlFlow()
                } else if (requestPoet is ObjectPoet<*> && requestPoet.isMultipartData) {
                    sink.addOptIn(ClassName("kiscord.ktor", "SerializableMultiPartDsl"), target = PoetSink.Target.Extra)
                    requestCodeBlock.beginControlFlow("%M", MemberName("kiscord.ktor", "multipartBody"))
                        .addStatement("%M(body)", MemberName(requestPoet.selfType.packageName, ObjectPoet.TAKE_FROM))
                        .endControlFlow()
                } else {
                    requestCodeBlock.addStatement(
                        "%M(%L)",
                        MemberName("io.ktor.http", "contentType"),
                        contentType.toCodeBlock()
                    )
                    requestCodeBlock.add("%M<%T>(body)\n", SET_BODY, requestType)
                }

                if (requestPoet is BuildablePoet && requestPoet.withBuilder) {
                    val builderFun = FunSpec.builder(operationName)
                        .addModifiers(KModifier.INLINE, KModifier.SUSPEND)
                        .receiver((sink as PoetSink.InnerClass).typeName)
                        .addAnnotation(GENERATED)
                        .addAnnotation(DISCORD_API_DSL)
                        .returns(resultType)

                    (operation.description ?: operation.summary)?.let {
                        builderFun.addKdoc("%L\n\n", it)
                    }

                    operation.externalDocs?.writeTo(builderFun)

                    if (operation.deprecated == true) {
                        builderFun.addAnnotation(
                            AnnotationSpec.builder(Deprecated::class)
                                .addMember("message = %S", "Deprecated in OpenAPI Schema")
                                .build()
                        )
                    }

                    val parametersCodeBlocks = mutableListOf<CodeBlock>()
                    parameters.forEach {
                        val spec = it.toSpec(snap)
                        builderFun.addParameter(spec)
                        parametersCodeBlocks += CodeBlock.of("%1L·=·%1L", spec.name)
                        it.description?.let {
                            builderFun.addKdoc("@param %N %L\n", spec.name, it)
                        }
                    }

                    builderFun.addParameter(
                        ParameterSpec.builder(
                            "body",
                            LambdaTypeName.get(receiver = requestPoet.builderType, returnType = UNIT)
                        )
                            .defaultValue("{}")
                            .addAnnotation(BUILDER_DSL)
                            .build()
                    )


                    parametersCodeBlocks += CodeBlock.of(
                        "body·=·%L",
                        requestPoet.formatBuild(CodeBlock.of("%L.apply(body)", requestPoet.formatCreateBuilder()))
                    )
                    builderFun.addStatement("return %L(%L)", operationName, parametersCodeBlocks.joinToCode())
                    sink.addFunction(builderFun.build(), target = PoetSink.Target.Extra)
                }
            }
        }
    }

    sealed class ResponseRoutine(val code: HttpStatusCode, val name: String?, val description: String?) {
        abstract val acceptMediaTypes: Set<ContentType>

        abstract fun returnType(snap: PoetSnap, singleRoutine: Boolean): TypeName
        abstract fun receive(snap: PoetSnap, method: FunSpec.Builder): CodeBlock

        class NullableMedia(
            private val media: Media,
            private val nullCode: HttpStatusCode = HttpStatusCode.NotFound
        ) : ResponseRoutine(media.code, media.name, media.description) {
            override val acceptMediaTypes: Set<ContentType> get() = media.acceptMediaTypes

            override fun returnType(snap: PoetSnap, singleRoutine: Boolean): TypeName {
                return media.returnType(snap, singleRoutine).copy(nullable = true)
            }

            override fun receive(snap: PoetSnap, method: FunSpec.Builder) = buildCodeBlock {
                beginControlFlow("when(response.status.value)")
                addStatement("%L -> %L", media.code.value, media.receive(snap, method))
                addStatement("%L -> %L", nullCode.value, null)
                addStatement(
                    "else -> throw %T(%P)",
                    ClassName("kotlin", "IllegalStateException"),
                    "Don't know how to handle \${response.status}"
                )
                endControlFlow()
            }

            override fun toString(): String = "NullableMedia($media)"
        }

        class Media(
            code: HttpStatusCode,
            name: String?,
            description: String?,
            content: Content
        ) : ResponseRoutine(code, name, description) {
            private val content = content.mapKeys { ContentType.parse(it.key) }

            override val acceptMediaTypes: Set<ContentType>
                get() = content.keys.toSet()

            private val contentType: ContentType
            private val media: MediaType

            init {
                this.content.entries.single().let {
                    contentType = it.key
                    media = it.value
                }
            }

            private val isBinary = binaryTypes.any { contentType.match(it) }

            override fun returnType(snap: PoetSnap, singleRoutine: Boolean): TypeName {
                if (isBinary) {
                    return ClassName("io.ktor.utils.io.core", "ByteReadPacket")
                }
                return snap.resolveType(media.schema)
                    ?: throw IllegalArgumentException("No type for ${media.schema}")
            }

            override fun receive(snap: PoetSnap, method: FunSpec.Builder) = buildCodeBlock {
                if (isBinary) {
                    add("response.%M().%M()", BODY_AS_CHANNEL, MemberName("io.ktor.utils.io", "readRemaining"))
                } else {
                    val mediaType = returnType(snap, false)
                    add("response.%M<%T>()", BODY, mediaType)
                }
            }

            override fun toString(): String = "Media($media)"
        }

        class Code(code: HttpStatusCode, name: String?, description: String?) :
            ResponseRoutine(code, name, description) {
            override val acceptMediaTypes: Set<ContentType> get() = emptySet()

            override fun returnType(snap: PoetSnap, singleRoutine: Boolean): TypeName =
                if (singleRoutine) BOOLEAN else UNIT

            override fun receive(snap: PoetSnap, method: FunSpec.Builder) = buildCodeBlock {
                add("response.status.value == %L", code.value)
            }

            override fun toString(): String = "Code($code)"
        }
    }

}

private val binaryTypes = setOf(
    ContentType.Audio.Any,
    ContentType.Image.Any,
    ContentType.Video.Any
)

private val Parameter.location: ParameterIn
    get() = enumValues<ParameterIn>().find { it.toString() == `in` } ?: ParameterIn.DEFAULT

private object MostValuedParameterInComparator : Comparator<ParameterIn> {
    private val order = listOf(ParameterIn.PATH, ParameterIn.QUERY, ParameterIn.HEADER, ParameterIn.COOKIE).asReversed()
    override fun compare(o1: ParameterIn?, o2: ParameterIn?): Int = order.indexOf(o2) - order.indexOf(o1)
}

private data class FusibleProperty(
    val name: String,
    val contentType: String? = null
)

private val Schema<*>.codegenFusible
    get() = extensions?.get("x-kiscord-codegen-fusible")?.let {
        codegenMapper.convertValue<List<FusibleProperty>>(it)
    }

