/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.codegen.poet

import com.fasterxml.jackson.module.kotlin.*
import com.squareup.kotlinpoet.*
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import io.swagger.v3.oas.models.media.*
import kiscord.codegen.*

class GatewayEventNamePoet(schema: Schema<String>, selfName: ClassName) : EnumPoet.StringSchema(schema, selfName) {
    override fun process(snap: PoetSnap, sink: PoetSink, builder: TypeSpec.Builder) {
        super.process(snap, sink, builder)

        val serializerType =
            KSERIALIZER.parameterizedBy(WildcardTypeName.producerOf(GATEWAY_DISPATCH_DATA)).copy(nullable = true)

        builder.primaryConstructor(
            FunSpec.constructorBuilder()
                .addParameter(ParameterSpec.builder("serializer", serializerType).defaultValue("null").build())
                .build()
        )

        builder.addProperty(
            PropertySpec.builder("serializer", serializerType)
                .addModifiers(KModifier.INTERNAL)
                .initializer("serializer").build()
        )

        val dispatch = FunSpec.builder("dispatch")
            .addModifiers(KModifier.INTERNAL, KModifier.SUSPEND)
            .receiver(PIPELINE_CONTEXT.parameterizedBy(STAR, GATEWAY_CALL))
            .addParameter("payload", GATEWAY_PAYLOAD.parameterizedBy(STAR))
            .returns(ANY.copy(nullable = true))
            .beginControlFlow("return when(payload.eventName)")

        enumValues.forEach { enumValue ->
            val dataRef = enumValue.extensions["x-kiscord-codegen-data-ref"] ?: return@forEach
            val dataSchema = snap.resolve(codegenMapper.convertValue<Schema<*>>(dataRef))
            val dataType = snap.resolveType(dataSchema) ?: return@forEach
            val passthrough = dataSchema.extensions["x-kiscord-codegen-dispatch-passthrough"] == true
            val parameters = mutableListOf<CodeBlock>()
            if (!passthrough) parameters += CodeBlock.of("this")
            dispatch.addStatement(
                "%T.%L·->·(payload.data·as·%T).dispatch(%L)",
                selfType,
                enumValue.identifier,
                dataType,
                parameters.joinToCode()
            )
        }

        dispatch.addStatement("else -> null").endControlFlow()

        sink.addFunction(dispatch.build(), target = PoetSink.Target.Extra)
    }

    override fun processEnumValue(
        snap: PoetSnap,
        sink: PoetSink,
        enumValue: EnumValue,
        builder: TypeSpec.Builder,
        extraData: Map<String, Schema<*>>?
    ) {
        enumValue.extensions["x-kiscord-codegen-data-ref"]?.let {
            val schema = codegenMapper.convertValue<Schema<*>>(it)
            val type = snap.resolveType(schema) as ClassName
            val poet = snap.resolvePoet(type, schema)
            builder.addSuperclassConstructorParameter(poet.serializerInstance(type))
        }

        super.processEnumValue(snap, sink, enumValue, builder, extraData)
    }

    fun mapping(snap: PoetSnap): Map<EnumValue, TypeName?> = enumValues.associateWith { enumValue ->
        enumValue.extensions["x-kiscord-codegen-data-ref"]?.let {
            val schema = codegenMapper.convertValue<Schema<*>>(it)
            snap.resolveType(schema)
        }
    }
}
