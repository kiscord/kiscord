/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.codegen.poet

import com.squareup.kotlinpoet.*
import kiscord.codegen.*

class BuildConfigPoet(override val selfType: ClassName, fields: List<String>) : ClassPoet {
    override val category: String? get() = null
    private val fields = fields.map { Field.parse(it) }

    override fun build(snap: PoetSnap, sink: PoetSink): TypeSpec {
        sink.addAnnotation(
            AnnotationSpec.builder(Suppress::class)
                .addMember("%S", "RedundantVisibilityModifier")
                .build()
        )

        val builder = TypeSpec.objectBuilder(selfType)
            .addAnnotation(GENERATED)
            .addModifiers(KModifier.INTERNAL)
        fields.forEach { field ->
            builder.addProperty(
                PropertySpec.builder(field.name, field.type)
                    .addModifiers(KModifier.CONST)
                    .initializer(field.value)
                    .build()
            )
        }
        return builder.build()
    }

    data class Field(
        val name: String,
        val type: TypeName,
        val value: CodeBlock
    ) {
        companion object {
            fun parse(line: String): Field {
                val parts = line.split(':', limit = 3)
                val name = parts[0]
                val type = when (parts[1]) {
                    "kotlin.String" -> STRING
                    "kotlin.Int" -> INT
                    "kotlin.Boolean" -> BOOLEAN
                    else -> throw IllegalArgumentException()
                }
                val value = when (type) {
                    STRING -> CodeBlock.of("%S", parts[2])
                    INT -> CodeBlock.of("%L", parts[2].toInt())
                    BOOLEAN -> CodeBlock.of("%L", parts[2].toBoolean())
                    else -> throw IllegalArgumentException()
                }
                return Field(name, type, value)
            }
        }
    }
}
