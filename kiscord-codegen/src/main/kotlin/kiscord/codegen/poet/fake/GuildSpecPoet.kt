/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.codegen.poet.fake

import com.squareup.kotlinpoet.*
import kiscord.codegen.poet.*

object GuildSpecPoet : FakePoet<ClassName>(ClassName("kiscord.api", "GuildSpec")), ExtensionsForDTO {
    private val serializerType = ClassName("kiscord.api", "UnavailableGuild", "SpecSerializer")
        .withTag(SerializerMetadata(isObject = true))
    private val setSerializerType = ClassName("kiscord.api", "UnavailableGuild", "SpecSetSerializer")
        .withTag(SerializerMetadata(isObject = true))

    override fun serializerTypeFor(type: TypeName): ClassName? = when {
        type is ParameterizedTypeName && type.rawType == SET && type.typeArguments[0] == selfType -> setSerializerType
        type == selfType -> serializerType
        else -> null
    }

    override fun isSubtypeOf(type: TypeName): Boolean {
        return type == ClassName("kiscord.api", "Guild")
    }
}
