/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.codegen.poet.fake

import com.squareup.kotlinpoet.*
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import io.swagger.v3.oas.models.media.*
import kiscord.codegen.*
import kiscord.codegen.poet.*

open class EnumSetFakePoet<out T>(private val schema: Schema<T>, val itemPoet: EnumPoet<*>) :
    FakePoet<ParameterizedTypeName>(SET.parameterizedBy(itemPoet.selfType)),
    ValueResolver.MonoResolver, ExtensionsForDTO {
    override val isExternal: Boolean get() = true

    override fun serializerTypeFor(type: TypeName): ClassName? = itemPoet.serializerTypeFor(type)

    override fun resolveValue(value: Any?): CodeBlock {
        if (value == null) return CodeBlock.of("null")
        if (value is Int) {
            check(schema.enumIsBitmask == true) { "EnumSet should be bitmask to support this" }
            val enumValue = itemPoet.enumValues.single { it.actualIntValue == value }
            val valueLiteral = with(itemPoet) {
                enumValue.valueLiteral
            }
            return CodeBlock.of("%T.of(%L)", selfType, valueLiteral)
        }
        throw IllegalArgumentException("Unknown enum value type: ${value::class}")
    }

    override fun <S : Schema<Any?>> generateExtensionsForDTO(
        poet: ObjectPoet<S>,
        member: ObjectPoet<S>.ObjectMember,
        kind: ObjectPoet.Kind,
        sink: PoetSink,
        snap: PoetSnap
    ) {
        if (kind != ObjectPoet.Kind.Builder || !poet.withBuilder) return
        val enumReference = schema.enumReference ?: return
        val enumType = snap.resolveType(snap.resolve(enumReference)) ?: return
        val plurals = member.schema.codegenPlurals ?: Plurals()

        fun generateFun(name: String, parameterSpec: ParameterSpec) {
            val funBuilder = FunSpec.builder(name)
            funBuilder.addAnnotation(GENERATED)
            funBuilder.addAnnotation(BUILDER_DSL)
            funBuilder.addKdoc("Adds [%L] to according set in the [builder][%T]", name, poet.builderType)
            funBuilder.receiver(poet.builderType)
            funBuilder.addParameter(parameterSpec)
            if (member.isBoxed) {
                funBuilder.beginControlFlow(
                    "this.%1N = this.%1N.%2M",
                    member.propertyName,
                    MemberName("kiscord.api", "map")
                ).addStatement(
                    "(it.orEmpty() + %1N).%2M()", name, TO_ENUM_SET
                ).endControlFlow()
            } else if (member.required) {
                funBuilder.addStatement("this.%1N = (this.%1N + %2N).%3M()", member.propertyName, name, TO_ENUM_SET)
            } else {
                funBuilder.addStatement(
                    "this.%1N = (this.%1N.orEmpty() + %2N).%3M()",
                    member.propertyName,
                    name,
                    TO_ENUM_SET
                )
            }

            sink.addFunction(funBuilder.build(), target = PoetSink.Target.Extra)
        }

        val singular = plurals.singular?.toKiscordCase() ?: member.propertyName
        generateFun(singular, ParameterSpec.builder(singular, enumType).build())
        generateFun(member.propertyName, ParameterSpec.builder(member.propertyName, enumType).addModifiers(KModifier.VARARG).build())
        generateFun(member.propertyName, ParameterSpec.builder(member.propertyName, COLLECTION.parameterizedBy(enumType)).build())
    }
}
