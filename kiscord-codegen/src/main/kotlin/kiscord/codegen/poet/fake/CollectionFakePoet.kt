/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.codegen.poet.fake

import com.squareup.kotlinpoet.*
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import io.swagger.v3.oas.models.media.*
import kiscord.codegen.*
import kiscord.codegen.poet.*

sealed class CollectionFakePoet private constructor(
    internal val collectionType: ClassName,
    internal val mutableCollectionType: ClassName,
    internal val itemType: TypeName,
    internal val itemPoet: TypePoet
) : FakePoet<ParameterizedTypeName>(collectionType.parameterizedBy(itemType)), BuildablePoet, ExtensionsForDTO {
    override val withSpec: Boolean get() = true
    override val withBuilder: Boolean get() = true
    override val specType: TypeName
        get() = collectionType.parameterizedBy(itemSpecType)
    override val builderType: TypeName
        get() = mutableCollectionType.parameterizedBy(itemBuilderType)

    internal val itemSpecType: TypeName
        get() = when {
            itemPoet is BuildablePoet && itemPoet.withSpec -> itemPoet.specType
            else -> itemType
        }

    internal val itemBuilderType: TypeName
        get() = when {
            itemPoet is BuildablePoet && itemPoet.withBuilder -> itemPoet.builderType
            else -> itemType
        }

    override fun serializerTypeFor(type: TypeName) = itemPoet.serializerTypeFor(type)

    override fun <S : Schema<Any?>> generateExtensionsForDTO(
        poet: ObjectPoet<S>,
        member: ObjectPoet<S>.ObjectMember,
        kind: ObjectPoet.Kind,
        sink: PoetSink,
        snap: PoetSnap
    ) {
        if (kind != ObjectPoet.Kind.Builder || !poet.withBuilder) return

        fun formatToBuilder(value: CodeBlock, preserveBuilder: Boolean = false) = when {
            itemPoet is BuildablePoet && itemPoet.withBuilder -> itemPoet.formatToBuilder(value, preserveBuilder)
            else -> value
        }

        fun createTemplate(name: String = member.propertyName) = FunSpec.builder(name)
            .addAnnotation(GENERATED)
            .addAnnotation(BUILDER_DSL)
            .addKdoc("Adds [%L] to according collection in the [builder][%T]", name, poet.builderType)
            .receiver(poet.builderType)

        fun createCollectionLikeTemplate(): FunSpec.Builder {
            val itValue = CodeBlock.of("it")
            val toBuilder = formatToBuilder(itValue, true)
            val template = createTemplate()
            if (itValue == toBuilder) {
                template.addStatement("%L.addAll(%N)", member.formatOrCreate(CodeBlock.of("this")), member.propertyName)
            } else {
                template.beginControlFlow(
                    "%N.mapTo(%L)",
                    member.propertyName,
                    member.formatOrCreate(CodeBlock.of("this"))
                )
                    .addStatement("%L", toBuilder)
                    .endControlFlow()
            }
            return template
        }

        val plurals = member.schema.codegenPlurals ?: Plurals()

        val singleFun = createTemplate(plurals.singular?.toKiscordCase() ?: member.propertyName)
            .addParameter(ParameterSpec.builder(member.propertyName, itemSpecType).build())
            .addStatement(
                "%L.add(%L)",
                member.formatOrCreate(CodeBlock.of("this")),
                formatToBuilder(CodeBlock.of("%N", member.propertyName))
            )
            .build()

        val iterableFun = createCollectionLikeTemplate()
            .addParameter(ParameterSpec.builder(member.propertyName, ITERABLE.parameterizedBy(itemSpecType)).build())
            .build()

        val sequenceFun = createCollectionLikeTemplate()
            .addParameter(ParameterSpec.builder(member.propertyName, SEQUENCE.parameterizedBy(itemSpecType)).build())
            .build()

        val varargFun = createCollectionLikeTemplate()
            .addParameter(
                ParameterSpec.builder(member.propertyName, itemSpecType).addModifiers(KModifier.VARARG).build()
            )
            .build()


        sink.addFunction(singleFun, target = PoetSink.Target.Extra)
        sink.addFunction(iterableFun, target = PoetSink.Target.Extra)
        sink.addFunction(sequenceFun, target = PoetSink.Target.Extra)
        sink.addFunction(varargFun, target = PoetSink.Target.Extra)

        if (itemPoet is BuildablePoet && itemPoet.withBuilder) {
            val builderLambda = LambdaTypeName.get(receiver = itemPoet.builderType, returnType = UNIT)
            val builderFun = createTemplate(plurals.singular?.toKiscordCase() ?: member.propertyName)
                .addParameter(ParameterSpec.builder("block", builderLambda).build())
                .addStatement(
                    "%L.add(%T.%M(block))",
                    member.formatOrCreate(CodeBlock.of("this")),
                    itemType,
                    MemberName("kiscord.builder", "builder")
                )
                .addModifiers(KModifier.INLINE)
                .build()

            sink.addFunction(builderFun, target = PoetSink.Target.Extra)
        }
    }

    class OfList(itemType: TypeName, itemPoet: TypePoet) : CollectionFakePoet(LIST, MUTABLE_LIST, itemType, itemPoet) {
        override fun formatToBuilder(value: CodeBlock, preserveBuilder: Boolean): CodeBlock =
            if (itemType != itemSpecType && itemPoet is BuildablePoet) {
                CodeBlock.of(
                    "%L.mapTo(%L) { %L }",
                    value,
                    formatCreateBuilder(),
                    itemPoet.formatToBuilder(CodeBlock.of("it"), preserveBuilder = preserveBuilder)
                )
            } else {
                CodeBlock.of("%L.toMutableList()", value)
            }

        override fun formatBuild(builder: CodeBlock): CodeBlock =
            if (itemType != itemSpecType && itemPoet is BuildablePoet) {
                CodeBlock.of("%L.map { %L }", builder, itemPoet.formatBuild(CodeBlock.of("it")))
            } else {
                CodeBlock.of("%L.toList()", builder)
            }

        override fun formatCreateBuilder(): CodeBlock = CodeBlock.of("mutableListOf<%T>()", itemBuilderType)

        override fun serializerInstanceForSelf(): CodeBlock {
            return CodeBlock.of("%M(%L)", LIST_SERIALIZER, itemPoet.serializerInstance(itemType))
        }
    }

    class OfSet(itemType: TypeName, itemPoet: TypePoet) : CollectionFakePoet(SET, MUTABLE_SET, itemType, itemPoet) {
        override fun formatToBuilder(value: CodeBlock, preserveBuilder: Boolean): CodeBlock =
            if (itemType != itemSpecType && itemPoet is BuildablePoet) {
                CodeBlock.of(
                    "%L.mapTo(%L) { %L }",
                    value,
                    formatCreateBuilder(),
                    itemPoet.formatToBuilder(CodeBlock.of("it"), preserveBuilder = preserveBuilder)
                )
            } else {
                CodeBlock.of("%L.toMutableSet()", value)
            }

        override fun formatBuild(builder: CodeBlock): CodeBlock =
            if (itemType != itemSpecType && itemPoet is BuildablePoet) {
                CodeBlock.of(
                    "%L.mapTo(%L) { %L }",
                    builder,
                    formatCreateBuilder(),
                    itemPoet.formatBuild(CodeBlock.of("it"))
                )
            } else {
                CodeBlock.of("%L.toSet()", builder)
            }

        override fun formatCreateBuilder(): CodeBlock = CodeBlock.of("mutableSetOf<%T>()", itemBuilderType)

        override fun serializerInstanceForSelf(): CodeBlock {
            return CodeBlock.of("%M(%L)", SET_SERIALIZER, itemPoet.serializerInstance(itemType))
        }
    }
}
