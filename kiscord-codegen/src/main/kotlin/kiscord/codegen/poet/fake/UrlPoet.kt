/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.codegen.poet.fake

import com.squareup.kotlinpoet.*
import io.swagger.v3.oas.models.media.*
import kiscord.codegen.*
import kiscord.codegen.poet.*

object UrlPoet : FakePoet<ClassName>(URL), ExtensionsForDTO {
    override fun serializerTypeForUse(snap: PoetSnap, visitor: RecursivePoetVisitor): Set<ClassName> {
        return setOf(ClassName("kiscord.serialization", "UrlSerializer").withTag(SerializerMetadata(isObject = true)))
    }

    override fun <S : Schema<Any?>> generateExtensionsForDTO(
        poet: ObjectPoet<S>,
        member: ObjectPoet<S>.ObjectMember,
        kind: ObjectPoet.Kind,
        sink: PoetSink,
        snap: PoetSnap
    ) {
        if (kind != ObjectPoet.Kind.Builder) return
        val funBuilder = FunSpec.builder(member.propertyName)
            .addAnnotation(GENERATED)
            .addAnnotation(BUILDER_DSL)
            .addKdoc("Creates `%N` from the given string", member.propertyName)
            .receiver(poet.builderType)
            .addParameter(member.propertyName, STRING)
        if (member.isBoxed) {
            funBuilder.addStatement("this.%1N = %3T.of(%2T(%1N))", member.propertyName, selfType, BOX)
        } else {
            funBuilder.addStatement("this.%1N = %2T(%1N)", member.propertyName, selfType)
        }
        sink.addFunction(funBuilder.build(), target = PoetSink.Target.Extra)
    }
}
