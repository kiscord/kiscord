/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.codegen.poet.fake

import com.squareup.kotlinpoet.*
import io.swagger.v3.oas.models.media.*
import kiscord.codegen.*
import kiscord.codegen.poet.*

object BoxPoet : FakePoet<ClassName>(BOX), ExtensionsForDTO {
    override fun <S : Schema<Any?>> generateExtensionsForDTO(
        poet: ObjectPoet<S>,
        member: ObjectPoet<S>.ObjectMember,
        kind: ObjectPoet.Kind,
        sink: PoetSink,
        snap: PoetSnap
    ) {
        if (kind != ObjectPoet.Kind.Builder || !poet.withBuilder) return
        val boxType = member.builderType
        if (boxType !is ParameterizedTypeName || boxType.rawType != BOX) return
        val valueType = boxType.typeArguments[0]
        val funSpec = ObjectPoet.DefaultExtensions.basicTemplate(member)
            .receiver(poet.builderType)
            .addParameter(member.propertyName, valueType)
            .addStatement("this.%1N = %2T.of(%1N)", member.propertyName, BOX)

        sink.addFunction(funSpec.build(), target = PoetSink.Target.Extra)
    }
}
