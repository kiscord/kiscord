/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.codegen.poet

import com.squareup.kotlinpoet.*
import io.swagger.v3.oas.models.tags.*
import kiscord.codegen.*

class ApiPoet(private val tag: Tag) : ClassPoet {
    override val category: String? get() = null
    override val selfType: ClassName

    init {
        val packageName = tag.extensions.getValue("x-kiscord-codegen-package") as String
        val name = tag.extensions.getOrDefault("x-kiscord-codegen-name", "${tag.name}Api") as String
        selfType = ClassName(packageName, name)
    }

    override fun build(snap: PoetSnap, sink: PoetSink): TypeSpec {
        sink.addAnnotation(
            AnnotationSpec.builder(Suppress::class)
                .addMember("%S", "RemoveExplicitTypeArguments")
                .useSiteTarget(AnnotationSpec.UseSiteTarget.FILE)
                .build(),
            target = PoetSink.Target.Extra
        )

        val builder = TypeSpec.classBuilder(selfType)
            .addAnnotation(GENERATED)
            .addAnnotation(DISCORD_API_DSL)
            .addAnnotation(JvmInline::class)
            .addModifiers(KModifier.VALUE)
            .primaryConstructor(
                FunSpec.constructorBuilder()
                    .addAnnotation(PUBLISHED_API)
                    .addModifiers(KModifier.INTERNAL)
                    .addParameter(CLIENT, KISCORD)
                    .build()
            )
            .addProperty(
                PropertySpec.builder(CLIENT, KISCORD)
                    .initializer(CLIENT)
                    .build()
            )

        val prop = PropertySpec.builder(tag.name.toKiscordCase(), selfType)
            .addAnnotation(DISCORD_API_DSL)
            .addAnnotation(GENERATED)
            .receiver(KISCORD)
            .getter(
                FunSpec.getterBuilder()
                    .addModifiers(KModifier.INLINE)
                    .addStatement("return %T(this)", selfType)
                    .build()
            )

        tag.description?.let {
            builder.addKdoc("%L\n\n", it)
            prop.addKdoc("%L\n\n", it)
        }

        tag.externalDocs?.writeTo(builder)
        tag.externalDocs?.writeTo(prop)

        val selfSink = PoetSink.InnerClass(builder, selfType, sink)

        snap.resolveOperations(tag).sortedBy { it.operation.operationId }.forEach { operationPoet ->
            val funSpecs = operationPoet.build(snap, selfSink)
                .map { it.receiver(selfType).build() }

            operationPoet.resolveOperationMethods(snap).forEach { name ->
                builder.addKdoc("@see %N\n", name)
            }

            funSpecs.forEach { funSpec ->
                selfSink.addFunction(funSpec, target = PoetSink.Target.Extra)
            }
        }

        snap.resolveDirectInners(selfType).forEach {
            it.addTo(snap, selfSink)
        }

        sink.addProperty(prop.build(), target = PoetSink.Target.Extra)

        return builder.build()
    }

    companion object {
        const val CLIENT = "client"
    }
}
