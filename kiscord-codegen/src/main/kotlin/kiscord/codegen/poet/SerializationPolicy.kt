/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.codegen.poet

import com.fasterxml.jackson.annotation.*
import io.swagger.v3.oas.models.media.*

data class SerializationPolicy(
    val serializer: Boolean = true,
    @JsonProperty("enum-to-value")
    val enumToValue: Boolean = serializer,
    @JsonProperty("value-to-enum")
    val valueToEnum: Boolean = serializer,
    @JsonProperty("all-set")
    val allSet: Boolean = false,
    @JsonProperty("all-mask")
    val allMask: Boolean = false,
    @JsonProperty("literal-format")
    val literalFormat: String? = null,
    val unsigned: Boolean = false,
    @JsonProperty("null-on-unknown")
    val nullOnUnknown: Boolean = false,
    @JsonProperty("optional-serializer")
    val optionalSerializer: Boolean = false,
    @JsonProperty("not-serializable")
    val notSerializable: Boolean = false,
    @JsonProperty("value-type-name")
    val valueTypeName: String? = null,
    @JsonProperty("value-type-data")
    val valueTypeData: Schema<*>? = null,
    @JsonProperty("encode-default")
    val encodeDefault: String? = null,
    @JsonProperty("set-serializer")
    val setSerializer: Boolean = false,
)
