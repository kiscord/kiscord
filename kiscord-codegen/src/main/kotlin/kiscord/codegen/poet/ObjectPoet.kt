/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.codegen.poet

import com.squareup.kotlinpoet.*
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import io.ktor.http.*
import io.swagger.v3.oas.models.media.*
import kiscord.codegen.*
import kiscord.codegen.poet.fake.*

open class ObjectPoet<S : Schema<Any?>>(
    schema: S, selfName: ClassName, val metadata: PoetSnap.PoetResolveMetadata
) : SchemaPoet<Any?, S>(schema, selfName) {
    val serializationPolicy = schema.codegenSerializationPolicy ?: SerializationPolicy()

    final override val withBuilder = schema.codegenGenerateBuilder == true
    final override val withSpec = schema.codegenGenerateSpec == true || withBuilder
    final override val specType: ClassName
    final override val builderType: ClassName

    val updateDataType: ClassName
    val updaterType: ClassName

    override fun formatToBuilder(value: CodeBlock, preserveBuilder: Boolean): CodeBlock =
        CodeBlock.of("%L.%M(preserve = %L)", value, MemberName(selfType.packageName, TO_BUILDER), preserveBuilder)

    override fun formatBuild(builder: CodeBlock): CodeBlock =
        CodeBlock.of("%L.%N()", builder, BUILD)

    override fun formatCreateBuilder(): CodeBlock =
        CodeBlock.of("%T.builder()", selfType)

    init {
        specType = if (withSpec || withBuilder) {
            selfName.peerClass("${selfName.simpleName}$SPEC")
        } else selfName
        builderType = if (withBuilder) {
            selfName.nestedClass(BUILDER)
        } else selfName
        updateDataType = selfName.nestedClass(UPDATE_DATA)
        updaterType = selfName.nestedClass(UPDATER)
    }

    data class Member(
        val name: String,
        val schema: Schema<*>,
        val transient: Boolean = false
    )

    private fun collectMembers(snap: PoetSnap): Map<String, Member> = buildMap {
        collectMembers(snap, schema)
    }

    private fun MutableMap<String, Member>.collectMembers(snap: PoetSnap, schema: Schema<*>) {
        val r = snap.resolve(schema)

        if (r is ComposedSchema) {
            r.allOf?.forEach { collectMembers(snap, it) }
        }

        r.properties?.let {
            putAll(it.mapValues { (name, schema) -> Member(name, schema) })
        }

        r.codegenTransientProperties?.let {
            putAll(it.mapValues { (name, schema) -> Member(name, schema, transient = true) })
        }
    }

    open fun resolveParents(snap: PoetSnap, includeFake: Boolean = false): Map<Schema<*>, TypePoet?> {
        val r = snap.resolve(schema)
        if (r !is ComposedSchema) return emptyMap()
        var list = r.allOf.orEmpty().map { snap.resolve(it) }
        if (!includeFake) {
            list = list.filter { it.codegenClassName != null }
        }
        return list.associateWith { snap.resolvePoet(it) }
    }

    open fun resolveMembers(snap: PoetSnap): List<ObjectMember> {
        return collectMembers(snap).map { (name, s) ->
            ObjectMember(name, snap.resolve(s.schema), s.transient, snap)
        }
    }

    open fun isSingleton(snap: PoetSnap): Boolean = collectMembers(snap).isEmpty()

    override fun createBuilder(snap: PoetSnap): TypeSpec.Builder {
        val builder = if (isSingleton(snap)) {
            TypeSpec.objectBuilder(selfType)
        } else {
            TypeSpec.classBuilder(selfType)
        }

        return builder.addAnnotation(GENERATED)
    }

    override fun process(snap: PoetSnap, sink: PoetSink, builder: TypeSpec.Builder) {
        if (schema is ComposedSchema) {
            schema.oneOf?.forEach { snap.createPoetIfNeeded(it) }
            schema.allOf?.forEach { snap.createPoetIfNeeded(it) }
            schema.anyOf?.forEach { snap.createPoetIfNeeded(it) }
        }

        val members = resolveMembers(snap)

        val constructor = FunSpec.constructorBuilder()
        val specConstructor = FunSpec.constructorBuilder()
        val specConstructorArgs = mutableListOf<CodeBlock>()

        val specBuilder = TypeSpec.interfaceBuilder(specType).addAnnotation(GENERATED)

        val builderBuilder = TypeSpec.classBuilder(builderType).addAnnotation(GENERATED)
        val builderConstructor = FunSpec.constructorBuilder().addKdoc("Creates builder from given values")
        val builderEmptyConstructor = FunSpec.constructorBuilder().addKdoc("Creates empty builder")
        val builderSpecConstructor = FunSpec.constructorBuilder().addKdoc("Creates builder from given spec")
        val builderBuildFunction = FunSpec.builder(BUILD)
        val updateDataBuilder = TypeSpec.classBuilder(updateDataType).addAnnotation(GENERATED)
        val updateDataConstructor = FunSpec.constructorBuilder()
        val updaterBuilder = TypeSpec.classBuilder(updaterType).addAnnotation(GENERATED)

        val toBuilderFunction = FunSpec.builder(TO_BUILDER).addAnnotation(GENERATED).addAnnotation(BUILDER_DSL)
            .addParameter(ParameterSpec.builder(PRESERVE, BOOLEAN).defaultValue("%L", false).build())

        val parent = resolveParents(snap).values.singleOrNull()

        if (withSpec) {
            builder.addSuperinterface(specType)

            if (withBuilder) {
                specBuilder.addKdoc("Specification used by [%T] and [%T]\n\n", selfType, builderType)
            } else {
                specBuilder.addKdoc("Specification used by [%T]\n\n", selfType)
            }

            builderSpecConstructor.addParameter("spec", specType)

            if (parent != null && parent is BuildablePoet && parent.withSpec) {
                specBuilder.addSuperinterface(parent.specType)
            }
        } else {
            if (parent != null && parent is BuildablePoet && parent.withSpec) {
                builder.addSuperinterface(parent.specType)
            }
        }

        if (withBuilder) {
            builderBuilder.addSuperinterface(specType)
                .addSuperinterface(BUILDER_FOR.parameterizedBy(selfType))
                .addKdoc("Builder for [%T]", selfType)
                .addAnnotation(BUILDER_DSL)

            specConstructor.addModifiers(KModifier.PRIVATE)
                .addParameter("builder", builderType)

            toBuilderFunction.receiver(specType).returns(builderType)
                .beginControlFlow("if (%N && this is %T)", PRESERVE, builderType)
                .addStatement("return this")
                .endControlFlow()
                .addStatement("return %T(this)", builderType)
                .addKdoc("Creates [%T] from the given spec\n", builderType)
                .addKdoc("@param %N if true and given spec is the builder return it directly", PRESERVE)

            builderBuildFunction.addModifiers(KModifier.OVERRIDE)
                .returns(selfType)
                .addKdoc("Constructs [%T] from the current builder state", selfType)

            companion.addSuperinterface(BUILDER_SPEC.parameterizedBy(selfType, builderType))
            companion.addFunction(
                FunSpec.builder("builder")
                    .addModifiers(KModifier.OVERRIDE)
                    .returns(builderType)
                    .addStatement("return %T()", builderType)
                    .build()
            )
        }

        if (members.any { it.isDeprecated }) {
            val suppress = AnnotationSpec.builder(Suppress::class)
                .addMember("%S", "DEPRECATION")
                .build()
            builderConstructor.addAnnotation(suppress)
            specConstructor.addAnnotation(suppress)
            builderEmptyConstructor.addAnnotation(suppress)
            builderSpecConstructor.addAnnotation(suppress)
        }

        members.forEach { member ->
            if (!member.isReadOnly) {
                constructor.addParameter(member.asParameterSpec(Kind.Value, snap))
                updateDataConstructor.addParameter(member.asParameterSpec(Kind.Updater, snap))
                updateDataBuilder.addProperty(member.asPropertySpec(false, Kind.Updater, snap, sink))
            }

            builder.addProperty(member.asPropertySpec(withSpec, Kind.Value, snap, sink))

            if (withSpec) {
                if (!member.isExtendedFromParent) {
                    specBuilder.addProperty(member.asPropertySpec(true, Kind.Spec, snap, sink))

                    member.extensions.forEach {
                        it.generateExtensionsForDTO(this, member, Kind.Spec, sink, snap)
                    }
                }
            }

            member.extensions.forEach {
                it.generateExtensionsForDTO(this, member, Kind.Value, sink, snap)
            }

            if (withBuilder) {
                if (!member.isReadOnly) {
                    builderBuilder.addProperty(member.asPropertySpec(true, Kind.Builder, snap, sink))
                    builderConstructor.addParameter(member.asParameterSpec(Kind.Builder, snap))
                }

                member.extensions.forEach {
                    it.generateExtensionsForDTO(this, member, Kind.Builder, sink, snap)
                }

                if (member.poet is BuildablePoet && member.poet.withBuilder) {
                    val memberBuilder = FunSpec.builder(member.propertyName)
                        .addModifiers(KModifier.INLINE)
                        .receiver(builderType.copy(nullable = false))
                        .addParameter(
                            "block",
                            LambdaTypeName.get(member.poet.builderType, returnType = UNIT)
                        )
                        .addAnnotation(GENERATED)
                        .addAnnotation(BUILDER_DSL)

                    if (member.isBoxed) {
                        builderConstructor.beginControlFlow(
                            "this.%1N·=·%1N.%2M { unboxed ->",
                            member.propertyName,
                            BOX_MAP
                        ).addStatement(
                            "%L",
                            member.poet.formatToBuilder(CodeBlock.of("unboxed?"), preserveBuilder = true)
                        ).endControlFlow()
                        builderSpecConstructor.beginControlFlow(
                            "this.%1N·=·spec.%1N.%2M { unboxed ->",
                            member.propertyName,
                            BOX_MAP
                        ).addStatement(
                            "%L",
                            member.poet.formatToBuilder(CodeBlock.of("unboxed?"), preserveBuilder = true)
                        ).endControlFlow()
                        specConstructorArgs += CodeBlock.builder()
                            .indent()
                            .beginControlFlow("\n%1N·=·builder.%1N.%2M { unboxed ->", member.propertyName, BOX_MAP)
                            .addStatement("%L", member.poet.formatBuild(CodeBlock.of("unboxed?")))
                            .unindent()
                            .add("}")
                            .unindent()
                            .build()

                        val memberBuilderCreator = FunSpec.builder("${member.propertyName}OrCreate")
                            .addModifiers(KModifier.INTERNAL)
                            .addAnnotation(PUBLISHED_API)
                            .returns(member.poet.builderType)
                            .beginControlFlow(
                                "return %1N.%2M",
                                member.propertyName,
                                BOX_COMPUTE_IF_NULL
                            ).addStatement(
                                "%2L.also·{ %1N·=·%3T.of(it) }",
                                member.propertyName,
                                member.poet.formatCreateBuilder(),
                                BOX
                            ).endControlFlow()
                            .build()

                        builderBuilder.addFunction(memberBuilderCreator)
                        memberBuilder.addStatement("%N().block()", memberBuilderCreator.name)
                            .addKdoc("Configures inner `%N` object, creating it if necessary", member.propertyName)
                    } else if (member.required && !member.isValueNullable) {
                        builderConstructor.addCode(
                            "this.%N·=·%L\n",
                            member.propertyName,
                            member.poet.formatToBuilder(CodeBlock.of("%N", member.propertyName), preserveBuilder = true)
                        )
                        builderSpecConstructor.addCode(
                            "this.%N·=·%L\n",
                            member.propertyName,
                            member.poet.formatToBuilder(
                                CodeBlock.of("spec.%N", member.propertyName),
                                preserveBuilder = true
                            )
                        )
                        specConstructorArgs += CodeBlock.builder()
                            .indent()
                            .add(
                                "\n%N·=·%L",
                                member.propertyName,
                                member.poet.formatBuild(CodeBlock.of("builder.%N", member.propertyName))
                            ).unindent().build()
                        memberBuilder.addStatement("%N.block()", member.propertyName)
                            .addKdoc("Configures inner `%N` object", member.propertyName)
                    } else {
                        builderConstructor.addCode(
                            "this.%N·=·%L\n",
                            member.propertyName,
                            member.poet.formatToBuilder(
                                CodeBlock.of("%N?", member.propertyName),
                                preserveBuilder = true
                            )
                        )
                        builderSpecConstructor.addCode(
                            "this.%N·=·%L\n",
                            member.propertyName,
                            member.poet.formatToBuilder(
                                CodeBlock.of("spec.%N?", member.propertyName),
                                preserveBuilder = true
                            )
                        )
                        specConstructorArgs += CodeBlock.builder()
                            .indent()
                            .add(
                                "\n%N·=·%L",
                                member.propertyName,
                                member.poet.formatBuild(CodeBlock.of("builder.%N?", member.propertyName))
                            ).unindent().build()

                        val memberBuilderCreator = FunSpec.builder("${member.propertyName}OrCreate")
                            .addModifiers(KModifier.INTERNAL)
                            .addAnnotation(PUBLISHED_API)
                            .returns(member.poet.builderType)
                            .addStatement(
                                "return %1N ?: %2L.also·{ %1N·=·it }",
                                member.propertyName,
                                member.poet.formatCreateBuilder()
                            )
                            .build()

                        builderBuilder.addFunction(memberBuilderCreator)
                        memberBuilder.addStatement("%N().block()", memberBuilderCreator.name)
                            .addKdoc("Configures inner `%N` object, creating it if necessary", member.propertyName)
                    }

                    sink.addFunction(memberBuilder.build(), target = PoetSink.Target.Extra)
                } else if (!member.isReadOnly) {
                    builderConstructor.addCode("this.%1N·=·%1N\n", member.propertyName)
                    builderSpecConstructor.addCode("this.%1N·=·spec.%1N\n", member.propertyName)
                    specConstructorArgs += CodeBlock.builder()
                        .indent()
                        .add("\n%1N·=·builder.%1N", member.propertyName)
                        .unindent()
                        .build()
                }

                if (!member.isReadOnly && member.isValueRequired(Kind.Builder, snap)) {
                    if ((member.builderType == STRING || !member.builderType.isPrimitive) && !member.poet.isValueClass) {
                        builderBuildFunction.addCode(
                            "check(this::%1N.isInitialized)·{·%2S·}\n", member.propertyName,
                            "Property \"${member.propertyName}\" must be initialized"
                        )
                    }
                } else if (!member.isReadOnly) {
                    member.defaultValue(Kind.Builder, snap)?.let {
                        builderEmptyConstructor.addCode("this.%1N·=·%2L\n", member.propertyName, it)
                    }
                }
            }
        }

        if (!isSingleton(snap) && !isSealedParent(snap)) {
            builder.addModifiers(KModifier.DATA)
            builder.primaryConstructor(constructor.build())
        }

        builderBuildFunction.addStatement("return %T(this)", selfType)

        if (withSpec) {
            check(!isSingleton(snap))
            sink.addType(
                specBuilder.build(),
                if (sink.isRoot) PoetSink.Target.ExternalFile else PoetSink.Target.Default
            )
        }

        if (withBuilder) {
            specConstructor.callThisConstructor(*specConstructorArgs.toTypedArray())
            builder.addFunction(specConstructor.build())

            builderBuilder
                .addFunction(builderConstructor.build())
                .addFunction(builderEmptyConstructor.build())
                .addFunction(builderSpecConstructor.build())
                .addFunction(builderBuildFunction.build())

            builder.addType(builderBuilder.build())

            sink.addFunction(toBuilderFunction.build(), target = PoetSink.Target.Extra)
        }

        if (schema.codegenGenerateUpdateSerializer == true) {
            updaterBuilder.addSuperinterface(DESERIALIZATION_STRATEGY.parameterizedBy(selfType))
                .addModifiers(KModifier.PRIVATE)
                .addProperty(
                    PropertySpec.builder("descriptor", SERIAL_DESCRIPTOR)
                        .addModifiers(KModifier.OVERRIDE)
                        .getter(
                            FunSpec.getterBuilder()
                                .addStatement("return %T.serializer().descriptor", updateDataType)
                                .build()
                        )
                        .build()
                )
                .addProperty(
                    PropertySpec.builder("original", selfType).initializer("original")
                        .addModifiers(KModifier.PRIVATE)
                        .build()
                )
                .addFunction(
                    FunSpec.builder("deserialize")
                        .addModifiers(KModifier.OVERRIDE)
                        .addParameter("decoder", DECODER)
                        .returns(selfType)
                        .addStatement(
                            "return decoder.decodeSerializableValue(%T.serializer()).update(original)",
                            updateDataType
                        )
                        .build()
                )
                .primaryConstructor(FunSpec.constructorBuilder().addParameter("original", selfType).build())

            updateDataBuilder
                .addSuperinterface(UPDATER_FOR.parameterizedBy(selfType))
                .addAnnotation(SERIALIZABLE)
                .addModifiers(KModifier.DATA)
                .primaryConstructor(updateDataConstructor.build())
                .addFunction(
                    FunSpec.builder("update")
                        .addModifiers(KModifier.OVERRIDE)
                        .addParameter("original", selfType)
                        .returns(selfType)
                        .addCode(
                            "return original.copy(\n⇥%L⇤\n)\n",
                            members.map {
                                val op = if (it.isBoxed) BOX_OR else BOX_GET_OR
                                CodeBlock.of("%1N·=·%1N·%2M·original.%1N", it.propertyName, op)
                            }.joinToCode(separator = ",\n")
                        )
                        .build()
                )

            builder.addType(updateDataBuilder.build())
            builder.addType(updaterBuilder.build())

            companion.addFunction(
                FunSpec.builder("updaterFor")
                    .addParameter("data", selfType)
                    .returns(DESERIALIZATION_STRATEGY.parameterizedBy(selfType))
                    .addStatement("return %T(data)", updaterType)
                    .build()
            )
        }

        if (members.any { it.schema.codegenSensitiveData == true }) {
            fun ObjectMember.sanitizeToString(): String {
                if (schema.codegenSensitiveData == true) {
                    return "${propertyName}=***REDACTED***"
                }
                return "${propertyName}=\$${propertyName}"
            }
            builder.addFunction(
                FunSpec.builder("toString")
                    .addModifiers(KModifier.OVERRIDE)
                    .returns(STRING)
                    .addStatement(
                        "return %P",
                        "${selfType.simpleName}(${members.joinToString(separator = ", ") { it.sanitizeToString() }})"
                    )
                    .build()
            )
        }

        when {
            isTuple -> {
                val arity = members.size

                var tupleSuperClass = TupleSerializerPoet[arity].selfType.parameterizedBy(selfType)
                val tupleSerializer = TypeSpec.objectBuilder("Serializer")
                    .addAnnotation(GENERATED)
                    .addModifiers(KModifier.INTERNAL)
                    .addSuperclassConstructorParameter("%S", selfType)

                val toResult = FunSpec.builder("toResult")
                    .addModifiers(KModifier.OVERRIDE)
                    .returns(selfType)
                val toResultArgs = mutableListOf<CodeBlock>()

                members.forEachIndexed { index, member ->
                    tupleSuperClass = tupleSuperClass.plusParameter(member.propertyType)
                    tupleSerializer.addSuperclassConstructorParameter(member.serializerInstance())

                    val typeVarName = "value${index}"

                    val prop = PropertySpec.builder(typeVarName, member.propertyType)
                        .addModifiers(KModifier.OVERRIDE)
                        .receiver(selfType)
                        .getter(
                            FunSpec.getterBuilder()
                                .addStatement("return %N", member.propertyName)
                                .build()
                        )
                        .build()

                    tupleSerializer.addProperty(prop)

                    toResult.addParameter(typeVarName, member.propertyType)
                    toResultArgs += CodeBlock.of("%N·=·%N", member.propertyName, typeVarName)
                }

                toResult.addStatement("return %T(%L)", selfType, toResultArgs.joinToCode())
                tupleSerializer.addFunction(toResult.build())

                builder.addType(tupleSerializer.superclass(tupleSuperClass).build())

                builder.addAnnotation(
                    AnnotationSpec.builder(SERIALIZABLE)
                        .addMember("with = %T::class", selfType.nestedClass("Serializer"))
                        .build()
                )
            }

            isMultipartData -> {
                val multipartBuilder = FunSpec.builder(TAKE_FROM)
                    .addAnnotation(GENERATED)
                    .addModifiers(KModifier.INTERNAL)
                    .receiver(SERIALIZABLE_MULTI_PART.nestedClass("Builder"))
                    .addParameter("body", selfType)

                members.forEach { member ->
                    multipartBuilder.beginControlFlow("part")
                        .addCode(member.toPartData(CodeBlock.of("body")))
                        .endControlFlow()
                }

                sink.addFunction(multipartBuilder.build(), target = PoetSink.Target.Extra)
            }

            else -> {
                val serializableSpec = AnnotationSpec.builder(SERIALIZABLE)
                var annotationRequired = !serializationPolicy.notSerializable
                when (schema.extensions["x-kiscord-codegen-external-serializer"]) {
                    true -> {
                        val rawName = schema.codegenName ?: schema.title ?: error("No name? :thinking:")
                        val serializerType = ClassName(selfType.packageName, "${rawName.replace(".", "")}Serializer")
                        serializableSpec.addMember("with = %T::class", serializerType)
                    }

                    "none" -> {
                        annotationRequired = false
                    }

                    "use-parent" -> {
                        parent ?: throw IllegalArgumentException()
                        val serializerType =
                            parent.serializerTypeFor(parent.selfType) ?: throw IllegalArgumentException()
                        serializableSpec.addMember("with = %T::class", serializerType)
                    }
                }
                if (annotationRequired) {
                    builder.addAnnotation(serializableSpec.build())
                }
            }
        }
    }

    override fun serializerTypeFor(type: TypeName): ClassName? = when {
        type == selfType && schema.extensions["x-kiscord-codegen-external-serializer"] == true ->
            selfType.peerClass("${selfType.simpleName}Serializer")
                .withTag(SerializerMetadata(isObject = true, isDefaultSerializer = true))

        else -> super.serializerTypeFor(type)
    }

    open fun isSealedParent(snap: PoetSnap): Boolean = false

    override fun serializerTypeForUse(snap: PoetSnap, visitor: RecursivePoetVisitor): Set<ClassName> =
        buildSet {
            resolveMembers(snap).forEach { member ->
                visitor.visit(member.poet) { poet ->
                    this += poet.serializerTypeForUse(snap, RecursivePoetVisitor.Noop)
                }
            }

            snap.resolveDirectInners(this@ObjectPoet).forEach {
                visitor.visit(it) { poet ->
                    this += poet.serializerTypeForUse(snap, visitor)
                }
            }
        }

    open fun isRequiredMember(snap: PoetSnap, name: String): Boolean {
        fun isRequiredInSchema(schema: Schema<*>): Boolean {
            val required = schema.required ?: return false
            return name in required
        }

        return resolveParents(snap, includeFake = true).any { (schema, poet) ->
            isRequiredInSchema(schema) || (poet is SchemaPoet<*, *> && isRequiredInSchema(poet.schema))
        } || isRequiredInSchema(schema)
    }

    open val isTuple get() = false
    open val isMultipartData get() = false

    open inner class ObjectMember(
        val name: String,
        val schema: Schema<*>,
        val transient: Boolean,
        snap: PoetSnap
    ) {
        val propertyName = schema.codegenName ?: name.toKiscordCase()

        val required = isRequiredMember(snap, name)

        val type = snap.resolveType(schema)
            ?: throw IllegalArgumentException("Couldn't resolve type for $name")

        val poet = snap.resolvePoet(type.copy(nullable = false), schema)

        val isBoxed = !required && schema.nullable == true

        val isValueNullable = when {
            isBoxed -> true
            else -> schema.default == null && !required || schema.nullable == true
        }

        val propertyType = when {
            isBoxed -> BOX.parameterizedBy(poet.selfType.copy(nullable = true))
            else -> poet.selfType.copy(nullable = isValueNullable)
        }

        val specType = when {
            isBoxed && poet is BuildablePoet && poet.withBuilder -> {
                BOX.parameterizedBy(poet.specType.copy(nullable = true))
            }

            poet is BuildablePoet && poet.withBuilder -> {
                poet.specType.copy(nullable = isValueNullable)
            }

            else -> propertyType
        }

        val builderType = when {
            isBoxed && poet is BuildablePoet && poet.withBuilder -> {
                BOX.parameterizedBy(poet.builderType.copy(nullable = true))
            }

            poet is BuildablePoet && poet.withBuilder -> {
                poet.builderType.copy(nullable = isValueNullable)
            }

            else -> propertyType
        }

        val updaterType = when {
            isBoxed -> propertyType
            else -> BOX.parameterizedBy(propertyType)
        }

        val isExtendedFromParent by lazy {
            resolveParents(snap).any { (schema, poet) ->
                if (schema.codegenGenerateSpec == true || schema.codegenGenerateBuilder == true) {
                    if (schema.properties.orEmpty().any { (name, _) -> this.name == name }) return@any true
                }
                if (poet is ObjectPoet<*>) {
                    if (poet.resolveMembers(snap).any { this.name == it.name }) return@any true
                }
                false
            }
        }

        open val isReadOnly by lazy { schema.readOnly ?: false }
        val isDeprecated: Boolean = schema.deprecated == true
        val deprecationMessage: String? by lazy { schema.deprecationMessage }

        val memberSerializationPolicy = schema.codegenSerializationPolicy ?: SerializationPolicy()

        fun formatOrCreate(value: CodeBlock): CodeBlock = when {
            required && !isValueNullable -> CodeBlock.of("%L.%N", value, propertyName)
            else -> CodeBlock.of("%L.%N()", value, "${propertyName}OrCreate")
        }

        fun isValueRequired(kind: Kind, snap: PoetSnap): Boolean {
            return required && defaultValue(kind, snap) == null
        }

        fun defaultValue(kind: Kind, snap: PoetSnap): CodeBlock? {
            val default = when (kind) {
                Kind.Builder -> schema.codegenBuilderDefault ?: schema.codegenDefault ?: schema.default
                Kind.Value -> schema.codegenDefault ?: schema.default
                else -> throw IllegalArgumentException()
            }.takeUnless { it == "null" }

            return when {
                default == null && isBoxed -> CodeBlock.of("%T.absent", BOX)
                default == null && !isValueNullable -> null
                default == "%NULL%" && isBoxed -> CodeBlock.of("%T.nulled", BOX)
                isBoxed && !isValueNullable -> CodeBlock.of(
                    "%T.of(%L)",
                    BOX,
                    snap.valueResolver.resolve(propertyType, default, kind)
                )

                else -> snap.valueResolver.resolve(propertyType, default, kind)
            }
        }

        fun serializerInstance(): CodeBlock {
            return poet.serializerInstance(propertyType)
        }

        fun asParameterSpec(kind: Kind, snap: PoetSnap): ParameterSpec {
            val specType = when (kind) {
                Kind.Builder -> builderType
                Kind.Value -> propertyType
                Kind.Spec -> specType
                Kind.Updater -> updaterType
            }

            val spec = ParameterSpec.builder(propertyName, specType)

            if (kind == Kind.Updater) {
                spec.defaultValue("%T.absent", BOX)
            } else if (kind != Kind.Spec) {
                defaultValue(kind, snap)?.let {
                    spec.defaultValue(it)
                }
            }

            return spec.build()
        }

        open fun asPropertySpec(withSpec: Boolean, kind: Kind, snap: PoetSnap, sink: PoetSink): PropertySpec {
            val specType = when (kind) {
                Kind.Spec -> specType
                Kind.Builder -> builderType
                Kind.Value -> propertyType
                Kind.Updater -> updaterType
            }

            val spec = PropertySpec.builder(propertyName, specType)
            if (isDeprecated) {
                val annotationSpec = AnnotationSpec.builder(Deprecated::class)
                deprecationMessage?.let {
                    annotationSpec.addMember("message = %S", it)
                }
                spec.addAnnotation(annotationSpec.build())
            }
            val isSerializable = !isTuple && !isMultipartData && !serializationPolicy.notSerializable

            if (kind == Kind.Value || kind == Kind.Updater) {
                if (kind == Kind.Value && required && isSerializable) {
                    spec.addAnnotation(REQUIRED)
                }

                if (transient) {
                    spec.addAnnotation(TRANSIENT)
                } else if (name != propertyName && isSerializable) {
                    spec.addAnnotation(
                        AnnotationSpec.builder(SERIAL_NAME)
                            .addMember("%S", name)
                            .build()
                    )
                }

                fun addEncodeDefault(kind: String) {
                    spec.addAnnotation(
                        AnnotationSpec.builder(ENCODE_DEFAULT)
                            .addMember("%T.%L", ENCODE_DEFAULT_MODE, kind)
                            .build()
                    )
                    sink.addOptIn(EXPERIMENTAL_SERIALIZATION_API)
                }

                if (kind == Kind.Value) {
                    when {
                        memberSerializationPolicy.encodeDefault == "always" -> addEncodeDefault("ALWAYS")
                        memberSerializationPolicy.encodeDefault == "never" -> addEncodeDefault("NEVER")
                        isSerializable && isBoxed -> addEncodeDefault("NEVER")
                    }
                }

                var serializerType = poet.serializerTypeFor(specType)
                if (serializerType == null && kind != Kind.Value) {
                    serializerType = poet.serializerTypeFor(propertyType)
                }

                if (serializerType != null && isSerializable) {
                    spec.addAnnotation(
                        AnnotationSpec.builder(SERIALIZABLE)
                            .addMember("with = %T::class", serializerType)
                            .build()
                    )
                }

                if (kind == Kind.Value && !withSpec && !isExtendedFromParent) {
                    schema.description?.let {
                        spec.addKdoc("%L", it)
                    }
                }
            }

            if (kind == Kind.Spec) {
                schema.description?.let {
                    spec.addKdoc("%L", it)
                }
            } else {
                if (isReadOnly) {
                    val value = snap.valueResolver.resolve(propertyType, schema.enum.single(), kind)
                    spec.getter(FunSpec.getterBuilder().addStatement("return %L", value).build())
                } else if (kind != Kind.Builder) {
                    spec.initializer(propertyName)
                }

                if (kind != Kind.Updater && (withSpec || isExtendedFromParent)) {
                    spec.addModifiers(KModifier.OVERRIDE)
                }
            }

            if (kind == Kind.Builder && !isReadOnly) {
                spec.addAnnotation(BUILDER_DSL)
                spec.mutable(true)

                if (isValueRequired(kind, snap)) {
                    if (specType != STRING && specType.isPrimitive || poet.isValueClass) {
                        spec.delegate("%T.notNull<%T>()", ClassName("kotlin.properties", "Delegates"), specType)
                    } else {
                        spec.addModifiers(KModifier.LATEINIT)
                    }
                }
            }

            return spec.build()
        }

        fun toPartData(selfType: CodeBlock): CodeBlock {
            val propertyValueBlock = CodeBlock.of("%L.%N", selfType, propertyName)
            return poet.toPartData(propertyValueBlock, CodeBlock.of("%S", name))
        }

        val extensions: List<ExtensionsForDTO>
            get() = buildList {
                if (!isReadOnly) {
                    if (poet is ExtensionsForDTO) this += poet
                    if (isBoxed) this += BoxPoet
                    else this += DefaultExtensions
                }
            }
    }

    object DefaultExtensions : ExtensionsForDTO {
        fun <S : Schema<Any?>> basicTemplate(member: ObjectPoet<S>.ObjectMember): FunSpec.Builder {
            val funSpec = FunSpec.builder(member.propertyName)
                .addAnnotation(GENERATED)
                .addAnnotation(BUILDER_DSL)

            if (member.isDeprecated) {
                val deprecatedSpec = AnnotationSpec.builder(Deprecated::class)
                member.deprecationMessage?.let { deprecatedSpec.addMember("message = %S", it) }
                funSpec.addAnnotation(deprecatedSpec.build())
                funSpec.addAnnotation(
                    AnnotationSpec.builder(Suppress::class)
                        .addMember("%S", "DEPRECATION")
                        .build()
                )
            }

            return funSpec
        }

        override fun <S : Schema<Any?>> generateExtensionsForDTO(
            poet: ObjectPoet<S>,
            member: ObjectPoet<S>.ObjectMember,
            kind: Kind,
            sink: PoetSink,
            snap: PoetSnap
        ) {
            if (kind != Kind.Builder || !poet.withBuilder) return
            val funSpec = basicTemplate(member)
                .receiver(poet.builderType)
                .addParameter(member.propertyName, member.builderType)
                .addStatement("this.%1N = %1N", member.propertyName)

            sink.addFunction(funSpec.build(), target = PoetSink.Target.Extra)
        }
    }

    override fun toPartData(
        selfValue: CodeBlock,
        partName: CodeBlock,
        overrideContentType: ContentType?
    ): CodeBlock = buildCodeBlock {
        addStatement(
            "contentDisposition = %T(%S)\n⇥.withParameter(%T.Name, %L)⇤",
            CONTENT_DISPOSITION, "form-data", CONTENT_DISPOSITION.nestedClass("Parameters"), partName
        )
        addStatement("contentType = %L", (overrideContentType ?: ContentType.Application.Json).toCodeBlock())
        addStatement("%M(%L, %L)", MemberName("kiscord.ktor", "partBody"), selfValue, serializerInstance(selfType))
    }

    enum class Kind {
        Value,
        Spec,
        Builder,
        Updater,
    }

    companion object {
        const val SPEC = "Spec"
        const val BUILDER = "Builder"
        const val UPDATE_DATA = "UpdateData"
        const val UPDATER = "Updater"
        const val TO_BUILDER = "toBuilder"
        const val BUILD = "build"
        const val PRESERVE = "preserve"
        const val TAKE_FROM = "takePartsFrom"
    }
}
