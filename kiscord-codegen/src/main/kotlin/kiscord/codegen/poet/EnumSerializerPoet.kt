/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.codegen.poet

import com.squareup.kotlinpoet.*
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import kiscord.codegen.*
import kotlinx.serialization.*

object EnumSerializerPoet : ClassPoet {
    override val selfType: ClassName = ClassName("kiscord.serialization", "EnumSerializer")
    override val category: String? get() = null

    override fun build(snap: PoetSnap, sink: PoetSink): TypeSpec {
        val t = TypeVariableName("T")
        val enumT = t.copy(bounds = listOf(ENUM.parameterizedBy(t)))

        val builder = TypeSpec.interfaceBuilder(selfType)
            .addAnnotation(GENERATED)
            .addAnnotation(KISCORD_UNSTABLE_API)
            .addModifiers(KModifier.SEALED)
            .addTypeVariable(enumT)
            .addProperty("descriptor", SERIAL_DESCRIPTOR)
            .addKdoc("@suppress")

        val companion = TypeSpec.companionObjectBuilder()
            .addModifiers(KModifier.PRIVATE)

        fun TypeSpec.Builder.common(kind: Kind, withDescriptor: Boolean = true): TypeSpec.Builder {
            addAnnotation(KISCORD_UNSTABLE_API)
            addModifiers(KModifier.ABSTRACT)
            addTypeVariable(enumT)
            primaryConstructor(
                FunSpec.constructorBuilder()
                    .addParameter("name", STRING)
                    .build()
            )
            if (withDescriptor) addProperty(
                PropertySpec.builder("descriptor", SERIAL_DESCRIPTOR)
                    .addModifiers(KModifier.FINAL, KModifier.OVERRIDE)
                    .initializer("%M(name,·%L)", PRIMITIVE_SERIAL_DESCRIPTOR, kind.serialKind)
                    .build()
            )
            addFunction(
                FunSpec.builder("valueToEnum")
                    .addModifiers(KModifier.PROTECTED, KModifier.ABSTRACT)
                    .addParameter("value", kind.primitive)
                    .returns(t.copy(nullable = true))
                    .build()
            )
            addFunction(
                FunSpec.builder("enumToValue")
                    .addModifiers(KModifier.PROTECTED, KModifier.ABSTRACT)
                    .addParameter("value", t)
                    .returns(kind.primitive)
                    .build()
            )
            return this
        }

        registeredPrimitives.forEach { kind ->
            val kindBuilder = TypeSpec.classBuilder(kind.serializerClassName)
                .common(kind)
                .addSuperinterface(selfType.parameterizedBy(t))
                .addSuperinterface(KSERIALIZER.parameterizedBy(t))
                .addFunction(
                    FunSpec.builder("deserialize")
                        .addModifiers(KModifier.FINAL, KModifier.OVERRIDE)
                        .addParameter("decoder", DECODER)
                        .returns(t)
                        .addStatement("val value = decoder.%L()", kind.decodeFun)
                        .addStatement(
                            "return valueToEnum(value) ?: throw·%T(%L)",
                            ILLEGAL_ARGUMENT_EXCEPTION,
                            kind.unknownEnumValueMessage()
                        )
                        .build()
                )
                .addFunction(
                    FunSpec.builder("serialize")
                        .addModifiers(KModifier.FINAL, KModifier.OVERRIDE)
                        .addParameter("encoder", ENCODER)
                        .addParameter("value", t)
                        .addStatement("encoder.%L(enumToValue(value))", kind.encodeFun)
                        .build()
                )

            builder.addType(kindBuilder.build())
        }

        registeredBitmasks.forEach { kind ->
            val zeroValName = "ZERO_${kind.primitive.simpleName.uppercase()}"
            companion.addProperty(
                PropertySpec.builder(zeroValName, kind.primitive)
                    .addModifiers(KModifier.CONST)
                    .initializer("0")
                    .build()
            )

            val ops = listOf("and", "or", "inv")
            val (and, or, inv) = if (kind.experimentalBitOperations) {
                ops.map { CodeBlock.of("%M", MemberName("kotlin.experimental", it)) }
            } else {
                ops.map { CodeBlock.of("%L", it) }
            }

            val kindBuilder = TypeSpec.classBuilder(kind.serializerBitmaskClassName)
                .common(kind)
                .addSuperinterface(selfType.parameterizedBy(t))
                .addSuperinterface(KSERIALIZER.parameterizedBy(SET.parameterizedBy(t)))
                .addFunction(
                    FunSpec.builder("unknownValue")
                        .addModifiers(KModifier.PROTECTED, KModifier.ABSTRACT)
                        .addParameter("value", kind.primitive)
                        .build()
                )
                .addFunction(
                    FunSpec.builder("deserialize")
                        .addModifiers(KModifier.FINAL, KModifier.OVERRIDE)
                        .addParameter("decoder", DECODER)
                        .returns(SET.parameterizedBy(t))
                        .addStatement("var mask = decoder.%L()", kind.decodeFun)
                        .addStatement("if (mask == %L) return %M()", zeroValName, EMPTY_SET)
                        .addStatement("val set = %M<%T>()", MUTABLE_SET_OF, t)
                        .beginControlFlow("while (mask != %L)", zeroValName)
                        .addStatement("val flag = mask.%M()", MemberName("kotlin", "takeLowestOneBit"))
                        .addStatement("mask = mask %L flag.%L()", and, inv)
                        .addStatement("val enum = valueToEnum(flag)")
                        .beginControlFlow("if (enum != null)")
                        .addStatement("set += enum")
                        .nextControlFlow("else")
                        .addStatement("unknownValue(flag)")
                        .endControlFlow()
                        .endControlFlow()
                        .addStatement("return set.%M()", TO_ENUM_SET)
                        .build()
                )
                .addFunction(
                    FunSpec.builder("serialize")
                        .addModifiers(KModifier.FINAL, KModifier.OVERRIDE)
                        .addParameter("encoder", ENCODER)
                        .addParameter("value", SET.parameterizedBy(t))
                        .beginControlFlow("val mask = value.fold(%L) { acc, enum ->", zeroValName)
                        .addStatement("acc %L enumToValue(enum)", or)
                        .endControlFlow()
                        .addStatement("encoder.%L(mask)", kind.encodeFun)
                        .build()
                )

            builder.addType(kindBuilder.build())
        }

        registeredSets.forEach { kind ->
            val kindBuilder = TypeSpec.classBuilder(kind.serializerSetClassName)
                .common(kind, withDescriptor = false)
                .addSuperinterface(selfType.parameterizedBy(t))
                .addSuperinterface(KSERIALIZER.parameterizedBy(SET.parameterizedBy(t)))
                .addProperty(
                    PropertySpec.builder("descriptor", SERIAL_DESCRIPTOR)
                        .addModifiers(KModifier.FINAL, KModifier.OVERRIDE)
                        .initializer(
                            "%M(%M(name,·%L))",
                            SET_SERIAL_DESCRIPTOR,
                            PRIMITIVE_SERIAL_DESCRIPTOR,
                            kind.serialKind
                        )
                        .build()
                )
                .addFunction(
                    FunSpec.builder("deserialize")
                        .addModifiers(KModifier.FINAL, KModifier.OVERRIDE)
                        .addParameter("decoder", DECODER)
                        .returns(SET.parameterizedBy(t))
                        .beginControlFlow("return decoder.%M(descriptor)", DECODE_STRUCTURE)
                        .addStatement("val set = %T(decodeCollectionSize(descriptor).takeIf { it >= 0 } ?: 16)", HASH_SET.parameterizedBy(t))
                        .beginControlFlow("while(true)")
                        .addStatement("val index = decodeElementIndex(descriptor)")
                        .addStatement("if (index == %T.DECODE_DONE) break", COMPOSITE_DECODER)
                        .beginControlFlow("valueToEnum(%L(descriptor, index))?.let", kind.decodeElementFun)
                        .addStatement("set.add(it)")
                        .endControlFlow()
                        .endControlFlow()
                        .addStatement("set.%M()", TO_ENUM_SET)
                        .endControlFlow()
                        .build()
                )
                .addFunction(
                    FunSpec.builder("serialize")
                        .addModifiers(KModifier.FINAL, KModifier.OVERRIDE)
                        .addParameter("encoder", ENCODER)
                        .addParameter("value", SET.parameterizedBy(t))
                        .beginControlFlow("encoder.%M(descriptor, value) { i, v ->", ENCODE_COLLECTION)
                        .addStatement("%L(descriptor, i, enumToValue(v))", kind.encodeElementFun)
                        .endControlFlow()
                        .build()
                )

            builder.addType(kindBuilder.build())
        }

        builder.addType(companion.build())

        sink.addOptIn(EXPERIMENTAL_SERIALIZATION_API)

        return builder.build()
    }

    private val PRIMITIVE_SERIAL_DESCRIPTOR =
        MemberName("kotlinx.serialization.descriptors", "PrimitiveSerialDescriptor")
    private val ENCODE_COLLECTION = MemberName("kotlinx.serialization.encoding", "encodeCollection")
    private val DECODE_STRUCTURE = MemberName("kotlinx.serialization.encoding", "decodeStructure")
    private val HASH_SET = ClassName("kotlin.collections", "HashSet")

    private val registeredPrimitives = mutableSetOf<Kind>()
    private val registeredBitmasks = mutableSetOf<Kind>()
    private val registeredSets = mutableSetOf<Kind>()
    fun getSerializerFor(kind: Kind): ClassName {
        registeredPrimitives += kind
        return selfType.nestedClass(kind.serializerClassName)
    }

    fun getBitmaskSerializerFor(kind: Kind): ClassName {
        if (!kind.bitmaskCapable) throw IllegalArgumentException()
        registeredBitmasks += kind
        return selfType.nestedClass(kind.serializerBitmaskClassName)
    }

    fun getSetSerializer(kind: Kind): ClassName {
        registeredSets += kind
        return selfType.nestedClass(kind.serializerSetClassName)
    }

    enum class Kind(
        val primitive: ClassName,
        val decodeFun: String = "decode${primitive.simpleName}",
        val decodeElementFun: String = "decode${primitive.simpleName}Element",
        val encodeFun: String = "encode${primitive.simpleName}",
        val encodeElementFun: String = "encode${primitive.simpleName}Element",
        val serialKind: CodeBlock = CodeBlock.of("%T.%L", PRIMITIVE_KIND, primitive.simpleName.uppercase()),
        val serializerClassName: String = "As${primitive.simpleName}",
        val bitmaskCapable: Boolean = false,
        val serializerBitmaskClassName: String = "As${primitive.simpleName}Bitmask",
        val serializerSetClassName: String = "As${primitive.simpleName}Set",
        val experimentalBitOperations: Boolean = false,
    ) {
        AsBoolean(BOOLEAN),
        AsByte(BYTE, bitmaskCapable = true, experimentalBitOperations = true),
        AsChar(CHAR),
        AsShort(SHORT, bitmaskCapable = true, experimentalBitOperations = true),
        AsInt(INT, bitmaskCapable = true),
        AsLong(LONG, bitmaskCapable = true),
        AsFloat(FLOAT),
        AsDouble(DOUBLE),
        AsString(STRING) {
            override fun unknownEnumValueMessage(): CodeBlock =
                CodeBlock.of("%P", "Unknown enum value for \${descriptor.serialName}: \$value")
        },
        ;

        open fun unknownEnumValueMessage(): CodeBlock =
            CodeBlock.of("%P", "Unknown enum value for \${descriptor.serialName}: \$value (0x\${value.toString(16)})")
    }
}
