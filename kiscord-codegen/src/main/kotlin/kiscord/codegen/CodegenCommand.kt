/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.codegen

import com.fasterxml.jackson.core.type.*
import com.fasterxml.jackson.core.util.*
import com.fasterxml.jackson.databind.*
import com.fasterxml.jackson.dataformat.yaml.*
import com.fasterxml.jackson.module.kotlin.*
import com.github.ajalt.clikt.core.*
import com.github.ajalt.clikt.parameters.options.*
import com.github.ajalt.clikt.parameters.types.*
import com.squareup.kotlinpoet.*
import io.swagger.v3.core.util.*
import io.swagger.v3.oas.models.*
import io.swagger.v3.oas.models.media.*
import io.swagger.v3.oas.models.parameters.*
import io.swagger.v3.oas.models.parameters.Parameter
import io.swagger.v3.oas.models.responses.*
import io.swagger.v3.parser.*
import io.swagger.v3.parser.core.models.*
import kiscord.apiSpider.*
import kiscord.apiSpider.extension
import kiscord.codegen.poet.*
import kiscord.codegen.swagger.*
import java.lang.reflect.*
import java.nio.file.*
import java.nio.file.attribute.*
import kotlin.io.path.*

class CodegenCommand : CliktCommand() {
    private val input by option(help = "OpenAPI specification file")
        .path(mustExist = true, canBeDir = false, mustBeReadable = true)
        .required()

    private val additionalSchemas by option(help = "Additional schemas to generate, but don't include to output json")
        .path(mustExist = true, canBeFile = false, mustBeReadable = true)

    private val jsonOutput by option(help = "Output JSON specification to file")
        .path(canBeDir = false)

    private val codegenOutput by option(help = "Codegen output directory")
        .path(canBeFile = false)
        .required()

    private val apiDocs by option(help = "API docs path")
        .path(mustExist = true, canBeFile = false, mustBeReadable = true)

    private val apiReport by option(help = "Directory to store html api report")
        .path(canBeFile = false)

    private val apiReportMarkdown by option(help = "File to store markdown api report")
        .path(canBeDir = false)

    val category by option(help = "Codegen category to filter")

    private val buildConfigName by option(help = "Build config class name")

    private val buildConfigFields by option("--build-config-field", help = "Add build config field").multiple()

    private val reformatComponents by option(help = "Reformat components dir").flag()

    override fun run() {
        val parser = OpenAPIV3Parser()
        val result = parser.readLocation(input.toAbsolutePath().toString(), null, ParseOptions())
        result.messages.forEach { message ->
            System.err.println(message)
        }
        val openapi = result.openAPI ?: error("no openapi :(")
        if (openapi.components.schemas == null) {
            openapi.components.schemas = LinkedHashMap()
        }

//    yamlPrettyWriter.writeValue(inputFile, openapi)

        additionalSchemas?.let { path ->
            codegenMapper.readValue<MutableMap<String, Schema<*>>>(path.toFile()).forEach { (name, schema) ->
                if (openapi.components.schemas.containsKey(name)) {
                    throw IllegalStateException("Duplicate schema in additional files: $name")
                }
                openapi.components.addSchemas(name, schema)
            }
        }

        val resolver = KiscordOpenAPIResolver(openapi, emptyList(), input.toString())
        resolver.resolve()

        val snap = PoetSnap(resolver.cache)

        buildConfigName?.let { buildConfigName ->
            val i = buildConfigName.lastIndexOf('.')
            val className = ClassName(buildConfigName.substring(0, i), buildConfigName.substring(i + 1))
            snap.addPoet(BuildConfigPoet(className, buildConfigFields))
        }

        snap.read(openapi)

        Files.createDirectories(codegenOutput)
        snap.writeTo(codegenOutput, category)

        sortModel(openapi)
        normalizeModel(openapi)
        jsonOutput?.let {
            Files.createDirectories(it.parent)
            jsonPrettyWriter.writeValue(it.toFile(), openapi)
        }

        if (reformatComponents) {
            val components = input.parent.resolve("components")
            Files.walkFileTree(
                components.resolve("parameters"), ReformatFileVisitor(object : TypeReference<Parameter>() {})
            )
            Files.walkFileTree(
                components.resolve("request-body"), ReformatFileVisitor(object : TypeReference<RequestBody>() {})
            )
            Files.walkFileTree(
                components.resolve("resources"), ReformatFileVisitor(object : TypeReference<PathItem>() {})
            )
            Files.walkFileTree(
                components.resolve("responses"), ReformatFileVisitor(object : TypeReference<ApiResponse>() {})
            )
            Files.walkFileTree(
                components.resolve("schemas"), ReformatFileVisitor(object : TypeReference<Schema<*>>() {})
            )
        }

        val snapshots by lazy {
            listOf(
                crawlOpenAPI(openapi, resolver),
                crawlApiDocs(apiDocs ?: throw IllegalArgumentException("Missing --api-docs")),
            ).map { it.purify() }
        }

        apiReport?.let { HtmlApiReporter.report(it.toFile(), snapshots) }
        apiReportMarkdown?.let { MarkdownApiReporter.report(it.toFile(), snapshots) }
    }
}

val codegenMapper: ObjectMapper = Yaml.mapper().registerModule(KotlinModule.Builder().build())

val jsonPrettyWriter: ObjectWriter = Json.pretty()
val yamlPrettyWriter: ObjectWriter = run {
    val mapper = Yaml.mapper()
    (mapper.factory as YAMLFactory).enable(YAMLGenerator.Feature.INDENT_ARRAYS_WITH_INDICATOR)
    mapper.writer(DefaultPrettyPrinter())
}

private fun normalizeModel(openapi: OpenAPI) {
    openapi.paths.forEach { (_, path) ->
        val operations = path.readOperations()
        val commonParameters = operations.map { it.parameters.orEmpty() }.reduceOrNull { o1, o2 ->
            val r = o1.toMutableList()
            r.retainAll(o2)
            r
        }.orEmpty().filter { it.`in` == "path" }

        commonParameters.forEach { path.addParametersItem(it) }
        if (commonParameters.isNotEmpty()) operations.forEach {
            it.parameters.removeAll(commonParameters)
            if (it.parameters.isEmpty()) {
                it.parameters = null
            }
        }
    }
}

private val fieldSorterRestrictions = mapOf(
    "io.swagger.v3.oas.models.media.Schema" to setOf(
        "_enum",
        "properties"
    )
)

private val schemaExtensionsSorterRestrictions = setOf(
    "x-kiscord-codegen-structure-ref"
)

/*
 * A high-quality sorter of anything with reflection ©
 */
@Suppress("UNCHECKED_CAST")
private fun sortModel(model: Any, restricted: Boolean = false, schemaExtensions: Boolean = false) {
    if (model is String) return
    if (model is Collection<*>) {
        model.forEach {
            if (it != null) sortModel(it)
        }
        if (!restricted && model is MutableList<*> && model.all { it is String }) {
            (model as MutableList<String>).sort()
        }
    } else if (model is MutableMap<*, *>) {
        if (model.keys.all { it is String }) {
            val map = model as MutableMap<String, Any?>
            if (schemaExtensions)
                if (restricted) {
                    map.forEach { entry ->
                        entry.value?.let {
                            sortModel(it, restricted = entry.key in schemaExtensionsSorterRestrictions)
                        }
                    }
                } else {
                    val copy = map.toSortedMap(String.CASE_INSENSITIVE_ORDER)
                    copy.forEach { entry ->
                        entry.value?.let {
                            sortModel(it, restricted = entry.key in schemaExtensionsSorterRestrictions)
                        }
                    }
                    map.clear()
                    map.putAll(copy)
                }
        }
    } else {
        var clazz = model::class.java
        while (clazz != Any::class.java) {
            val sorterRestrictions = fieldSorterRestrictions[clazz.name] ?: emptySet()
            clazz.declaredFields.forEach { field ->
                if (!field.isSynthetic &&
                    !field.type.isPrimitive &&
                    (field.modifiers and (Modifier.TRANSIENT or Modifier.STATIC) == 0)
                ) {
                    try {
                        field.isAccessible = true
                        field.get(model)?.let {
                            sortModel(
                                it,
                                restricted = field.name in sorterRestrictions,
                                schemaExtensions = field.name == "extensions" && field.type == Schema::class.java
                            )
                        }
                    } catch (_: SecurityException) {
                    } catch (_: InaccessibleObjectException) {
                    }
                }
            }
            clazz = clazz.superclass
        }
    }
}

class ReformatFileVisitor<T : Any>(private val typeReference: TypeReference<T>) :
    SimpleFileVisitor<Path>() {
    override fun visitFile(file: Path, attrs: BasicFileAttributes): FileVisitResult {
        if (file.extension != "yaml") return FileVisitResult.CONTINUE
        val original = file.readText()
        val value = codegenMapper.readValue(original, typeReference)
        sortModel(value)
        val new = yamlPrettyWriter.writeValueAsString(value)
        if (new != original) {
            file.writeText(new)
        }
        return FileVisitResult.CONTINUE
    }
}

fun main(args: Array<out String>) = CodegenCommand().main(args)
