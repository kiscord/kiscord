/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.codegen.swagger.processors

import io.swagger.v3.oas.models.*
import io.swagger.v3.oas.models.examples.*
import io.swagger.v3.oas.models.media.*
import io.swagger.v3.oas.models.parameters.*
import io.swagger.v3.oas.models.responses.*
import io.swagger.v3.parser.util.*
import kiscord.codegen.swagger.*

class KiscordPathsProcessor(processors: KiscordProcessor) : KiscordProcessor by processors {
    fun processPaths() {
        api.paths?.forEach { (pathStr, pathItem) ->
            if (pathItem.`$ref` != null) {
                val resolvedPath = processReferencePath(pathItem)
                val pathRef = pathItem.`$ref`.split("#")[0]
                updateRefs(resolvedPath, pathRef)
                //we need to put the resolved path into swagger object
                api.path(pathStr, resolvedPath)
            }
        }

        api.paths?.forEach { (_, pathItem) ->
            pathItem.parameters?.forEach { parameter ->
                parameterProcessor.processParameter(parameter)
            }
            pathItem.readOperationsMap().forEach { (_, operation) ->
                operationProcessor.processOperation(operation)
            }
        }
    }

    private fun updateRefs(path: PathItem, pathRef: String) {
        if (path.parameters != null) {
            val params = path.parameters
            for (param in params) {
                updateRefs(param, pathRef)
            }
        }
        val ops = path.readOperations()
        for (op in ops) {
            if (op.parameters != null) {
                for (param in op.parameters) {
                    updateRefs(param, pathRef)
                }
            }
            if (op.responses != null) {
                for (response in op.responses.values) {
                    updateRefs(response, pathRef)
                }
            }
            if (op.requestBody != null) {
                updateRefs(op.requestBody, pathRef)
            }
            op.callbacks?.forEach { (_, callback) ->
                callback.forEach { _, pathItem ->
                    updateRefs(pathItem, pathRef)
                }
            }
        }
    }

    private fun updateRefs(response: ApiResponse, pathRef: String) {
        if (response.`$ref` != null) {
            response.`$ref` = computeRef(response.`$ref`, pathRef)
        }
        if (response.content != null) {
            val content: Map<String, MediaType> = response.content
            for (key in content.keys) {
                val mediaType = content[key]
                if (mediaType!!.schema != null) {
                    updateRefs(mediaType.schema, pathRef)
                }
                val examples = content[key]!!.examples
                if (examples != null) {
                    for (example in examples.values) {
                        updateRefs(example, pathRef)
                    }
                }
            }
        }
    }

    private fun updateRefs(example: Example, pathRef: String) {
        if (example.`$ref` != null) {
            example.`$ref` = computeRef(example.`$ref`, pathRef)
        }
    }

    private fun updateRefs(param: Parameter, pathRef: String) {
        if (param.`$ref` != null) {
            param.`$ref` = computeRef(param.`$ref`, pathRef)
        }
        if (param.schema != null) {
            updateRefs(param.schema, pathRef)
        }
        if (param.content != null) {
            val content: Map<String, MediaType> = param.content
            for (key in content.keys) {
                val mediaType = content[key]
                if (mediaType!!.schema != null) {
                    updateRefs(mediaType.schema, pathRef)
                }
            }
        }
    }

    private fun updateRefs(body: RequestBody, pathRef: String) {
        if (body.`$ref` != null) {
            body.`$ref` = computeRef(body.`$ref`, pathRef)
        }
        if (body.content != null) {
            val content: Map<String, MediaType> = body.content
            for (key in content.keys) {
                val mediaType = content[key]
                if (mediaType!!.schema != null) {
                    updateRefs(mediaType.schema, pathRef)
                }
                val examples = content[key]!!.examples
                if (examples != null) {
                    for (example in examples.values) {
                        updateRefs(example, pathRef)
                    }
                }
            }
        } else if (body.`$ref` != null) {
        }
    }

    private fun updateRefs(model: Schema<*>, pathRef: String) {
        if (model.`$ref` != null) {
            model.`set$ref`(computeRef(model.`$ref`, pathRef))
        } else if (model.properties != null) {
            // process properties
            if (model.properties != null) {
                val properties = model.properties
                for (key in properties.keys) {
                    val property = properties[key]
                    property?.let { updateRefs(it, pathRef) }
                }
            }
        } else if (model is ComposedSchema) {
            val composedSchema = model
            if (composedSchema.allOf != null) {
                for (innerModel in composedSchema.allOf) {
                    updateRefs(innerModel, pathRef)
                }
            }
            if (composedSchema.anyOf != null) {
                for (innerModel in composedSchema.anyOf) {
                    updateRefs(innerModel, pathRef)
                }
            }
            if (composedSchema.oneOf != null) {
                for (innerModel in composedSchema.oneOf) {
                    updateRefs(innerModel, pathRef)
                }
            }
        } else if (model is ArraySchema) {
            val arraySchema = model
            if (arraySchema.items != null) {
                updateRefs(arraySchema.items, pathRef)
            }
        }
    }

    private fun isLocalRef(ref: String): Boolean = ref.startsWith("#")

    private fun isAbsoluteRef(ref: String): Boolean = ref.startsWith("./")

    private fun computeRef(ref: String, prefix: String): String {
        if (isLocalRef(ref)) return computeLocalRef(ref, prefix)
        return if (isAbsoluteRef(ref)) ref else computeRelativeRef(ref, prefix)
    }

    private fun computeRelativeRef(ref: String, prefix: String): String {
        val iIdxOfSlash = prefix.lastIndexOf('/')
        return if (iIdxOfSlash != -1) {
            prefix.substring(0, iIdxOfSlash + 1) + ref
        } else prefix + ref
    }

    private fun computeLocalRef(ref: String, prefix: String): String {
        return prefix + ref
    }

    fun processReferencePath(pathItem: PathItem): PathItem {
        val ref = pathItem.`$ref`
        val refFormat = RefUtils.computeRefFormat(ref)
        return if (RefUtils.isAnExternalRefFormat(refFormat)) {
            externalRefProcessor.processRefToExternalPathItem(ref, refFormat)
        } else {
            cache.loadRef(ref, refFormat, PathItem::class.java)
        }
    }
}
