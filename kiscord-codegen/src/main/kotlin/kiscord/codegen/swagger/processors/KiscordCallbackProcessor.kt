/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.codegen.swagger.processors

import io.swagger.v3.oas.models.callbacks.*
import io.swagger.v3.parser.util.*
import kiscord.codegen.swagger.*

class KiscordCallbackProcessor(processor: KiscordProcessor) : KiscordProcessor.Delegate(processor) {
    fun processCallback(callback: Callback) {
        if (callback.`$ref` != null) {
            processReferenceCallback(callback)
        }
        //Resolver PathItem
        callback.forEach { (_, pathItem) ->
            pathItem.readOperationsMap().forEach { (_, operation) ->
                operationProcessor.processOperation(operation)
            }
            pathItem.parameters?.forEach { parameter ->
                parameterProcessor.processParameter(parameter)
            }
        }
    }

    fun processReferenceCallback(callback: Callback) {
        val ref = callback.`$ref`
        val refFormat = RefUtils.computeRefFormat(ref)
        if (RefUtils.isAnExternalRefFormat(refFormat)) {
            val newRef = externalRefProcessor.processRefToExternalCallback(ref, refFormat)
            callback.`$ref` = "#/components/callbacks/$newRef"
        }
    }
}
