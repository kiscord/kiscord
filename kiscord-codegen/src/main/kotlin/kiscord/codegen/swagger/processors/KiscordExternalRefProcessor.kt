/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

@file:Suppress("UNCHECKED_CAST")

package kiscord.codegen.swagger.processors

import io.swagger.v3.oas.models.*
import io.swagger.v3.oas.models.callbacks.*
import io.swagger.v3.oas.models.examples.*
import io.swagger.v3.oas.models.headers.*
import io.swagger.v3.oas.models.links.*
import io.swagger.v3.oas.models.media.*
import io.swagger.v3.oas.models.parameters.*
import io.swagger.v3.oas.models.responses.*
import io.swagger.v3.oas.models.security.*
import io.swagger.v3.parser.models.*
import io.swagger.v3.parser.util.*
import kiscord.codegen.swagger.*
import org.apache.commons.lang3.*
import org.slf4j.*
import java.net.*
import java.nio.file.Paths

class KiscordExternalRefProcessor(processors: KiscordProcessor) : KiscordProcessor by processors {
    private fun finalNameRec(
        schemas: Map<String, Schema<*>>, possiblyConflictingDefinitionName: String, newSchema: Schema<*>,
        iteration: Int
    ): String {
        val tryName = when (iteration) {
            0 -> possiblyConflictingDefinitionName
            else -> possiblyConflictingDefinitionName + "_" + iteration
        }
        val existingModel = schemas[tryName]
        if (existingModel != null) {
            if (existingModel.`$ref` == null && newSchema != existingModel) {
                if (cache.resolutionCache[newSchema.`$ref`] != null) {
                    return tryName
                }
                LOGGER.debug("A model for $existingModel already exists")
                return finalNameRec(schemas, possiblyConflictingDefinitionName, newSchema, iteration + 1)
            }
        } else {
            // validate the name
            for (name in schemas.keys) {
                if (name.equals(tryName, ignoreCase = true)) {
                    return name
                }
            }
        }
        return tryName
    }

    fun processRefToExternalSchema(`$ref`: String, refFormat: RefFormat?): String {
        val renamedRef = cache.getRenamedRef(`$ref`)
        if (renamedRef != null) {
            return renamedRef
        }
        val schema = cache.loadRef(`$ref`, refFormat, Schema::class.java)
        if (schema == null) {
            // stop!  There's a problem.  retain the original ref
            LOGGER.warn(
                "unable to load model reference from `" + `$ref` + "`.  It may not be available " +
                    "or the reference isn't a valid model schema"
            )
            return `$ref`
        }
        val newRef: String
        if (api.components == null) {
            api.components = Components()
        }
        var schemas = api.components.schemas
        if (schemas == null) {
            schemas = LinkedHashMap()
        }
        val possiblyConflictingDefinitionName = RefUtils.computeDefinitionName(`$ref`)
        newRef = finalNameRec(schemas, possiblyConflictingDefinitionName, schema, 0)
        cache.putRenamedRef(`$ref`, newRef)
        var existingModel = schemas[newRef]
        if (existingModel != null && existingModel.`$ref` != null) {
            // use the new model
            existingModel = null
        }
        if (existingModel == null) {
            // don't overwrite existing model reference
            api.components.addSchemas(newRef, schema)
            cache.addReferencedKey(newRef)
            val file = `$ref`.split("#/").toTypedArray()[0]
            if (schema.`$ref` != null) {
                val ref = RefUtils.computeRefFormat(schema.`$ref`)
                if (RefUtils.isAnExternalRefFormat(ref)) {
                    if (ref != RefFormat.URL) {
                        var schemaFullRef = schema.`$ref`
                        val parent = file.substring(0, file.lastIndexOf('/'))
                        if (!parent.isEmpty()) {
                            schemaFullRef = if (schemaFullRef.contains("#/")) {
                                val parts = schemaFullRef.split("#/").toTypedArray()
                                val schemaFullRefFilePart = parts[0]
                                val schemaFullRefInternalRefPart = parts[1]
                                Paths.get(parent, schemaFullRefFilePart).normalize()
                                    .toString() + "#/" + schemaFullRefInternalRefPart
                            } else {
                                Paths.get(parent, schemaFullRef).normalize().toString()
                            }
                        }
                        schema.`$ref` = processRefToExternalSchema(schemaFullRef, ref)
                    }
                } else {
                    processRefToExternalSchema(file + schema.`$ref`, RefFormat.RELATIVE)
                }
            }
            if (schema is ComposedSchema) {
                val composedSchema = schema
                if (composedSchema.allOf != null) {
                    for (item in composedSchema.allOf) {
                        processSchema(item, file)
                    }
                }
                if (composedSchema.oneOf != null) {
                    for (item in composedSchema.oneOf) {
                        processSchema(item, file)
                    }
                }
                if (composedSchema.anyOf != null) {
                    for (item in composedSchema.anyOf) {
                        processSchema(item, file)
                    }
                }
            }
            //Loop the properties and recursively call this method;
            val subProps = schema.properties
            processProperties(subProps, file)
            processDiscriminator(schema.discriminator, file)
            if (schema.additionalProperties != null && schema.additionalProperties is Schema<*>) {
                val additionalProperty = schema.additionalProperties as Schema<*>
                if (additionalProperty.`$ref` != null) {
                    processRefSchema(additionalProperty, file)
                } else if (additionalProperty is ArraySchema) {
                    val arrayProp = additionalProperty
                    if (arrayProp.items != null && arrayProp.items.`$ref` != null &&
                        StringUtils.isNotBlank(arrayProp.`$ref`)
                    ) {
                        processRefSchema(arrayProp.items, file)
                    }
                } else if (additionalProperty.additionalProperties != null && additionalProperty.additionalProperties is Schema<*>) {
                    val mapProp = additionalProperty.additionalProperties as Schema<*>
                    if (mapProp.`$ref` != null) {
                        processRefSchema(mapProp, file)
                    } else if (mapProp.additionalProperties is ArraySchema && (mapProp as ArraySchema).items != null && mapProp.items.`$ref` != null && StringUtils.isNotBlank(
                            mapProp.items.`$ref`
                        )
                    ) {
                        processRefSchema(mapProp.items, file)
                    }
                }
            }
            if (schema is ArraySchema && schema.items != null) {
                val arraySchema = schema
                if (StringUtils.isNotBlank(arraySchema.items.`$ref`)) {
                    processRefSchema(schema.items, file)
                } else {
                    processProperties(arraySchema.items.properties, file)
                }
            }
            if (schema.extensions != null) {
                processExtensionValue(schema.extensions, file)
            }
        }
        return newRef
    }

    private fun processSchema(property: Schema<*>?, file: String) {
        if (property != null) {
            if (StringUtils.isNotBlank(property.`$ref`)) {
                processRefSchema(property, file)
            }
            if (property.properties != null) {
                processProperties(property.properties, file)
            }
            if (property is ArraySchema) {
                processSchema(property.items, file)
            }
            if (property.additionalProperties is Schema<*>) {
                processSchema(property.additionalProperties as Schema<*>, file)
            }
            if (property is ComposedSchema) {
                val composed = property
                processProperties(composed.allOf, file)
                processProperties(composed.anyOf, file)
                processProperties(composed.oneOf, file)
            }
            if (property.extensions != null) {
                processExtensionValue(property.extensions, file)
            }
        }
    }

    fun processExtensionValue(value: Any, file: String): Any {
        if (value is Schema<*>) {
            val schema = value
            processRefSchemaObject(schema, file)
            val `$ref` = schema.`$ref`
            if (`$ref` != null) {
                val refFormat = RefUtils.computeRefFormat(`$ref`)
                if (RefUtils.isAnExternalRefFormat(refFormat)) {
                    schema.`$ref` = processRefToExternalSchema(`$ref`, refFormat)
                }
            }
        } else if (value is MutableMap<*, *>) {
            val map = value as MutableMap<String, Any>
            if (map.containsKey("\$ref") && map.size == 1) {
                return processExtensionValue(Schema<Any>().`$ref`(map["\$ref"] as String?), file)
            } else {
                for (entry in map.entries) {
                    val v1 = entry.value
                    val v2 = processExtensionValue(v1, file)
                    if (v1 !== v2) {
                        entry.setValue(v2)
                    }
                }
            }
        } else if (value is MutableList<*>) {
            val list = value as MutableList<Any>
            for (i in list.indices) {
                val v1 = list[i]
                val v2 = processExtensionValue(v1, file)
                if (v1 !== v2) {
                    list[i] = v2
                }
            }
        }
        return value
    }

    private fun processProperties(properties: Collection<Schema<*>>?, file: String) {
        if (properties != null) {
            for (property in properties) {
                processSchema(property, file)
            }
        }
    }

    private fun processProperties(properties: Map<String, Schema<*>>?, file: String) {
        if (properties != null) {
            processProperties(properties.values, file)
        }
    }

    fun processRefToExternalPathItem(ref: String, refFormat: RefFormat): PathItem {
        val pathItem = cache.loadRef(ref, refFormat, PathItem::class.java)
        val newRef: String
        var paths: Map<String?, PathItem?>? = api.paths
        if (paths == null) {
            paths = LinkedHashMap()
        }
        val possiblyConflictingDefinitionName = RefUtils.computeDefinitionName(ref)
        val existingPathItem = paths[possiblyConflictingDefinitionName]
        if (existingPathItem != null) {
            LOGGER.debug("A model for $existingPathItem already exists")
        }
        newRef = possiblyConflictingDefinitionName
        cache.putRenamedRef(ref, newRef)
        if (pathItem != null) {
            if (pathItem.readOperationsMap() != null) {
                val operationMap = pathItem.readOperationsMap()
                for (httpMethod in operationMap.keys) {
                    val operation = operationMap[httpMethod]
                    if (operation!!.responses != null) {
                        val responses: Map<String, ApiResponse>? = operation.responses
                        if (responses != null) {
                            for (responseCode in responses.keys) {
                                val response = responses[responseCode]
                                if (response != null) {
                                    var schema: Schema<*>?
                                    if (response.content != null) {
                                        val content: Map<String, MediaType> = response.content
                                        for (mediaName in content.keys) {
                                            val mediaType = content[mediaName]
                                            if (mediaType!!.schema != null) {
                                                schema = mediaType.schema
                                                if (schema != null) {
                                                    processRefSchemaObject(mediaType.schema, ref)
                                                }
                                                if (mediaType.examples != null) {
                                                    processRefExamples(mediaType.examples, ref)
                                                }
                                            }
                                        }
                                    }
                                    if (response.links != null) {
                                        processRefLinks(response.links, ref)
                                    }
                                }
                            }
                        }
                    }
                    if (operation.requestBody != null) {
                        val body = operation.requestBody
                        if (body.content != null) {
                            val content: Map<String, MediaType> = body.content
                            for (mediaName in content.keys) {
                                val mediaType = content[mediaName]
                                mediaType?.schema?.let { processRefSchemaObject(it, ref) }
                            }
                        }
                    }
                    val parameters = operation.parameters
                    parameters?.stream()
                        ?.filter { parameter: Parameter -> parameter.schema != null }
                        ?.forEach { parameter: Parameter -> processRefSchemaObject(parameter.schema, ref) }
                }
            }
        }
        return pathItem
    }

    private fun processDiscriminator(d: Discriminator?, file: String) {
        if (d != null && d.mapping != null) {
            processDiscriminatorMapping(d.mapping, file)
        }
    }

    private fun processDiscriminatorMapping(mapping: MutableMap<String, String>, file: String) {
        for (key in mapping.keys) {
            val ref = mapping[key]
            val subtype = Schema<Any?>().`$ref`(ref)
            processSchema(subtype, file)
            mapping[key] = subtype.`$ref`
        }
    }

    fun processRefToExternalResponse(`$ref`: String, refFormat: RefFormat?): String {
        val renamedRef = cache.getRenamedRef(`$ref`)
        if (renamedRef != null) {
            return renamedRef
        }
        val response = cache.loadRef(`$ref`, refFormat, ApiResponse::class.java)
        val newRef: String
        if (api.components == null) {
            api.components = Components()
        }
        var responses = api.components.responses
        if (responses == null) {
            responses = LinkedHashMap()
        }
        val possiblyConflictingDefinitionName = RefUtils.computeDefinitionName(`$ref`)
        val existingResponse = responses[possiblyConflictingDefinitionName]
        if (existingResponse != null) {
            LOGGER.debug("A model for $existingResponse already exists")
        }
        newRef = possiblyConflictingDefinitionName
        cache.putRenamedRef(`$ref`, newRef)
        if (response != null) {
            if (response.content != null) {
                processRefContent(response.content, `$ref`)
            }
            if (response.headers != null) {
                processRefHeaders(response.headers, `$ref`)
            }
            if (response.links != null) {
                processRefLinks(response.links, `$ref`)
            }
        }
        return newRef
    }

    fun processRefToExternalRequestBody(`$ref`: String, refFormat: RefFormat?): String {
        val renamedRef = cache.getRenamedRef(`$ref`)
        if (renamedRef != null) {
            return renamedRef
        }
        val body = cache.loadRef(`$ref`, refFormat, RequestBody::class.java)
        if (body == null) {
            // stop!  There's a problem.  retain the original ref
            LOGGER.warn(
                "unable to load model reference from `" + `$ref` + "`.  It may not be available " +
                    "or the reference isn't a valid model schema"
            )
            return `$ref`
        }
        val newRef: String
        if (api.components == null) {
            api.components = Components()
        }
        var bodies = api.components.requestBodies
        if (bodies == null) {
            bodies = LinkedHashMap()
        }
        val possiblyConflictingDefinitionName = RefUtils.computeDefinitionName(`$ref`)
        var existingBody = bodies[possiblyConflictingDefinitionName]
        if (existingBody != null) {
            LOGGER.debug("A model for $existingBody already exists")
            if (existingBody.`$ref` != null) {
                // use the new model
                existingBody = null
            }
        }
        newRef = possiblyConflictingDefinitionName
        cache.putRenamedRef(`$ref`, newRef)
        if (existingBody == null) {
            // don't overwrite existing model reference
            api.components.addRequestBodies(newRef, body)
            cache.addReferencedKey(newRef)
            val file = `$ref`.split("#/").toTypedArray()[0]
            if (body.`$ref` != null) {
                val format = RefUtils.computeRefFormat(body.`$ref`)
                if (RefUtils.isAnExternalRefFormat(format)) {
                    body.`$ref` = processRefToExternalRequestBody(body.`$ref`, format)
                } else {
                    processRefToExternalRequestBody(file + body.`$ref`, RefFormat.RELATIVE)
                }
            } else if (body.content != null) {
                processRefContent(body.content, `$ref`)
            }
        }
        return newRef
    }

    fun processRefToExternalHeader(`$ref`: String, refFormat: RefFormat?): String {
        val renamedRef = cache.getRenamedRef(`$ref`)
        if (renamedRef != null) {
            return renamedRef
        }
        val header = cache.loadRef(`$ref`, refFormat, Header::class.java)
        if (header == null) {
            // stop!  There's a problem.  retain the original ref
            LOGGER.warn(
                "unable to load model reference from `" + `$ref` + "`.  It may not be available " +
                    "or the reference isn't a valid model schema"
            )
            return `$ref`
        }
        val newRef: String
        if (api.components == null) {
            api.components = Components()
        }
        var headers = api.components.headers
        if (headers == null) {
            headers = LinkedHashMap()
        }
        val possiblyConflictingDefinitionName = RefUtils.computeDefinitionName(`$ref`)
        var existingHeader = headers[possiblyConflictingDefinitionName]
        if (existingHeader != null) {
            LOGGER.debug("A model for $existingHeader already exists")
            if (existingHeader.`$ref` != null) {
                // use the new model
                existingHeader = null
            }
        }
        newRef = possiblyConflictingDefinitionName
        cache.putRenamedRef(`$ref`, newRef)
        if (existingHeader == null) {
            // don't overwrite existing model reference
            api.components.addHeaders(newRef, header)
            cache.addReferencedKey(newRef)
            val file = `$ref`.split("#/").toTypedArray()[0]
            if (header.`$ref` != null) {
                val format = RefUtils.computeRefFormat(header.`$ref`)
                if (RefUtils.isAnExternalRefFormat(format)) {
                    header.`$ref` = processRefToExternalHeader(header.`$ref`, format)
                } else {
                    processRefToExternalHeader(file + header.`$ref`, RefFormat.RELATIVE)
                }
            }
        }
        if (header.content != null) {
            processRefContent(header.content, `$ref`)
        }
        if (header.schema != null) {
            processRefSchemaObject(header.schema, `$ref`)
        }
        return newRef
    }

    fun processRefToExternalSecurityScheme(`$ref`: String, refFormat: RefFormat?): String {
        val renamedRef = cache.getRenamedRef(`$ref`)
        if (renamedRef != null) {
            return renamedRef
        }
        val securityScheme = cache.loadRef(`$ref`, refFormat, SecurityScheme::class.java)
        if (securityScheme == null) {
            // stop!  There's a problem.  retain the original ref
            LOGGER.warn(
                "unable to load model reference from `" + `$ref` + "`.  It may not be available " +
                    "or the reference isn't a valid model schema"
            )
            return `$ref`
        }
        val newRef: String
        if (api.components == null) {
            api.components = Components()
        }
        var securitySchemeMap = api.components.securitySchemes
        if (securitySchemeMap == null) {
            securitySchemeMap = LinkedHashMap()
        }
        val possiblyConflictingDefinitionName = RefUtils.computeDefinitionName(`$ref`)
        var existingSecurityScheme = securitySchemeMap[possiblyConflictingDefinitionName]
        if (existingSecurityScheme != null) {
            LOGGER.debug("A model for $existingSecurityScheme already exists")
            if (existingSecurityScheme.`$ref` != null) {
                // use the new model
                existingSecurityScheme = null
            }
        }
        newRef = possiblyConflictingDefinitionName
        cache.putRenamedRef(`$ref`, newRef)
        if (existingSecurityScheme == null) {
            // don't overwrite existing model reference
            api.components.addSecuritySchemes(newRef, securityScheme)
            cache.addReferencedKey(newRef)
            val file = `$ref`.split("#/").toTypedArray()[0]
            if (securityScheme.`$ref` != null) {
                val format = RefUtils.computeRefFormat(securityScheme.`$ref`)
                if (RefUtils.isAnExternalRefFormat(format)) {
                    securityScheme.`$ref` = processRefToExternalSecurityScheme(securityScheme.`$ref`, format)
                } else {
                    processRefToExternalSecurityScheme(file + securityScheme.`$ref`, RefFormat.RELATIVE)
                }
            }
        }
        return newRef
    }

    fun processRefToExternalLink(`$ref`: String, refFormat: RefFormat?): String {
        val renamedRef = cache.getRenamedRef(`$ref`)
        if (renamedRef != null) {
            return renamedRef
        }
        val link = cache.loadRef(`$ref`, refFormat, Link::class.java)
        if (link == null) {
            // stop!  There's a problem.  retain the original ref
            LOGGER.warn(
                "unable to load model reference from `" + `$ref` + "`.  It may not be available " +
                    "or the reference isn't a valid model schema"
            )
            return `$ref`
        }
        val newRef: String
        if (api.components == null) {
            api.components = Components()
        }
        var links = api.components.links
        if (links == null) {
            links = LinkedHashMap()
        }
        val possiblyConflictingDefinitionName = RefUtils.computeDefinitionName(`$ref`)
        var existingLink = links[possiblyConflictingDefinitionName]
        if (existingLink != null) {
            LOGGER.debug("A model for $existingLink already exists")
            if (existingLink.`$ref` != null) {
                // use the new model
                existingLink = null
            }
        }
        newRef = possiblyConflictingDefinitionName
        cache.putRenamedRef(`$ref`, newRef)
        if (existingLink == null) {
            // don't overwrite existing model reference
            api.components.addLinks(newRef, link)
            cache.addReferencedKey(newRef)
            val file = `$ref`.split("#/").toTypedArray()[0]
            if (link.`$ref` != null) {
                val format = RefUtils.computeRefFormat(link.`$ref`)
                if (RefUtils.isAnExternalRefFormat(format)) {
                    link.`$ref` = processRefToExternalLink(link.`$ref`, format)
                } else {
                    processRefToExternalLink(file + link.`$ref`, RefFormat.RELATIVE)
                }
            }
        }
        return newRef
    }

    fun processRefToExternalExample(`$ref`: String, refFormat: RefFormat?): String {
        val renamedRef = cache.getRenamedRef(`$ref`)
        if (renamedRef != null) {
            return renamedRef
        }
        val example = cache.loadRef(`$ref`, refFormat, Example::class.java)
        if (example == null) {
            // stop!  There's a problem.  retain the original ref
            LOGGER.warn(
                "unable to load model reference from `" + `$ref` + "`.  It may not be available " +
                    "or the reference isn't a valid model schema"
            )
            return `$ref`
        }
        val newRef: String
        if (api.components == null) {
            api.components = Components()
        }
        var examples = api.components.examples
        if (examples == null) {
            examples = LinkedHashMap()
        }
        val possiblyConflictingDefinitionName = RefUtils.computeDefinitionName(`$ref`)
        var existingExample = examples[possiblyConflictingDefinitionName]
        if (existingExample != null) {
            LOGGER.debug("A model for $existingExample already exists")
            if (existingExample.`$ref` != null) {
                // use the new model
                existingExample = null
            }
        }
        newRef = possiblyConflictingDefinitionName
        cache.putRenamedRef(`$ref`, newRef)
        if (existingExample == null) {
            // don't overwrite existing model reference
            api.components.addExamples(newRef, example)
            cache.addReferencedKey(newRef)
            val file = `$ref`.split("#/").toTypedArray()[0]
            if (example.`$ref` != null) {
                val format = RefUtils.computeRefFormat(example.`$ref`)
                if (RefUtils.isAnExternalRefFormat(format)) {
                    example.`$ref` = processRefToExternalExample(example.`$ref`, format)
                } else {
                    processRefToExternalExample(file + example.`$ref`, RefFormat.RELATIVE)
                }
            }
        }
        return newRef
    }

    fun processRefToExternalParameter(`$ref`: String, refFormat: RefFormat?): String {
        val renamedRef = cache.getRenamedRef(`$ref`)
        if (renamedRef != null) {
            return renamedRef
        }
        val parameter = cache.loadRef(`$ref`, refFormat, Parameter::class.java)
        if (parameter == null) {
            // stop!  There's a problem.  retain the original ref
            LOGGER.warn(
                "unable to load model reference from `" + `$ref` + "`.  It may not be available " +
                    "or the reference isn't a valid model schema"
            )
            return `$ref`
        }
        val newRef: String
        if (api.components == null) {
            api.components = Components()
        }
        var parameters = api.components.parameters
        if (parameters == null) {
            parameters = LinkedHashMap()
        }
        val possiblyConflictingDefinitionName = RefUtils.computeDefinitionName(`$ref`)
        var existingParameters = parameters[possiblyConflictingDefinitionName]
        if (existingParameters != null) {
            LOGGER.debug("A model for $existingParameters already exists")
            if (existingParameters.`$ref` != null) {
                // use the new model
                existingParameters = null
            }
        }
        newRef = possiblyConflictingDefinitionName
        cache.putRenamedRef(`$ref`, newRef)
        if (existingParameters == null) {
            // don't overwrite existing model reference
            api.components.addParameters(newRef, parameter)
            cache.addReferencedKey(newRef)
            val file = `$ref`.split("#/").toTypedArray()[0]
            if (parameter.`$ref` != null) {
                val format = RefUtils.computeRefFormat(parameter.`$ref`)
                if (RefUtils.isAnExternalRefFormat(format)) {
                    parameter.`$ref` = processRefToExternalParameter(parameter.`$ref`, format)
                } else {
                    processRefToExternalParameter(file + parameter.`$ref`, RefFormat.RELATIVE)
                }
            }
        }
        if (parameter.content != null) {
            processRefContent(parameter.content, `$ref`)
        }
        if (parameter.schema != null) {
            processRefSchemaObject(parameter.schema, `$ref`)
        }
        if (parameter.extensions != null) {
            processExtensionValue(parameter.extensions, `$ref`)
        }
        return newRef
    }

    fun processRefToExternalCallback(`$ref`: String, refFormat: RefFormat?): String {
        val renamedRef = cache.getRenamedRef(`$ref`)
        if (renamedRef != null) {
            return renamedRef
        }
        val callback = cache.loadRef(`$ref`, refFormat, Callback::class.java)
        if (callback == null) {
            // stop!  There's a problem.  retain the original ref
            LOGGER.warn(
                "unable to load model reference from `" + `$ref` + "`.  It may not be available " +
                    "or the reference isn't a valid model schema"
            )
            return `$ref`
        }
        val newRef: String
        if (api.components == null) {
            api.components = Components()
        }
        var callbacks = api.components.callbacks
        if (callbacks == null) {
            callbacks = LinkedHashMap()
        }
        val possiblyConflictingDefinitionName = RefUtils.computeDefinitionName(`$ref`)
        var existingCallback = callbacks[possiblyConflictingDefinitionName]
        if (existingCallback != null) {
            LOGGER.debug("A model for $existingCallback already exists")
            if (existingCallback.`$ref` != null) {
                // use the new model
                existingCallback = null
            }
        }
        newRef = possiblyConflictingDefinitionName
        cache.putRenamedRef(`$ref`, newRef)
        if (existingCallback == null) {
            // don't overwrite existing model reference
            api.components.addCallbacks(newRef, callback)
            cache.addReferencedKey(newRef)
            val file = `$ref`.split("#/").toTypedArray()[0]
            if (callback.`$ref` != null) {
                if (callback.`$ref` != null) {
                    val format = RefUtils.computeRefFormat(callback.`$ref`)
                    if (RefUtils.isAnExternalRefFormat(format)) {
                        callback.`$ref` = processRefToExternalCallback(callback.`$ref`, format)
                    } else {
                        processRefToExternalCallback(file + callback.`$ref`, RefFormat.RELATIVE)
                    }
                }
            }
        }
        return newRef
    }

    private fun processRefContent(content: Map<String, MediaType>, `$ref`: String) {
        for (mediaType in content.values) {
            if (mediaType.schema != null) {
                processRefSchemaObject(mediaType.schema, `$ref`)
            }
            if (mediaType.examples != null) {
                processRefExamples(mediaType.examples, `$ref`)
            }
        }
    }

    private fun processRefExamples(examples: Map<String, Example>, `$ref`: String) {
        val file = `$ref`.split("#/").toTypedArray()[0]
        for (example in examples.values) {
            if (example.`$ref` != null) {
                val ref = RefUtils.computeRefFormat(example.`$ref`)
                if (RefUtils.isAnExternalRefFormat(ref)) {
                    processRefExample(example, `$ref`)
                } else {
                    processRefToExternalExample(file + example.`$ref`, RefFormat.RELATIVE)
                }
            }
        }
    }

    private fun processRefExample(example: Example, externalFile: String) {
        val format = RefUtils.computeRefFormat(example.`$ref`)
        if (!RefUtils.isAnExternalRefFormat(format)) {
            example.`$ref` =
                RefType.SCHEMAS.internalPrefix + processRefToExternalSchema(
                    externalFile + example.`$ref`,
                    RefFormat.RELATIVE
                )
            return
        }
        var `$ref` = example.`$ref`
        val subRefExternalPath = RefUtils.getExternalPath(example.`$ref`)
            .orElse(null)
        if (format == RefFormat.RELATIVE && subRefExternalPath != externalFile) {
            `$ref` = join(externalFile, example.`$ref`)
            example.`$ref` = `$ref`
        } else {
            processRefToExternalExample(`$ref`, format)
        }
    }

    private fun processRefSchemaObject(schema: Schema<*>, `$ref`: String) {
        val file = `$ref`.split("#/").toTypedArray()[0]
        if (schema.`$ref` != null) {
            val ref = RefUtils.computeRefFormat(schema.`$ref`)
            if (RefUtils.isAnExternalRefFormat(ref)) {
                processRefSchema(schema, file)
            } else {
                processRefToExternalSchema(file + schema.`$ref`, RefFormat.RELATIVE)
            }
        } else {
            processSchema(schema, file)
        }
    }

    private fun processRefHeaders(headers: Map<String, Header>, `$ref`: String) {
        val file = `$ref`.split("#/").toTypedArray()[0]
        for (header in headers.values) {
            if (header.`$ref` != null) {
                val ref = RefUtils.computeRefFormat(header.`$ref`)
                if (RefUtils.isAnExternalRefFormat(ref)) {
                    processRefHeader(header, `$ref`)
                } else {
                    processRefToExternalHeader(file + header.`$ref`, RefFormat.RELATIVE)
                }
            }
        }
    }

    private fun processRefLinks(links: Map<String, Link>, `$ref`: String) {
        val file = `$ref`.split("#/").toTypedArray()[0]
        for (link in links.values) {
            if (link.`$ref` != null) {
                val ref = RefUtils.computeRefFormat(link.`$ref`)
                if (RefUtils.isAnExternalRefFormat(ref)) {
                    processRefLink(link, `$ref`)
                } else {
                    processRefToExternalLink(file + link.`$ref`, RefFormat.RELATIVE)
                }
            }
        }
    }

    private fun processRefSchema(subRef: Schema<*>, externalFile: String) {
        val format = RefUtils.computeRefFormat(subRef.`$ref`)
        if (!RefUtils.isAnExternalRefFormat(format)) {
            subRef.`$ref` =
                RefType.SCHEMAS.internalPrefix + processRefToExternalSchema(
                    externalFile + subRef.`$ref`,
                    RefFormat.RELATIVE
                )
            return
        }
        var `$ref` = subRef.`$ref`
        val subRefExternalPath = RefUtils.getExternalPath(subRef.`$ref`)
            .orElse(null)
        if (format == RefFormat.RELATIVE && subRefExternalPath != externalFile) {
            `$ref` = constructRef(subRef, externalFile)
            subRef.`$ref` = `$ref`
        } else {
            processRefToExternalSchema(`$ref`, format)
        }
    }

    protected fun constructRef(refProperty: Schema<*>, rootLocation: String): String {
        val ref = refProperty.`$ref`
        return join(rootLocation, ref)
    }

    private fun processRefHeader(subRef: Header, externalFile: String) {
        val format = RefUtils.computeRefFormat(subRef.`$ref`)
        if (!RefUtils.isAnExternalRefFormat(format)) {
            subRef.`$ref` =
                RefType.SCHEMAS.internalPrefix + processRefToExternalSchema(
                    externalFile + subRef.`$ref`,
                    RefFormat.RELATIVE
                )
            return
        }
        var `$ref` = subRef.`$ref`
        val subRefExternalPath = RefUtils.getExternalPath(subRef.`$ref`)
            .orElse(null)
        if (format == RefFormat.RELATIVE && subRefExternalPath != externalFile) {
            `$ref` = join(externalFile, subRef.`$ref`)
            subRef.`$ref` = `$ref`
        } else {
            processRefToExternalHeader(`$ref`, format)
        }
    }

    private fun processRefLink(subRef: Link, externalFile: String) {
        val format = RefUtils.computeRefFormat(subRef.`$ref`)
        if (!RefUtils.isAnExternalRefFormat(format)) {
            subRef.`$ref` =
                RefType.SCHEMAS.internalPrefix + processRefToExternalSchema(
                    externalFile + subRef.`$ref`,
                    RefFormat.RELATIVE
                )
            return
        }
        var `$ref` = subRef.`$ref`
        val subRefExternalPath = RefUtils.getExternalPath(subRef.`$ref`)
            .orElse(null)
        if (format == RefFormat.RELATIVE && subRefExternalPath != externalFile) {
            `$ref` = join(externalFile, subRef.`$ref`)
            subRef.`$ref` = `$ref`
        } else {
            processRefToExternalLink(`$ref`, format)
        }
    }

    companion object {
        private val LOGGER = LoggerFactory.getLogger(KiscordExternalRefProcessor::class.java)

        // visible for testing
        fun join(source: String, fragment: String): String {
            return try {
                var isRelative = false
                if (source.startsWith("/") || source.startsWith(".")) {
                    isRelative = true
                }
                var uri = URI(source)
                if (!source.endsWith("/") && fragment.startsWith("./") && "" == uri.path) {
                    uri = URI("$source/")
                } else if ("" == uri.path && !fragment.startsWith("/")) {
                    uri = URI("$source/")
                }
                val f = URI(fragment)
                val resolved = uri.resolve(f)
                val normalized = resolved.normalize()
                if (Character.isAlphabetic(normalized.toString().codePointAt(0)) && isRelative) {
                    "./$normalized"
                } else normalized.toString()
            } catch (e: Exception) {
                source
            }
        }
    }
}
