/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.codegen.swagger.processors

import io.swagger.v3.oas.models.callbacks.*
import io.swagger.v3.oas.models.examples.*
import io.swagger.v3.oas.models.headers.*
import io.swagger.v3.oas.models.links.*
import io.swagger.v3.oas.models.media.*
import io.swagger.v3.oas.models.parameters.*
import io.swagger.v3.oas.models.responses.*
import io.swagger.v3.oas.models.security.*
import kiscord.codegen.swagger.*

class KiscordComponentsProcessor(processor: KiscordProcessor) : KiscordProcessor.Delegate(processor) {
    fun processComponents() {
        if (api.components == null) {
            return
        }

        fun <K, V, M : Map<K, V>> process(map: M?, block: (MutableSet<K>, M) -> Unit) {
            if (map == null) return
            val processed = LinkedHashSet<K>()
            while (map.size > processed.size) {
                block(processed, map)
            }
        }

        process(api.components.responses, this::processResponses)
        process(api.components.requestBodies, this::processRequestBodies)
        process(api.components.parameters, this::processParameters)
        process(api.components.headers, this::processHeaders)
        process(api.components.examples, this::processExamples)
        process(api.components.links, this::processLinks)
        process(api.components.callbacks, this::processCallbacks)
        process(api.components.securitySchemes, this::processSecuritySchemes)
        process(api.components.schemas, this::processSchemas)
    }

    private fun processSecuritySchemes(
        securitySchemeKey: MutableSet<String>,
        securitySchemes: MutableMap<String, SecurityScheme?>
    ) {
        securitySchemeKey.addAll(securitySchemes.keys)
        for (securitySchemeName in securitySchemeKey) {
            val securityScheme = securitySchemes[securitySchemeName]
            val resolvedSecurityScheme = securitySchemeProcessor.processSecurityScheme(
                securityScheme!!
            )
            securitySchemes.replace(securitySchemeName, securityScheme, resolvedSecurityScheme)
        }
    }

    private fun processCallbacks(callbackKey: MutableSet<String>, callbacks: Map<String, Callback>) {
        callbackKey.addAll(callbacks.keys)
        for (callbackName in callbackKey) {
            val callback = callbacks[callbackName]!!
            callbackProcessor.processCallback(callback)
        }
    }

    private fun processLinks(linkKey: MutableSet<String>, links: Map<String, Link>) {
        linkKey.addAll(links.keys)
        for (linkName in linkKey) {
            val link = links[linkName]!!
            linkProcessor.processLink(link)
        }
    }

    private fun processExamples(exampleKey: MutableSet<String>, examples: Map<String, Example>) {
        exampleKey.addAll(examples.keys)
        for (exampleName in exampleKey) {
            val example = examples[exampleName]!!
            exampleProcessor.processExample(example)
        }
    }

    private fun processHeaders(HeaderKey: MutableSet<String>, headers: Map<String, Header>) {
        HeaderKey.addAll(headers.keys)
        for (headersName in HeaderKey) {
            val header = headers[headersName]!!
            headerProcessor.processHeader(header)
        }
    }

    private fun processParameters(ParametersKey: MutableSet<String>, parameters: Map<String, Parameter>) {
        ParametersKey.addAll(parameters.keys)
        for (parametersName in ParametersKey) {
            val parameter = parameters[parametersName]!!
            parameterProcessor.processParameter(parameter)
        }
    }

    private fun processRequestBodies(requestBodyKey: MutableSet<String>, requestBodies: Map<String, RequestBody>) {
        requestBodyKey.addAll(requestBodies.keys)
        for (requestBodyName in requestBodyKey) {
            val requestBody = requestBodies[requestBodyName]!!
            requestBodyProcessor.processRequestBody(requestBody)
        }
    }

    private fun processResponses(responseKey: MutableSet<String>, responses: Map<String, ApiResponse>) {
        responseKey.addAll(responses.keys)
        for (responseName in responseKey) {
            val response = responses[responseName]!!
            responseProcessor.processResponse(response)
        }
    }

    private fun processSchemas(schemaKeys: MutableSet<String>, schemas: MutableMap<String, Schema<*>>) {
        schemaKeys.addAll(schemas.keys)
        for (modelName in schemaKeys) {
            val model = schemas[modelName]!!
            val originalRef = if (model.`$ref` != null) model.`$ref` else null
            schemaProcessor.processSchema(model)

            //if we process a RefModel here, in the #/components/schemas table, we want to overwrite it with the referenced value
            if (model.`$ref` != null) {
                val renamedRef = cache.getRenamedRef(originalRef)
                if (renamedRef != null) {
                    //we definitely resolved the referenced and shoved it in the components map
                    // because the referenced model may be in the components map, we need to remove old instances
                    val resolvedModel = schemas[renamedRef]!!

                    // ensure the reference isn't still in use
                    if (!cache.hasReferencedKey(renamedRef)) {
                        schemas.remove(renamedRef)
                    }

                    // add the new key
                    schemas[modelName] = resolvedModel
                }
            }
        }
        //process schemas again to check properties that hasn't been solved
        for (modelName in schemaKeys) {
            val model = schemas[modelName]!!
            val properties = model.properties
            if (properties != null) {
                for ((_, property) in properties) {
                    if (property.`$ref` != null) {
                        schemaProcessor.processSchema(model)
                    }
                }
            }
        }
    }
}
