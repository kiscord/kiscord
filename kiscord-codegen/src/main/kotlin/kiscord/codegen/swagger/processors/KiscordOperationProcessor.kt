/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.codegen.swagger.processors

import io.swagger.v3.oas.models.*
import io.swagger.v3.oas.models.responses.*
import io.swagger.v3.parser.util.*
import kiscord.codegen.swagger.*

class KiscordOperationProcessor(processor: KiscordProcessor) : KiscordProcessor.Delegate(processor) {
    fun processOperation(operation: Operation) {
        operation.parameters?.forEach { parameter ->
            parameterProcessor.processParameter(parameter)
        }
        val requestBody = operation.requestBody
        if (requestBody != null) {
            requestBodyProcessor.processRequestBody(requestBody)
        }
        operation.responses = operation.responses?.mapValuesTo(ApiResponses()) { (_, response) ->
            val ref = response.`$ref`
            if (ref != null) {
                responseProcessor.processResponse(response)
                val refFormat = RefUtils.computeRefFormat(ref)
                cache.loadRef(ref, refFormat, ApiResponse::class.java)?.let { return@mapValuesTo it }
            }
            response
        }
        operation.responses?.forEach { (_, response) ->
            responseProcessor.processResponse(response)
        }
        operation.callbacks?.forEach { (_, callback) ->
            val ref = callback.`$ref`
            if (ref != null) {
                val refFormat = RefUtils.computeRefFormat(ref)
                if (RefUtils.isAnExternalRefFormat(refFormat)) {
                    callback.`$ref` = externalRefProcessor.processRefToExternalCallback(ref, refFormat)
                }
            }
            callback.forEach { (_, pathItem) ->
                pathItem.readOperationsMap().forEach { (_, op) ->
                    processOperation(op)
                }
                pathItem.parameters?.forEach { parameter ->
                    parameterProcessor.processParameter(parameter)
                }
            }
        }
    }
}
