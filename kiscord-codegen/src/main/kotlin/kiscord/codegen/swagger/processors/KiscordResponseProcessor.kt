/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.codegen.swagger.processors

import io.swagger.v3.oas.models.responses.*
import io.swagger.v3.parser.util.*
import kiscord.codegen.swagger.*

class KiscordResponseProcessor(processor: KiscordProcessor) : KiscordProcessor.Delegate(processor) {
    fun processResponse(response: ApiResponse) {
        if (response.`$ref` != null) {
            processReferenceResponse(response)
        }
        if (response.content != null) {
            contentProcessor.processContent(response.content)
        }
        response.headers?.forEach { (_, header) ->
            headerProcessor.processHeader(header)
        }
        response.links?.forEach { (_, link) ->
            linkProcessor.processLink(link)
        }
    }

    fun processReferenceResponse(response: ApiResponse) {
        val ref = response.`$ref`
        val refFormat = RefUtils.computeRefFormat(ref)
        if (RefUtils.isAnExternalRefFormat(refFormat)) {
            externalRefProcessor.processRefToExternalResponse(ref, refFormat)
        }
    }
}
