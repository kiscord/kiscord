/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.codegen.swagger.processors

import io.swagger.v3.oas.models.media.*
import io.swagger.v3.parser.models.*
import io.swagger.v3.parser.util.*
import kiscord.codegen.swagger.*

class KiscordSchemaProcessor(processor: KiscordProcessor) : KiscordProcessor.Delegate(processor) {
    fun processSchema(schema: Schema<*>?) {
        if (schema == null) {
            return
        }
        if (schema.`$ref` != null) {
            processReferenceSchema(schema)
        } else {
            processSchemaType(schema)
        }
    }

    fun processSchemaType(schema: Schema<*>) {
        if (schema is ArraySchema) {
            processArraySchema(schema)
        }
        if (schema is ComposedSchema) {
            processComposedSchema(schema)
        }
        if (schema.properties != null && schema.properties.isNotEmpty()) {
            processPropertySchema(schema)
        }
        if (schema.not != null) {
            processNotSchema(schema)
        }
        if (schema.additionalProperties != null) {
            processAdditionalProperties(schema)
        }
        if (schema.propertyNames != null) {
            processPropertyNames(schema)
        }
        if (schema.discriminator != null) {
            processDiscriminatorSchema(schema)
        }
        @Suppress("UNCHECKED_CAST")
        if (schema.extensions != null) {
            schema.extensions = processExtensionValue(schema.extensions) as MutableMap<String, Any>?
        }
    }

    fun processExtensionValue(value: Any?): Any? = when (value) {
        null -> null
        is MutableMap<*, *> -> {
            if (value.containsKey("\$ref") && value.size == 1) {
                processExtensionValue(Schema<Any>().`$ref`(value["\$ref"] as String?))
            } else {
                value.mapValues { processExtensionValue(it.value) }
            }
        }
        is MutableList<*> -> {
            value.map {
                processExtensionValue(it)
            }
        }
        else -> value
    }

    private fun processDiscriminatorSchema(schema: Schema<*>) {
        if (schema.discriminator != null && schema.discriminator.mapping != null) {
            val mapping = schema.discriminator.mapping
            for (ref in mapping.values) {
                processReferenceSchema(Schema<Any?>().`$ref`(ref))
            }
        }
    }

    private fun processAdditionalProperties(additionalProperties: Any) {
        if (additionalProperties is Schema<*>) {
            val schema = additionalProperties
            if (schema.additionalProperties != null && schema.additionalProperties is Schema<*>) {
                val additionalPropertiesSchema = schema.additionalProperties as Schema<*>
                if (additionalPropertiesSchema.`$ref` != null) {
                    processReferenceSchema(additionalPropertiesSchema)
                } else {
                    processSchemaType(additionalPropertiesSchema)
                }
            }
        }
    }

    private fun processPropertyNames(schema: Schema<*>) {
        if (schema.propertyNames != null) {
            val propertyNames = schema.propertyNames
            if (propertyNames.`$ref` != null) {
                processReferenceSchema(propertyNames)
            } else {
                processSchemaType(propertyNames)
            }
        }
    }

    private fun processNotSchema(schema: Schema<*>) {
        if (schema.not != null) {
            if (schema.not.`$ref` != null) {
                processReferenceSchema(schema.not)
            } else {
                processSchemaType(schema.not)
            }
        }
    }

    fun processPropertySchema(schema: Schema<*>) {
        if (schema.`$ref` != null) {
            processReferenceSchema(schema)
        }
        val properties = schema.properties
        if (properties != null) {
            for ((_, property) in properties) {
                if (property.`$ref` != null) {
                    processReferenceSchema(property)
                } else {
                    processSchemaType(property)
                }
            }
        }
    }

    fun processComposedSchema(composedSchema: ComposedSchema) {
        if (composedSchema.allOf != null) {
            val schemas = composedSchema.allOf
            if (schemas != null) {
                for (schema in schemas) {
                    if (schema.`$ref` != null) {
                        processReferenceSchema(schema)
                    } else {
                        processSchemaType(schema)
                    }
                }
            }
        }
        if (composedSchema.oneOf != null) {
            val schemas = composedSchema.oneOf
            if (schemas != null) {
                for (schema in schemas) {
                    if (schema.`$ref` != null) {
                        val oldRef = schema.`$ref`
                        processReferenceSchema(schema)
                        val newRef = schema.`$ref`
                        changeDiscriminatorMapping(composedSchema, oldRef, newRef)
                    } else {
                        processSchemaType(schema)
                    }
                }
            }
        }
        if (composedSchema.anyOf != null) {
            val schemas = composedSchema.anyOf
            if (schemas != null) {
                for (schema in schemas) {
                    if (schema.`$ref` != null) {
                        processReferenceSchema(schema)
                    } else {
                        processSchemaType(schema)
                    }
                }
            }
        }
    }

    private fun changeDiscriminatorMapping(composedSchema: ComposedSchema, oldRef: String, newRef: String) {
        val discriminator = composedSchema.discriminator
        if (oldRef != newRef && discriminator != null) {
            val oldName = RefUtils.computeDefinitionName(oldRef)
            val newName = RefUtils.computeDefinitionName(newRef)
            var mappingName: String? = null
            if (discriminator.mapping != null) {
                for (name in discriminator.mapping.keys) {
                    if (oldRef == discriminator.mapping[name]) {
                        mappingName = name
                        break
                    }
                }
                if (mappingName != null) {
                    discriminator.mapping[mappingName] = newRef
                }
            }
            if (mappingName == null && oldName != newName) {
                if (discriminator.mapping == null) {
                    discriminator.mapping = mutableMapOf()
                }
                discriminator.mapping[oldName] = newRef
            }
        }
    }

    fun processArraySchema(arraySchema: ArraySchema) {
        val items = arraySchema.items
        if (items.`$ref` != null) {
            processReferenceSchema(items)
        } else {
            processSchemaType(items)
        }
    }

    private fun processReferenceSchema(schema: Schema<*>) {
        val ref = schema.`$ref`
        val refFormat = RefUtils.computeRefFormat(ref)
        if (RefUtils.isAnExternalRefFormat(refFormat)) {
            schema.`$ref` =
                RefType.SCHEMAS.internalPrefix + externalRefProcessor.processRefToExternalSchema(ref, refFormat)
        }
    }
}
