/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.codegen.swagger

import io.swagger.v3.oas.models.*
import io.swagger.v3.parser.*
import io.swagger.v3.parser.core.models.*

class KiscordOpenAPIResolver(
    private val openApi: OpenAPI,
    auths: List<AuthorizationValue> = emptyList(),
    parentFileLocation: String
) {
    val cache: ResolverCache = ResolverCache(openApi, auths, parentFileLocation)
    val processors: KiscordProcessor = KiscordProcessor.Holder(cache, openApi)

    fun resolve(): OpenAPI {
        processors.pathsProcessor.processPaths()
        processors.componentsProcessor.processComponents()
        openApi.paths?.forEach { (_, pathItem) ->
            pathItem?.readOperations()?.forEach { operation ->
                processors.operationProcessor.processOperation(operation)
            }
        }
        return openApi
    }
}
