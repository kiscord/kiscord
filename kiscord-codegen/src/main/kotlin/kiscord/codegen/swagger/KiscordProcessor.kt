/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.codegen.swagger

import io.swagger.v3.oas.models.*
import io.swagger.v3.parser.*
import kiscord.codegen.swagger.processors.*

interface KiscordProcessor {
    val cache: ResolverCache
    val api: OpenAPI

    val callbackProcessor: KiscordCallbackProcessor
    val componentsProcessor: KiscordComponentsProcessor
    val contentProcessor: KiscordContentProcessor
    val exampleProcessor: KiscordExampleProcessor
    val externalRefProcessor: KiscordExternalRefProcessor
    val headerProcessor: KiscordHeaderProcessor
    val linkProcessor: KiscordLinkProcessor
    val operationProcessor: KiscordOperationProcessor
    val parameterProcessor: KiscordParameterProcessor
    val pathsProcessor: KiscordPathsProcessor
    val requestBodyProcessor: KiscordRequestBodyProcessor
    val responseProcessor: KiscordResponseProcessor
    val schemaProcessor: KiscordSchemaProcessor
    val securitySchemeProcessor: KiscordSecuritySchemeProcessor

    abstract class Delegate(processor: KiscordProcessor) : KiscordProcessor by processor

    class Holder(
        override val cache: ResolverCache,
        override val api: OpenAPI,
    ) : KiscordProcessor {
        override val callbackProcessor by lazy(this) { KiscordCallbackProcessor(this) }
        override val componentsProcessor by lazy(this) { KiscordComponentsProcessor(this) }
        override val contentProcessor by lazy(this) { KiscordContentProcessor(this) }
        override val exampleProcessor by lazy(this) { KiscordExampleProcessor(this) }
        override val externalRefProcessor by lazy(this) { KiscordExternalRefProcessor(this) }
        override val headerProcessor by lazy(this) { KiscordHeaderProcessor(this) }
        override val linkProcessor by lazy(this) { KiscordLinkProcessor(this) }
        override val operationProcessor by lazy(this) { KiscordOperationProcessor(this) }
        override val parameterProcessor by lazy(this) { KiscordParameterProcessor(this) }
        override val pathsProcessor by lazy(this) { KiscordPathsProcessor(this) }
        override val requestBodyProcessor by lazy(this) { KiscordRequestBodyProcessor(this) }
        override val responseProcessor by lazy(this) { KiscordResponseProcessor(this) }
        override val schemaProcessor by lazy(this) { KiscordSchemaProcessor(this) }
        override val securitySchemeProcessor by lazy(this) { KiscordSecuritySchemeProcessor(this) }
    }
}
