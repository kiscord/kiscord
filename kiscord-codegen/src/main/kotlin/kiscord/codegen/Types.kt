/*
 * Kiscord - Discord REST/Gateway/Interactions client
 * Copyright (C) 2019-2024 Kiscord Authors and Contributors
 *
 * This file is part of Kiscord.
 *
 * Kiscord is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, version 3.
 * Kiscord is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License along with Kiscord. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-only
 */

package kiscord.codegen

import com.squareup.kotlinpoet.*

val ABSTRACT_TUPLE_SERIALIZER = ClassName("kiscord.serialization", "AbstractTupleSerializer")
val ATOMIC_INT = ClassName("kotlinx.atomicfu", "AtomicInt")
val ATOMIC_LONG = ClassName("kotlinx.atomicfu", "AtomicLong")
val BOX = ClassName("kiscord.api", "Box")
val BUILDER_DSL = ClassName("kiscord.builder", "BuilderDsl")
val BUILDER_FOR = ClassName("kiscord.builder", "BuilderFor")
val BUILDER_SPEC = ClassName("kiscord.builder", "BuilderSpec")
val COMPOSITE_DECODER = ClassName("kotlinx.serialization.encoding", "CompositeDecoder")
val CONTENT_DISPOSITION = ClassName("io.ktor.http", "ContentDisposition")
val CONTENT_TYPE = ClassName("io.ktor.http", "ContentType")
val COROUTINE_SCOPE = ClassName("kotlinx.coroutines", "CoroutineScope")
val DECODER = ClassName("kotlinx.serialization.encoding", "Decoder")
val DESERIALIZATION_STRATEGY = ClassName("kotlinx.serialization", "DeserializationStrategy")
val DISCORD_API_DSL = ClassName("kiscord.api", "DiscordApiDsl")
val DISPOSABLE_HANDLE = ClassName("kotlinx.coroutines", "DisposableHandle")
val ENCODER = ClassName("kotlinx.serialization.encoding", "Encoder")
val ENCODE_DEFAULT = ClassName("kotlinx.serialization", "EncodeDefault")
val ENCODE_DEFAULT_MODE = ENCODE_DEFAULT.nestedClass("Mode")
val EVENT_NAME = ClassName("kiscord.api.gateway", "EventName")
val EXPERIMENTAL_SERIALIZATION_API = ClassName("kotlinx.serialization", "ExperimentalSerializationApi")
val GATEWAY_CALL = ClassName("kiscord.gateway", "GatewayCall")
val GATEWAY_DISPATCH_DATA = ClassName("kiscord.api.gateway", "GatewayDispatchData")
val GATEWAY_DISPATCH_EVENT = ClassName("kiscord.gateway.events", "GatewayDispatchEvent")
val GATEWAY_EVENT_DEFINITION = ClassName("kiscord.gateway.events", "GatewayEventDefinition")
val GATEWAY_EVENT_DSL = ClassName("kiscord.gateway.events", "GatewayEventDsl")
val GATEWAY_EVENT_HANDLER = ClassName("kiscord.gateway.events", "GatewayEventHandler")
val GATEWAY_EVENT_SINK = ClassName("kiscord.gateway.events", "GatewayEventSink")
val GATEWAY_OPCODE = ClassName("kiscord.api.gateway", "GatewayOpcode")
val GATEWAY_PAYLOAD = ClassName("kiscord.api.gateway", "GatewayPayload")
val GATEWAY_PAYLOAD_DATA = ClassName("kiscord.api.gateway", "GatewayPayloadData")
val GATEWAY_PAYLOAD_EVENT = ClassName("kiscord.gateway.events", "GatewayPayloadEvent")
val GENERATED = ClassName("kiscord.api", "Generated")
val ILLEGAL_ARGUMENT_EXCEPTION = ClassName("kotlin", "IllegalArgumentException")
val INSTANT = ClassName("kotlinx.datetime", "Instant")
val INTERNAL_SERIALIZATION_API = ClassName("kotlinx.serialization", "InternalSerializationApi")
val JVM_INLINE = ClassName("kotlin.jvm", "JvmInline")
val JVM_NAME = ClassName("kotlin.jvm", "JvmName")
val KISCORD = ClassName("kiscord", "Kiscord")
val KISCORD_UNSTABLE_API = ClassName("kiscord.api", "KiscordUnstableAPI")
val KSERIALIZER = ClassName("kotlinx.serialization", "KSerializer")
val LOGGER = ClassName("org.kodein.log", "Logger")
val LOGGER_FACTORY = ClassName("org.kodein.log", "LoggerFactory")
val PIPELINE_CONTEXT = ClassName("io.ktor.util.pipeline", "PipelineContext")
val PRIMITIVE_KIND = ClassName("kotlinx.serialization.descriptors", "PrimitiveKind")
val PUBLISHED_API = ClassName("kotlin", "PublishedApi")
val REQUIRED = ClassName("kotlinx.serialization", "Required")
val SEQUENCE = ClassName("kotlin.sequences", "Sequence")
val SERIALIZABLE = ClassName("kotlinx.serialization", "Serializable")
val SERIALIZABLE_MULTI_PART = ClassName("kiscord.ktor", "SerializableMultiPart")
val SERIAL_DESCRIPTOR = ClassName("kotlinx.serialization.descriptors", "SerialDescriptor")
val SERIAL_NAME = ClassName("kotlinx.serialization", "SerialName")
val TRANSIENT = ClassName("kotlinx.serialization", "Transient")
val UPDATER_FOR = ClassName("kiscord.builder", "UpdaterFor")
val URL = ClassName("io.ktor.http", "Url")

// Members
val ATOMIC = MemberName("kotlinx.atomicfu", "atomic")
val BODY_AS_CHANNEL = MemberName("io.ktor.client.statement", "bodyAsChannel")
val BOX_COMPUTE_IF_NULL = MemberName("kiscord.api", "computeIfNull")
val BOX_GET_OR = MemberName("kiscord.api", "getOr")
val BOX_MAP = MemberName("kiscord.api", "map")
val BOX_OR = MemberName("kiscord.api", "or")
val EMPTY_SET = MemberName("kotlin.collections", "emptySet")
val ENUM_SET_OF = MemberName("kiscord.internal", "enumSetOf")
val HANDLE_PAYLOAD = MemberName("kiscord.gateway", "handle")
val LIST_SERIALIZER = MemberName("kotlinx.serialization.builtins", "ListSerializer")
val MUTABLE_SET_OF = MemberName("kotlin.collections", "mutableSetOf")
val SET_SERIALIZER = MemberName("kotlinx.serialization.builtins", "SetSerializer")
val SET_SERIAL_DESCRIPTOR = MemberName("kotlinx.serialization.descriptors", "setSerialDescriptor")
val TO_ENUM_SET = MemberName("kiscord.internal", "toEnumSet")
val WITH_SERIAL_NAME = MemberName("kiscord.serialization", "withSerialName")
