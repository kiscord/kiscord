kiscord-codegen
===============

Internal utility, used to generate DTO classes from openapi specification

### Extensions to openapi

#### x-kiscord-codegen-external \[boolean]
Used to mark a type as external type
(in other words - "don't generate this, it was written by hands somewhere else")

#### x-kiscord-codegen-package \[string]
Package name to place generated stuff into

#### x-kiscord-codegen-name \[string]
Override the name of declaration.
Normally, it should be used only as last resort, as name could be extracted from other sources
(such as titles, property names /etc)

#### x-kiscord-codegen-generate-spec \[boolean]
Generate spec for the given type (interface that contains all properties from the type itself) and implement it

#### x-kiscord-codegen-generate-builder \[boolean]
Generate builder to create that type in dsl manner.
Note that builder implies spec (`x-kiscord-codegen-generate-spec`), so only one is enough

#### x-kiscord-codegen-collection \[string]
Specify collection type to used in `type: array` schemas
`tuple` is special case and it should be applied to `type: object` schema
Valid values: `array`, `list`, `set`, `tuple`

#### x-kiscord-codegen-default
Default value for the builder
